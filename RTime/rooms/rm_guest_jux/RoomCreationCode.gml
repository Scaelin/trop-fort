///Spawning the players for the first time



//Move it in another object to create delay

//Spawner list of this room
obj_gameManager.spawnerList[0, 0] = (room_width/2)-750;
obj_gameManager.spawnerList[0, 1] = 200;
obj_gameManager.spawnerList[1, 0] = (room_width/2)+750;
obj_gameManager.spawnerList[1, 1] = 200;
obj_gameManager.spawnerList[2, 0] = (room_width/2)-350;
obj_gameManager.spawnerList[2, 1] = 200;
obj_gameManager.spawnerList[3, 0] = (room_width/2)+350;
obj_gameManager.spawnerList[3, 1] = 200;

if(obj_gameManager.state = "game lifecount")
{
	if(obj_gameManager.player1hp != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[3, 0], obj_gameManager.spawnerList[3, 1], -100, obj_player1);
	}
	if(obj_gameManager.player2hp != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[2, 0], obj_gameManager.spawnerList[2, 1], -100, obj_player2);
	}
	if(obj_gameManager.player3hp != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[1, 0], obj_gameManager.spawnerList[1, 1], -100, obj_player3);
	}
	if(obj_gameManager.player4hp != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[0, 0], obj_gameManager.spawnerList[0, 1], -100, obj_player4);
	}
}
else if(obj_gameManager.state = "game killcount")
{
	if(obj_gameManager.player1killcount != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[3, 0], obj_gameManager.spawnerList[3, 1], -100, obj_player1);
	}
	if(obj_gameManager.player2killcount != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[2, 0], obj_gameManager.spawnerList[2, 1], -100, obj_player2);
	}
	if(obj_gameManager.player3killcount != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[1, 0], obj_gameManager.spawnerList[1, 1], -100, obj_player3);
	}
	if(obj_gameManager.player4killcount != -1)
	{
		instance_create_depth(obj_gameManager.spawnerList[0, 0], obj_gameManager.spawnerList[0, 1], -100, obj_player4);
	}
}
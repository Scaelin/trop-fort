///ConfirmMenu()
//Check for confirmation action to move through menus

var no = false;

if(gamepad_button_check_pressed(0, gp_face2) or
   gamepad_button_check_pressed(1, gp_face2) or
   gamepad_button_check_pressed(2, gp_face2) or
   gamepad_button_check_pressed(3, gp_face2) or
   keyboard_check_pressed(vk_escape))
{
	no = true;
}


return no;
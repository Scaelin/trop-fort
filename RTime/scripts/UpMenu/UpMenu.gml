///UpMenu(index, mx)
//Check for direction "up" to move through the menu
// argument mx indicates the maximum index it can reach, getting over this value will cycle back to 0;

up = argument0;
mx = argument1;

if(gamepad_button_check_pressed(0, gp_padu) or
   gamepad_button_check_pressed(1, gp_padu) or
   gamepad_button_check_pressed(2, gp_padu) or
   gamepad_button_check_pressed(3, gp_padu) or
   keyboard_check_pressed(vk_up) or
   keyboard_check_pressed(ord("Z")))
{
	up = up - 1;
}



if(up < 0)
{
	up = mx-1;	
}


return up;
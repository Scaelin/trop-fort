///ConfirmMenu()
//Check for confirmation action to move through menus

var yes = false;

if(gamepad_button_check_pressed(0, gp_face1) or
   gamepad_button_check_pressed(1, gp_face1) or
   gamepad_button_check_pressed(2, gp_face1) or
   gamepad_button_check_pressed(3, gp_face1) or
   keyboard_check_pressed(vk_enter) or
   keyboard_check_pressed(vk_space))
{
	yes = true;
}




return yes;
///VictoryScreen(victor)
//Displays the victory screen. The message is different depending on who won (argument).

victor = argument0;

global.stop = 1;

//Créer un objet victoire qui affiche le victorieux
var sweet = instance_create_depth(0, 0, -1000, obj_victory);

sweet.victor = victor;

//Faire des feux d'artifice sur les côtés 

//Attendre qu'un joueur fasse suivant pour repartir en lobby
//--> Fait du côté de l'obj_victory
///AddScreentilt(value)
//ADD SCREENTILT

tilt = argument0;

if(obj_gameManager.screenshake)
{
	obj_camera.screentilt = obj_camera.screentilt + tilt*(obj_gameManager.screenshake/10);
}

//The screentilt is also affected by the same option as the screenshake, meaning that the player can
//reduce it with the same option.
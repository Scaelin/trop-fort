///HitSomethingElse(avoided)
//Checks if the calling instance is hitting something solid other that the given argument

avoid = argument0;

if(avoid == obj_player1)
{
	if(place_meeting(x, y, obj_player2))
	{
		return instance_place(x, y, obj_player2);
	}
	else if(place_meeting(x, y, obj_player3))
	{
		return instance_place(x, y, obj_player3);
	}
	else if(place_meeting(x, y, obj_player4))
	{
		return instance_place(x, y, obj_player4);
	}
	else if(place_meeting(x, y, obj_solid))
	{
		return 1;		
	}
}
if(avoid == obj_player2)
{
	if(place_meeting(x, y, obj_player1))
	{
		return instance_place(x, y, obj_player1);
	}
	else if(place_meeting(x, y, obj_player3))
	{
		return instance_place(x, y, obj_player3);
	}
	else if(place_meeting(x, y, obj_player4))
	{
		return instance_place(x, y, obj_player4);
	}
	else if(place_meeting(x, y, obj_solid))
	{
		return 1;		
	}
}
if(avoid == obj_player3)
{
	
	if(place_meeting(x, y, obj_player1))
	{
		return instance_place(x, y, obj_player1);
	}
	else if(place_meeting(x, y, obj_player2))
	{
		return instance_place(x, y, obj_player2);
	}
	else if(place_meeting(x, y, obj_player4))
	{
		return instance_place(x, y, obj_player4);
	}
	else if(place_meeting(x, y, obj_solid))
	{
		return 1;		
	}
}
if(avoid == obj_player4)
{
	
	if(place_meeting(x, y, obj_player1))
	{
		return instance_place(x, y, obj_player1);
	}
	else if(place_meeting(x, y, obj_player2))
	{
		return instance_place(x, y, obj_player2);
	}
	else if(place_meeting(x, y, obj_player3))
	{
		return instance_place(x, y, obj_player3);
	}
	else if(place_meeting(x, y, obj_solid))
	{
		return 1;	
	}
}

return 0;
///RightMenu(index, mx)
//Check for direction "right" to move through the menu
// argument mx indicates the maximum index it can reach, getting over this value will cycle back to 0;

right = argument0;
mx = argument1;

if(gamepad_button_check_pressed(0, gp_padr) or
   gamepad_button_check_pressed(1, gp_padr) or
   gamepad_button_check_pressed(2, gp_padr) or
   gamepad_button_check_pressed(3, gp_padr) or
   keyboard_check_pressed(vk_right) or
   keyboard_check_pressed(ord("D")))
{
	right = right + 1;
}
	


if(right >= mx)
{
	right = 0;	
}

return right;
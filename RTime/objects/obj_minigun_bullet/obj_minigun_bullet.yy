{
    "id": "382dddbd-84a0-421e-bab6-44d22927b4c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_minigun_bullet",
    "eventList": [
        {
            "id": "8ee5fea2-efa6-4e19-bc63-258120677157",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "382dddbd-84a0-421e-bab6-44d22927b4c5"
        },
        {
            "id": "004773af-f7c3-40e7-8f0e-18a9f732a245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "382dddbd-84a0-421e-bab6-44d22927b4c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "40718c3b-b5df-459a-92bf-31e810501ffd",
    "visible": true
}
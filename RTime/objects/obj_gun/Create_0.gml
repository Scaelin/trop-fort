/// @description state based event
// Father of all gunz

depth = -300;

//To refer in other objects, such as players
name = "shotgun";
//Finite State Machine (floor, thrown, hold)
state = "floor";
//Name of the player holding it, 0 while on the ground or flying
holder = 0;

//Spin when thrown
spin = 0;
//Speed when thrown
fast = 0;
//Knockback when thrown into someone
throwKnockback = 15;
//Damage when throwing a weapon into someone else
throwDamage = 1;

//Fall on the floor while "floor"
drop = 15;

//Fading
//Weapons disapear 20s (default) after they appeared
//They start to glow red 180 frames before their final moments to warn the player
fading = 20*room_speed;

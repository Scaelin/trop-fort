/// @description FSM

// Hold
if(state == "hold")
{
	var aim = holder.aim;
	
	if((aim >= 0) and (aim <= 90))
	{
		image_xscale = 1;
		direction = aim;
		image_angle = aim;
	}
	if((aim < 360) and (aim >= 270))
	{
		image_xscale = 1;
		direction = aim;
		image_angle = aim;
	}
	if((aim <=180) and (aim > 90))
	{
		image_xscale = -1;
		direction = aim - 180;
		image_angle = aim - 180;
	}
	if((aim <=270) and (aim > 180))
	{
		image_xscale = -1;
		direction = aim - 180;
		image_angle = aim - 180;
	}
	
	x = holder.x;
	y = holder.y;
}
// Thrown 
else if(state == "thrown")
{
	if(global.stop)
	{
		fast = 0;
	}
	else
	{
		fast = 35;
	}	
	
	spin = spin +15;
	if(spin >= 360)
	{
		spin = 0;	
	}

	image_angle = spin;
	
	x = x + lengthdir_x(fast, direction);
	y = y + lengthdir_y(fast, direction);

	var collide = HitSomethingElse(holder);
	
	if(collide)
	{
		x = x - lengthdir_x(fast, direction);
		y = y - lengthdir_y(fast, direction);
		
		if(collide != 1)
		{
			collide.hp = collide.hp - throwDamage;
			collide.killer = holder;
			
			collide.else_hspd = collide.else_hspd + lengthdir_x(throwKnockback, direction);
			collide.else_vspd = collide.else_vspd + lengthdir_y(throwKnockback, direction);
			AddScreenshake(8);
		}
		
		state = "floor";
		spin = 0;
		holder = 0;
		drop = 10;
		fast = 0;

	}

}
// Floor
else if(state == "floor")
{	
	if(place_meeting(x, y+drop, obj_solid))
	{
		while(!instance_place(x+1, y, obj_solid))
		{
			y = y+1;
		}
		drop = 0;
	}
	y = y + drop;
	
	
}

//Flying in menu
else if(state == "menu")
{
	depth = 99;
	spin = spin +5;
	if(spin >= 360)
	{
		spin = 0;	
	}

	image_angle = spin;
	
	x = x + lengthdir_x(fast, direction);
	y = y + lengthdir_y(fast, direction);
}


if(global.stop == 0)
{
	if(fading > 0)
	{
		fading = fading - 1;
	}
	else
	{
		if(state = "hold")
		{
			holder.equiped = "none";
		}
		instance_destroy();	
	}
}
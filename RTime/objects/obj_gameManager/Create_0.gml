/// @description Greetings
// Shit's about to start

depth = -300;

//We're gonna use a finite state machine to deal with the different state the game can be in
//Starts on "menu"
//Can be :
// Menu
// Options
// Lobby
// Modifier
// Game killcount
// Game lifecount
// Pause is not a state, more like a substate of an in-game state



state = "menu";

draw_set_font(font_menuNormal);

//This will cover all of the options in the Option menu
screenshake = 10;
generalVolume = 10;
musicVolume = 10;
soundVolume = 10;

//Cover the number of current players and their use of gamepads
playerCount = 1; //M+K is by default the first player, is replaced by gamepad 4 if a fourth one is connected
gamepadCount = 0; //Actualizes in pregame lobby. If a fourth gamepad is connected, it is used for the first player (instead of K+M)




#region // Data needed to navigate through the main menu

	//Index used to navigate through the main menu option table
menuIndex = 0;

	//Table of options in the main menu
mainMenu[0] = "Play!";
mainMenu[1] = "Options";
mainMenu[2] = "Quit";

	//Total number of options
maxMainMenu = 3;


#endregion

#region // Data needed to navigate through the main menu

	//Index used to navigate through the main menu option table
optionsIndex = 0;

	//Table of options in the main menu
optionsMenu[0] = "General volume";
optionsMenu[1] = "Music volume";
optionsMenu[2] = "Sound volume";
optionsMenu[3] = "Screenshake intensity";
optionsMenu[4] = "Back";

	//Total number of options
maxOptionsMenu = 5;


#endregion

#region // Data needed to navigate through the main menu

playersReady = 0;
playersJoin = 0;

#endregion

#region //Modifier menu

modifierIndex = 0;

modifierMenu[0] = "Start";
modifierMenu[1] = "Gamemode";
modifierMenu[2] = "Points";
modifierMenu[3] = "Weapons";
modifierMenu[4] = "Back";

maxModifierMenu = 5;

gamemodeSelected = 0;
//Gamemodes : 
// [0] is lives gamemode
// [1] is killcount gamemode

objectiveCount = 5;
//Meaning the players have 5 lives/Need 5 kills to win

//Table of weapons
weaponTableIndexx = 1;
weaponTableIndexy = 1;

weaponTable[0, 0] = obj_shotgun;
weaponTable[1, 0] = obj_sawed_off;
weaponTable[2, 0] = obj_sniper;

weaponTable[0, 1] = obj_flamethrower;
weaponTable[1, 1] = obj_minigun;
weaponTable[2, 1] = obj_fukin_cheese;

weaponTable[0, 2] = obj_windgun;
weaponTable[1, 2] = obj_swapgun;
weaponTable[2, 2] = obj_chronobat;

maxWeaponTable = 3;

#endregion

#region //Data revolving around all gamemodes

	//Respawn timers, act as countdowns
	//-1 means that they can't respawn (not present in the game or out of lives for example)
player1respawn = -1;
player2respawn = -1;
player3respawn = -1;
player4respawn = -1;

	//SpawnerList, lists the different locations where the players can respawn
	//Cycles through them with the use of cycle
	//Rooms will eventually make another list to replace this one 
	//[0] is the X coordinate
	//[1] is the Y coordinate
	
spawnerList[0, 0] = (room_width/2)-750;
spawnerList[0, 1] = 200;
spawnerList[1, 0] = (room_width/2)+750;
spawnerList[1, 1] = 200;
spawnerList[2, 0] = (room_width/2)-250;
spawnerList[2, 1] = 200;
spawnerList[3, 0] = (room_width/2)+250;
spawnerList[3, 1] = 200;

#endregion

#region // Data revolving around the lifecount gamemode

	//Player hp

player1hp = 0;
player2hp = 0;
player3hp = 0;
player4hp = 0;




playersAlive = 0;

#endregion

#region //Data revolving around the killcount gamemode

	//Player killcounts
player1killcount = 0;
player2killcount = 0;
player3killcount = 0;
player4killcount = 0;



#endregion

#region //Game modifiers




//Disable weapons
enabledShotgun = 1;
enabledSawedoff = 1;
enabledSniper = 1;
enabledFlamethrower = 1;
enabledMinigun = 1;
enabledFukincheese = 1;
enabledWindgun = 1;
enabledSwapgun = 1;
enabledChronobat = 1;

enabledWeapons = (
	enabledShotgun +
	enabledSawedoff +
	enabledSniper +
	enabledFlamethrower +
	enabledMinigun +
	enabledFukincheese +
	enabledWindgun +
	enabledSwapgun +
	enabledChronobat
);

#endregion

#region //List of maps (levels/rooms)

maps[0] = rm_game1;
maps[1] = rm_game2;
maps[2] = rm_guest_jux;

maxMaps = 2;
#endregion

#region //Skin list and attribution

//Attribution of the different skins available
player1skin = 0;
player2skin = 1;
player3skin = 2;
player4skin = 3;

	//Indexation of the different skins
//[0] = Idle
//[1] = Run
//[2] = Jump
//[3] = Dead
//[4] = Portrait (for HUD mainly)
/*
skinIndex[0, 0] = spr_skin_crusader_idle;
skinIndex[0, 1] = spr_skin_crusader_run;
skinIndex[0, 2] = spr_skin_crusader_jump;
skinIndex[0, 3] = spr_skin_crusader_dead;
skinIndex[0, 4] = spr_skin_crusader_portrait;

skinIndex[1, 0] = spr_skin_viking_idle;
skinIndex[1, 1] = spr_skin_viking_run;
skinIndex[1, 2] = spr_skin_viking_jump;
skinIndex[1, 3] = spr_skin_viking_dead;
skinIndex[1, 4] = spr_skin_viking_portrait;

skinIndex[2, 0] = spr_skin_windup_idle;
skinIndex[2, 1] = spr_skin_windup_run;
skinIndex[2, 2] = spr_skin_windup_jump;
skinIndex[2, 3] = spr_skin_windup_dead;
skinIndex[2, 4] = spr_skin_windup_portrait;

skinIndex[3, 0] = spr_skin_pirate_idle;
skinIndex[3, 1] = spr_skin_pirate_run;
skinIndex[3, 2] = spr_skin_pirate_jump;
skinIndex[3, 3] = spr_skin_pirate_dead;
skinIndex[3, 4] = spr_skin_pirate_portrait;

skinIndex[4, 0] = spr_skin_cat_idle;
skinIndex[4, 1] = spr_skin_cat_run;
skinIndex[4, 2] = spr_skin_cat_jump;
skinIndex[4, 3] = spr_skin_cat_dead;
skinIndex[4, 4] = spr_skin_cat_portrait;


	
*/
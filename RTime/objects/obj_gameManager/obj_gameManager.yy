{
    "id": "e5d142f1-c911-403c-8681-9d6ac342aa29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameManager",
    "eventList": [
        {
            "id": "cd8c1a09-87ee-4326-8a7b-36da66c444d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "e5d142f1-c911-403c-8681-9d6ac342aa29"
        },
        {
            "id": "2593d5a8-029e-4b24-b3e9-a1699aaaef34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5d142f1-c911-403c-8681-9d6ac342aa29"
        },
        {
            "id": "097cdb32-87f9-469d-8086-435b71ff9571",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5d142f1-c911-403c-8681-9d6ac342aa29"
        },
        {
            "id": "f375a9f8-62ec-4c01-b942-39f531e1abdf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e5d142f1-c911-403c-8681-9d6ac342aa29"
        },
        {
            "id": "3b00a661-9236-49ff-8afb-30e3763ed570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e5d142f1-c911-403c-8681-9d6ac342aa29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
/// @description In game info about players

// Lifecount gamemode
if(state == "game lifecount")
{
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	draw_set_font(font_menuNormal);
	
	draw_sprite(spr_hud_scoreboard, 0, room_width/2, 41);
	
	//Player1
	if(player1hp != -1)
	{
		//Draw the portrait of the skin on the left
		draw_sprite(skinIndex[player1skin, 4], 0, (room_width/2) - 204, 40);
	}
	else
	{
		
	}
	
}
else if(state == "game killcount")
{
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	if(playersJoin == 4)
	{
			//Player 1
		//Draw the background
		draw_sprite(spr_hud_player1_kill, 0, 100, 40);
		//Draw the number of lives
		draw_text_transformed(50, 50, string(obj_gameManager.player1hp), 2, 2, 0);
		if(instance_exists(obj_player1))
		{
			//draw the ammo 
			if(obj_player1.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, 150, 50);
			}
			else
			{
				draw_text_transformed(150, 50, string(obj_player1.ammo), 2, 2, 0);
			}
			
		}
		
			//Player 2
		//Draw the background
		draw_sprite(spr_hud_player2_kill, 0, view_wport[0]/3, 40);
		//Draw the number of lives
		draw_text_transformed((view_wport[0]/3) - 50, 50, string(obj_gameManager.player2hp), 2, 2, 0);
		if(instance_exists(obj_player2))
		{
			//draw the ammo 
			if(obj_player2.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, (view_wport[0]/3) + 50, 50);
			}
			else
			{
				draw_text_transformed((view_wport[0]/3) + 50, 50, string(obj_player2.ammo), 2, 2, 0);
			}
		}
		
			//Player 3
		//Draw the background
		draw_sprite(spr_hud_player3_kill, 0, view_wport[0]-100, 40);
		//Draw the number of lives
		draw_text_transformed((view_wport[0]*2/3) - 50, 50, string(obj_gameManager.player3hp), 2, 2, 0);
		if(instance_exists(obj_player3))
		{
			//draw the ammo 
			if(obj_player3.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, (view_wport[0]*2/3) + 50, 50);
			}
			else
			{
				draw_text_transformed((view_wport[0]*2/3) + 50, 50, string(obj_player3.ammo), 2, 2, 0);
			}
		}
		
			//Player 4
		//Draw the background
		draw_sprite(spr_hud_player4_kill, 0, view_wport[0]-100, 40);
		//Draw the number of lives
		draw_text_transformed(view_wport[0] - 150, 50, string(obj_gameManager.player4hp), 2, 2, 0);
		if(instance_exists(obj_player4))
		{
			//draw the ammo 
			if(obj_player4.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, view_wport[0] - 50, 50);
			}
			else
			{
				draw_text_transformed(view_wport[0] - 50, 50, string(obj_player4.ammo), 2, 2, 0);
			}
		}
		
	}
	if(playersJoin ==3)
	{
			//Player 1
		//Draw the background
		draw_sprite(spr_hud_player1_kill, 0, 100, 40);
		//Draw the number of lives
		draw_text_transformed(50, 50, string(obj_gameManager.player1hp), 2, 2, 0);
		if(instance_exists(obj_player1))
		{
			//draw the ammo 
			if(obj_player1.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, 150, 50);
			}
			else
			{
				draw_text_transformed(150, 50, string(obj_player1.ammo), 2, 2, 0);
			}
			
		}
		
			//Player 2
		//Draw the background
		draw_sprite(spr_hud_player2_kill, 0, view_wport[0]/2, 40);
		//Draw the number of lives
		draw_text_transformed((view_wport[0]/2) - 50, 50, string(obj_gameManager.player2hp), 2, 2, 0);
		if(instance_exists(obj_player2))
		{
			//draw the ammo 
			if(obj_player2.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, (view_wport[0]/2) + 50, 50);
			}
			else
			{
				draw_text_transformed((view_wport[0]/2) + 50, 50, string(obj_player2.ammo), 2, 2, 0);
			}
		}
		
			//Player 3
		//Draw the background
		draw_sprite(spr_hud_player3_kill, 0, view_wport[0]-100, 40);
		//Draw the number of lives
		draw_text_transformed(view_wport[0] - 150, 50, string(obj_gameManager.player3hp), 2, 2, 0);
		if(instance_exists(obj_player3))
		{
			//draw the ammo 
			if(obj_player3.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, view_wport[0] - 50, 50);
			}
			else
			{
				draw_text_transformed(view_wport[0] - 50, 50, string(obj_player3.ammo), 2, 2, 0);
			}
		}
	}
	if(playersJoin == 2)
	{
			//Player 1
		//Draw the background
		draw_sprite(spr_hud_player1_kill, 0, 100, 40);
		//Draw the number of lives
		draw_text_transformed(50, 50, string(obj_gameManager.player1hp), 2, 2, 0);
		if(instance_exists(obj_player1))
		{
			//draw the ammo 
			if(obj_player1.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, 150, 50);
			}
			else
			{
				draw_text_transformed(150, 50, string(obj_player1.ammo), 2, 2, 0);
			}
			
		}
		
			//Player 2
		//Draw the background
		draw_sprite(spr_hud_player2_kill, 0, view_wport[0]-100, 40);
		//Draw the number of lives
		draw_text_transformed(view_wport[0] - 150, 50, string(obj_gameManager.player2hp), 2, 2, 0);
		if(instance_exists(obj_player2))
		{
			//draw the ammo 
			if(obj_player2.ammo == 0)
			{
				draw_sprite(spr_hud_noammo, 0, view_wport[0] - 50, 50);
			}
			else
			{
				draw_text_transformed(view_wport[0] - 50, 50, string(obj_player2.ammo), 2, 2, 0);
			}
		}
	}
}

/// @description 
// 




//Finite state machine

#region //Main menu
if(state == "menu")
{
	//Moving through the menu
	menuIndex = UpMenu(menuIndex, maxMainMenu);
	menuIndex = DownMenu(menuIndex, maxMainMenu);
	
	
	if(mainMenu[menuIndex] == "Play!")
	{
		if(ConfirmMenu())
		{
			state = "lobby";
			menuIndex = 0;
			
			
			
		}
	}
	else if(mainMenu[menuIndex] == "Options")
	{
		if(ConfirmMenu())
		{
			state = "options";
			menuIndex = 0;
		}
	}
	else if(mainMenu[menuIndex] == "Quit")
	{
		if(ConfirmMenu())
		{
			//Save things eventually
			game_end();
		}
	}
}
#endregion

#region	//Options
else if(state == "options")
{
	//Moving through the menu
	optionsIndex = UpMenu(optionsIndex, maxOptionsMenu);
	optionsIndex = DownMenu(optionsIndex, maxOptionsMenu);
	
	if(optionsMenu[optionsIndex] == "General volume")
	{
		generalVolume = LeftMenu(generalVolume, 11);
		generalVolume = RightMenu(generalVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Music volume")
	{
		musicVolume = LeftMenu(musicVolume, 11);
		musicVolume = RightMenu(musicVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Sound volume")
	{
		soundVolume = LeftMenu(soundVolume, 11);
		soundVolume = RightMenu(soundVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Screenshake intensity")
	{
		screenshake = LeftMenu(screenshake, 11);
		screenshake = RightMenu(screenshake, 11);
	}
	else if(optionsMenu[optionsIndex] == "Back")
	{
		if(ConfirmMenu())
		{
			state = "menu";
			optionsIndex = 0;
			//Save in the txt
		}
	}
}
#endregion

#region //Player joining in menu (lobby)
else if(state == "lobby")
{
	
	#region //Checking players joining
	
			
		//Player1
	if(keyboard_check_pressed(vk_enter) or keyboard_check_pressed(vk_space) or gamepad_button_check_pressed(3, gp_face1))
	{

		if(instance_exists(obj_player1))
		{
			if((playersReady == playersJoin ) and (playersReady > 1))
			{
				state = "modifier";
			}
			
			if((obj_lobby_bg1.player == "player1") and (obj_lobby_bg1.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg1.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg2.player == "player1") and (obj_lobby_bg2.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg2.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg3.player == "player1") and (obj_lobby_bg3.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg3.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg4.player == "player1") and (obj_lobby_bg4.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg4.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
		}
		else
		{
			if(playersJoin == 0)
			{
				instance_create_depth(obj_lobby_bg1.x, obj_lobby_bg1.y, depth, obj_player1);
				obj_lobby_bg1.player = "player1";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 1)
			{
				instance_create_depth(obj_lobby_bg2.x, obj_lobby_bg2.y, depth, obj_player1);
				obj_lobby_bg2.player = "player1";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 2)
			{
				instance_create_depth(obj_lobby_bg3.x, obj_lobby_bg3.y, depth, obj_player1);	
				obj_lobby_bg3.player = "player1";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 3)
			{
				instance_create_depth(obj_lobby_bg4.x, obj_lobby_bg4.y, depth, obj_player1);	
				obj_lobby_bg1.player = "player1";
				playersJoin = playersJoin + 1;
			}
		}
	}
	
		//Player2
	if(gamepad_button_check_pressed(0, gp_face1))
	{

		if(instance_exists(obj_player2))
		{
			if((playersReady == playersJoin ) and (playersReady > 1))
			{
				state = "modifier";
			}
			
			if((obj_lobby_bg1.player == "player2") and (obj_lobby_bg1.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg1.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg2.player == "player2") and (obj_lobby_bg2.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg2.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg3.player == "player2") and (obj_lobby_bg3.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg3.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg4.player == "player2") and (obj_lobby_bg4.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg4.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
		}
		else
		{
			if(playersJoin == 0)
			{
				instance_create_depth(obj_lobby_bg1.x, obj_lobby_bg1.y, depth, obj_player2);
				obj_lobby_bg1.player = "player2";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 1)
			{
				instance_create_depth(obj_lobby_bg2.x, obj_lobby_bg2.y, depth, obj_player2);
				obj_lobby_bg2.player = "player2";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 2)
			{
				instance_create_depth(obj_lobby_bg3.x, obj_lobby_bg3.y, depth, obj_player2);	
				obj_lobby_bg3.player = "player2";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 3)
			{
				instance_create_depth(obj_lobby_bg4.x, obj_lobby_bg4.y, depth, obj_player2);	
				obj_lobby_bg1.player = "player2";
				playersJoin = playersJoin + 1;
			}
		}
	}
	
		//Player3
	if(gamepad_button_check_pressed(1, gp_face1))
	{

		if(instance_exists(obj_player3))
		{
			if((playersReady == playersJoin ) and (playersReady > 1))
			{
				state = "modifier";
			}
			
			if((obj_lobby_bg1.player == "player3") and (obj_lobby_bg1.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg1.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg2.player == "player3") and (obj_lobby_bg2.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg2.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg3.player == "player3") and (obj_lobby_bg3.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg3.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg4.player == "player3") and (obj_lobby_bg4.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg4.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
		}
		else
		{
			if(playersJoin == 0)
			{
				instance_create_depth(obj_lobby_bg1.x, obj_lobby_bg1.y, depth, obj_player3);
				obj_lobby_bg1.player = "player3";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 1)
			{
				instance_create_depth(obj_lobby_bg2.x, obj_lobby_bg2.y, depth, obj_player3);
				obj_lobby_bg2.player = "player3";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 2)
			{
				instance_create_depth(obj_lobby_bg3.x, obj_lobby_bg3.y, depth, obj_player3);	
				obj_lobby_bg3.player = "player3";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 3)
			{
				instance_create_depth(obj_lobby_bg4.x, obj_lobby_bg4.y, depth, obj_player3);	
				obj_lobby_bg1.player = "player3";
				playersJoin = playersJoin + 1;
			}
		}
	}
	
		//Player4
	if(gamepad_button_check_pressed(2, gp_face1))
	{

		if(instance_exists(obj_player4))
		{
			if((playersReady == playersJoin ) and (playersReady > 1))
			{
				state = "modifier";
			}
			
			if((obj_lobby_bg1.player == "player4") and (obj_lobby_bg1.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg1.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg2.player == "player4") and (obj_lobby_bg2.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg2.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg3.player == "player4") and (obj_lobby_bg3.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg3.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
			else if((obj_lobby_bg4.player == "player4") and (obj_lobby_bg4.sprite_index == spr_lobby_waiting))
			{
				obj_lobby_bg4.sprite_index = spr_lobby_ready;
				playersReady = playersReady + 1;
			}
		}
		else
		{
			if(playersJoin == 0)
			{
				instance_create_depth(obj_lobby_bg1.x, obj_lobby_bg1.y, depth, obj_player4);
				obj_lobby_bg1.player = "player4";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 1)
			{
				instance_create_depth(obj_lobby_bg2.x, obj_lobby_bg2.y, depth, obj_player4);
				obj_lobby_bg2.player = "player4";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 2)
			{
				instance_create_depth(obj_lobby_bg3.x, obj_lobby_bg3.y, depth, obj_player4);	
				obj_lobby_bg3.player = "player4";
				playersJoin = playersJoin + 1;
			}
			else if(playersJoin == 3)
			{
				instance_create_depth(obj_lobby_bg4.x, obj_lobby_bg4.y, depth, obj_player4);	
				obj_lobby_bg1.player = "player4";
				playersJoin = playersJoin + 1;
			}
		}
	}

	#endregion
	
	#region //Checking players quitting the lobby/Unreadying
		//Player1
	if(keyboard_check_pressed(vk_escape) or gamepad_button_check_pressed(3, gp_face2))
	{

		if(instance_exists(obj_player1))
		{
			if(obj_lobby_bg1.player == "player1")
			{
				if(obj_lobby_bg1.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player1);
					obj_lobby_bg1.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg1.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg1.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg2.player == "player1")
			{
				if(obj_lobby_bg2.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player1);
					obj_lobby_bg2.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg2.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg2.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg3.player == "player1")
			{
				if(obj_lobby_bg3.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player1);
					obj_lobby_bg3.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg3.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg3.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg4.player == "player1")
			{
				if(obj_lobby_bg4.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player1);
					obj_lobby_bg4.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg4.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg4.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
		}
		else //Quitting the lobby
		{
				//Clean up the whole lobby
			//Players
			if(instance_exists(obj_player1))
			{
				instance_destroy(obj_player1);	
			}
			if(instance_exists(obj_player2))
			{
				instance_destroy(obj_player2);	
			}
			if(instance_exists(obj_player3))
			{
				instance_destroy(obj_player3);	
			}
			if(instance_exists(obj_player4))
			{
				instance_destroy(obj_player4);	
			}
			
			//Lobby backgrounds
			obj_lobby_bg1.sprite_index = spr_lobby_waiting;
			obj_lobby_bg2.sprite_index = spr_lobby_waiting;
			obj_lobby_bg3.sprite_index = spr_lobby_waiting;
			obj_lobby_bg4.sprite_index = spr_lobby_waiting;
			
			//Player counts
			playersJoin = 0;
			playersReady = 0;
			
			//Go back to the main menu
			state = "menu";
			
			
		}
	}
	
		//Player2
	else if(gamepad_button_check_pressed(0, gp_face2))
	{

		if(instance_exists(obj_player2))
		{
			if(obj_lobby_bg1.player == "player2")
			{
				if(obj_lobby_bg1.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player2);
					obj_lobby_bg1.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg1.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg1.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg2.player == "player2")
			{
				if(obj_lobby_bg2.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player2);
					obj_lobby_bg2.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg2.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg2.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg3.player == "player2")
			{
				if(obj_lobby_bg3.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player2);
					obj_lobby_bg3.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg3.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg3.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg4.player == "player2")
			{
				if(obj_lobby_bg4.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player2);
					obj_lobby_bg4.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg4.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg4.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
		}
		else //Quitting the lobby
		{
				//Clean up the whole lobby
			//Players
			if(instance_exists(obj_player1))
			{
				instance_destroy(obj_player1);	
			}
			if(instance_exists(obj_player2))
			{
				instance_destroy(obj_player2);	
			}
			if(instance_exists(obj_player3))
			{
				instance_destroy(obj_player3);	
			}
			if(instance_exists(obj_player4))
			{
				instance_destroy(obj_player4);	
			}
			
			//Lobby backgrounds
			obj_lobby_bg1.sprite_index = spr_lobby_waiting;
			obj_lobby_bg2.sprite_index = spr_lobby_waiting;
			obj_lobby_bg3.sprite_index = spr_lobby_waiting;
			obj_lobby_bg4.sprite_index = spr_lobby_waiting;
			
			//Player counts
			playersJoin = 0;
			playersReady = 0;
			
			//Go back to the main menu
			state = "menu";
			
			
		}
	}
	
		//Player2
	else if(gamepad_button_check_pressed(1, gp_face2))
	{

		if(instance_exists(obj_player3))
		{
			if(obj_lobby_bg1.player == "player3")
			{
				if(obj_lobby_bg1.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player3);
					obj_lobby_bg1.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg1.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg1.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg2.player == "player3")
			{
				if(obj_lobby_bg2.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player3);
					obj_lobby_bg2.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg2.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg2.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg3.player == "player3")
			{
				if(obj_lobby_bg3.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player3);
					obj_lobby_bg3.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg3.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg3.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg4.player == "player3")
			{
				if(obj_lobby_bg4.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player3);
					obj_lobby_bg4.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg4.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg4.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
		}
		else //Quitting the lobby
		{
				//Clean up the whole lobby
			//Players
			if(instance_exists(obj_player1))
			{
				instance_destroy(obj_player1);	
			}
			if(instance_exists(obj_player2))
			{
				instance_destroy(obj_player2);	
			}
			if(instance_exists(obj_player3))
			{
				instance_destroy(obj_player3);	
			}
			if(instance_exists(obj_player4))
			{
				instance_destroy(obj_player4);	
			}
			
			//Lobby backgrounds
			obj_lobby_bg1.sprite_index = spr_lobby_waiting;
			obj_lobby_bg2.sprite_index = spr_lobby_waiting;
			obj_lobby_bg3.sprite_index = spr_lobby_waiting;
			obj_lobby_bg4.sprite_index = spr_lobby_waiting;
			
			//Player counts
			playersJoin = 0;
			playersReady = 0;
			
			//Go back to the main menu
			state = "menu";
			
			
		}
	}
	
		//Player4
	else if(gamepad_button_check_pressed(2, gp_face2))
	{

		if(instance_exists(obj_player4))
		{
			if(obj_lobby_bg1.player == "player4")
			{
				if(obj_lobby_bg1.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player4);
					obj_lobby_bg1.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg1.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg1.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg2.player == "player4")
			{
				if(obj_lobby_bg2.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player4);
					obj_lobby_bg2.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg2.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg2.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg3.player == "player4")
			{
				if(obj_lobby_bg3.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player4);
					obj_lobby_bg3.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg3.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg3.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
			else if(obj_lobby_bg4.player == "player4")
			{
				if(obj_lobby_bg4.sprite_index == spr_lobby_waiting)
				{
					instance_destroy(obj_player4);
					obj_lobby_bg4.player = "none";
					playersJoin = playersJoin - 1;
				}
				else if(obj_lobby_bg4.sprite_index == spr_lobby_ready)
				{
					obj_lobby_bg4.sprite_index = spr_lobby_waiting;
					playersReady = playersReady - 1;
				}
			}
		}
		else //Quitting the lobby
		{
				//Clean up the whole lobby
			//Players
			if(instance_exists(obj_player1))
			{
				instance_destroy(obj_player1);	
			}
			if(instance_exists(obj_player2))
			{
				instance_destroy(obj_player2);	
			}
			if(instance_exists(obj_player3))
			{
				instance_destroy(obj_player3);	
			}
			if(instance_exists(obj_player4))
			{
				instance_destroy(obj_player4);	
			}
			
			//Lobby backgrounds
			obj_lobby_bg1.sprite_index = spr_lobby_waiting;
			obj_lobby_bg2.sprite_index = spr_lobby_waiting;
			obj_lobby_bg3.sprite_index = spr_lobby_waiting;
			obj_lobby_bg4.sprite_index = spr_lobby_waiting;
			
			//Player counts
			playersJoin = 0;
			playersReady = 0;
			
			//Go back to the main menu
			state = "menu";
			
			
		}
	}

	#endregion
	
}
#endregion

#region //Modifier screen
else if(state == "modifier")
{
	//Moving through the menu
	modifierIndex = UpMenu(modifierIndex, maxModifierMenu);
	modifierIndex = DownMenu(modifierIndex, maxModifierMenu);
	
	
	if(modifierMenu[modifierIndex] == "Start")
	{
		if(ConfirmMenu())
		{
			//Launch a game, picking a random map and going in it
			if(gamemodeSelected == 0)
			{
				//Lives gamemode
				state = "game lifecount";
				
				playersAlive = playersJoin;
				
				#region //Lives distribution and respawn initialisation
				if(instance_exists(obj_player1))
				{
					player1hp = objectiveCount;
				}
				else
				{
					player1hp = -1;
				}
				
				if(instance_exists(obj_player2))
				{
					player2hp = objectiveCount;	
				}
				else
				{
					player2hp = -1;
				}
				
				if(instance_exists(obj_player3))
				{
					player3hp = objectiveCount;	
				}
				else
				{
					player3hp = -1;
				}
				
				if(instance_exists(obj_player4))
				{
					player4hp = objectiveCount;
				}
				else
				{
					player4hp = -1;
				}
				
				player1respawn = -1;
				player2respawn = -1;
				player3respawn = -1;
				player4respawn = -1;
				
				//Selection of a random room
				var rmIndex = irandom_range(0, maxMaps);
				room_goto(maps[rmIndex]);
				
				#endregion
				
			}
			else if(gamemodeSelected == 1)
			{
				//Killcount gamemode
				state = "game killcount";
				
				
				
				#region //Killcount reinitialisation
				
				if(instance_exists(obj_player1))
				{
					player1killcount = 0;
				}
				else
				{
					player1killcount = -1;
				}
				
				if(instance_exists(obj_player2))
				{
					player2killcount = 0;
				}
				else
				{
					player2killcount = -1;
				}
				
				if(instance_exists(obj_player3))
				{
					player3killcount = 0;
				}
				else
				{
					player3killcount = -1;
				}
				
				if(instance_exists(obj_player4))
				{
					player4killcount = 0;
				}
				else
				{
					player4killcount = -1;
				}
				
				
				
				#endregion
				
				//Selection of a random room
				var rmIndex = irandom_range(0, maxMaps);
				room_goto(maps[rmIndex]);
				
			}
		}
	}
	else if(modifierMenu[modifierIndex] == "Gamemode")
	{
		gamemodeSelected = LeftMenu(gamemodeSelected, 2);
		gamemodeSelected = RightMenu(gamemodeSelected, 2);
	}
	else if(modifierMenu[modifierIndex] == "Points")
	{
		objectiveCount = LeftMenu(objectiveCount, 12);
		objectiveCount = RightMenu(objectiveCount, 12);
		
		if(objectiveCount == 0)
		{
			objectiveCount = 1;	
		}
		if(objectiveCount == 11)
		{
			objectiveCount = 10;	
		}
	}	
	else if(modifierMenu[modifierIndex] == "Weapons")
	{
		if(ConfirmMenu())
		{
			state = "weapon selection";
		}
	}
	else if(modifierMenu[modifierIndex] == "Back")
	{
		if(ConfirmMenu())
		{
			
			state = "lobby";	
		
			modifierIndex = 0;

		}
	}
	
	//Checking for escape/B/Circle for players that want to go back to the lobby
	//Same effect as the "Back" Option
	if((keyboard_check_pressed(vk_escape)) or 
	   (gamepad_button_check_pressed(3, gp_face2)) or 
	   (gamepad_button_check_pressed(2, gp_face2)) or 
	   (gamepad_button_check_pressed(1, gp_face2)) or 
	   (gamepad_button_check_pressed(3, gp_face2)))
	{
		state = "lobby";	
		
		modifierIndex = 0;
		
	}
}
#endregion

#region //Weapon selection
else if(state == "weapon selection")
{
	weaponTableIndexy = UpMenu(weaponTableIndexy, maxWeaponTable);
	weaponTableIndexy = DownMenu(weaponTableIndexy, maxWeaponTable);
	weaponTableIndexx = LeftMenu(weaponTableIndexx, maxWeaponTable);
	weaponTableIndexx = RightMenu(weaponTableIndexx, maxWeaponTable);
	
	if(ConfirmMenu())
	{
		#region //Enable/Disable the selected weapon
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_shotgun)
		{
			if(enabledShotgun)
			{
				enabledShotgun = 0;
			}
			else
			{
				enabledShotgun = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_sawed_off)
		{
			if(enabledSawedoff)
			{
				enabledSawedoff = 0;
			}
			else
			{
				enabledSawedoff = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_sniper)
		{
			if(enabledSniper)
			{
				enabledSniper = 0;
			}
			else
			{
				enabledSniper = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_flamethrower)
		{
			if(enabledFlamethrower)
			{
				enabledFlamethrower = 0;
			}
			else
			{
				enabledFlamethrower = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_minigun)
		{
			if(enabledMinigun)
			{
				enabledMinigun = 0;
			}
			else
			{
				enabledMinigun = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_fukin_cheese)
		{
			if(enabledFukincheese)
			{
				enabledFukincheese = 0;
			}
			else
			{
				enabledFukincheese = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_windgun)
		{
			if(enabledWindgun)
			{
				enabledWindgun = 0;
			}
			else
			{
				enabledWindgun = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_swapgun)
		{
			if(enabledSwapgun)
			{
				enabledSwapgun = 0;
			}
			else
			{
				enabledSwapgun = 1;
			}
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_chronobat)
		{
			if(enabledChronobat)
			{
				enabledChronobat = 0;
			}
			else
			{
				enabledChronobat = 1;
			}
		}
		#endregion
	}
	if(BackMenu())
	{
		state = "modifier";
	}
}

#endregion

#region
else if(state == "game killcount")
{
	if(global.stop == 0)
	{
		#region //Check condition de victoire
		if(playersAlive <= 1)
		{
			if(player1killcount >= objectiveCount)
			{
				VictoryScreen("Player 1");
			}
			else if(player2killcount >= objectiveCount)
			{
				VictoryScreen("Player 2");
			}
			else if(player3killcount >= objectiveCount)
			{
				VictoryScreen("Player 3");
			}
			else if(player4killcount >= objectiveCount)
			{
				VictoryScreen("Player 4");
			}
		}
		#endregion
		
		#region //Respawn cycles
			
			//Player1
		if(player1respawn > 0)
		{
			player1respawn = player1respawn - 1;
		}	
		else if(player1respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player1;
			player1respawn = -1;
		}	
			//Player2
		if(player2respawn > 0)
		{
			player2respawn = player2respawn - 1;
		}	
		else if(player2respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player2;
			player2respawn = -1;
		}
			//Player3
		if(player3respawn > 0)
		{
			player3respawn = player3respawn - 1;
		}	
		else if(player3respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player3;
			player3respawn = -1;
		}
			//Player1
		if(player4respawn > 0)
		{
			player4respawn = player4respawn - 1;
		}	
		else if(player4respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player4;
			player4respawn = -1;
		}
		#endregion
	}	
}
#endregion

#region
else if(state == "game lifecount")
{
	if(global.stop == 0)
	{
		#region //Check condition de victoire
		if(playersAlive <= 1)
		{
			if(player1hp > 0)
			{
				VictoryScreen("Player 1");
			}
			else if(player2hp > 0)
			{
				VictoryScreen("Player 2");
			}
			else if(player3hp > 0)
			{
				VictoryScreen("Player 3");
			}
			else if(player4hp > 0)
			{
				VictoryScreen("Player 4");
			}
		}
		#endregion
		
		#region //Respawn cycles
			
			//Player1
		if(player1respawn > 0)
		{
			player1respawn = player1respawn - 1;
		}	
		else if(player1respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player1;
			player1respawn = -1;
		}	
			//Player2
		if(player2respawn > 0)
		{
			player2respawn = player2respawn - 1;
		}	
		else if(player2respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player2;
			player2respawn = -1;
		}
			//Player3
		if(player3respawn > 0)
		{
			player3respawn = player3respawn - 1;
		}	
		else if(player3respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player3;
			player3respawn = -1;
		}
			//Player1
		if(player4respawn > 0)
		{
			player4respawn = player4respawn - 1;
		}	
		else if(player4respawn == 0)
		{
			var randspawn = random_range(0, 3);
			var spawn = instance_create_depth(spawnerList[randspawn, 0], spawnerList[randspawn, 1], 0, obj_playerSpawner);
			spawn.player = obj_player4;
			player4respawn = -1;
		}
		#endregion
	}
}
#endregion

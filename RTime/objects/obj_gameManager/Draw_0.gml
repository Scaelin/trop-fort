/// @description 
// 


#region //Main menu
if(state == "menu")
{
	//Moving through the menu		
	if(mainMenu[menuIndex] == "Play!")
	{
		draw_sprite(spr_menu_selector, 0, 500, 1000);
	}
	else if(mainMenu[menuIndex] == "Options")
	{
		draw_sprite(spr_menu_selector, 0, 500, 1200);
	}
	else if(mainMenu[menuIndex] == "Quit")
	{
		draw_sprite(spr_menu_selector, 0, 500, 1400);
	}
	
	
	//Drawing the text
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuLarge);
	
	draw_text(550, 1000, "Play!");
	
	draw_text(550, 1200, "Options");
	
	draw_text(550, 1400, "Quit");
}
#endregion

#region //Options
if(state == "options")
{
	//The selector
	if(optionsMenu[optionsIndex] == "General volume")
	{
		draw_sprite(spr_menu_selector, 0, 500, 2600);
	}
	else if(optionsMenu[optionsIndex] == "Music volume")
	{
		draw_sprite(spr_menu_selector, 0, 500, 2700);
	}
	else if(optionsMenu[optionsIndex] == "Sound volume")
	{
		draw_sprite(spr_menu_selector, 0, 500, 2800);
	}
	else if(optionsMenu[optionsIndex] == "Screenshake intensity")
	{
		draw_sprite(spr_menu_selector, 0, 500, 2900);
	}
	else if(optionsMenu[optionsIndex] == "Back")
	{
		draw_sprite(spr_menu_selector, 0, 500, 3400);
	}
	
	//Drawing the text
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuNormal);
	
	draw_text(550, 2600, "General volume");
	draw_text(1400, 2600, generalVolume);	
	
	draw_text(550, 2700, "Music volume");
	draw_text(1400, 2700, musicVolume);
	
	draw_text(550, 2800, "Sound volume");
	draw_text(1400, 2800, soundVolume);
	
	draw_text(550, 2900, "Screenshake intensity");
	draw_text(1400, 2900, screenshake);
	
	draw_text(550, 3400, "Back");
}
#endregion

#region //Lobby
else if(state == "lobby")
{
	draw_set_color(c_black);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuLarge);
	
	if(playersJoin == 0)
	{
		draw_text(3000, 1000, "Press 'Enter'/'A'/'X' to join the game!");
	}
	else if((playersJoin > 0) and (playersJoin > playersReady))
	{
		draw_text(3000, 950, string(playersReady) + "/" + string(playersJoin) + " are ready!");
		draw_text(3000, 1050, "Press 'Enter'/'A'/'X' to get ready!");
		
	}
	else if((playersJoin == playersReady) and (playersJoin > 0))
	{
		draw_text(3000, 950, string(playersReady) + "/" + string(playersJoin) + " are ready!");
		draw_text(3000, 1050, "Press 'Enter'/'A'/'X' to get to the game settings!");
	}
}

#endregion

#region //Modifier
else if((state == "modifier") or (state == "weapon selection"))
{
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuNormal);
	
	draw_text(2300, 2600, "Start!");
	draw_text(2300, 2700, "Gamemode : ");
	
	//Depending on the gamemode, writing the name of the different objectives
	if(gamemodeSelected == 0)
	{
		draw_text(2300, 2800, "Lives : ");
		draw_text(2600, 2700, "Lives");
	}
	else if(gamemodeSelected == 1)
	{
		draw_text(2300, 2800, "Kills to win : ");
		draw_text(2600, 2700, "Kill count");
	}
	draw_text(2600, 2800, string(objectiveCount));
	
	#region //Draw the cursor
	if(modifierMenu[modifierIndex] == "Start")
	{
		draw_sprite(spr_menu_selector, 0, 2150, 2600);
	}
	else if(modifierMenu[modifierIndex] == "Gamemode")
	{
		draw_sprite(spr_menu_selector, 0, 2150, 2700);
	}
	else if(modifierMenu[modifierIndex] == "Points")
	{
		draw_sprite(spr_menu_selector, 0, 2150, 2800);
	}
	else if(modifierMenu[modifierIndex] == "Weapons")
	{
		draw_sprite(spr_menu_selector, 0, 2150, 2900);
	}
	else if(modifierMenu[modifierIndex] == "Back")
	{
		draw_sprite(spr_menu_selector, 0, 2150, 3000);
	}
	#endregion
	
	
	draw_text(2300, 2900, "Weapons");
	draw_text(2300, 3000, "Back");
	
	
	#region //Displaying the enabled/disabled weapons
	//Shotgun
	if(enabledShotgun)
	{
		draw_sprite(spr_weapon_enabled, 0, 3100, 2800);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3100, 2800);
	}
	draw_sprite(spr_shotgun, 0, 3100, 2800);
	//Sawed off
	if(enabledSawedoff)
	{
		draw_sprite(spr_weapon_enabled, 0, 3300, 2800);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3300, 2800);
	}
	draw_sprite(spr_sawed_off, 0, 3300, 2800);
	//Sniper
	if(enabledSniper)
	{
		draw_sprite(spr_weapon_enabled, 0, 3500, 2800);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3500, 2800);
	}
	draw_sprite(spr_sniper, 0, 3470, 2800);
	//Flamethrower
	if(enabledFlamethrower)
	{
		draw_sprite(spr_weapon_enabled, 0, 3100, 3000);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3100, 3000);
	}
	draw_sprite(spr_flamethrower, 0, 3100, 3000);
	//Minigun
	if(enabledMinigun)
	{
		draw_sprite(spr_weapon_enabled, 0, 3300, 3000);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3300, 3000);
	}
	draw_sprite(spr_minigun, 0, 3280, 3000);
	//Fukin cheese
	if(enabledFukincheese)
	{
		draw_sprite(spr_weapon_enabled, 0, 3500, 3000);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3500, 3000);
	}
	draw_sprite(spr_fukin_cheese, 0, 3500, 3000);
	//Windgun
	if(enabledWindgun)
	{
		draw_sprite(spr_weapon_enabled, 0, 3100, 3200);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3100, 3200);
	}
	draw_sprite(spr_windgun, 0, 3100, 3200);
	//Swapgun
	if(enabledSwapgun)
	{
		draw_sprite(spr_weapon_enabled, 0, 3300, 3200);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3300, 3200);
	}
	draw_sprite(spr_swapgun, 0, 3300, 3200);
	//Chronobat
	if(enabledChronobat)
	{
		draw_sprite(spr_weapon_enabled, 0, 3500, 3200);
	}
	else
	{
		draw_sprite(spr_weapon_disabled, 0, 3500, 3200);
	}
	draw_sprite(spr_chronobat, 0, 3500, 3200);
	#endregion
	
	#region //Draw the weapon selector
	if(state == "weapon selection")
	{
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_shotgun)
		{
			draw_sprite(spr_weapon_selector, 0, 3100, 2800);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_sawed_off)
		{
			draw_sprite(spr_weapon_selector, 0, 3300, 2800);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_sniper)
		{
			draw_sprite(spr_weapon_selector, 0, 3500, 2800);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_flamethrower)
		{
			draw_sprite(spr_weapon_selector, 0, 3100, 3000);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_minigun)
		{
			draw_sprite(spr_weapon_selector, 0, 3300, 3000);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_fukin_cheese)
		{
			draw_sprite(spr_weapon_selector, 0, 3500, 3000);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_windgun)
		{
			draw_sprite(spr_weapon_selector, 0, 3100, 3200);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_swapgun)
		{
			draw_sprite(spr_weapon_selector, 0, 3300, 3200);
		}
		if(weaponTable[weaponTableIndexx, weaponTableIndexy] == obj_chronobat)
		{
			draw_sprite(spr_weapon_selector, 0, 3500, 3200);
		}
	}
	#endregion
}
#endregion
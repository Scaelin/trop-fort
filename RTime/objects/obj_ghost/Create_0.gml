/// @description Fading ghost

// When creating an instance of this object,
// you need to change its sprite according to the object you want to ghost
// You then need to set :
// - sprite_index
// - image_index

image_speed = 0;
image_alpha = 0.8;

//Fading speed is made a variable so that object that instanciate it can modify it
fading_speed = 0.025;
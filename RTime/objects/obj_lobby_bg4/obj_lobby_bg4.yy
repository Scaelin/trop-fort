{
    "id": "97da1e26-bb84-4369-84da-73b3f8daf3e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lobby_bg4",
    "eventList": [
        {
            "id": "f1104b05-9710-4f74-81eb-ccc216720195",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97da1e26-bb84-4369-84da-73b3f8daf3e6"
        },
        {
            "id": "b4e63303-eb15-4188-95a0-433cbf0ce652",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "97da1e26-bb84-4369-84da-73b3f8daf3e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
    "visible": true
}
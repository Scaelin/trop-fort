{
    "id": "1917fe93-0974-4435-b0bb-32feadbe3e75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shotgun_douille",
    "eventList": [
        {
            "id": "f477a432-8fed-47db-b639-0a1d9de70062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1917fe93-0974-4435-b0bb-32feadbe3e75"
        },
        {
            "id": "7d0ed22c-14a6-45b6-9757-24daadc2893e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1917fe93-0974-4435-b0bb-32feadbe3e75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e17da3d5-6f27-4e47-8ea2-ee715312e5e8",
    "visible": true
}
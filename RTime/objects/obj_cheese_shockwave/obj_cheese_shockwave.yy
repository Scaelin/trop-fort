{
    "id": "275b7e50-2371-4787-93e6-b99537dda5f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cheese_shockwave",
    "eventList": [
        {
            "id": "a5db5f16-2498-45ef-93e3-6226041d3cd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "275b7e50-2371-4787-93e6-b99537dda5f1"
        },
        {
            "id": "290527a9-c00a-4a07-b73f-0efa40b18f3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "275b7e50-2371-4787-93e6-b99537dda5f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
    "visible": true
}
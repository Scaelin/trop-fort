{
    "id": "d7f8a694-ec93-4a42-93b1-4f7357f4be0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sniper_bullet",
    "eventList": [
        {
            "id": "7f320516-7073-4c99-a528-4883eff1e71f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d7f8a694-ec93-4a42-93b1-4f7357f4be0c"
        },
        {
            "id": "f132ea9b-ade5-4b70-80b2-75c789e43032",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7f8a694-ec93-4a42-93b1-4f7357f4be0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b9b0cbcf-1364-4151-9b56-b5a2b692621b",
    "visible": true
}
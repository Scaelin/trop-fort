/// @description Laser if sniper
// 

draw_self();
if(global.stop == 0)
{
	if(equiped == "sniper")
	{
		var dir = aim;
		var len = 0;
		var maxlen = 1500;
		while((len < maxlen) and (!position_meeting(x+lengthdir_x(len, dir), y+lengthdir_y(len, dir), obj_solid)))
		{
			draw_sprite_ext(spr_laser, 0, x+lengthdir_x(len, dir), y+lengthdir_y(len, dir), 1, 1, dir, c_white, 1);
			len = len+6;
		}
	
	}
	
	#region //Draw the ammo
	var maxammo = -1;
	var i = 0;
	
	//Weapon ammo display
	while((maxammo == -1) and (i < obj_dataHolder.weaponNumber))
	{
		if(equiped == obj_dataHolder.weapon[i, 0])
		{
			maxammo = obj_dataHolder.weapon[i, 1];
		}
		i++;
	}
	if(maxammo > 0)
	{
		if(ammo/maxammo > 0.8 )
		{
			draw_sprite(spr_ammo_5, 0, x, y);
		}
		else if(ammo/maxammo > 0.6)
		{
			draw_sprite(spr_ammo_4, 0, x, y);
		}
		else if(ammo/maxammo > 0.4)
		{
			draw_sprite(spr_ammo_3, 0, x, y);
		}
		else if(ammo/maxammo > 0.2)
		{
			draw_sprite(spr_ammo_2, 0, x, y);
		}
		else if(ammo/maxammo > 0)
		{
			draw_sprite(spr_ammo_1, 0, x, y);
		}
		else if(ammo == 0)
		{
			draw_sprite(spr_ammo_0, 0, x, y);
		}
	}
	#endregion
	
	#region //Chronorekt indicator
	
	if(chronodelay > room_speed*3/4)
	{
		draw_sprite(spr_chronorekt_indicator, 0, x, y - 100);
	}
	else if(chronodelay > room_speed/2)
	{
		draw_sprite(spr_chronorekt_indicator, 1, x, y - 100);
	}
	else if(chronodelay > room_speed/4)
	{
		draw_sprite(spr_chronorekt_indicator, 2, x, y - 100);
	}
	else if(chronodelay > 0)
	{
		draw_sprite(spr_chronorekt_indicator, 3, x, y - 100);
	}
	
	#endregion
	
	#region //Aim indicator
	
	draw_sprite_ext(spr_aimarrow, 0, x + lengthdir_x(100, aim), y + lengthdir_y(100, aim), 1, 1, aim, c_white, 1);
	
	#endregion
}

/// @description EXPLODE
// 
if(global.stop)
{
	speed = 0;	
}
else
{
	speed = 30;
	
	var contact = HitSomethingElse(creator);
	
	//Contact with walls
	if(contact)
	{
		//Contact with dudes
		if(contact != 1)
		{
			contact.hp = contact.hp - damage;
			contact.killer = creator;
			
			//Swapping
			if((instance_exists(creator)) and (instance_exists(contact)))
			{
				//Dropping the weapons so that they can swap aswell
				if(contact.equiped != "none")
				{
					contact.equiped = "none";
					
					var weapon1 = instance_nearest(contact.x, contact.y, obj_gun);
					weapon1.state = "floor";
					
				}
				if(creator.equiped != "none")
				{
					creator.equiped = "none";
					
					var weapon2 = instance_nearest(creator.x, creator.y, obj_gun);
					weapon2.state = "floor";
					
				}
				
				
				
				
				var tempx = contact.x;
				var tempy = contact.y;
			
				contact.x = creator.x;
				contact.y = creator.y;
			
				creator.x = tempx;
				creator.y = tempy;
			
				instance_create_depth(creator.x, creator.y, creator.depth + 1, obj_swapgun_portal);
				instance_create_depth(contact.x, contact.y, contact.depth + 1, obj_swapgun_portal);
			}
		}
		instance_destroy();	
	}
}
{
    "id": "2a034ffb-5ba1-4a76-b971-286c0c81dd2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_swap_bullet",
    "eventList": [
        {
            "id": "139eba09-841e-4af4-a831-f90b2793e6c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a034ffb-5ba1-4a76-b971-286c0c81dd2b"
        },
        {
            "id": "0b30114b-9dd5-4919-b60e-21cfe1c986d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a034ffb-5ba1-4a76-b971-286c0c81dd2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aec8d394-501f-4d74-85eb-58569c596264",
    "visible": true
}
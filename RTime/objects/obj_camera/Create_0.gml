/// @description initial position
// 
if(obj_gameManager.state == "menu")
{
	x = 1000;
	y = 1000;
}
else
{
	x = room_width/2;
	y = room_height/2;
}



screenshake = 0;
screentilt = 0;


//Coordinates of all menu positions (out of game camera management)
menux = 1000;
menuy = 1000;

optionsx = 1000;
optionsy = 3000;

lobbyx = 3000;
lobbyy = 1000;

modifierx = 3000;
modifiery = 3000;

//Slow mo on kill camera management

target1x = 0;
target1y = 0;

target2x = 0;
target2y = 0;

//Will get to :
// -1 when the effect isn't occuring
// 0 when the slow mo starts
// 1 when it reached the killed
// 2 when it reached the killer, it will then wait for a small delay before resuming the game
slowmoStep = -1;

slowmoDelay = 0.4*room_speed;

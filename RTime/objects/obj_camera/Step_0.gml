/// @description Screenshake and such
// 

//Cases corresponding to the menus
if(obj_gameManager.state == "menu")
{
	x = (x*3 + menux)/4;
	y = (y*3 + menuy)/4;
}
else if(obj_gameManager.state == "options")
{
	x = (x*3 + optionsx)/4;
	y = (y*3 + optionsy)/4;
}
else if(obj_gameManager.state == "lobby")
{
	x = (x*3 + lobbyx)/4;
	y = (y*3 + lobbyy)/4;
}
else if(obj_gameManager.state == "modifier")
{
	x = (x*3 + modifierx)/4;
	y = (y*3 + modifiery)/4;
}
else if(obj_gameManager.state == "weapon selection")
{
	x = (x*3 + modifierx)/4;
	y = (y*3 + modifiery)/4;
}
//Case corresponding to the normal camera movement in game
else if(global.stop == 0)
{
	
	//Apply screenshake
	if(screenshake > 0)
	{
		x = room_width/2 + random_range(-screenshake, screenshake);
		y = room_height/2 + random_range(-screenshake, screenshake);
		screenshake = screenshake - 1;
	}
	else
	{
		x = room_width/2;
		y = room_height/2;
	}	
	
	
	//Apply screentilt
	camera_set_view_angle(view_camera[0], screentilt);
	
	//Neutralize the tilt	
	screentilt = screentilt*3/4;	
	if((screentilt > -1) and (screentilt < 1))
	{
		screentilt = 0;	
	}
	
	
}
else if((global.stop == 1) and (slowmoStep != -1))
{
	//Initiate the slow mo effect
	if(slowmoStep == 0)
	{
		if(distance_to_point(target1x, target1y) > 30)
		{
			x = (target1x + x*2)/3;
			y = (target1y + y*2)/3;
		}
		else
		{
			x = (target1x + x*9)/10;
			y = (target1y + y*9)/10;	
		}
		
		if((x <= target1x+0.1) and (x >= target1x - 0.1) and (y <= target1y + 0.1) and (y >= target1y - 0.1))
		{
			slowmoStep = 1;	
		}
	}
	else if(slowmoStep == 1)
	{
		if(distance_to_point(target2x, target2y) > 30)
		{
			x = (target2x + x*2)/3;
			y = (target2y + y*2)/3;
		}
		else
		{
			x = (target2x + x*6)/7;
			y = (target2y + y*6)/7;	
		}
		
		if((x <= target2x+0.1) and (x >= target2x - 0.1) and (y <= target2y + 0.1) and (y >= target2y - 0.1))
		{
			slowmoStep = 2;	
		}
	}
	else if(slowmoStep == 2)
	{
		slowmoDelay = slowmoDelay - 1;
		if(slowmoDelay == 0)
		{
			slowmoDelay = 0.4*room_speed;
			slowmoStep = -1;
			global.stop = 0;
			camera_set_view_size(view_camera[0], 1920, 1080);
			x = room_width/2;
			y = room_height/2;
			
		}
	}
	
}

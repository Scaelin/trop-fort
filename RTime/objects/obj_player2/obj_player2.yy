{
    "id": "4c571ec5-e2f2-4d4a-bde8-91b5c95201af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "3efabae2-bbb9-4f54-86c6-b24739c69eaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c571ec5-e2f2-4d4a-bde8-91b5c95201af"
        },
        {
            "id": "37b018a8-bbc4-4dbb-8aa5-0eba475b9eec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c571ec5-e2f2-4d4a-bde8-91b5c95201af"
        },
        {
            "id": "b53bb9bb-93bc-45a4-97e2-10fc24624481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4c571ec5-e2f2-4d4a-bde8-91b5c95201af"
        },
        {
            "id": "fa487965-3e22-4fa8-bb9b-1e8c5144af1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4c571ec5-e2f2-4d4a-bde8-91b5c95201af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb6e169b-4ed6-4ffc-ad92-af9fe21a9335",
    "visible": true
}
{
    "id": "a7417811-6760-4f77-bb05-2f0a2799a445",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lobby_bg1",
    "eventList": [
        {
            "id": "3af2716d-61a5-46ab-a59a-5fe23812000d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7417811-6760-4f77-bb05-2f0a2799a445"
        },
        {
            "id": "74b25890-f084-4acf-b4ac-8dc1ef8eb6ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a7417811-6760-4f77-bb05-2f0a2799a445"
        },
        {
            "id": "4ace1809-9c82-4357-a6ce-bc6f0941a500",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a7417811-6760-4f77-bb05-2f0a2799a445"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
    "visible": true
}
{
    "id": "cafb06b7-f0a2-4b32-b7e7-470a1a3221e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_swapgun_portal",
    "eventList": [
        {
            "id": "d6f08507-1258-4844-8c29-3ebde81e7174",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cafb06b7-f0a2-4b32-b7e7-470a1a3221e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
    "visible": true
}
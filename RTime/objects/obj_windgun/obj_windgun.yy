{
    "id": "b68fa4b6-7897-443e-aae5-03afc6ff1308",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_windgun",
    "eventList": [
        {
            "id": "4f8d60de-aa7e-4add-8d0b-b685f3214c1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b68fa4b6-7897-443e-aae5-03afc6ff1308"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
    "visible": true
}
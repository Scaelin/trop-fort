{
    "id": "87b00db9-19d5-4928-85e6-83c5fc71a83f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_green",
    "eventList": [
        {
            "id": "9b379bc1-2f3d-4115-bd39-501fc27fda9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87b00db9-19d5-4928-85e6-83c5fc71a83f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a5eb646c-32f6-4eca-bc1c-2418eb406024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
    "visible": true
}
{
    "id": "319a0be0-20b1-4a07-973b-2c8773027bf4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_pellet_blue",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c72ef54f-06c2-4a68-b48a-c874d5c00d0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13259223-7048-4491-91ff-97957abffd65",
    "visible": true
}
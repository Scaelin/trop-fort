{
    "id": "27e736bb-b1c7-4887-bc23-a5595818c47e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player3",
    "eventList": [
        {
            "id": "53356e07-c08a-4ff3-ba26-f729148612e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27e736bb-b1c7-4887-bc23-a5595818c47e"
        },
        {
            "id": "35b3e05c-54b4-4566-bbc1-e94fa3971994",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27e736bb-b1c7-4887-bc23-a5595818c47e"
        },
        {
            "id": "51afb3bb-887e-4fe8-86f8-8978094a9bca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "27e736bb-b1c7-4887-bc23-a5595818c47e"
        },
        {
            "id": "c3ecc225-2a36-426b-a98e-2b2c1da14002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "27e736bb-b1c7-4887-bc23-a5595818c47e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "592ba594-d91c-4c43-8925-070da9bae691",
    "visible": true
}
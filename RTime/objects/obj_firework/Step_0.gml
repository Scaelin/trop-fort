/// @description 
// 
if((global.stop == 0) or (creator == -1))
{
	#region	//Movement
	
	x = x + lengthdir_x(fireworkPower, direction);
	y = y + lengthdir_y(fireworkPower, direction);
	

	grav = grav+0.3; //A GARDER ABSOLUMENT /!\
	

	y = y + grav;
	
	#endregion
	
	#region //Ghosting
	
	var ghost = instance_create_depth(x, y, depth+1, obj_ghost);
	ghost.sprite_index = sprite_index;
	ghost.image_index = image_index;
	ghost.image_alpha = 0.5;
	
	#endregion
	
	#region //Detonation

	//This part will be done in the child, in order to pick the right colors
	
	#endregion
	
	#region //Hitting terrain or players
	
	//Same
	
	#endregion
	
	
}

/// @description Initialization of data
// 

//Defines the power of the shot :  a more powerful shot will go further
fireworkPower = 0;

//Defines how long it takes before the firework explodes 
fireworkDelay = 0;

//Creator is used to know whether the firework is used in combat or as a decorative effect
creator = -1;
//if creator is still at the default value -1, it is considered decorative and cannot deal damage

//Gravity variable, to apply some physics to the firework
grav = 0;

//Those are used to define an angle where the pellets can go
minAngle = 0;
maxAngle = 359;

//Number of firework pellets
pellets = 12;

	//Fight part
	
damage = 2;


{
    "id": "51a85966-77eb-44cf-a03f-91c39f798ab9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_pellet_white",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c72ef54f-06c2-4a68-b48a-c874d5c00d0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10f4c51e-ac05-4852-8757-9d4bd80d97db",
    "visible": true
}
{
    "id": "3499323a-d2cf-4e29-a595-a9dba9c6ff6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_swapgun",
    "eventList": [
        {
            "id": "65556299-baf2-4ade-8306-11f8dfc2aa9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3499323a-d2cf-4e29-a595-a9dba9c6ff6c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
    "visible": true
}
{
    "id": "a50368a1-0e2e-40c7-ab11-75095edfe58a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player3_corpse",
    "eventList": [
        {
            "id": "a5dafa60-e65c-4f33-893b-0a0a6e875da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a50368a1-0e2e-40c7-ab11-75095edfe58a"
        },
        {
            "id": "8511724a-c7f8-482e-ae46-f2c95a1d5fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a50368a1-0e2e-40c7-ab11-75095edfe58a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
    "visible": true
}
{
    "id": "86e75af9-deb9-475f-9043-8c640e4539a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chronorekt_impact",
    "eventList": [
        {
            "id": "02c00cb5-87b2-4d65-ae60-621f8904bfb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86e75af9-deb9-475f-9043-8c640e4539a1"
        },
        {
            "id": "e5b508a7-e0df-4a4a-9636-012695bde3f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "86e75af9-deb9-475f-9043-8c640e4539a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2caf99c-2b2c-4b62-ab4d-63a717c8f401",
    "visible": true
}
/// @description 
// 

if(image_index > image_number - 1)
{
	var spawned = 0;
	var index = 0;
	while(!spawned)
	{
		//Checks if it can spawn a shotgun
		index = irandom_range(0, obj_dataHolder.weaponNumber-1);
		if((obj_dataHolder.weapon[index, 6] == obj_shotgun) and (obj_gameManager.enabledShotgun == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a sawed off shotgun
		if((obj_dataHolder.weapon[index, 6] == obj_sawed_off) and (obj_gameManager.enabledSawedoff == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a sniper
		if((obj_dataHolder.weapon[index, 6] == obj_sniper) and (obj_gameManager.enabledSniper == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a flamethrower
		if((obj_dataHolder.weapon[index, 6] == obj_flamethrower) and (obj_gameManager.enabledFlamethrower == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a minigun
		if((obj_dataHolder.weapon[index, 6] == obj_minigun) and (obj_gameManager.enabledMinigun == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a fukin cheese
		if((obj_dataHolder.weapon[index, 6] == obj_fukin_cheese) and (obj_gameManager.enabledFukincheese == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a windgun
		if((obj_dataHolder.weapon[index, 6] == obj_windgun) and (obj_gameManager.enabledWindgun == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a swapgun
		if((obj_dataHolder.weapon[index, 6] == obj_swapgun) and (obj_gameManager.enabledSwapgun == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
		//Checks if it can spawn a chronobat
		if((obj_dataHolder.weapon[index, 6] == obj_chronobat) and (obj_gameManager.enabledChronobat == 1))
		{
			instance_create_depth(x, y, depth, obj_dataHolder.weapon[index, 6]);
			spawned = 1;
		}
	}
	instance_destroy();	
}
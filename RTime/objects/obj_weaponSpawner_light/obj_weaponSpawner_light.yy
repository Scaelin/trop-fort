{
    "id": "d8e6ea47-ead6-4c56-8b66-e8a1d0e4714d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_weaponSpawner_light",
    "eventList": [
        {
            "id": "26b1085c-ff77-4b13-8161-f960f631662c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8e6ea47-ead6-4c56-8b66-e8a1d0e4714d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
    "visible": true
}
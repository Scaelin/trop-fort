{
    "id": "56c91124-f8a2-4b54-9105-971dbe8b1dff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flamethrower",
    "eventList": [
        {
            "id": "05634016-7827-451d-b323-f6447180926a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56c91124-f8a2-4b54-9105-971dbe8b1dff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f7d9248-c769-413f-a4e1-987a9f190c87",
    "visible": true
}
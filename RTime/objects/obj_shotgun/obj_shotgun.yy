{
    "id": "cb7bcc18-3dd5-4b04-9a67-bb5dfa05b834",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shotgun",
    "eventList": [
        {
            "id": "459aa54e-8fa1-4bdd-a1ef-8cdb011bfa58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb7bcc18-3dd5-4b04-9a67-bb5dfa05b834"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "faef97b8-2430-4d62-9e9d-9c42b046cad6",
    "visible": true
}
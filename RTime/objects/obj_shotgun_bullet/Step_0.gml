/// @description EXPLODE
// 
if(global.stop)
{
	speed = 0;	
}
else
{
	speed = 30;
	
	var contact = HitSomethingElse(creator);
	
	//Contact with walls
	if(contact)
	{
		//Contact with dudes
		if(contact != 1)
		{
			contact.hp = contact.hp - damage;
			contact.killer = creator;
			contact.else_hspd = contact.else_hspd + lengthdir_x(damage*3, direction);
			contact.else_vspd = contact.else_vspd + lengthdir_y(damage*3, direction);
		}
		instance_destroy();	
	}
}
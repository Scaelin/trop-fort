{
    "id": "5db9696a-1235-47f4-9602-7861d04c4cca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shotgun_bullet",
    "eventList": [
        {
            "id": "1d450295-416e-4b42-a3d7-a2e0f96c9244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5db9696a-1235-47f4-9602-7861d04c4cca"
        },
        {
            "id": "a7cb4767-6733-485e-91d5-516955a70e30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5db9696a-1235-47f4-9602-7861d04c4cca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afda2db8-649d-4161-964d-ced115f65a3e",
    "visible": true
}
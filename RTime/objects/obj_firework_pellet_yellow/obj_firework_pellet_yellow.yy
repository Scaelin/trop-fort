{
    "id": "7538b5dc-ea2a-4999-ad0d-2ee262c2e6d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_pellet_yellow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c72ef54f-06c2-4a68-b48a-c874d5c00d0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6fa70214-b2da-4945-8d8f-905a4a97fd6a",
    "visible": true
}
{
    "id": "eb3a3a26-baaf-4a3a-bcba-873cc686f8fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sniper",
    "eventList": [
        {
            "id": "bf3b67eb-c5b0-45c5-8352-03992f38a573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb3a3a26-baaf-4a3a-bcba-873cc686f8fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5e5b122-40b8-44f5-bd59-8ea4278ec866",
    "visible": true
}
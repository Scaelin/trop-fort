{
    "id": "454f6d8a-4a92-47ec-81be-3efef9282851",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1_corpse",
    "eventList": [
        {
            "id": "4b11eea8-7872-4b3c-9b4d-21a2a2925a98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "454f6d8a-4a92-47ec-81be-3efef9282851"
        },
        {
            "id": "64e2b17f-c333-48c5-990b-862e6d40e432",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "454f6d8a-4a92-47ec-81be-3efef9282851"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
    "visible": true
}
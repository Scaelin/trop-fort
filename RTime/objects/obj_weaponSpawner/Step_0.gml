/// @description 
// 

if(global.stop == 0)
{
	if((spawnTimer == 0) and (obj_gameManager.enabledWeapons != 0))
	{
		instance_create_depth(x, y-45, depth, obj_weaponSpawner_light);
		
		spawnTimer = spawnCooldown;
	}
	
	if(spawnTimer > 0)
	{
		spawnTimer = spawnTimer - 1;
	}
}
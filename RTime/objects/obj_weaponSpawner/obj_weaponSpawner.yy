{
    "id": "15ed7a8f-14e9-4727-9397-ae9d787cad11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_weaponSpawner",
    "eventList": [
        {
            "id": "5b3b5dbc-b784-46a9-b3df-71ce2109d931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15ed7a8f-14e9-4727-9397-ae9d787cad11"
        },
        {
            "id": "03c5c4ae-821b-40d4-888a-57b98c5aad5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15ed7a8f-14e9-4727-9397-ae9d787cad11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6c89b4ef-ae15-4d62-b7d7-7650f0e7d1de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "8f059d04-921d-4dd2-9fde-2bb17db1952a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "414535c6-7c4e-4b3c-a4bc-697b46d2405c",
    "visible": true
}
{
    "id": "be5617ed-45a3-4ef0-9a5c-1d1e951b94f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dataHolder",
    "eventList": [
        {
            "id": "b19198f6-12aa-4322-8bd1-ab222e2f4eec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be5617ed-45a3-4ef0-9a5c-1d1e951b94f6"
        },
        {
            "id": "c8b0352f-54df-4465-958b-0f84f9b2dd4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be5617ed-45a3-4ef0-9a5c-1d1e951b94f6"
        },
        {
            "id": "ac17693a-20ce-41c5-9660-3ecb632c0136",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be5617ed-45a3-4ef0-9a5c-1d1e951b94f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
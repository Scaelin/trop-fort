/// @description  Draw the pause screen if the game is paused, basically
//
if(global.pause = 1)
{
	depth = -100;
	draw_sprite(spr_pause_screen, 0, room_width/2, room_height/2);
}
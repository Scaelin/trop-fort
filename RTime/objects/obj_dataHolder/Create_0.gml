/// @description EVERYTHING

#region // GUNZ

//[0] = Name of the gun
//[1] = Max ammo
//[2] = Recoil
//[3] = Reload time
//[4] = Shot Delay
//[5] = Dispersion
//[6] = Object name

weapon[0, 0] = "shotgun";
weapon[0, 1] = 5;
weapon[0, 2] = 30;
weapon[0, 3] = 2*room_speed;
weapon[0, 4] = 0.4*room_speed;
weapon[0, 5] = 10;
weapon[0, 6] = obj_shotgun;

weapon[1, 0] = "sawed off";
weapon[1, 1] = 10;
weapon[1, 2] = 30;
weapon[1, 3] = 2.5*room_speed;
weapon[1, 4] = 0.25*room_speed;
weapon[1, 5] = 25;
weapon[1, 6] = obj_sawed_off;

weapon[2, 0] = "sniper";
weapon[2, 1] = 1;
weapon[2, 2] = 30;
weapon[2, 3] = 1*room_speed;
weapon[2, 4] = 1*room_speed;
weapon[2, 5] = 0;
weapon[2, 6] = obj_sniper;

weapon[3, 0] = "minigun";
weapon[3, 1] = 100;
weapon[3, 2] = 4;
weapon[3, 3] = 3*room_speed;
weapon[3, 4] = 0.05*room_speed;
weapon[3, 5] = 20;
weapon[3, 6] = obj_minigun;

weapon[4, 0] = "swapgun";
weapon[4, 1] = 1;
weapon[4, 2] = 25;
weapon[4, 3] = 0.5*room_speed;
weapon[4, 4] = 0*room_speed;
weapon[4, 5] = 5;
weapon[4, 6] = obj_swapgun;

weapon[5, 0] = "windgun";
weapon[5, 1] = 5;
weapon[5, 2] = 25;
weapon[5, 3] = 1.3*room_speed;
weapon[5, 4] = 0.8*room_speed;
weapon[5, 5] = 5;
weapon[5, 6] = obj_windgun;

weapon[6, 0] = "flamethrower";
weapon[6, 1] = 150;
weapon[6, 2] = 2;
weapon[6, 3] = 2.5*room_speed;
weapon[6, 4] = 0*room_speed;
weapon[6, 5] = 0;
weapon[6, 6] = obj_flamethrower;

weapon[7, 0] = "fukin cheese";
weapon[7, 1] = 0;
weapon[7, 2] = 0;
weapon[7, 3] = 0*room_speed;
weapon[7, 4] = 0*room_speed;
weapon[7, 5] = 0;
weapon[7, 6] = obj_fukin_cheese;

weapon[8, 0] = "chronobat";
weapon[8, 1] = 0;
weapon[8, 2] = 15;
weapon[8, 3] = 0;
weapon[8, 4] = 0.8*room_speed;
weapon[8, 5] = 0;
weapon[8, 6] = obj_chronobat;


weaponNumber = 9;

#endregion

//Pause options
global.pause = 0;
global.stop = 0;
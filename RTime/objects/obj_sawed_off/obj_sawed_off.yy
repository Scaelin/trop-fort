{
    "id": "abffec5d-4cf8-4678-8e17-a6a6ccd15f1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sawed_off",
    "eventList": [
        {
            "id": "b4695b10-b698-498f-a61c-5c485a4c0ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "abffec5d-4cf8-4678-8e17-a6a6ccd15f1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d6e9459-8eae-422a-98e2-9b6491bf59a1",
    "visible": true
}
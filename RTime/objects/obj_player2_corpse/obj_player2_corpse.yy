{
    "id": "7aa5613b-d7cc-41ff-804f-7ba7c46fe2bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2_corpse",
    "eventList": [
        {
            "id": "ab43920a-752b-435f-be2c-40957fb74697",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7aa5613b-d7cc-41ff-804f-7ba7c46fe2bf"
        },
        {
            "id": "20e6716c-424d-49ac-90eb-86a3345201b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7aa5613b-d7cc-41ff-804f-7ba7c46fe2bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
    "visible": true
}
{
    "id": "ffeab80c-142b-4c0f-bfd4-d1167323dba9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fukin_cheese",
    "eventList": [
        {
            "id": "d1c52f6e-5836-4f13-86dd-4eaf59c7bec9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffeab80c-142b-4c0f-bfd4-d1167323dba9"
        },
        {
            "id": "85d5dc07-90e3-4b48-baad-5734aaecbca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffeab80c-142b-4c0f-bfd4-d1167323dba9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b635602-c4b1-47b1-830e-0968e9b9d9bd",
    "visible": true
}
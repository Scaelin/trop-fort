/// @description 
// 

//Creator is used to know whether the firework is used in combat or as a decorative effect
creator = -1;
//if creator is still at the default value -1, it is considered decorative and cannot deal damage

//Gravity variable, to apply some physics to the firework
grav = 0;

//Defines the power of the shot :  a more powerful shot will go further
fireworkPower = 10;

//Defines how long it takes before the firework explodes 
fireworkDelay = 1.5*room_speed;


	//Fight part
	
damage = 1.5;

/// @description 
// 

if((global.stop == 0) or (creator == -1))
{
	#region	//Movement
	
	x = x + lengthdir_x(fireworkPower, direction);
	y = y + lengthdir_y(fireworkPower, direction);
	
		//Meilleur jeu
	
	grav = grav+0.3; //A GARDER ABSOLUMENT /!\

	y = y + grav;
	
	#endregion
	
	#region //Ghosting
	
	var ghost = instance_create_depth(x, y, depth+1, obj_ghost);
	ghost.sprite_index = sprite_index;
	ghost.image_index = image_index;
	ghost.image_alpha = 0.5;
	
	#endregion
	
	#region //Detonation

	if(fireworkDelay > 0)
	{
		fireworkDelay = fireworkDelay - 1;	
	}
	else
	{
		instance_destroy();
	}
	
	#endregion
	
	#region //Hurting players
	
	if(creator =! -1)
	{
		var contact = HitSomethingElse(creator);
	
		//Contact with walls
		if(contact)
		{
			//Contact with dudes
			if(contact != 1)
			{
				contact.killer = creator;
				contact.hp = contact.hp - damage;
				
				instance_destroy();	
			}
		}
	}
	
	#endregion
	
	
}
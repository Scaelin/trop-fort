{
    "id": "005670c1-d2d2-493c-ae26-24518b45b656",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_blue",
    "eventList": [
        {
            "id": "655a4dfd-a46e-4aac-b1ca-c230669a1257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "005670c1-d2d2-493c-ae26-24518b45b656"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a5eb646c-32f6-4eca-bc1c-2418eb406024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
    "visible": true
}
{
    "id": "11967d8f-04b7-4f39-933f-cd8a3db22dc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_minigun",
    "eventList": [
        {
            "id": "cc17b458-cb4f-4baf-a1d3-e5e4e5cd4355",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11967d8f-04b7-4f39-933f-cd8a3db22dc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8fb6001b-9498-451a-87b5-97b7a54496dd",
    "visible": true
}
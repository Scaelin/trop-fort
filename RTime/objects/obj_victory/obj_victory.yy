{
    "id": "a0258aea-56b0-4c47-aad6-c52629dc1b8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_victory",
    "eventList": [
        {
            "id": "36221e21-7b8e-44b1-b3fc-05141fb9ea08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0258aea-56b0-4c47-aad6-c52629dc1b8e"
        },
        {
            "id": "3751a537-91ea-4457-b943-8e8e413de0a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0258aea-56b0-4c47-aad6-c52629dc1b8e"
        },
        {
            "id": "19fe569c-898d-46b8-ba49-02dac4bf964b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a0258aea-56b0-4c47-aad6-c52629dc1b8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
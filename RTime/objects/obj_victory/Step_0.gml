/// @description 
// 
if(fireworksTriggered == 0)
{
	#region //Victorious fireworks
	if(victor == "Player 1")
	{
		var firework1 = instance_create_depth(room_width/4, room_height*3/4, depth, obj_firework_launcher);
		firework1.mode = "selection";
		firework1.selection = 1;
	
		var firework2 = instance_create_depth(room_width*3/4, room_height*3/4, depth, obj_firework_launcher);
		firework2.mode = "selection";
		firework2.selection = 1;
	}
	else if(victor == "Player 2")
	{
		var firework1 = instance_create_depth(room_width/4, room_height*3/4, depth, obj_firework_launcher);
		firework1.mode = "selection";
		firework1.selection = 2;
	
		var firework2 = instance_create_depth(room_width*3/4, room_height*3/4, depth, obj_firework_launcher);
		firework2.mode = "selection";
		firework2.selection = 2;
	}
	else if(victor == "Player 3")
	{
		var firework1 = instance_create_depth(room_width/4, room_height*3/4, depth, obj_firework_launcher);
		firework1.mode = "selection";
		firework1.selection = 3;
	
		var firework2 = instance_create_depth(room_width*3/4, room_height*3/4, depth, obj_firework_launcher);
		firework2.mode = "selection";
		firework2.selection = 3;
	}
	else if(victor == "Player 4")
	{
		var firework1 = instance_create_depth(room_width/4, room_height*3/4, depth, obj_firework_launcher);
		firework1.mode = "selection";
		firework1.selection = 4;
	
		var firework2 = instance_create_depth(room_width*3/4, room_height*3/4, depth, obj_firework_launcher);
		firework2.mode = "selection";
		firework2.selection = 4;
	}
	#endregion		
	
	fireworksTriggered = 1;
}

//Timer to continue
if(nextTimer > 0)
{
	nextTimer = nextTimer - 1;
	
	if(nextTimer == 0)
	{
		
	}
}
if((nextTimer == 0) and (ConfirmMenu()))
{
	obj_gameManager.state = "menu";
	obj_gameManager.playersReady = 0;
	obj_gameManager.playersJoin = 0;
	room_goto(rm_main);
	global.stop = 0;
	
}
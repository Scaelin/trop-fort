/// @description Things are happening
if(global.stop == 0)
{

	#region //Contrôles 
	left = keyboard_check(ord("Q"));
	right = keyboard_check(ord("D"));

	fire = mouse_check_button(mb_left);
	throw = mouse_check_button_pressed(mb_right);

	aim = point_direction(x, y, mouse_x, mouse_y);
	
	//Partie contrôles dans le cas de 4 manettes
	if(gamepad_is_connected(3))
	{
		gamepad_set_axis_deadzone(3, 0.3);
		
		if(gamepad_axis_value(3, gp_axislh) > 1)
		{
			left = 0;
			right = gamepad_axis_value(3, gp_axislh);
		}
		else
		{
			left = gamepad_axis_value(3, gp_axislh);
			right = 0;
		}
		
		fire = gamepad_button_check(3, gp_shoulderrb);
		throw = gamepad_button_check_pressed(3, gp_shoulderlb);
		
		if((gamepad_axis_value(3, gp_axisrh) != 0) or (gamepad_axis_value(3, gp_axisrv) != 0))
		{
			aim = point_direction(0, 0, gamepad_axis_value(3, gp_axisrh), gamepad_axis_value(3, gp_axisrv));
		}
	}
	
	#endregion
	
	

	#region //Movement
		//From the player's input
	input_hspd = (right - left)*movespeed;
	input_vspd = 0;


		//Gravity
	if((!instance_place(x, y+1, obj_solid)))
	{
		if(grav <= 18)		//Meilleur jeu
		{
			grav = grav+0.6; //A GARDER ABSOLUMENT /!\
		}
		input_vspd = input_vspd + grav;
	}
	
	#region Chronoclaque
	if(chronodelay > 0)
	{
		chronodelay = chronodelay - 1;
	}
	else if(chronodelay == 0)
	{
		else_hspd = else_hspd + hchronorekt;	
		else_vspd = else_vspd + vchronorekt;	
		
		AddScreenshake((abs(hchronorekt) + abs(vchronorekt))/4);
		if(x > room_width/2)
		{
			AddScreentilt(-((abs(hchronorekt) + abs(vchronorekt))/2));
		}
		else
		{
			AddScreentilt(((abs(hchronorekt) + abs(vchronorekt))/2));	
		}
		
		//Chronoghost
		var ghost = instance_create_depth(x, y, depth, obj_ghost);
		ghost.sprite_index = sprite_index;
		ghost.image_index = image_index;
		
		//Hourglass background
		var hourglass = instance_create_depth(x, y, depth+100, obj_chronorekt_impact);
		
		
		
		hchronorekt = 0;
		vchronorekt = 0;
		
		chronodelay = -1;
		
	}
	
	#endregion

	if(instance_place(x, y+1, obj_solid))
	{
		grav = 0;	
	}

		//Friction
	if(else_hspd > 0)
	{
		if(instance_place(x+1, y, obj_solid))
		{
			else_hspd = else_hspd - 1.5;
		}
		else
		{
			else_hspd = else_hspd - 0.5;
		}
	}
	if(else_hspd < 0)
	{
		if(instance_place(x-1, y, obj_solid))
		{
			else_hspd = else_hspd + 2.5;
		}
		else
		{
			else_hspd = else_hspd + 0.5;
		}
	}
	if(else_vspd < 0)
	{
		if(instance_place(x, y-1, obj_solid))
		{
			else_vspd = else_vspd + 1.5;
		}
		else
		{
			else_vspd = else_vspd + 0.5;
		}
	}
	if(else_vspd > 0)
	{
		if(instance_place(x, y+1, obj_solid))
		{
			else_vspd = else_vspd - 1.5;
		}
		else
		{
			else_vspd = else_vspd - 0.5;
		}
	}
		//Annulation
	if(else_hspd < 0.5) and (else_hspd > -0.5)
	{
		else_hspd = 0;	
	}
	if(else_vspd < 0.5) and (else_vspd > -0.5)
	{
		else_vspd = 0;	
	}
	

		//Final speed
	vspd = input_vspd + else_vspd;
	hspd = input_hspd + else_hspd;

		//Colisions

	#region	//Horizontal
	
	//Movement segmentation for ultra high speed

	if(hspd > 0)
	{
		//Movement segmentation for ultra high speed
		while(hspd > 15)
		{
			if(instance_place(x+15, y, obj_solid))
			{
				while(!instance_place(x+1, y, obj_solid))
				{
					x = x + 1;	
				}
				hspd = 0;
			}
			if(hspd > 0)
			{
				x = x + 15;	
				hspd = hspd - 15;
			}
		}
		//Leftover (hspd < 15)
		if(instance_place(x+hspd, y, obj_solid))
		{
			while(!instance_place(x+1, y, obj_solid))
			{
				x = x + 1;	
			}
			hspd = 0;
		}

		x = x + hspd;	
	}
	if(hspd < 0)
	{
		//Movement segmentation for ultra high speed
		while(hspd < -15)
		{
			if(instance_place(x-15, y, obj_solid))
			{
				while(!instance_place(x-1, y, obj_solid))
				{
					x = x - 1;	
				}
				hspd = 0;
			}
			if(hspd < 0)
			{
				x = x - 15;	
				hspd = hspd + 15;
			}
		}
		//Leftover (hspd > -15)
		if(instance_place(x+hspd, y, obj_solid))
		{
			while(!instance_place(x-1, y, obj_solid))
			{
				x = x - 1;	
			}
			hspd = 0;
		}

		x = x + hspd;
	}
	#endregion

	#region	//Vertical
	
	//Movement segmentation for ultra high speed

	if(vspd > 0)
	{
		//Movement segmentation for ultra high speed
		while(vspd > 15)
		{
			if(instance_place(x, y +15, obj_solid))
			{
				while(!instance_place(x, y+1, obj_solid))
				{
					y = y + 1;	
				}
				vspd = 0;
			}
			if(vspd > 0)
			{
				y = y + 15;	
				vspd = vspd - 15;
			}
		}
		//Leftover (vspd < 15)
		if(instance_place(x, y+vspd, obj_solid))
		{
			while(!instance_place(x, y+1, obj_solid))
			{
				y = y + 1;	
			}
			vspd = 0;
		}

		y = y + vspd;	
	}
	if(vspd < 0)
	{
		//Movement segmentation for ultra high speed
		while(vspd < -15)
		{
			if(instance_place(x, y-15, obj_solid))
			{
				while(!instance_place(x, y-1, obj_solid))
				{
					y = y - 1;	
				}
				vspd = 0;
			}
			if(vspd < 0)
			{
				y = y - 15;	
				vspd = vspd + 15;
			}
		}
		//Leftover (hspd > -15)
		if(instance_place(x, y+vspd, obj_solid))
		{
			while(!instance_place(x, y-1, obj_solid))
			{
				y = y - 1;	
			}
			vspd = 0;
		}

		y = y + vspd;
	}
	#endregion

	#endregion

	#region //Fire the weapon
	if(fire)
	{
		#region //None
		if(equiped == "none")
		{
			if(claqueDelay == 0)
			{
				var claque = instance_create_depth(x, y, depth -1,  obj_claque);
				claque.creator = obj_player1;
				claque.direction = aim;
				claque.image_angle = aim;
				
				claqueDelay = claqueDelayMax;
				AddScreenshake(5);
			}
		}
		#endregion
	
		#region //Shotgun
		else if((equiped == obj_dataHolder.weapon[0, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet1.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet1.direction = bullet1.image_angle;
			bullet1.creator = obj_player1;
			
			var bullet2 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet2.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet2.direction = bullet2.image_angle;
			bullet2.creator = obj_player1;
			
			var bullet3 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet3.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet3.direction = bullet3.image_angle;
			bullet3.creator = obj_player1;
			
			var bullet4 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet4.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet4.direction = bullet4.image_angle;
			bullet4.creator = obj_player1;
			
			var bullet5 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet5.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet5.direction = bullet5.image_angle;
			bullet5.creator = obj_player1;
			
			var bullet6 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet6.image_angle = random_range(aim-obj_dataHolder.weapon[0, 5], aim+obj_dataHolder.weapon[0, 5]);
			bullet6.direction = bullet6.image_angle;
			bullet6.creator = obj_player1;
			
			var douille = instance_create_depth(x, y, 10000, obj_shotgun_douille);
			douille.direction = aim + 180;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[0, 3];	
			shotdelay = obj_dataHolder.weapon[0, 4];
		
			AddScreenshake(15);
			
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[0, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[0, 2], aim+180);
		}
		#endregion
	
		#region //Sawed off shotgun
		else if((equiped == obj_dataHolder.weapon[1, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet1.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet1.direction = bullet1.image_angle;
			bullet1.creator = obj_player1;
			
			var bullet2 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet2.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet2.direction = bullet2.image_angle;
			bullet2.creator = obj_player1;
			
			var bullet3 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet3.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet3.direction = bullet3.image_angle;
			bullet3.creator = obj_player1;
			
			var bullet4 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet4.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet4.direction = bullet4.image_angle;
			bullet4.creator = obj_player1;
			
			var bullet5 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet5.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet5.direction = bullet5.image_angle;
			bullet5.creator = obj_player1;
			
			var bullet6 = instance_create_depth(x, y, depth, obj_shotgun_bullet);
			bullet6.image_angle = random_range(aim-obj_dataHolder.weapon[1, 5], aim+obj_dataHolder.weapon[1, 5]);
			bullet6.direction = bullet6.image_angle;
			bullet6.creator = obj_player1;
			
			var douille = instance_create_depth(x, y, 10000, obj_shotgun_douille);
			douille.direction = aim + 180;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[1, 3];	
			shotdelay = obj_dataHolder.weapon[1, 4];
			
			AddScreenshake(15);
		
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[1, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[1, 2], aim+180);
		}
		#endregion
		
		#region //Sniper
		else if((equiped == obj_dataHolder.weapon[2, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_sniper_bullet);
			bullet1.image_angle = aim;
			bullet1.direction = bullet1.image_angle;	
			bullet1.creator = obj_player1;
			
			var douille = instance_create_depth(x, y, 10000, obj_sniper_douille);
			douille.direction = aim + 180;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[2, 3];	
			shotdelay = obj_dataHolder.weapon[2, 4];
		
			//AddScreenshake(10);
			if(x > room_width/2)
			{
				AddScreentilt(-10);
			}
			else
			{
				AddScreentilt(10);	
			}
			
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[2, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[2, 2], aim+180);
		}
		#endregion 
		
		#region //Minigun
		else if((equiped == obj_dataHolder.weapon[3, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_minigun_bullet);
			bullet1.image_angle = random_range(aim-obj_dataHolder.weapon[3, 5], aim+obj_dataHolder.weapon[3, 5]);
			bullet1.direction = bullet1.image_angle;
			bullet1.creator = obj_player1;
			
			var douille = instance_create_depth(x, y, 10000, obj_minigun_douille);
			douille.direction = aim + 180;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[3, 3];	
			shotdelay = obj_dataHolder.weapon[3, 4];
			
			AddScreenshake(3);
		
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[3, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[3, 2], aim+180);
		}
		#endregion
		
		#region //Flamethrower
		else if((equiped == obj_dataHolder.weapon[6, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_flamethrower_bullet);
			bullet1.image_angle = aim;
			bullet1.direction = bullet1.image_angle;
			bullet1.creator = obj_player1;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[6, 3];	
			shotdelay = obj_dataHolder.weapon[6, 4];
			
			AddScreenshake(1.1);
		
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[6, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[6, 2], aim+180);
		}
		#endregion 
		
		#region //Fukin cheese
		else if(equiped == "fukin cheese")
		{
			if(claqueDelay == 0)
			{
				var claque = instance_create_depth(x, y, depth -1,  obj_claque);
				claque.creator = obj_player1;
				claque.direction = aim;
				claque.image_angle = aim;
				
				claqueDelay = claqueDelayMax;
				AddScreenshake(5);
			}
		}
		
		#endregion
		
		#region //Chronobat
		else if(equiped == "chronobat")
		{
			if(claqueDelay == 0)
			{
				var claque = instance_create_depth(x, y, depth -1,  obj_chronoclaque);
				claque.creator = obj_player1;
				claque.direction = aim;
				claque.image_angle = aim;
				
				claqueDelay = claqueDelayMax;
				AddScreenshake(5);
			}
		}
		#endregion
		
		#region //Swapgun
		else if((equiped == obj_dataHolder.weapon[4, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_swap_bullet);
			bullet1.image_angle = aim;
			bullet1.direction = bullet1.image_angle;	
			bullet1.creator = obj_player1;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[4, 3];	
			shotdelay = obj_dataHolder.weapon[4, 4];
		
			AddScreenshake(20);
			
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[4, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[4, 2], aim+180);
		}
		#endregion 
		
		#region //Windgun
		else if((equiped == obj_dataHolder.weapon[5, 0]) and (ammo > 0) and (shotdelay == 0))
		{
		
			var bullet1 = instance_create_depth(x, y, depth, obj_windgun_bullet);
			bullet1.image_angle = aim;
			bullet1.direction = bullet1.image_angle;	
			bullet1.creator = obj_player1;
		
			ammo = ammo -1;
			reload = obj_dataHolder.weapon[5, 3];	
			shotdelay = obj_dataHolder.weapon[5, 4];
		
			AddScreenshake(20);
			
			//recul
			else_hspd = else_hspd + lengthdir_x(obj_dataHolder.weapon[5, 2], aim+180);
			else_vspd = else_vspd + lengthdir_y(obj_dataHolder.weapon[5, 2], aim+180);
		}
		#endregion 
		
	}


	#endregion

	
	#region //THROW THE CHEESE
	
	if((equiped != "none") and (throw))
	{
		equiped = "none";
		
		var weapon = instance_nearest(x, y, obj_gun);
		weapon.direction = aim;
		weapon.state = "thrown";
		ammo = 0;
		
	}
		
	
	#endregion
	
	#region //Pickup weapons
	if(equiped == "none")
	{
		var weapon = instance_nearest(x, y, obj_gun);
		
		if((instance_place(x, y, weapon)) and (weapon.state == "floor"))
		{
			equiped = weapon.name;
			weapon.holder = obj_player1;
			weapon.state = "hold";
			reload = 0;
		}
	}
	
	#endregion	
	
	#region //Reloading
	if((reload > 0) and (!fire))
	{
		reload = reload - 1;	
	}
	if(reload <= 0)
	{
		if(equiped == obj_dataHolder.weapon[0, 0])
		{
			ammo = obj_dataHolder.weapon[0, 1];
			reload = obj_dataHolder.weapon[0, 3]
		}
		else if(equiped == obj_dataHolder.weapon[1, 0])
		{
			ammo = obj_dataHolder.weapon[1, 1];
			reload = obj_dataHolder.weapon[1, 3]
		}
		else if(equiped == obj_dataHolder.weapon[2, 0])
		{
			ammo = obj_dataHolder.weapon[2, 1];
			reload = obj_dataHolder.weapon[2, 3]
		}
		else if(equiped == obj_dataHolder.weapon[3, 0])
		{
			ammo = obj_dataHolder.weapon[3, 1];
			reload = obj_dataHolder.weapon[3, 3]
		}
		else if(equiped == obj_dataHolder.weapon[4, 0])
		{
			ammo = obj_dataHolder.weapon[4, 1];
			reload = obj_dataHolder.weapon[4, 3]
		}
		else if(equiped == obj_dataHolder.weapon[5, 0])
		{
			ammo = obj_dataHolder.weapon[5, 1];
			reload = obj_dataHolder.weapon[5, 3]
		}
		else if(equiped == obj_dataHolder.weapon[6, 0])
		{
			ammo = obj_dataHolder.weapon[6, 1];
			reload = obj_dataHolder.weapon[6, 3]
		}
	
	}

	if(shotdelay > 0)
	{
		shotdelay = shotdelay - 1;	
	}
	
	if(claqueDelay > 0)
	{
		claqueDelay = claqueDelay -1;	
	}
	#endregion

	#region //Dying
	
	//Falling off the map
	if((x < 0) or(x > room_width) or (y < -500) or (y > room_height))
	{
		hp = -100;	
	}
	
	//death procedure
	if(hp <= 0)
	{
		
		if(obj_gameManager.state == "game lifecount")
		{
			obj_gameManager.player1hp = obj_gameManager.player1hp - 1;
			if(obj_gameManager.player1hp > 0)
			{
				obj_gameManager.player1respawn = 3*room_speed;	
			}
			else 
			{
				obj_gameManager.playersAlive = obj_gameManager.playersAlive - 1;
			}
			
		}
		else if(obj_gameManager.state == "game killcount")
		{
			obj_gameManager.player1respawn = 3*room_speed;
			if(killer != -1)
			{
				if(killer == obj_player1)
				{
					obj_gameManager.player1killcount = obj_gameManager.player1killcount + 1;
				}
				if(killer == obj_player2)
				{
					obj_gameManager.player2killcount = obj_gameManager.player2killcount + 1;
				}
				if(killer == obj_player3)
				{
					obj_gameManager.player3killcount = obj_gameManager.player3killcount + 1;
				}
				if(killer == obj_player4)
				{
					obj_gameManager.player4killcount = obj_gameManager.player4killcount + 1;
				}
			}
			
		}
		
		//Release the weapon in the case he's holding any
		if(equiped != "none")
		{
			equiped = "none";
		
			var weapon = instance_nearest(x, y, obj_gun);
			weapon.direction = aim;
			weapon.state = "thrown";
		}
		
		//Create a corpse
		var corpse = instance_create_depth(x, y, depth+1, obj_player1_corpse);
		corpse.input_vspd = input_vspd;
		corpse.else_hspd = else_hspd;
		corpse.else_vspd = else_vspd;
		corpse.grav = grav;
		corpse.hchronorekt = hchronorekt;
		corpse.vchronorekt = vchronorekt;
		corpse.chronodelay = chronodelay;
		
		//KILL SLOW MO
		if(killer != -1)
		{		
			if(instance_exists(killer))
			{
				obj_camera.slowmoStep = 0;
				global.stop = 1;
				camera_set_view_size(view_camera[0], 480, 270);
		
				if(hp == -100)
				{	
					obj_camera.target1x = killer.x;
					obj_camera.target1y = killer.y;
				}
				else
				{
					obj_camera.target1x = x;
					obj_camera.target1y = y;
				}
				obj_camera.target2x = killer.x;
				obj_camera.target2y = killer.y;

			}
			
		}
		
		
		instance_destroy();
	}
	#endregion
	
}

{
    "id": "311cbca6-4703-4f18-a6ae-46b6f5909938",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1",
    "eventList": [
        {
            "id": "b974d965-9e76-4557-ba22-aec5f2b341ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "311cbca6-4703-4f18-a6ae-46b6f5909938"
        },
        {
            "id": "73f82a1b-11d2-43fd-83dd-4b51d94c76dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "311cbca6-4703-4f18-a6ae-46b6f5909938"
        },
        {
            "id": "93822e94-6415-478f-b558-933ac5fd74bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "311cbca6-4703-4f18-a6ae-46b6f5909938"
        },
        {
            "id": "65dba821-b9f0-4847-9c0a-f7916b9d308e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "311cbca6-4703-4f18-a6ae-46b6f5909938"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c872cf8-7f26-4a6e-916a-8a39cc12ec2c",
    "visible": true
}
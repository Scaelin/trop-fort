/// @description EXPLODE
// 
if(global.stop)
{
	speed = 0;	
}
else
{
	speed = actualSpeed;
	
	//Slowing down
	if(actualSpeed > 0)
	{
		actualSpeed = actualSpeed - 1.5;
	}
	else 
	{
		instance_destroy();	
	}
	
	//Damage scaling down with the speed
	if(damage > 1)
	{
		damage = damage - 0.05;
	}
	
	
	var contact = HitSomethingElse(creator);
	
	//Contact with walls
	if(contact)
	{
		//Contact with dudes
		if(contact != 1)
		{
			contact.hp = contact.hp - damage;
			contact.killer = creator;
			
			contact.else_hspd = contact.else_hspd + lengthdir_x(actualSpeed, direction);	
			contact.else_vspd = contact.else_vspd + lengthdir_y(actualSpeed, direction);
		}
		instance_destroy();	
	}
	
	//Wind trail
	var ghost = instance_create_depth(x, y, depth+1, obj_ghost);
	ghost.image_index = image_index;
	ghost.sprite_index = sprite_index;
	ghost.direction = direction;
	ghost.image_angle = image_angle;
}
{
    "id": "bbfd7895-26fc-444c-9d28-499518e10c4e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_windgun_bullet",
    "eventList": [
        {
            "id": "77c19d9b-b8b8-4670-b57c-ac50e49daeec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbfd7895-26fc-444c-9d28-499518e10c4e"
        },
        {
            "id": "5150602f-b640-4cb2-b086-80d94086f040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbfd7895-26fc-444c-9d28-499518e10c4e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0c34d21-f2cb-4ad6-beef-752b3ad0a236",
    "visible": true
}
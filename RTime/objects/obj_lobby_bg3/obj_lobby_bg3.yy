{
    "id": "6a1d4dbf-b405-4176-a7fe-a384cee97352",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lobby_bg3",
    "eventList": [
        {
            "id": "e1664c2a-895b-4c3e-87af-012a2d12bf9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a1d4dbf-b405-4176-a7fe-a384cee97352"
        },
        {
            "id": "270a4af0-ebd4-4967-af85-a415bc61cbc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6a1d4dbf-b405-4176-a7fe-a384cee97352"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
    "visible": true
}
{
    "id": "808620bd-67ea-4a13-889c-e92b7ed26ee5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flamethrower_bullet",
    "eventList": [
        {
            "id": "57ef5048-038a-4aa5-8c6d-ab414839efb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "808620bd-67ea-4a13-889c-e92b7ed26ee5"
        },
        {
            "id": "c16ea21e-07ab-44a0-b84f-875740bf065a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "808620bd-67ea-4a13-889c-e92b7ed26ee5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef42507e-9886-4996-825b-2d943000bbeb",
    "visible": true
}
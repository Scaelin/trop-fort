/// @description 
// 
if(global.stop)
{
	image_speed = 0;
}
else
{
	image_speed = 1;

	var contact = HitSomethingElse(creator);
	
	//Contact with walls
	if(contact)
	{
		//Contact with dudes
		if(contact != 1)
		{
			contact.hp = contact.hp - damage;
			contact.killer = creator;
		}
	}
	
	
	
	if(image_index > image_number -1)
	{
		var smoke = instance_create_depth(x, y, depth, obj_flamethrower_smoke);
		smoke.direction = direction;
		smoke.image_angle = image_angle;
		
		
		instance_destroy();
	}
}
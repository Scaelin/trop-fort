/// @description Create the fireworks

//
if(timer == 0)
{
	//Launch a firework
	if(mode == "random")
	{
		var i = 0;
		while(i < fireworkAmount)
		{
			var firework = instance_create_depth(x, y, depth, fireworks[irandom_range(0, 4)]);
			firework.direction = irandom_range(minAngle, maxAngle);
			firework.fireworkPower = fireworkPower;
			firework.fireworkDelay = fireworkDelay;
			
			i++;
		}
	}
	else if(mode == "selection")
	{
		var i = 0;
		while(i < fireworkAmount)
		{
			var select = instance_create_depth(x, y, depth, fireworks[selection]);
			select.direction = irandom_range(minAngle, maxAngle);
			select.fireworkPower = fireworkPower;
			select.fireworkDelay = fireworkDelay;
			
			i++;
		}
	}
	
	timer = frequency;
}
else 
{	
	timer = timer - 1;
}

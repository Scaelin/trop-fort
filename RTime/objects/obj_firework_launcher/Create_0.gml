/// @description Initialize the configuration

//Depth : Make it so that the fireworks are always on top and visible
depth = -250;

//Defines the min and max angle for shooting the fireworks
minAngle = 70;
maxAngle = 110;

//Defines the power of the shot :  a more powerful shot will go further
fireworkPower = 15;

//Defines how long it takes before the firework explodes 
fireworkDelay = 1*room_speed;

//Frequency of fireworks
frequency = 2*room_speed;
timer = frequency;


//List of fireworks (differ mostly in colors)
fireworks[0] = obj_firework_white;
fireworks[1] = obj_firework_blue;
fireworks[2] = obj_firework_red;
fireworks[3] = obj_firework_green;
fireworks[4] = obj_firework_yellow;

//Number of simultaneous fireworks launched
fireworkAmount = 3;

//Whether the fireworks are randomly selected or not
mode = "random"
selection = 0;
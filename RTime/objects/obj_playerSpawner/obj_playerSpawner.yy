{
    "id": "c94fa35d-6953-4efd-8fd1-568482fd2620",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerSpawner",
    "eventList": [
        {
            "id": "26235d48-0622-4c2c-992d-6c465cc27b5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c94fa35d-6953-4efd-8fd1-568482fd2620"
        },
        {
            "id": "388a70f7-a945-4d72-9fde-02dc35b409f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c94fa35d-6953-4efd-8fd1-568482fd2620"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
    "visible": true
}
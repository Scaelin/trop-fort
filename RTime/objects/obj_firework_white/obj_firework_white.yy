{
    "id": "99f0cef3-96ca-4211-b632-2a6c303ed21e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_white",
    "eventList": [
        {
            "id": "0589b7a5-6564-4569-aae7-b0af9cc22d46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99f0cef3-96ca-4211-b632-2a6c303ed21e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a5eb646c-32f6-4eca-bc1c-2418eb406024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
    "visible": true
}
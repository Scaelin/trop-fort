/// @description 
// 
event_inherited();

if((global.stop == 0) or (creator == -1))
{
	#region //	Detonation
	
	//Ticking part
	if(fireworkDelay > 0)
	{
		fireworkDelay = fireworkDelay - 1;	
	}
	else
	{
		var i = 0;
		while(i < pellets)
		{
			var pellet = instance_create_depth(x, y, depth, obj_firework_pellet_white);
			pellet.direction = irandom_range(minAngle, maxAngle);
			pellet.creator = creator;
			
			i++;
		}
		
		instance_destroy();
	}
	
	#endregion
}
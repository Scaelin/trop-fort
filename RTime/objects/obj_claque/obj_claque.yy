{
    "id": "b18b9cf2-d074-4e30-8566-4444be2ef307",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_claque",
    "eventList": [
        {
            "id": "f0b739ff-166f-4a83-a922-175aeca149c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b18b9cf2-d074-4e30-8566-4444be2ef307"
        },
        {
            "id": "a1ca3436-7613-4e3a-aeb8-8dac5af22f65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b18b9cf2-d074-4e30-8566-4444be2ef307"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
    "visible": true
}
/// @description 
// 

if(global.stop)
{
	speed = 0;
	image_speed = 0;
}
else
{
	speed = 10;
	image_speed = 10;

	if(instance_exists(creator))
	{
		var collide = HitSomethingElse(creator);
		if((collide != 0) and (firstFrame))
		{
			//Checking for enemy hit
			if(collide != 1)
			{
				collide.else_hspd = collide.else_hspd + lengthdir_x(selfKnockback, creator.aim);	
				collide.else_vspd = collide.else_vspd + lengthdir_y(selfKnockback, creator.aim);
				collide.hp = collide.hp - damage;
				collide.killer = creator;
			}
			else
			{
				//Applying self knockback against walls
				creator.else_hspd = creator.else_hspd + lengthdir_x(selfKnockback, creator.aim+180);	
				creator.else_vspd = creator.else_vspd + lengthdir_y(selfKnockback, creator.aim+180);	
			}
		
	
		}
	
	}
	
	//Self destruction at the end of the animation cycle
	if(image_index > image_number -1)
	{
		instance_destroy();
	}
	
	firstFrame = 0;
}
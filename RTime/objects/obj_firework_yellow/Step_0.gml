/// @description 
// 
event_inherited();

if((global.stop == 0) or (creator == -1))
{
	#region //	Detonation
	
	//Ticking part
	if(fireworkDelay > 0)
	{
		fireworkDelay = fireworkDelay - 1;	
	}
	else
	{
		var i = 0;
		while(i < pellets)
		{
			var pellet = instance_create_depth(x, y, depth, obj_firework_pellet_yellow);
			pellet.direction = irandom_range(minAngle, maxAngle);
			pellet.creator = creator;
			
			i++;
		}
		
		instance_destroy();
	}
	
	#endregion
	
	#region //Hurting other players
	
	var contact = HitSomethingElse(creator);
	//Contact with walls
	if(contact)
	{
		//Contact with dudes
		if(contact != 1)
		{
			contact.killer = creator;
			contact.hp = contact.hp - damage;
			contact.else_hspd = contact.else_vspd + lengthdir_x(damage*3, direction);
			contact.else_vspd = contact.else_vspd + lengthdir_y(damage*3, direction);
		}
		
		
		//Explode anyway
		var i = 0;
		while(i < pellets)
		{
			var pellet = instance_create_depth(x, y, depth, obj_firework_pellet_yellow);
			pellet.direction = irandom_range(minAngle, maxAngle);
			pellet.creator = creator;
			
			i++;
		}
		
		instance_destroy();	
	}
	
	#endregion
}
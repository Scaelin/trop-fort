{
    "id": "4074293d-3af3-4e56-85f0-1a7f2880b247",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_yellow",
    "eventList": [
        {
            "id": "cddc3e5c-26ca-4fea-93b5-6ead01693e7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4074293d-3af3-4e56-85f0-1a7f2880b247"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a5eb646c-32f6-4eca-bc1c-2418eb406024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
    "visible": true
}
{
    "id": "d0de8461-aa16-4d07-a026-78f087b5eccb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lobby_bg2",
    "eventList": [
        {
            "id": "b3e3fedc-bb1e-4ee5-bfdb-3d400d886c7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0de8461-aa16-4d07-a026-78f087b5eccb"
        },
        {
            "id": "f8eb750e-6c0a-4d7f-88a4-297b934a23b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d0de8461-aa16-4d07-a026-78f087b5eccb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
    "visible": true
}
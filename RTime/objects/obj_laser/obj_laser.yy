{
    "id": "f9c4b12e-43e5-402d-bc59-2d3fa0a7ec4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laser",
    "eventList": [
        {
            "id": "f67f94db-64ac-4c1c-95d9-34e135fffc97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9c4b12e-43e5-402d-bc59-2d3fa0a7ec4a"
        },
        {
            "id": "b09136a6-b0de-494e-9a07-ef7e984643be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9c4b12e-43e5-402d-bc59-2d3fa0a7ec4a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33e73682-7fd1-447a-9cc0-25524efe8cdc",
    "visible": true
}
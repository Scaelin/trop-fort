/// @description Shit's going on
if(global.stop == 0)
{

	#region //Movement

	input_vspd = 0;


		//Gravity
	if((!instance_place(x, y+1, obj_solid)))
	{
		if(grav <= 18)		//Meilleur jeu
		{
			grav = grav+0.6; //A GARDER ABSOLUMENT /!\
		}
		input_vspd = input_vspd + grav;
	}
	
	#region Chronoclaque
	if(chronodelay > 0)
	{
		chronodelay = chronodelay - 1;
	}
	else
	{
		else_hspd = else_hspd + hchronorekt;	
		else_vspd = else_vspd + vchronorekt;	
		
		AddScreenshake((abs(hchronorekt) + abs(vchronorekt))/2);
		
		hchronorekt = 0;
		vchronorekt = 0;
		
		
	}
	
	#endregion

	if(instance_place(x, y+1, obj_solid))
	{
		grav = 0;	
	}

		//Friction
	if(else_hspd > 0)
	{
		if(instance_place(x+1, y, obj_solid))
		{
			else_hspd = else_hspd - 1.5;
		}
		else
		{
			else_hspd = else_hspd - 0.5;
		}
	}
	if(else_hspd < 0)
	{
		if(instance_place(x-1, y, obj_solid))
		{
			else_hspd = else_hspd + 2.5;
		}
		else
		{
			else_hspd = else_hspd + 0.5;
		}
	}
	if(else_vspd < 0)
	{
		if(instance_place(x, y-1, obj_solid))
		{
			else_vspd = else_vspd + 1.5;
		}
		else
		{
			else_vspd = else_vspd + 0.5;
		}
	}
	if(else_vspd > 0)
	{
		if(instance_place(x, y+1, obj_solid))
		{
			else_vspd = else_vspd - 1.5;
		}
		else
		{
			else_vspd = else_vspd - 0.5;
		}
	}
		//Annulation
	if(else_hspd < 0.5) and (else_hspd > -0.5)
	{
		else_hspd = 0;	
	}
	if(else_vspd < 0.5) and (else_vspd > -0.5)
	{
		else_vspd = 0;	
	}
	

		//Final speed
	vspd = input_vspd + else_vspd;
	hspd = else_hspd;

		//Colisions

		//Horizontal
	if(instance_place(x+hspd, y, obj_solid))
	{
		if(hspd > 0)
		{
			while(!instance_place(x+1, y, obj_solid))
			{
				x = x+1;
			}
		}
		else
		{
			while(!instance_place(x-1, y, obj_solid))
			{
				x = x-1;
			}
		}
		hspd = 0;
	}

		//Vertical
	if(instance_place(x, y+vspd, obj_solid))
	{
		if(vspd > 0)
		{
			while(!instance_place(x, y+1, obj_solid))
			{
				y = y+1;
			}
		}
		else
		{
			while(!instance_place(x, y-1, obj_solid))
			{
				y = y-1;
			}
		}
		vspd = 0;
	}
	

	x = x + hspd;
	y = y + vspd;

	#endregion
	
	#region //Decay
	if(image_index > image_number -1)
	{
		instance_destroy();	
	}
	#endregion

	image_speed = 15;
}
else
{
	image_speed = 0;	
}

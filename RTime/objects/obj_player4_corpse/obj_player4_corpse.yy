{
    "id": "54d40acd-5f10-4869-90d9-4c2c47fd3e90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player4_corpse",
    "eventList": [
        {
            "id": "4bee5cc5-d8d8-4863-b797-c52a34724137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54d40acd-5f10-4869-90d9-4c2c47fd3e90"
        },
        {
            "id": "402f2a0c-4bf5-427e-b4b0-6b89dcf6af7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54d40acd-5f10-4869-90d9-4c2c47fd3e90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
    "visible": true
}
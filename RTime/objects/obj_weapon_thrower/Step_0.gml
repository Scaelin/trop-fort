/// @description 
// 

if((spawnTimer == 0) and (obj_gameManager.enabledWeapons != 0))
{
	var index = irandom_range(0, obj_dataHolder.weaponNumber-1);
	var gun = instance_create_depth(x, y, 99, obj_dataHolder.weapon[index, 6]);
	gun.state = "menu";
	gun.direction = irandom_range(0, 359);
	gun.fast = 5;
		
	spawnTimer = spawnCooldown;
}
	
if(spawnTimer > 0)
{
	spawnTimer = spawnTimer - 1;
}

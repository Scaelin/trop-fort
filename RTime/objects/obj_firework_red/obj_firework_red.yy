{
    "id": "347c6d75-d317-4c10-98f5-ee042315d343",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_red",
    "eventList": [
        {
            "id": "3eb21a7b-43ee-492d-b9f7-ed56bfecbcbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "347c6d75-d317-4c10-98f5-ee042315d343"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a5eb646c-32f6-4eca-bc1c-2418eb406024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
    "visible": true
}
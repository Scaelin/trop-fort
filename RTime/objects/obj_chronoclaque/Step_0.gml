/// @description 
// 

if(global.stop)
{
	speed = 0;
	image_speed = 0;
}
else
{
	speed = 20;
	image_speed = 10;


	var collide = HitSomethingElse(creator);
	if((collide != 0) and (firstFrame))
	{
		//Checking for enemy hit
		if(collide != 1)
		{
			collide.hchronorekt = collide.hchronorekt + lengthdir_x(selfKnockback, creator.aim);	
			collide.vchronorekt = collide.vchronorekt + lengthdir_y(selfKnockback, creator.aim);
			collide.hp = collide.hp - damage;
			collide.killer = creator;
			collide.chronodelay = 1*room_speed;
			
		}
		else
		{
			if(instance_exists(creator))
			{
				
				//Applying self knockback
				creator.else_hspd = creator.else_hspd + lengthdir_x(selfKnockback, creator.aim+180);	
				creator.else_vspd = creator.else_vspd + lengthdir_y(selfKnockback, creator.aim+180);	
			}
		}
		
	}
	
	//Self destruction at the end of the animation cycle
	if(image_index > image_number -1)
	{
		instance_destroy();
	}
	
	firstFrame = 0;
}
{
    "id": "116ea9d2-34e0-4ae0-9839-e97672680304",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chronoclaque",
    "eventList": [
        {
            "id": "912a5abc-1377-402c-a925-5d83ca3b5b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "116ea9d2-34e0-4ae0-9839-e97672680304"
        },
        {
            "id": "7ea65fe2-72d2-4a98-a921-965aa83c77bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "116ea9d2-34e0-4ae0-9839-e97672680304"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
    "visible": true
}
{
    "id": "8cf289ac-29c7-4057-9f02-07c178b89761",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework_pellet_red",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c72ef54f-06c2-4a68-b48a-c874d5c00d0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9b2aeeb9-579b-4af2-9f40-70583015e29c",
    "visible": true
}
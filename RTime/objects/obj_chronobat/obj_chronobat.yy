{
    "id": "d3bb7492-0524-4741-aa10-8a95ec0bc741",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chronobat",
    "eventList": [
        {
            "id": "a6d2b402-7f79-4a16-ad1d-3d3bfcdbffeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3bb7492-0524-4741-aa10-8a95ec0bc741"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0112978e-c94b-440a-8e64-575c55e85cf0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
    "visible": true
}
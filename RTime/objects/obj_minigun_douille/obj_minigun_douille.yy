{
    "id": "e37d92b2-adcd-42ea-84ef-3c302a1b144f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_minigun_douille",
    "eventList": [
        {
            "id": "a8fb4b1b-70f7-4b9c-aa4d-9bbba301fe0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e37d92b2-adcd-42ea-84ef-3c302a1b144f"
        },
        {
            "id": "70e1c413-209a-4fa6-8cf7-6917989ff5ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e37d92b2-adcd-42ea-84ef-3c302a1b144f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02074761-624e-48ff-b478-581d35fa8401",
    "visible": true
}
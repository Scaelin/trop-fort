{
    "id": "3d255c07-520c-4032-a841-13d18a806d66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sniper_douille",
    "eventList": [
        {
            "id": "e2c4e3ed-f907-40f3-904b-8f3ad0fb34f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d255c07-520c-4032-a841-13d18a806d66"
        },
        {
            "id": "f14170e9-91c3-4ae3-91bb-07b85c8f8b2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d255c07-520c-4032-a841-13d18a806d66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ffe7bf33-1a6f-426f-b59e-40f0552dfe22",
    "visible": true
}
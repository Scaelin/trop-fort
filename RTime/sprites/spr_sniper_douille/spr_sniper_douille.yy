{
    "id": "ffe7bf33-1a6f-426f-b59e-40f0552dfe22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sniper_douille",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6730579b-2aff-4fb3-b547-32c2add6f727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffe7bf33-1a6f-426f-b59e-40f0552dfe22",
            "compositeImage": {
                "id": "79cdf2c3-249c-4264-a892-36221eab79d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6730579b-2aff-4fb3-b547-32c2add6f727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5648c329-92b4-4013-976b-53145814abb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6730579b-2aff-4fb3-b547-32c2add6f727",
                    "LayerId": "efa3ab10-46d5-4506-970b-c72896487731"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "efa3ab10-46d5-4506-970b-c72896487731",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffe7bf33-1a6f-426f-b59e-40f0552dfe22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 2
}
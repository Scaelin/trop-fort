{
    "id": "c9a70cf3-87db-4091-983e-28fc4acd5bf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player4_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2851df6-aa09-4be2-afd5-7d8ed644c5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9a70cf3-87db-4091-983e-28fc4acd5bf1",
            "compositeImage": {
                "id": "1b6d52bd-a56f-4474-9351-f46046fdc034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2851df6-aa09-4be2-afd5-7d8ed644c5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c999e1-1f81-4f6b-81e5-ce0caa976e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2851df6-aa09-4be2-afd5-7d8ed644c5be",
                    "LayerId": "a29307d4-5934-4f57-97a2-d705789df53a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "a29307d4-5934-4f57-97a2-d705789df53a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9a70cf3-87db-4091-983e-28fc4acd5bf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
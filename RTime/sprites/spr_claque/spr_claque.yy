{
    "id": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_claque",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 28,
    "bbox_right": 127,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7143863c-83d1-48f9-b869-14ee8cade1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
            "compositeImage": {
                "id": "271c2df8-4689-4b4c-a4c2-dd3ace6ea7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7143863c-83d1-48f9-b869-14ee8cade1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2aa8e49-6760-4f49-b204-11281a6df7c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7143863c-83d1-48f9-b869-14ee8cade1c0",
                    "LayerId": "aae6c025-506e-4732-a7b9-962da000a9c9"
                }
            ]
        },
        {
            "id": "d662f195-301b-45e7-bb55-30bac0e6502c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
            "compositeImage": {
                "id": "d6a10412-daec-480d-973c-62f4105a22cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d662f195-301b-45e7-bb55-30bac0e6502c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25b2d51-e19b-4daf-8507-5ffa3f1bbea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d662f195-301b-45e7-bb55-30bac0e6502c",
                    "LayerId": "aae6c025-506e-4732-a7b9-962da000a9c9"
                }
            ]
        },
        {
            "id": "d88c40c3-2bb0-4263-b6b3-d54b86dd31d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
            "compositeImage": {
                "id": "41c3b984-3e5a-4a3c-a54a-caf58db5a58c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88c40c3-2bb0-4263-b6b3-d54b86dd31d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f826be6a-067f-49e4-b4f4-17b6e68b475f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88c40c3-2bb0-4263-b6b3-d54b86dd31d9",
                    "LayerId": "aae6c025-506e-4732-a7b9-962da000a9c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aae6c025-506e-4732-a7b9-962da000a9c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec22cfec-4481-4993-9d59-134ddbbd41e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 14,
    "yorig": 31
}
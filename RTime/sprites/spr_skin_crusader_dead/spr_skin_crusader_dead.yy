{
    "id": "9dd4ac4e-b2ef-4f5b-bd56-d5e56468c43f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_crusader_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb15f7d1-8b4e-4078-ad7d-b86d144a70d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dd4ac4e-b2ef-4f5b-bd56-d5e56468c43f",
            "compositeImage": {
                "id": "7e9c9567-216a-4d62-966c-f497b0dc4873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb15f7d1-8b4e-4078-ad7d-b86d144a70d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3497633c-6924-4883-9a1d-c3e1e9af55bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb15f7d1-8b4e-4078-ad7d-b86d144a70d7",
                    "LayerId": "65fa7148-034d-4a31-a33f-2da2a64cd724"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "65fa7148-034d-4a31-a33f-2da2a64cd724",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dd4ac4e-b2ef-4f5b-bd56-d5e56468c43f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
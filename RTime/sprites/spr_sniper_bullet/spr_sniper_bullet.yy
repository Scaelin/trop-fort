{
    "id": "b9b0cbcf-1364-4151-9b56-b5a2b692621b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sniper_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "423a8e27-3ea1-483c-ad9a-f9950b10c4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9b0cbcf-1364-4151-9b56-b5a2b692621b",
            "compositeImage": {
                "id": "cd8a1d5f-f1a0-439a-8ade-792b7609fb25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423a8e27-3ea1-483c-ad9a-f9950b10c4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd00be22-a143-43c5-bdbe-879db73e2e10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423a8e27-3ea1-483c-ad9a-f9950b10c4d3",
                    "LayerId": "cfaae3ca-7e7f-49cc-8b56-3693db74859a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "cfaae3ca-7e7f-49cc-8b56-3693db74859a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9b0cbcf-1364-4151-9b56-b5a2b692621b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 4
}
{
    "id": "35b7a258-ca6b-4c52-8fef-0c37db6f335b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d20a0f7-0f1e-44af-9d92-725b20c69a7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35b7a258-ca6b-4c52-8fef-0c37db6f335b",
            "compositeImage": {
                "id": "948e6410-3611-4434-ac3b-88d8e795fd21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d20a0f7-0f1e-44af-9d92-725b20c69a7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb14deb9-5a9f-4202-9027-de2b63cc2bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d20a0f7-0f1e-44af-9d92-725b20c69a7e",
                    "LayerId": "1bf6e5e7-a4a1-445c-a0a7-60108dae194f"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "1bf6e5e7-a4a1-445c-a0a7-60108dae194f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35b7a258-ca6b-4c52-8fef-0c37db6f335b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
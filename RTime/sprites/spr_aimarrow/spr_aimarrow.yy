{
    "id": "3b608a70-b2f7-46e0-b545-93917cd30fa7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_aimarrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18b5d562-97a9-4d15-ac8f-f9d254ac9df6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b608a70-b2f7-46e0-b545-93917cd30fa7",
            "compositeImage": {
                "id": "0c7bb06d-a52d-47be-8d6d-d8e4735a0297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18b5d562-97a9-4d15-ac8f-f9d254ac9df6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d0e324-e1bd-45d1-a1e8-ebfe3a68c244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18b5d562-97a9-4d15-ac8f-f9d254ac9df6",
                    "LayerId": "95124425-46c4-4563-8ea7-94758b94bcd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "95124425-46c4-4563-8ea7-94758b94bcd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b608a70-b2f7-46e0-b545-93917cd30fa7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
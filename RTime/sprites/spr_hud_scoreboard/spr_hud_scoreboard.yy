{
    "id": "86a6f2b4-b786-457f-9490-4b0804fd09c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_scoreboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 494,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d39782f-8432-4fd4-9830-d05932262cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86a6f2b4-b786-457f-9490-4b0804fd09c7",
            "compositeImage": {
                "id": "b825da65-8033-4f60-9a5f-c90092085efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d39782f-8432-4fd4-9830-d05932262cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "012a09ad-6dcb-4e20-8a59-423ad9a2f779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d39782f-8432-4fd4-9830-d05932262cf5",
                    "LayerId": "9372b1f3-537d-426f-a674-05b6d9c224ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "9372b1f3-537d-426f-a674-05b6d9c224ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86a6f2b4-b786-457f-9490-4b0804fd09c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 495,
    "xorig": 247,
    "yorig": 41
}
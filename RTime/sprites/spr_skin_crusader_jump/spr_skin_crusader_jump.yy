{
    "id": "008bcc4d-0bd6-4faf-b46c-2cf040f7be38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_crusader_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43cb0a45-6d5b-4f25-9e09-a12c0792c6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008bcc4d-0bd6-4faf-b46c-2cf040f7be38",
            "compositeImage": {
                "id": "5704b458-6755-41f2-a31e-2718a6082729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43cb0a45-6d5b-4f25-9e09-a12c0792c6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2a3871-d190-4031-abd3-cf5180997884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43cb0a45-6d5b-4f25-9e09-a12c0792c6e8",
                    "LayerId": "52088d8f-300e-4134-9103-96f5c26c8e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "52088d8f-300e-4134-9103-96f5c26c8e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "008bcc4d-0bd6-4faf-b46c-2cf040f7be38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
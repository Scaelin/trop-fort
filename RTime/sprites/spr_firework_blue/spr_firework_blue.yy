{
    "id": "af20a461-29f7-4214-879a-b390d5a666f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6de6a9d0-c121-47c8-b7ec-a80094b8a975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "4ba363c9-bced-4220-a24d-bba7b2996529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de6a9d0-c121-47c8-b7ec-a80094b8a975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa818f9d-1ff5-48b8-a899-49ac75332add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de6a9d0-c121-47c8-b7ec-a80094b8a975",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        },
        {
            "id": "073ac666-024e-466f-b2ab-f8287d644ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "818cf36d-2e39-482e-adff-8d6432fe1506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "073ac666-024e-466f-b2ab-f8287d644ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c76a60-1abb-4236-a82a-ce680d889470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "073ac666-024e-466f-b2ab-f8287d644ccc",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        },
        {
            "id": "8bdcd676-7f45-4b52-a2f7-e016c750dd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "1c21f221-f4c4-435b-acea-1c60aeb57c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bdcd676-7f45-4b52-a2f7-e016c750dd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0425a0-eaa5-4538-a4f7-9e91a458dbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bdcd676-7f45-4b52-a2f7-e016c750dd5a",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        },
        {
            "id": "a50fa1e8-2c1b-46c1-a933-9d77665ffa01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "d4d90ec1-9d7d-46a3-9793-95a4b4dae4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a50fa1e8-2c1b-46c1-a933-9d77665ffa01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe360f8-af16-42e5-9886-4f7d634a4ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a50fa1e8-2c1b-46c1-a933-9d77665ffa01",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        },
        {
            "id": "a43f22a2-7056-4fc4-af20-4e44df30c1ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "7c605320-27d6-49d7-879f-788bc9539570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a43f22a2-7056-4fc4-af20-4e44df30c1ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4452211e-86f8-4718-b805-232b3debf2e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a43f22a2-7056-4fc4-af20-4e44df30c1ce",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        },
        {
            "id": "b22d47aa-a38a-4716-a69b-0550444461bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "compositeImage": {
                "id": "9e272abf-15ba-43b0-92ca-ea90b5a46967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22d47aa-a38a-4716-a69b-0550444461bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e04164ad-cf77-4860-a877-ead801241f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22d47aa-a38a-4716-a69b-0550444461bd",
                    "LayerId": "e615bab7-0269-4dfd-8ea9-8937743046f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "e615bab7-0269-4dfd-8ea9-8937743046f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af20a461-29f7-4214-879a-b390d5a666f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
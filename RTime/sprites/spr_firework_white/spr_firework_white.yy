{
    "id": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c85b789b-f35a-4a09-874a-7549b5087c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "024275e8-0e74-4f76-ab5d-cca439353595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85b789b-f35a-4a09-874a-7549b5087c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9505c274-32a6-4717-9ea8-627b1ddb690b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85b789b-f35a-4a09-874a-7549b5087c51",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        },
        {
            "id": "e29113be-2e69-4ceb-a83f-d107d5a60b01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "86ac6861-e8bb-4182-8c63-cc04f6dcadec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e29113be-2e69-4ceb-a83f-d107d5a60b01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61bc245-7c2d-4785-b475-73a952757a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e29113be-2e69-4ceb-a83f-d107d5a60b01",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        },
        {
            "id": "0ea59afb-d778-4970-add3-ca8c48e4c2ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "c0bcedb5-81df-4685-95d8-26056a9d740c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea59afb-d778-4970-add3-ca8c48e4c2ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49392d55-afac-4835-b082-2aed855aa0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea59afb-d778-4970-add3-ca8c48e4c2ec",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        },
        {
            "id": "af964d15-259d-4d3f-9168-6f5a1aee25e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "356120e2-9fdf-48d6-87bf-fc37de451d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af964d15-259d-4d3f-9168-6f5a1aee25e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039a5f9a-3465-4f06-a1ed-6b3a29424770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af964d15-259d-4d3f-9168-6f5a1aee25e8",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        },
        {
            "id": "393b59a3-f8b1-47c5-a720-07f7821fd4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "31c2fb0e-bc88-490c-b73d-a3a47a9906f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393b59a3-f8b1-47c5-a720-07f7821fd4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe93f59-784c-4c0e-89f0-459244b9d59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393b59a3-f8b1-47c5-a720-07f7821fd4e6",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        },
        {
            "id": "89559dca-3841-48cb-b2ae-6e64976637cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "compositeImage": {
                "id": "838e7502-e834-4210-abff-cb4300fd1504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89559dca-3841-48cb-b2ae-6e64976637cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50dba838-2e44-4ec7-aed9-4e523048029b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89559dca-3841-48cb-b2ae-6e64976637cf",
                    "LayerId": "faa1d616-ae18-4ca1-8c8e-22f446d798d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "faa1d616-ae18-4ca1-8c8e-22f446d798d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "534994cb-3f5a-4c12-a4a6-ab7bb2fd94a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
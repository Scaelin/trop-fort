{
    "id": "de6c0b50-379b-4986-87fc-86bf694e9882",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chronoclaque",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 29,
    "bbox_right": 251,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9301ac42-5a68-4378-8e6d-c3cfe3e6a0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
            "compositeImage": {
                "id": "dabf0abd-f170-472b-b37b-42eca40943ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9301ac42-5a68-4378-8e6d-c3cfe3e6a0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d94afe-c150-4468-b5aa-a68d17bdf9d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9301ac42-5a68-4378-8e6d-c3cfe3e6a0f6",
                    "LayerId": "1f64014c-33c7-461d-a175-0914f6b8a096"
                }
            ]
        },
        {
            "id": "6949a78c-f3f7-41e7-9f7d-5483927ef3a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
            "compositeImage": {
                "id": "9e944ac2-36a9-4f1b-b126-8f5223e7853a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6949a78c-f3f7-41e7-9f7d-5483927ef3a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17790a81-e16a-4e53-910f-904b2474b49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6949a78c-f3f7-41e7-9f7d-5483927ef3a5",
                    "LayerId": "1f64014c-33c7-461d-a175-0914f6b8a096"
                }
            ]
        },
        {
            "id": "8467d24d-3754-4261-a86c-0ad11fe61c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
            "compositeImage": {
                "id": "95138a94-7f41-40dc-8bf6-5da8f28cc658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8467d24d-3754-4261-a86c-0ad11fe61c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103be673-5df7-4d28-b432-2657da1dee81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8467d24d-3754-4261-a86c-0ad11fe61c0f",
                    "LayerId": "1f64014c-33c7-461d-a175-0914f6b8a096"
                }
            ]
        },
        {
            "id": "d13a94e7-f892-4970-ab67-f980f5cd13f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
            "compositeImage": {
                "id": "dfc9a6f3-4596-4b42-a8ec-9c03c2b44b6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13a94e7-f892-4970-ab67-f980f5cd13f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38218fa9-c128-4345-aa06-625db152752b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13a94e7-f892-4970-ab67-f980f5cd13f5",
                    "LayerId": "1f64014c-33c7-461d-a175-0914f6b8a096"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1f64014c-33c7-461d-a175-0914f6b8a096",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de6c0b50-379b-4986-87fc-86bf694e9882",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 34,
    "yorig": 63
}
{
    "id": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f3ec060-c0f0-44fe-baa9-bce2b6d2b66c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "4fe59d76-0588-4b8f-ba1c-80337718c5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3ec060-c0f0-44fe-baa9-bce2b6d2b66c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4075c0-e0e4-4444-aad0-e74d84262910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3ec060-c0f0-44fe-baa9-bce2b6d2b66c",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        },
        {
            "id": "53f335d8-36b7-4d00-8000-14cd3c147629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "c872937c-71a5-4cc0-8a6d-8d38dcfde2ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f335d8-36b7-4d00-8000-14cd3c147629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f69b6f-226d-412f-afc4-756d4ba7ff7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f335d8-36b7-4d00-8000-14cd3c147629",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        },
        {
            "id": "2ba5a0ea-6460-46c3-a808-edd2745e069b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "e12242c2-4a7b-432c-926e-18906f9db81f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba5a0ea-6460-46c3-a808-edd2745e069b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a885d3-9ec6-4218-ae7f-29b18e1ee39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba5a0ea-6460-46c3-a808-edd2745e069b",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        },
        {
            "id": "5d0aa8e2-9db6-46fd-b9dd-29161c484f1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "148d5019-b99c-4ede-9246-f5d46e350555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0aa8e2-9db6-46fd-b9dd-29161c484f1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b681b189-dfbe-405c-ae9b-a88f5bfc4a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0aa8e2-9db6-46fd-b9dd-29161c484f1b",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        },
        {
            "id": "8778ec03-9ff3-4dde-a90d-fb62f2e5c9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "1c52a3a0-85db-4550-baf9-c379612a6e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8778ec03-9ff3-4dde-a90d-fb62f2e5c9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dc1ffc-7d52-48c5-be77-1b9af368f06a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8778ec03-9ff3-4dde-a90d-fb62f2e5c9ea",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        },
        {
            "id": "940ed0cc-b90b-4517-8c32-10cfca890b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "compositeImage": {
                "id": "c11c2dd9-4c0f-48a0-b46f-2c5a2cd6c9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940ed0cc-b90b-4517-8c32-10cfca890b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a749ad-78d9-4c77-bb6f-7558ce9d2fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940ed0cc-b90b-4517-8c32-10cfca890b52",
                    "LayerId": "fbd78dd2-cb07-4ba5-a554-86a50d83587d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "fbd78dd2-cb07-4ba5-a554-86a50d83587d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc236e0d-92bb-4c67-9304-29ef66360bd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
{
    "id": "afda2db8-649d-4161-964d-ced115f65a3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shotgun_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c99115-aa8d-4acd-97b9-1ea81aef5f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afda2db8-649d-4161-964d-ced115f65a3e",
            "compositeImage": {
                "id": "e38429d7-3f61-4b91-bb2d-1677f18e8af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c99115-aa8d-4acd-97b9-1ea81aef5f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60fa66af-70cd-4de1-b0d9-4e20a32b49d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c99115-aa8d-4acd-97b9-1ea81aef5f24",
                    "LayerId": "a61df631-3237-4e33-b5d8-fef8f01d6f0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a61df631-3237-4e33-b5d8-fef8f01d6f0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afda2db8-649d-4161-964d-ced115f65a3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
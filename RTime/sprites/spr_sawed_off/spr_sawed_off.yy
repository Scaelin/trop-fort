{
    "id": "7d6e9459-8eae-422a-98e2-9b6491bf59a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sawed_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 11,
    "bbox_right": 47,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83d02253-d26b-4c1a-990e-83bdcd0044fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d6e9459-8eae-422a-98e2-9b6491bf59a1",
            "compositeImage": {
                "id": "d3ee8423-1427-4d57-ae5a-cc34b0798ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d02253-d26b-4c1a-990e-83bdcd0044fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354f7abe-e7a5-4d2e-9829-f96de558debd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d02253-d26b-4c1a-990e-83bdcd0044fd",
                    "LayerId": "8a7aa5a4-a8ab-41fe-9185-bfe9a525860f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a7aa5a4-a8ab-41fe-9185-bfe9a525860f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d6e9459-8eae-422a-98e2-9b6491bf59a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}
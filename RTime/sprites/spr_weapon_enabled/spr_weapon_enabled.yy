{
    "id": "1c650a63-b257-41da-923e-bf12e000e387",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_enabled",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae67b3ad-0513-4614-bea2-ea93a7f4fbfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c650a63-b257-41da-923e-bf12e000e387",
            "compositeImage": {
                "id": "7416948c-85f7-49c2-9e8d-1be619240f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae67b3ad-0513-4614-bea2-ea93a7f4fbfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e5f9be-98cb-489e-b3a3-0059f95836bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae67b3ad-0513-4614-bea2-ea93a7f4fbfa",
                    "LayerId": "ab56f138-42c6-4397-b20f-0d8a1ff2f965"
                }
            ]
        }
    ],
    "gridX": 20,
    "gridY": 20,
    "height": 80,
    "layers": [
        {
            "id": "ab56f138-42c6-4397-b20f-0d8a1ff2f965",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c650a63-b257-41da-923e-bf12e000e387",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 40
}
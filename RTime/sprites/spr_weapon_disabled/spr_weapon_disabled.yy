{
    "id": "2f2adcfc-1faa-44a7-b96d-ce4b0dca609f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_disabled",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7177b335-b466-4b27-a6ff-24a59441a7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f2adcfc-1faa-44a7-b96d-ce4b0dca609f",
            "compositeImage": {
                "id": "fbf3d527-c85b-42db-b335-fded4e98beb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7177b335-b466-4b27-a6ff-24a59441a7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a4957e7-acc1-437a-94f3-c99dfd870451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7177b335-b466-4b27-a6ff-24a59441a7dc",
                    "LayerId": "a684e19b-dccf-424e-8ae4-eead08d8ed84"
                }
            ]
        }
    ],
    "gridX": 20,
    "gridY": 20,
    "height": 80,
    "layers": [
        {
            "id": "a684e19b-dccf-424e-8ae4-eead08d8ed84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f2adcfc-1faa-44a7-b96d-ce4b0dca609f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 40
}
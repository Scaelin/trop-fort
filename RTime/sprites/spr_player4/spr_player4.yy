{
    "id": "e2ed0ee7-a280-4a05-95ab-76b34c0626ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33b34622-8c35-491b-b824-f2333d9e99e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2ed0ee7-a280-4a05-95ab-76b34c0626ab",
            "compositeImage": {
                "id": "ee8e8a46-48e0-46a8-8f79-823476642f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b34622-8c35-491b-b824-f2333d9e99e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50407a4-7a4b-47d8-8db6-7107ab3ebcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b34622-8c35-491b-b824-f2333d9e99e0",
                    "LayerId": "707f3880-43ac-49d7-86f8-5e28d5d40b57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "707f3880-43ac-49d7-86f8-5e28d5d40b57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2ed0ee7-a280-4a05-95ab-76b34c0626ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
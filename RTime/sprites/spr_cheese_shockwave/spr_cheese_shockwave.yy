{
    "id": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cheese_shockwave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 18,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6f97c2b-7301-443c-8b26-e1655455411b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "ddefe739-a1f7-4155-8a36-3541515c7e59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f97c2b-7301-443c-8b26-e1655455411b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a15e86-4826-4f2e-897e-c785be2c21e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f97c2b-7301-443c-8b26-e1655455411b",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "82f66793-e665-4529-8e84-e8aa2e340d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "7c624324-fc7c-4216-9e7d-6997a72633f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f66793-e665-4529-8e84-e8aa2e340d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7492fb6-cd2a-4066-b4ee-759ef7512347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f66793-e665-4529-8e84-e8aa2e340d83",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "e0d9fbb7-97d1-43e1-a542-91bd6ce0abdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "ea438138-f585-437a-ab5a-742388c621d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d9fbb7-97d1-43e1-a542-91bd6ce0abdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db38fe2-35a2-4a4b-981f-ce52d545edb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d9fbb7-97d1-43e1-a542-91bd6ce0abdd",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "9767af8b-81df-4e27-a77f-6e80f82ad821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "5103741b-a954-4aed-8b10-600046d8850e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9767af8b-81df-4e27-a77f-6e80f82ad821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f4a9cb-fcc0-4a88-b4ce-6b0b6d3d2916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9767af8b-81df-4e27-a77f-6e80f82ad821",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "41808b89-d8b4-435f-857e-967e5a76d023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "ddfd1dfb-9555-4bd8-a9d8-470612a48eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41808b89-d8b4-435f-857e-967e5a76d023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b293c0c-deae-4291-b89c-37bbed2a9736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41808b89-d8b4-435f-857e-967e5a76d023",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "65b8a269-ef36-45f6-a3f0-54bf136ffd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "c13dfebe-8f61-4ab1-80bb-7bf32f811732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b8a269-ef36-45f6-a3f0-54bf136ffd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183e7c9e-9457-4f73-8905-dc61b612ecb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b8a269-ef36-45f6-a3f0-54bf136ffd6f",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "a3dd13e0-b936-4c7f-b810-30d78010b139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "09055007-8c8d-4235-873c-2b917f6ec61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3dd13e0-b936-4c7f-b810-30d78010b139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf5778d-345d-4f8f-a36e-5405cc91e57b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3dd13e0-b936-4c7f-b810-30d78010b139",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        },
        {
            "id": "813bbd3e-be43-4e43-8e4d-ed4d5ecffbaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "compositeImage": {
                "id": "986352b9-684a-48ef-980c-2c9719ebe110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "813bbd3e-be43-4e43-8e4d-ed4d5ecffbaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f193fbe0-0be5-400a-a650-a79d13d509f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "813bbd3e-be43-4e43-8e4d-ed4d5ecffbaf",
                    "LayerId": "519fb29f-3834-4e00-b8a4-4d97c35df464"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 16,
    "height": 192,
    "layers": [
        {
            "id": "519fb29f-3834-4e00-b8a4-4d97c35df464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f62ffec0-d80a-4470-a5f2-acdb8195f945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 96
}
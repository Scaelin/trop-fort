{
    "id": "9d9bf7d6-3549-4b91-88c6-29955be61863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_noammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad1c24c7-bf8e-4692-9c8f-3dc91025da39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9bf7d6-3549-4b91-88c6-29955be61863",
            "compositeImage": {
                "id": "bb8ac236-e810-4c0d-9149-12e7db44b550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1c24c7-bf8e-4692-9c8f-3dc91025da39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb1d6f2-61d3-4b34-8fd3-a95bb07d1287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1c24c7-bf8e-4692-9c8f-3dc91025da39",
                    "LayerId": "dc3f2274-78ac-474e-afd7-758726c0e258"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "dc3f2274-78ac-474e-afd7-758726c0e258",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9bf7d6-3549-4b91-88c6-29955be61863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}
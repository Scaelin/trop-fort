{
    "id": "59fccc20-f106-466f-be2a-818f67731c62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d26f5eec-f2f1-40f0-bfe5-81e4dc70997c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fccc20-f106-466f-be2a-818f67731c62",
            "compositeImage": {
                "id": "7251628e-0152-4758-85d6-12edf9a848fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d26f5eec-f2f1-40f0-bfe5-81e4dc70997c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f48df5e-21f8-4491-abd2-8d7ff24ac6e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d26f5eec-f2f1-40f0-bfe5-81e4dc70997c",
                    "LayerId": "3e0d9ed8-27ff-4a5a-8b30-57eb3d1da168"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "3e0d9ed8-27ff-4a5a-8b30-57eb3d1da168",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59fccc20-f106-466f-be2a-818f67731c62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
{
    "id": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player2_corpse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b164570-a422-461c-af31-1094a6d103fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "39673ce1-fd20-484e-b04c-3a4a8d526e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b164570-a422-461c-af31-1094a6d103fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d9d6dc-c7d5-49a0-833a-c6e518f3784b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b164570-a422-461c-af31-1094a6d103fb",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "962b86ca-c4c4-4d0f-a4bf-ba16d66ff394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "552f3592-dc3a-452f-bd89-8801a5aed134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962b86ca-c4c4-4d0f-a4bf-ba16d66ff394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caff5e20-4586-4e12-a5b7-8417238afc68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962b86ca-c4c4-4d0f-a4bf-ba16d66ff394",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "b4e34a33-2f19-4408-b441-ba4eab4bdea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "54842441-e55a-4358-8e30-1ab21c2e80a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e34a33-2f19-4408-b441-ba4eab4bdea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d76acdc-ccd1-4803-b52b-3b873f7d24e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e34a33-2f19-4408-b441-ba4eab4bdea5",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "b513f46c-678f-4b8f-8027-e45484a2582a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "ce360b34-503b-4e38-b893-15298b35a948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b513f46c-678f-4b8f-8027-e45484a2582a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57ae7c3-3223-42a8-911e-1cf89d833900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b513f46c-678f-4b8f-8027-e45484a2582a",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "8ff08dec-89b1-456a-a96e-e4aeab14055c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "b50cbd71-2b36-43b3-bfc7-83f85f9fc1a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff08dec-89b1-456a-a96e-e4aeab14055c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89615dfb-f42e-4727-b060-63e977ecf3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff08dec-89b1-456a-a96e-e4aeab14055c",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "6aebad36-c23a-43b0-a49c-694bf9de597a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "6d29f6ba-94ea-4333-805a-92d7d14e0dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aebad36-c23a-43b0-a49c-694bf9de597a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cc10f4d-fb1d-44aa-8a76-8a99131d5d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aebad36-c23a-43b0-a49c-694bf9de597a",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        },
        {
            "id": "4ba24e34-92a5-4d1c-9787-8af4ee9c3441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "compositeImage": {
                "id": "a50db166-94c2-4418-ae2b-87c6a4287ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba24e34-92a5-4d1c-9787-8af4ee9c3441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d995c62-c7e7-4f0b-a1d6-a4f7bc556f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba24e34-92a5-4d1c-9787-8af4ee9c3441",
                    "LayerId": "2fc73a28-f74e-4425-bc33-63cdfffb776c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "2fc73a28-f74e-4425-bc33-63cdfffb776c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efa90224-e6bd-4d69-b4a9-7cb4c684c178",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
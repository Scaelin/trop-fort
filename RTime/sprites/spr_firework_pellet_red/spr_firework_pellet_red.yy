{
    "id": "9b2aeeb9-579b-4af2-9f40-70583015e29c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_pellet_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b011ef79-c247-4612-a9b4-23d72e686392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2aeeb9-579b-4af2-9f40-70583015e29c",
            "compositeImage": {
                "id": "5943a544-1657-4198-b5e7-0b4d21597206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b011ef79-c247-4612-a9b4-23d72e686392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f31faeb-beff-4d94-9cdd-35356b4e9a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b011ef79-c247-4612-a9b4-23d72e686392",
                    "LayerId": "0ec15ca6-d7d4-4468-a228-a208eee5f8eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "0ec15ca6-d7d4-4468-a228-a208eee5f8eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b2aeeb9-579b-4af2-9f40-70583015e29c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}
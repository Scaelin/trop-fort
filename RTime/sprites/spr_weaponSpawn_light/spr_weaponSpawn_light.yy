{
    "id": "3a413714-c085-4858-bc0f-5c708f61a3f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weaponSpawn_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d61007b5-24e0-4f86-8f1c-a69a7b51bfae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "e5f6fde0-e10a-4569-ab36-c2ebc1eae341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61007b5-24e0-4f86-8f1c-a69a7b51bfae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a24bd9-6c06-4838-bc67-0d3f3ec73214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61007b5-24e0-4f86-8f1c-a69a7b51bfae",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "e160d25c-704f-4dd6-a9ab-83184b2f66c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "698364eb-67dd-4439-a885-0047b5998fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e160d25c-704f-4dd6-a9ab-83184b2f66c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4637f42d-f2a5-4596-b8dc-21ebd71cf2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e160d25c-704f-4dd6-a9ab-83184b2f66c6",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "363440be-c4c7-4b10-aa5a-4c7b8b0256db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "6b66e668-b3c4-4f91-8a37-3c7cc908c7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363440be-c4c7-4b10-aa5a-4c7b8b0256db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1851da6-9afe-48fc-8449-f4378ec4a483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363440be-c4c7-4b10-aa5a-4c7b8b0256db",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "4b5843a5-589a-4564-8f98-8bef0e22bd64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "4d39796c-ce14-4379-8e21-48b3e981c4e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5843a5-589a-4564-8f98-8bef0e22bd64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68bdac4c-4fa6-45e1-a865-c1b595906712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5843a5-589a-4564-8f98-8bef0e22bd64",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "42107cda-cff4-4415-9e7c-7c89c29ab7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "4f4016bb-0133-4020-babf-4591daf2b6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42107cda-cff4-4415-9e7c-7c89c29ab7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f03a67-1c2e-4787-9b23-5ec5dc80121d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42107cda-cff4-4415-9e7c-7c89c29ab7d4",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "f2ebfd4e-2511-4eef-a585-9e95eac5c9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "f83bb1e0-3dc9-47a4-9048-bdcd0f13a8bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ebfd4e-2511-4eef-a585-9e95eac5c9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9aa37b-ce00-44bf-bd7d-373b3c63e691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ebfd4e-2511-4eef-a585-9e95eac5c9c4",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "f6bff5f2-dbb9-476a-8848-68489b4122f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "190a4d2e-390a-4391-ab81-8a19af3f4309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6bff5f2-dbb9-476a-8848-68489b4122f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4f0c691-0456-44e5-85b0-3cc84e1f45a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6bff5f2-dbb9-476a-8848-68489b4122f2",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "e5629766-d655-4180-a775-8750f6fb2893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "71fb827d-44fc-4112-9a45-2770ff76ed02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5629766-d655-4180-a775-8750f6fb2893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1387c90a-656a-4424-ab3f-f525369efa6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5629766-d655-4180-a775-8750f6fb2893",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "0760286d-a917-4e92-b0ae-6bb55107ad50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "fcb02e05-2b62-4a58-af12-8a9d80d7bd50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0760286d-a917-4e92-b0ae-6bb55107ad50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636acc1b-2e4f-4b42-878c-fc4c8b040f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0760286d-a917-4e92-b0ae-6bb55107ad50",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "ff3565c0-f6b3-4839-884b-8af9b20c1ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "5b980afa-47f1-44dc-b740-59cb11dbc47d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3565c0-f6b3-4839-884b-8af9b20c1ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b722ae6-9236-4227-a5fe-6be51321b321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3565c0-f6b3-4839-884b-8af9b20c1ee5",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "06e777c5-bd51-4268-b46b-a0f875e008ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "1d07fc59-6d74-4465-83e8-131ea7eac08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e777c5-bd51-4268-b46b-a0f875e008ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4220f736-3e5c-43d0-b6d1-4a708f3f9492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e777c5-bd51-4268-b46b-a0f875e008ac",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        },
        {
            "id": "b00ccb70-3d11-4f0c-9f65-09e8f1e73640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "compositeImage": {
                "id": "0ed153c1-8a4a-4f2b-8ad1-e8b486eb2d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00ccb70-3d11-4f0c-9f65-09e8f1e73640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56f00d3a-933a-4768-8eef-42a0fa6d2676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00ccb70-3d11-4f0c-9f65-09e8f1e73640",
                    "LayerId": "e2f3836b-7fb9-43c0-9669-057971ee3e2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e2f3836b-7fb9-43c0-9669-057971ee3e2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a413714-c085-4858-bc0f-5c708f61a3f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
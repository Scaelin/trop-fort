{
    "id": "8875c0e2-654b-455e-b70d-26a6531520d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player1_kill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26e4ee5e-80bf-4678-8615-08fcef057f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8875c0e2-654b-455e-b70d-26a6531520d5",
            "compositeImage": {
                "id": "93a13d99-9793-4fce-a1b7-e068bf40ca1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e4ee5e-80bf-4678-8615-08fcef057f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d986777d-a6e8-4b02-9989-80037f030476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e4ee5e-80bf-4678-8615-08fcef057f7b",
                    "LayerId": "61817bda-25bf-422d-aacc-1f721d6706f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "61817bda-25bf-422d-aacc-1f721d6706f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8875c0e2-654b-455e-b70d-26a6531520d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
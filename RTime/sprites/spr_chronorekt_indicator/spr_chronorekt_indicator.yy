{
    "id": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chronorekt_indicator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eafe4e8f-8845-406b-9466-e7b01027b4b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
            "compositeImage": {
                "id": "320301e7-6a6b-490e-9c84-59a7137c71bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eafe4e8f-8845-406b-9466-e7b01027b4b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f239b2-dc88-4b9d-b245-b49e20884cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eafe4e8f-8845-406b-9466-e7b01027b4b8",
                    "LayerId": "7bc43998-8990-4b86-a6df-0863ceb7b1db"
                }
            ]
        },
        {
            "id": "082af5c4-abec-4df7-928e-2ea4b05a9d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
            "compositeImage": {
                "id": "86407aa3-8e27-47f9-86d5-b6e19eb3672f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082af5c4-abec-4df7-928e-2ea4b05a9d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd081a1-bb7f-4cbd-b547-bd32bef02d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082af5c4-abec-4df7-928e-2ea4b05a9d25",
                    "LayerId": "7bc43998-8990-4b86-a6df-0863ceb7b1db"
                }
            ]
        },
        {
            "id": "65985ae7-0a7b-462e-aec2-c4f4eaf5316e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
            "compositeImage": {
                "id": "89b0033e-e247-49cc-9bda-b5a8a698302e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65985ae7-0a7b-462e-aec2-c4f4eaf5316e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd1b1a5-1c63-4425-84ec-4fb609cd3a0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65985ae7-0a7b-462e-aec2-c4f4eaf5316e",
                    "LayerId": "7bc43998-8990-4b86-a6df-0863ceb7b1db"
                }
            ]
        },
        {
            "id": "f8664294-4f10-4ce9-a684-1766154a2f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
            "compositeImage": {
                "id": "9bbb11f5-d59a-4137-9e4f-7c539b25157f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8664294-4f10-4ce9-a684-1766154a2f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7064d3c5-1adb-4753-8048-80727c8059c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8664294-4f10-4ce9-a684-1766154a2f8a",
                    "LayerId": "7bc43998-8990-4b86-a6df-0863ceb7b1db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7bc43998-8990-4b86-a6df-0863ceb7b1db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "426efdd1-62e3-4b89-9d06-beb3d0b09ef9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
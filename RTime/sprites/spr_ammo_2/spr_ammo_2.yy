{
    "id": "c78e8715-f47b-458e-9bae-e55325272e3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4db5a6c0-e6b7-4747-8a1f-6f77dca6e9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c78e8715-f47b-458e-9bae-e55325272e3e",
            "compositeImage": {
                "id": "c72316b5-1756-4b94-b1f6-da98b48745bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db5a6c0-e6b7-4747-8a1f-6f77dca6e9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35547294-0dda-455b-a2a9-0faba3f05e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db5a6c0-e6b7-4747-8a1f-6f77dca6e9f7",
                    "LayerId": "2c37ddd1-7fef-45c4-82fc-99d4d2ab7eec"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "2c37ddd1-7fef-45c4-82fc-99d4d2ab7eec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c78e8715-f47b-458e-9bae-e55325272e3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
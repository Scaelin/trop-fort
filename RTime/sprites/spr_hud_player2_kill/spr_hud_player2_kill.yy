{
    "id": "1a70f0fd-8e6b-4d61-90e6-b5fd71fd8c9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player2_kill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "064204cd-f0fe-4acd-9bd9-00d08a44e282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a70f0fd-8e6b-4d61-90e6-b5fd71fd8c9c",
            "compositeImage": {
                "id": "8a37055d-6c81-4d9e-b768-55278d54bd49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "064204cd-f0fe-4acd-9bd9-00d08a44e282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62e8849-5c8f-496d-b3b2-7ab5c22d2139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "064204cd-f0fe-4acd-9bd9-00d08a44e282",
                    "LayerId": "3f8aebde-4900-4e1a-8406-536352f5769e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "3f8aebde-4900-4e1a-8406-536352f5769e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a70f0fd-8e6b-4d61-90e6-b5fd71fd8c9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
{
    "id": "f5e5b122-40b8-44f5-bd59-8ea4278ec866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sniper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 75,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "160f71cb-ec68-469d-ad90-e72ee4986b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e5b122-40b8-44f5-bd59-8ea4278ec866",
            "compositeImage": {
                "id": "3b135a93-4108-4d4a-98f7-e8e2bc0d90fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "160f71cb-ec68-469d-ad90-e72ee4986b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e3b9ca-718d-4e72-a257-9a0843a35179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "160f71cb-ec68-469d-ad90-e72ee4986b1e",
                    "LayerId": "ee496586-8b7c-492e-b613-27b7fc3441b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ee496586-8b7c-492e-b613-27b7fc3441b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5e5b122-40b8-44f5-bd59-8ea4278ec866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 23,
    "yorig": 12
}
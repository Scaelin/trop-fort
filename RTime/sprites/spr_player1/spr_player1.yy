{
    "id": "8c872cf8-7f26-4a6e-916a-8a39cc12ec2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12014a60-8b55-44fc-9ae7-b6579a7da34d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c872cf8-7f26-4a6e-916a-8a39cc12ec2c",
            "compositeImage": {
                "id": "4020ca9f-8f6e-4a48-837e-129c984bbf6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12014a60-8b55-44fc-9ae7-b6579a7da34d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30e8f4e-1501-45c6-9843-1c9ff198a308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12014a60-8b55-44fc-9ae7-b6579a7da34d",
                    "LayerId": "e89b88c4-2a11-4082-9549-cce18dcc1ce1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "e89b88c4-2a11-4082-9549-cce18dcc1ce1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c872cf8-7f26-4a6e-916a-8a39cc12ec2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
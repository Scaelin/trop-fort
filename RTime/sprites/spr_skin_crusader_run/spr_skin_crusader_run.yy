{
    "id": "de01b5fa-f782-428e-ae68-efe396f1f834",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_crusader_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d2e4355-4f2f-42bb-8158-1d3161722a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de01b5fa-f782-428e-ae68-efe396f1f834",
            "compositeImage": {
                "id": "06933d77-a8d2-4ed9-b04d-ecdc9aed892c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2e4355-4f2f-42bb-8158-1d3161722a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7cfe71-c30d-48a7-8e68-a0a9dee9afda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2e4355-4f2f-42bb-8158-1d3161722a3c",
                    "LayerId": "4c967c01-ef5a-438a-ac75-0b5f4ff53aec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "4c967c01-ef5a-438a-ac75-0b5f4ff53aec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de01b5fa-f782-428e-ae68-efe396f1f834",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
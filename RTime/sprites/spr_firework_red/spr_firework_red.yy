{
    "id": "8fef6ea3-b2ac-499b-b067-b590254360bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f950809-bc8d-4f6e-b481-7af670d4058b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "d389826f-c5b4-4a8c-92bc-21c7f63e202b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f950809-bc8d-4f6e-b481-7af670d4058b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f41998-32c5-4804-b650-55c455af86dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f950809-bc8d-4f6e-b481-7af670d4058b",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        },
        {
            "id": "c9a2e9b9-5be4-4aa3-8482-b978c7a88fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "8584e1bd-de3a-42c3-ae8a-6e2a6e7fa7db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9a2e9b9-5be4-4aa3-8482-b978c7a88fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342fa9a4-54d1-4d3c-a292-554ab4518dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9a2e9b9-5be4-4aa3-8482-b978c7a88fcb",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        },
        {
            "id": "bd6ae6ff-1960-4023-82c8-4216514d2112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "5c3fcdff-5764-461d-b456-b35bb30d5ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd6ae6ff-1960-4023-82c8-4216514d2112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a0c7ac-fc0b-4b3a-beb9-52b00df7641b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd6ae6ff-1960-4023-82c8-4216514d2112",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        },
        {
            "id": "44faf589-4de0-4493-b091-7bafa340a049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "5b9f3436-757e-4213-8a81-08828a30ee2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44faf589-4de0-4493-b091-7bafa340a049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3cadc1-8f11-4670-ae7e-2826a2766b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44faf589-4de0-4493-b091-7bafa340a049",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        },
        {
            "id": "a6a03034-5f12-48cb-b13e-349aa4b0bb1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "a4d1e182-ec9e-41ae-9792-3ec98511ec48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a03034-5f12-48cb-b13e-349aa4b0bb1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f96e55-08ab-4008-9127-a26c61a63d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a03034-5f12-48cb-b13e-349aa4b0bb1b",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        },
        {
            "id": "af8a7491-0c7e-4098-9774-cc58f5a9c978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "compositeImage": {
                "id": "1dc7e1a9-3248-4ae7-8f9c-d17f0d7f657c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8a7491-0c7e-4098-9774-cc58f5a9c978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7202389b-7004-4565-9c0b-777611599580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8a7491-0c7e-4098-9774-cc58f5a9c978",
                    "LayerId": "038d88dd-bc43-40c7-a8c2-f6efd25a8629"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "038d88dd-bc43-40c7-a8c2-f6efd25a8629",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fef6ea3-b2ac-499b-b067-b590254360bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4279438975,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
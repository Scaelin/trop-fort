{
    "id": "68b563ae-e9b2-4cf1-9c76-2d965143ef94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player3_kill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2cfc67e-4918-4ad0-870b-cbbf88f79f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b563ae-e9b2-4cf1-9c76-2d965143ef94",
            "compositeImage": {
                "id": "4831e385-4be5-4595-9b1d-ee74d88c96bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cfc67e-4918-4ad0-870b-cbbf88f79f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33335be1-3ec8-4538-862d-e38033b7d29f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cfc67e-4918-4ad0-870b-cbbf88f79f7b",
                    "LayerId": "a56d1471-0784-46ab-9a93-2db3a83b8400"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "a56d1471-0784-46ab-9a93-2db3a83b8400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b563ae-e9b2-4cf1-9c76-2d965143ef94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
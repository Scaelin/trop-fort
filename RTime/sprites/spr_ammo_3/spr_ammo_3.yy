{
    "id": "70b6d80f-f9c0-41fa-af3d-bd9b72f06746",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e8b61fd-e601-415e-a9d2-d9d9041f8efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b6d80f-f9c0-41fa-af3d-bd9b72f06746",
            "compositeImage": {
                "id": "e00eaf63-8a03-4ed8-9d4e-cec75db6252b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8b61fd-e601-415e-a9d2-d9d9041f8efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e44f925-23c7-47ae-91fc-35af2ac70cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8b61fd-e601-415e-a9d2-d9d9041f8efb",
                    "LayerId": "f1b6bfac-66b1-47a5-b0e7-a18b26b685dd"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "f1b6bfac-66b1-47a5-b0e7-a18b26b685dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70b6d80f-f9c0-41fa-af3d-bd9b72f06746",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
{
    "id": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flamethrower_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 102,
    "bbox_right": 166,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52eeb1b7-d420-427d-8504-765357207e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "36b6cb49-f823-4ca2-9226-7e5eec38023a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52eeb1b7-d420-427d-8504-765357207e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb08051-0bdc-4668-9e5d-79d7348351aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52eeb1b7-d420-427d-8504-765357207e1e",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        },
        {
            "id": "251b6752-9365-4813-a07a-0b9716a21cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "06a5518c-c239-4892-9faf-f9e88db4f6a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "251b6752-9365-4813-a07a-0b9716a21cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25a85fe6-401c-4089-a975-af13bf893a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "251b6752-9365-4813-a07a-0b9716a21cd7",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        },
        {
            "id": "3c910459-c0cd-4677-ba6c-3e233737d41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "3a7874b1-220e-4ddc-a57f-fed9420e6733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c910459-c0cd-4677-ba6c-3e233737d41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3df3a8-58d3-4910-8e49-77130e410891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c910459-c0cd-4677-ba6c-3e233737d41c",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        },
        {
            "id": "d6105839-28e7-4089-90b9-a37696d83e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "b846c4b6-0795-4a7a-b8fc-376ed17fa0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6105839-28e7-4089-90b9-a37696d83e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73d238e-4f15-4fc0-a589-dc0f374400c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6105839-28e7-4089-90b9-a37696d83e2f",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        },
        {
            "id": "8a99cc26-27e6-4936-9dca-224054c422e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "45923af6-9d41-469d-9d20-322fd617d709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a99cc26-27e6-4936-9dca-224054c422e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83779de8-4f47-497e-bef8-4aedb9880328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a99cc26-27e6-4936-9dca-224054c422e6",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        },
        {
            "id": "28a8248d-ea84-4ed2-8f86-6c7ae0ead3f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "compositeImage": {
                "id": "1c48e4e0-66e2-41da-b973-4553ac213605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a8248d-ea84-4ed2-8f86-6c7ae0ead3f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f2070d-388c-40de-a573-dd8fa0935b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a8248d-ea84-4ed2-8f86-6c7ae0ead3f4",
                    "LayerId": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4a4d3318-3df6-4453-8e27-b7f00aa0dad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37f2527a-9caf-45b0-b08a-ac48a8d40256",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 40,
    "yorig": 48
}
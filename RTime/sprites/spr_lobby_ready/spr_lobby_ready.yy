{
    "id": "4fe95876-2ac4-4c6c-8163-2e0366d8ec9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lobby_ready",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 0,
    "bbox_right": 727,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f231b16-ad15-4a8e-baf6-4b19bf75f1c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fe95876-2ac4-4c6c-8163-2e0366d8ec9b",
            "compositeImage": {
                "id": "bff170da-47af-40c4-aca9-ad6ae450ae40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f231b16-ad15-4a8e-baf6-4b19bf75f1c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68bd34bd-02e6-4746-a63f-3fa7ed36239f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f231b16-ad15-4a8e-baf6-4b19bf75f1c2",
                    "LayerId": "3d7b97db-7d03-44c0-b31e-08a284f1ca35"
                }
            ]
        }
    ],
    "gridX": 20,
    "gridY": 20,
    "height": 280,
    "layers": [
        {
            "id": "3d7b97db-7d03-44c0-b31e-08a284f1ca35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fe95876-2ac4-4c6c-8163-2e0366d8ec9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 728,
    "xorig": 364,
    "yorig": 140
}
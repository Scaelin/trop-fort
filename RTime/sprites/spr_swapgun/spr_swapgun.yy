{
    "id": "2fbf3f46-6798-4853-9c32-402ea7a29457",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swapgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 4,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2274af18-b334-4bb8-ab47-22e01186f413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
            "compositeImage": {
                "id": "e5b7cab0-4b6b-4eec-9535-f72f2c8a56fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2274af18-b334-4bb8-ab47-22e01186f413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d7db7a-50ea-4d32-ba1b-ea4f5791fc9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2274af18-b334-4bb8-ab47-22e01186f413",
                    "LayerId": "0b09a3e4-1e12-4335-93f5-9a9a0d6f1242"
                }
            ]
        },
        {
            "id": "2b19181c-092b-4e41-869c-9cf48a290d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
            "compositeImage": {
                "id": "a6ee93cf-8dde-4677-b2bc-0878bb6886f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b19181c-092b-4e41-869c-9cf48a290d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d63bff-6db9-4e47-8d2c-acb8f4f0cc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b19181c-092b-4e41-869c-9cf48a290d31",
                    "LayerId": "0b09a3e4-1e12-4335-93f5-9a9a0d6f1242"
                }
            ]
        },
        {
            "id": "87fb9119-7473-44c8-8ec9-ca5abc9899e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
            "compositeImage": {
                "id": "0dd5f9db-81bc-4f23-931e-37192301ca1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87fb9119-7473-44c8-8ec9-ca5abc9899e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "765d3941-bb84-4a65-842f-4f281bcec313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87fb9119-7473-44c8-8ec9-ca5abc9899e6",
                    "LayerId": "0b09a3e4-1e12-4335-93f5-9a9a0d6f1242"
                }
            ]
        },
        {
            "id": "a5ab22f8-9fbc-4ee8-89eb-0ac7905330bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
            "compositeImage": {
                "id": "0175d4e8-2f78-47cd-a6f6-01723f7294c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ab22f8-9fbc-4ee8-89eb-0ac7905330bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baea104e-33de-4437-8f9e-3be4f22ca1e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ab22f8-9fbc-4ee8-89eb-0ac7905330bc",
                    "LayerId": "0b09a3e4-1e12-4335-93f5-9a9a0d6f1242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b09a3e4-1e12-4335-93f5-9a9a0d6f1242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fbf3f46-6798-4853-9c32-402ea7a29457",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 19,
    "yorig": 15
}
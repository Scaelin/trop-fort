{
    "id": "940ad553-d9a7-4a31-abf2-7594704168fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9816ff7d-d7fd-4ce8-984c-e9c7f60b6b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940ad553-d9a7-4a31-abf2-7594704168fe",
            "compositeImage": {
                "id": "d31f268a-9875-4e05-ab81-2d53b7b5e89a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9816ff7d-d7fd-4ce8-984c-e9c7f60b6b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37aa6b7-b486-4702-bae3-06b8a3034f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9816ff7d-d7fd-4ce8-984c-e9c7f60b6b4d",
                    "LayerId": "04872c42-bd21-40cf-b2fa-fbf2ea4d0d15"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "04872c42-bd21-40cf-b2fa-fbf2ea4d0d15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "940ad553-d9a7-4a31-abf2-7594704168fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
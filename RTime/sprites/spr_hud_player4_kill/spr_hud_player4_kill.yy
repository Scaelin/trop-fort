{
    "id": "0ef3fe7d-9183-49bd-897a-6e4a7f3f1536",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player4_kill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e16d301-5b97-4a01-a62d-7e4a541cb6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ef3fe7d-9183-49bd-897a-6e4a7f3f1536",
            "compositeImage": {
                "id": "ea1c7bcc-7bc5-484b-a263-384d463958d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e16d301-5b97-4a01-a62d-7e4a541cb6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87383934-0829-46c3-a831-9887e2a8d3bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e16d301-5b97-4a01-a62d-7e4a541cb6ec",
                    "LayerId": "3a3db52a-e74f-4e2d-b6ca-e881fb403b2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "3a3db52a-e74f-4e2d-b6ca-e881fb403b2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ef3fe7d-9183-49bd-897a-6e4a7f3f1536",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
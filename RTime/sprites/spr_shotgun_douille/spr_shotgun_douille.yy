{
    "id": "e17da3d5-6f27-4e47-8ea2-ee715312e5e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shotgun_douille",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45acbbca-7220-4f04-b9ac-88ea5ffb3916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e17da3d5-6f27-4e47-8ea2-ee715312e5e8",
            "compositeImage": {
                "id": "48fe7ed8-ce36-4f9d-9068-89818b0d46cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45acbbca-7220-4f04-b9ac-88ea5ffb3916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1277ec58-049b-4303-977a-361aafaaa957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45acbbca-7220-4f04-b9ac-88ea5ffb3916",
                    "LayerId": "2ab02627-4b81-487f-942e-c9e2c5f99c17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "2ab02627-4b81-487f-942e-c9e2c5f99c17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e17da3d5-6f27-4e47-8ea2-ee715312e5e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}
{
    "id": "aec8d394-501f-4d74-85eb-58569c596264",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swap_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7be937c-393b-40af-b760-542253d543bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aec8d394-501f-4d74-85eb-58569c596264",
            "compositeImage": {
                "id": "eecfd60a-c148-486a-be80-e895ac71243e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7be937c-393b-40af-b760-542253d543bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae4f054-717b-4cbf-ba14-ac9c13fd407c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7be937c-393b-40af-b760-542253d543bc",
                    "LayerId": "041f6f53-8017-4cdf-bbca-49ec9a706a98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "041f6f53-8017-4cdf-bbca-49ec9a706a98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aec8d394-501f-4d74-85eb-58569c596264",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}
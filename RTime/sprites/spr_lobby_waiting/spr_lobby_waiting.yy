{
    "id": "0880c27b-c954-4104-81e6-5888eea89a12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lobby_waiting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 0,
    "bbox_right": 727,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21df86eb-691d-4e26-9f16-29246284fd88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "dff1770c-bbea-4b0f-b399-bd4ad518dd68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21df86eb-691d-4e26-9f16-29246284fd88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbe79f3-ea29-43e5-96a5-7a8beb3babcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21df86eb-691d-4e26-9f16-29246284fd88",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "11b53135-17c0-465a-9f56-3caf28af68ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "e8187106-96a6-44a8-8915-50a555c449b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11b53135-17c0-465a-9f56-3caf28af68ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd17bfaf-628b-4a3e-8ff4-feb331f3db4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11b53135-17c0-465a-9f56-3caf28af68ea",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "0aa4ad39-e37e-4a12-9131-88b7211d9eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "93cfe41c-b3d2-4353-b995-498e5cca89fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aa4ad39-e37e-4a12-9131-88b7211d9eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb0d79db-c5b7-49f1-9e83-d4f09c470533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aa4ad39-e37e-4a12-9131-88b7211d9eba",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "abe0a414-dbd7-46ab-b094-77a740f6df2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "271d9ae2-aeb4-4112-84a7-2f834e789331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe0a414-dbd7-46ab-b094-77a740f6df2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1a7d11-88ae-490b-ac77-36c2e9a67c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe0a414-dbd7-46ab-b094-77a740f6df2e",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "1a305582-eb46-455a-92ee-1e53d737b8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "c5cc8529-ac72-4aef-9045-c2bb593b5198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a305582-eb46-455a-92ee-1e53d737b8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8555cbea-2eb9-4bcc-88f0-1360b2163768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a305582-eb46-455a-92ee-1e53d737b8f7",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "40388c10-ffac-45e9-a82c-f0f32fb8f9cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "6d2e1490-fbf3-493d-9de8-82b1dd8c87d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40388c10-ffac-45e9-a82c-f0f32fb8f9cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53893d33-3e22-49e1-b19c-f9a457cf7e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40388c10-ffac-45e9-a82c-f0f32fb8f9cd",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "209ac61b-1ebd-4393-8b89-19eec886f54a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "c814eec0-f031-45c6-89c0-5d6f2e39695a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "209ac61b-1ebd-4393-8b89-19eec886f54a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b29d360-6acd-4849-9708-a42df70b1d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "209ac61b-1ebd-4393-8b89-19eec886f54a",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "6e1216a7-6783-4aa3-adde-0bd29f95f621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "5a2c4d2a-0b67-4a46-a653-e0c206c13587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e1216a7-6783-4aa3-adde-0bd29f95f621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4c7c24-3038-4678-b853-adbb18e3e2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e1216a7-6783-4aa3-adde-0bd29f95f621",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "ecb715db-c581-44fb-a93e-72d1cf13b303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "c938badf-e588-4167-a95f-6d37c4d4c118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb715db-c581-44fb-a93e-72d1cf13b303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35dc96c0-e4fa-40b2-b4de-857e3513624a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb715db-c581-44fb-a93e-72d1cf13b303",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "b38f0085-1a9e-4728-8459-010b0452e3ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "82df4447-3be4-4073-8946-28ad64242a28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38f0085-1a9e-4728-8459-010b0452e3ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eee895c-3631-462e-8505-13dc8cd18a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38f0085-1a9e-4728-8459-010b0452e3ed",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "5ea66517-bc8c-4ff8-8549-e130459994a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "35bc8d67-1cac-4053-8bc3-8b2612fef4ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ea66517-bc8c-4ff8-8549-e130459994a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e97e1625-cc43-4f93-96f7-576a3fd8ee4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ea66517-bc8c-4ff8-8549-e130459994a7",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "2757c79f-e8f1-410e-b5d8-cc5667272b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "86d3003d-4b6f-478f-b756-8f43bc174107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2757c79f-e8f1-410e-b5d8-cc5667272b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d2d02f-6f6c-4d3e-8487-23245f982daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2757c79f-e8f1-410e-b5d8-cc5667272b09",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        },
        {
            "id": "4a83b0af-9952-4569-8748-0c65b7a69a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "compositeImage": {
                "id": "7ab7c6ed-1c8f-4ef9-8608-2d26b0e3a60f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a83b0af-9952-4569-8748-0c65b7a69a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a4102e-0d46-4531-aa9b-01ff9e6d1420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a83b0af-9952-4569-8748-0c65b7a69a38",
                    "LayerId": "8c9bd3b6-ef68-4785-bf54-df868c35da2b"
                }
            ]
        }
    ],
    "gridX": 20,
    "gridY": 20,
    "height": 280,
    "layers": [
        {
            "id": "8c9bd3b6-ef68-4785-bf54-df868c35da2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0880c27b-c954-4104-81e6-5888eea89a12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 728,
    "xorig": 364,
    "yorig": 140
}
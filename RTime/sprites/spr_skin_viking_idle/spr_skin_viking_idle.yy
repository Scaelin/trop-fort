{
    "id": "dc5967b4-c0dd-45a3-9a10-2c737f4239dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_viking_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4bd0b51-07c9-4223-aa20-f43efc4de13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc5967b4-c0dd-45a3-9a10-2c737f4239dd",
            "compositeImage": {
                "id": "5d46bdd2-e165-4bba-8f6c-3fe05c1bb51b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4bd0b51-07c9-4223-aa20-f43efc4de13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd3e195-fb17-4d80-b499-cbf71d07498f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4bd0b51-07c9-4223-aa20-f43efc4de13d",
                    "LayerId": "8e304023-bd54-4f54-8f3b-89994c56a788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "8e304023-bd54-4f54-8f3b-89994c56a788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc5967b4-c0dd-45a3-9a10-2c737f4239dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
{
    "id": "1c328166-3cb5-4f76-b1d6-43e42549386e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player4_corpse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcbb506e-7621-48ea-b1c2-75e257913a55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "b51d1ffa-275b-4541-86c4-70082b317c62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcbb506e-7621-48ea-b1c2-75e257913a55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11285eba-8008-4a62-982b-658d7e31a426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbb506e-7621-48ea-b1c2-75e257913a55",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "7c3ac4c1-d621-4a13-b8a4-3453df0fa369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "79ddad75-7c26-4a91-bf39-2e53d977e28d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c3ac4c1-d621-4a13-b8a4-3453df0fa369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f030b18d-530a-4a25-af1e-049f8a13ce1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c3ac4c1-d621-4a13-b8a4-3453df0fa369",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "7517bda5-cc64-4917-8c73-7f1337e343d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "ebd243bb-5a0d-48c4-9d9e-c8fdd09c6b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7517bda5-cc64-4917-8c73-7f1337e343d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bba6e5-7bfc-4baa-a7b3-a6350505bb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7517bda5-cc64-4917-8c73-7f1337e343d2",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "3ae36dcd-ab25-4a22-afe2-b11e52e560c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "191214f8-b53c-4e7f-88eb-7db67632dbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae36dcd-ab25-4a22-afe2-b11e52e560c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be235250-d830-4c01-8724-2eac695aab2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae36dcd-ab25-4a22-afe2-b11e52e560c9",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "24a653f4-0f7d-4f70-82fa-ed91d495facd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "e056fc67-7266-4c08-bfe5-4fcafa57576c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a653f4-0f7d-4f70-82fa-ed91d495facd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96af3af-66eb-4856-8869-1be647980819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a653f4-0f7d-4f70-82fa-ed91d495facd",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "ab481bf8-0708-420d-a250-554528074ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "ad9c7fc8-3e96-4188-a00a-374e6969fed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab481bf8-0708-420d-a250-554528074ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb267073-78c6-4006-b6ae-a92ac4d5db62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab481bf8-0708-420d-a250-554528074ccc",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        },
        {
            "id": "be839d11-89d1-4777-b2e2-b7097c608745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "compositeImage": {
                "id": "9fd5cb18-755e-4a6a-bc5e-b47f7f40cbff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be839d11-89d1-4777-b2e2-b7097c608745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fc7e19-641a-475d-af51-477226adc540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be839d11-89d1-4777-b2e2-b7097c608745",
                    "LayerId": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "ac79e50c-27e4-4373-bf06-ac6e25b53d4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c328166-3cb5-4f76-b1d6-43e42549386e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
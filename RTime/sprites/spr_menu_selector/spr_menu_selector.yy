{
    "id": "1fe2a2f2-c857-41f6-b7a6-e56902661eac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b88f84d3-0922-41b9-8192-8310ba5ce49c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fe2a2f2-c857-41f6-b7a6-e56902661eac",
            "compositeImage": {
                "id": "3207daf6-4098-4fed-a67e-7bb5a885965a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88f84d3-0922-41b9-8192-8310ba5ce49c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e04a77-0895-4a9c-b480-61b399d7e192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88f84d3-0922-41b9-8192-8310ba5ce49c",
                    "LayerId": "0b0797de-86e5-4e26-a2a3-788da883daaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b0797de-86e5-4e26-a2a3-788da883daaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fe2a2f2-c857-41f6-b7a6-e56902661eac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
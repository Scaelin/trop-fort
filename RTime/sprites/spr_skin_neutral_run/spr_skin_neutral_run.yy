{
    "id": "4effea74-a38b-4c22-bd20-decd47291cd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_neutral_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce7dd4af-df60-4a04-9cc6-7fa8b30c30c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "7fc9b931-36a5-49dc-90b3-efff3934477a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce7dd4af-df60-4a04-9cc6-7fa8b30c30c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c149599-4016-403f-8917-0ac05491e2ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce7dd4af-df60-4a04-9cc6-7fa8b30c30c0",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "862cca49-7c7b-49b0-827c-99bd56883b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce7dd4af-df60-4a04-9cc6-7fa8b30c30c0",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "6d094c75-a709-4e52-852b-85a24366e303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "d99298ed-324f-4691-bc51-436e765f44e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d094c75-a709-4e52-852b-85a24366e303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a3d491-65c3-40fc-920b-2f3e5ca123b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d094c75-a709-4e52-852b-85a24366e303",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "9166b8df-0b89-4f82-9d21-d2fc006ef0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d094c75-a709-4e52-852b-85a24366e303",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "1cdd7fd6-d06a-4917-890e-7a7b6aff8e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "1b3cde3f-ebe3-4e2d-9d95-d3625b0f1fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cdd7fd6-d06a-4917-890e-7a7b6aff8e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fccfb30-2f8c-4f19-b4af-16a4ee5db6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cdd7fd6-d06a-4917-890e-7a7b6aff8e28",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "a43836a9-afe6-42b3-8260-45307fc05219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cdd7fd6-d06a-4917-890e-7a7b6aff8e28",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "cf1eac54-3557-47d9-a090-7691ff716120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "feb8436b-5a41-4cce-bcff-65256f027000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1eac54-3557-47d9-a090-7691ff716120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad9c604-447e-45dc-975d-70e35fdcdcb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1eac54-3557-47d9-a090-7691ff716120",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "87563846-df5e-4d8b-849d-f044eaffe10f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1eac54-3557-47d9-a090-7691ff716120",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "cf511af6-afe7-42c2-874c-d5d77e81c1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "200f17ca-2be2-436c-a1ee-e8e02f7658b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf511af6-afe7-42c2-874c-d5d77e81c1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e9fccb-4c9f-479c-9e6c-66ec26e6438e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf511af6-afe7-42c2-874c-d5d77e81c1bd",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "1f224482-6aba-489f-b1a0-b55250d0f87e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf511af6-afe7-42c2-874c-d5d77e81c1bd",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "22485c68-384c-4468-8ac3-1a7a3979cdeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "f63bd864-0605-4607-b2d5-8e8ccfe16d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22485c68-384c-4468-8ac3-1a7a3979cdeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639820ec-0762-4526-b001-77fd0ef0bde3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22485c68-384c-4468-8ac3-1a7a3979cdeb",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "4fe41f56-ee24-4024-812d-229dd006bcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22485c68-384c-4468-8ac3-1a7a3979cdeb",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "fc2ded70-9234-4a6a-aa8e-b61e735c439e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "a21d3a89-c60b-4b01-96c6-d8f164f86f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2ded70-9234-4a6a-aa8e-b61e735c439e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3f01f9-1875-4cec-8a7e-144662b0b6ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2ded70-9234-4a6a-aa8e-b61e735c439e",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "f9dee12f-44bf-484b-9c18-5f605c281d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2ded70-9234-4a6a-aa8e-b61e735c439e",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        },
        {
            "id": "2264dd09-6e23-4d45-a0a5-6f41f81b97f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "compositeImage": {
                "id": "8d63d04d-48b4-41e5-9e42-d5d32958641b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2264dd09-6e23-4d45-a0a5-6f41f81b97f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08cdde2d-daa5-454c-94ab-853e43ae407f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2264dd09-6e23-4d45-a0a5-6f41f81b97f0",
                    "LayerId": "2ac7acd9-3869-4193-9d7b-86f563781b43"
                },
                {
                    "id": "2cc52317-8839-40b4-ba7e-1d684d51993e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2264dd09-6e23-4d45-a0a5-6f41f81b97f0",
                    "LayerId": "3c9c56f6-f372-441b-a6a3-c743d512e96e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2ac7acd9-3869-4193-9d7b-86f563781b43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Hands",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3c9c56f6-f372-441b-a6a3-c743d512e96e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4effea74-a38b-4c22-bd20-decd47291cd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Body",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}
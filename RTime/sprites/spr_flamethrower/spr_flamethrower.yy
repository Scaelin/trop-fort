{
    "id": "7f7d9248-c769-413f-a4e1-987a9f190c87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flamethrower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fcc3109-7506-4b53-ae98-bff12c89d970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f7d9248-c769-413f-a4e1-987a9f190c87",
            "compositeImage": {
                "id": "3aed37e1-ec3c-4ee4-a701-dfae16606a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fcc3109-7506-4b53-ae98-bff12c89d970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7319bd-f7ce-4557-9775-17e0df118998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcc3109-7506-4b53-ae98-bff12c89d970",
                    "LayerId": "8852b2f3-bc2c-42c3-b140-d8504dcdcb16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8852b2f3-bc2c-42c3-b140-d8504dcdcb16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f7d9248-c769-413f-a4e1-987a9f190c87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}
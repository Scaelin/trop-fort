{
    "id": "761f06a0-edad-49f4-8bce-b3d006ab681e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player2_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffb49e11-0e24-425b-8899-e99e0954fe0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761f06a0-edad-49f4-8bce-b3d006ab681e",
            "compositeImage": {
                "id": "99021b72-438d-4637-b90b-e9a218883e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb49e11-0e24-425b-8899-e99e0954fe0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e211b05-cb41-4682-b05a-f6e1ce5c7303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb49e11-0e24-425b-8899-e99e0954fe0a",
                    "LayerId": "f1070a87-2950-4e53-b065-5803631b2476"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "f1070a87-2950-4e53-b065-5803631b2476",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "761f06a0-edad-49f4-8bce-b3d006ab681e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
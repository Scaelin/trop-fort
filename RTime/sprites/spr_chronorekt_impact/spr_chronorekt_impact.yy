{
    "id": "c2caf99c-2b2c-4b62-ab4d-63a717c8f401",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chronorekt_impact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74fbe7f1-a022-4a9e-a9bd-8544fa0b9041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2caf99c-2b2c-4b62-ab4d-63a717c8f401",
            "compositeImage": {
                "id": "8971462b-78fe-404d-9f48-da950da46c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74fbe7f1-a022-4a9e-a9bd-8544fa0b9041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58903065-3276-4a94-872c-1439f8bcecfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74fbe7f1-a022-4a9e-a9bd-8544fa0b9041",
                    "LayerId": "2bac5e24-6c5f-4d64-86b9-fa16c2c90d3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2bac5e24-6c5f-4d64-86b9-fa16c2c90d3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2caf99c-2b2c-4b62-ab4d-63a717c8f401",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}
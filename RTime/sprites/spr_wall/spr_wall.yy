{
    "id": "43f0b0e9-50b8-4572-89f8-a2ed739476a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1640b7e7-1236-4ba2-86e1-64d3b866a397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43f0b0e9-50b8-4572-89f8-a2ed739476a3",
            "compositeImage": {
                "id": "36359f3d-ebda-4c4d-8360-7c0dd8239ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1640b7e7-1236-4ba2-86e1-64d3b866a397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb81782-8c2e-4497-b1c6-3050d17cae52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1640b7e7-1236-4ba2-86e1-64d3b866a397",
                    "LayerId": "64e99330-5c15-4519-a647-4746f465e62f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "64e99330-5c15-4519-a647-4746f465e62f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43f0b0e9-50b8-4572-89f8-a2ed739476a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}
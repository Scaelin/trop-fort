{
    "id": "7885433a-ea19-4ed3-ae82-a417a44a512e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chronobat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6131e1c4-b279-46cf-b3ac-adf6f8759c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "edb2c67f-f35c-40b8-9c61-b25178e026bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6131e1c4-b279-46cf-b3ac-adf6f8759c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "429e7e61-d850-4d9b-9197-2e06afb14c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6131e1c4-b279-46cf-b3ac-adf6f8759c87",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "9b85a224-8074-4a0d-a9a0-66444627b4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "764adaa2-45cb-42f8-b706-adeea224a2c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b85a224-8074-4a0d-a9a0-66444627b4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9720e5d7-5c58-4f89-88d4-b08029d7facf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b85a224-8074-4a0d-a9a0-66444627b4e5",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "9929469f-d09d-461f-a8f1-dbb3cc434aee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "be2bbd43-7b80-470b-afa8-4bd44f785290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9929469f-d09d-461f-a8f1-dbb3cc434aee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50601a5-d4a2-4b16-a846-e7a146bdb27d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9929469f-d09d-461f-a8f1-dbb3cc434aee",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "7ebf69e1-3270-480e-ae28-e924b26871cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "ba345b4a-6424-4596-884a-9d15434e3469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ebf69e1-3270-480e-ae28-e924b26871cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac47c5d7-7e6c-4777-a2cd-2b4988fe2c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ebf69e1-3270-480e-ae28-e924b26871cb",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "d6139555-6d1a-41c3-9dae-a943bb08d622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "826118f2-8369-4ea7-89a2-abe26367d37f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6139555-6d1a-41c3-9dae-a943bb08d622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7778910-b546-479e-b5b1-bd9a009f5088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6139555-6d1a-41c3-9dae-a943bb08d622",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "604535da-c1fb-441d-8769-c7ee94011a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "bf253b90-ea43-425a-86bc-b7a2c7f52cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "604535da-c1fb-441d-8769-c7ee94011a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169b83e1-5215-4695-b88b-ea7bb9948152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "604535da-c1fb-441d-8769-c7ee94011a59",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "437f88d2-70b2-40a0-9021-8f8d4be76c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "87f52381-704c-4725-9658-92c3bd80e51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437f88d2-70b2-40a0-9021-8f8d4be76c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed84b4f-1c87-457b-a4e0-fc3e8c763067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437f88d2-70b2-40a0-9021-8f8d4be76c30",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "f886442b-9f40-43f3-b9d7-b9cab25de6ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "ce84e273-a0f3-4d1e-9d80-d1cbe89ce0ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f886442b-9f40-43f3-b9d7-b9cab25de6ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae778bf-cfb2-4f79-bfc8-93b50779092f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f886442b-9f40-43f3-b9d7-b9cab25de6ba",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "867ad281-4558-459f-8ce5-adb6d8bb03d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "737f3e3d-61b0-40ed-a492-73c7c2e35ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "867ad281-4558-459f-8ce5-adb6d8bb03d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9220e7d7-1a10-4b86-818b-39a677c071a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867ad281-4558-459f-8ce5-adb6d8bb03d9",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "b14ba05a-347a-4043-93aa-5a62931e6406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "755cc6d1-6539-40d1-9fb4-79a80deb349b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14ba05a-347a-4043-93aa-5a62931e6406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e37584b-4cce-4dc8-b4cc-7bc4a47a673f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14ba05a-347a-4043-93aa-5a62931e6406",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        },
        {
            "id": "8e25acb1-fee0-48ef-9693-7523bfb5eeb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "compositeImage": {
                "id": "e18f284f-4a9d-4bb6-9f7c-010ed4ff9e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e25acb1-fee0-48ef-9693-7523bfb5eeb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d2e77e-5c1d-4965-ad8a-18c046a848ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e25acb1-fee0-48ef-9693-7523bfb5eeb0",
                    "LayerId": "9668c632-4704-46ce-a6cb-5986f0ec4d66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9668c632-4704-46ce-a6cb-5986f0ec4d66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7885433a-ea19-4ed3-ae82-a417a44a512e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 14,
    "yorig": 15
}
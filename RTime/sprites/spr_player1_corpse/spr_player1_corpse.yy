{
    "id": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player1_corpse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a0b3ea4-a7a2-453e-936b-4325c8999e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "0a446fa2-b4c8-4a3e-a50c-831919dd6005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0b3ea4-a7a2-453e-936b-4325c8999e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806fef51-cffb-44d7-b847-68d4f196cf60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0b3ea4-a7a2-453e-936b-4325c8999e70",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "b94a0f79-8437-43de-84da-d8467d2bddf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "d01add58-e42f-438d-9788-8b0c489e8ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94a0f79-8437-43de-84da-d8467d2bddf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5295395b-3b4d-4856-9f59-aee5d7fc2c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94a0f79-8437-43de-84da-d8467d2bddf8",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "fb051fbf-e322-4c06-84fa-fc96723e6588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "432bab79-49d7-426e-b14c-e6ea253599bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb051fbf-e322-4c06-84fa-fc96723e6588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bbb5383-46e3-4990-a840-7aefc1e9f341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb051fbf-e322-4c06-84fa-fc96723e6588",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "3d74e179-610a-4d07-b539-e30521be92aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "3850ad63-ea29-42f8-a9b8-46fa722d3855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d74e179-610a-4d07-b539-e30521be92aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e84f5e-0a9a-46b2-b027-0d01851bff34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d74e179-610a-4d07-b539-e30521be92aa",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "d581fee8-7dd7-4f99-be11-c8ad01b2895a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "8d1944f5-e272-43ad-9ac4-6b84990a86e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d581fee8-7dd7-4f99-be11-c8ad01b2895a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e259939-8902-43db-81a8-10e7f63c6839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d581fee8-7dd7-4f99-be11-c8ad01b2895a",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "58f016a6-6377-4a90-bdf1-56b6171ddfcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "bbe04adf-a168-439e-b9a9-7ae676ab2083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f016a6-6377-4a90-bdf1-56b6171ddfcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e8167a-9f78-4cdd-b3e8-06df9205a161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f016a6-6377-4a90-bdf1-56b6171ddfcf",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        },
        {
            "id": "20fafab9-f3b5-4d08-904e-94031dc5846e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "compositeImage": {
                "id": "34315d7f-deb7-48ea-9d98-2b50672ce6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20fafab9-f3b5-4d08-904e-94031dc5846e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98dce199-5ed5-44e2-a651-10f2c10af7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20fafab9-f3b5-4d08-904e-94031dc5846e",
                    "LayerId": "7ba2335a-be5b-4483-9241-34bf8e6c65da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "7ba2335a-be5b-4483-9241-34bf8e6c65da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80e2ae81-581c-4c89-a9d8-215e2aa9989c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
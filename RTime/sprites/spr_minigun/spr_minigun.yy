{
    "id": "8fb6001b-9498-451a-87b5-97b7a54496dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_minigun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 75,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "121fa0c5-b4a8-4170-a6bf-d99cca14dfd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb6001b-9498-451a-87b5-97b7a54496dd",
            "compositeImage": {
                "id": "9bb9e16e-a709-4a05-9458-6916dbfdc84f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121fa0c5-b4a8-4170-a6bf-d99cca14dfd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a830c74a-1e43-4070-addf-6a3c8240260f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121fa0c5-b4a8-4170-a6bf-d99cca14dfd0",
                    "LayerId": "d350c937-992c-4722-ae87-5a50b206edba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d350c937-992c-4722-ae87-5a50b206edba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb6001b-9498-451a-87b5-97b7a54496dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 23,
    "yorig": 12
}
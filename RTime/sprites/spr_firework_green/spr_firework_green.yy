{
    "id": "df4db567-abf6-4713-91c6-9266a5663e16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b5392f4-426d-4599-9680-d74806350570",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "d5dd15be-84ab-4ce0-81b4-e56756c6e5cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5392f4-426d-4599-9680-d74806350570",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6270aa33-ebdc-4ec2-8a7c-7eda56a6e5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5392f4-426d-4599-9680-d74806350570",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        },
        {
            "id": "d3cf6d6b-9032-4585-9a23-646bd1944522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "770c552b-39b3-4430-a9e0-004b6c5dc133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3cf6d6b-9032-4585-9a23-646bd1944522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f81bf482-7ba8-4e21-8d32-7eb94dc2cf61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3cf6d6b-9032-4585-9a23-646bd1944522",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        },
        {
            "id": "f3347cb9-c832-4daf-abc6-e3cf7a870fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "bb445df6-52ca-4632-9054-b04ba4393c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3347cb9-c832-4daf-abc6-e3cf7a870fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a8677c-7cff-462b-9bcf-212d1cdc37e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3347cb9-c832-4daf-abc6-e3cf7a870fed",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        },
        {
            "id": "e555d61c-600e-42ad-8e27-02a5f4592b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "17cdac5f-94d1-4a64-98f5-471329018366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e555d61c-600e-42ad-8e27-02a5f4592b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d61e818f-54a9-4e3a-a112-d1ef763f288e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e555d61c-600e-42ad-8e27-02a5f4592b2f",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        },
        {
            "id": "e7f18695-32f0-4f7b-a96a-4e758a5d6add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "75c8d24d-3040-4fcc-a269-edcf58560f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f18695-32f0-4f7b-a96a-4e758a5d6add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf12468-9795-4ea5-8201-3c7bb3999436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f18695-32f0-4f7b-a96a-4e758a5d6add",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        },
        {
            "id": "46a6769d-bc4f-4218-9d1b-9c3aa5d896a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "compositeImage": {
                "id": "31536deb-1ce6-4b5a-8984-8aec0d3c3969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a6769d-bc4f-4218-9d1b-9c3aa5d896a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca3f2ab6-6b28-4a3f-a7d8-b532824ad53a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a6769d-bc4f-4218-9d1b-9c3aa5d896a1",
                    "LayerId": "764bc9cc-9c88-4386-acc2-a9306e97a68a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "764bc9cc-9c88-4386-acc2-a9306e97a68a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df4db567-abf6-4713-91c6-9266a5663e16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}
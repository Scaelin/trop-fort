{
    "id": "33e73682-7fd1-447a-9cc0-25524efe8cdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49d180c1-2cab-4757-b1e1-1d53f9961205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33e73682-7fd1-447a-9cc0-25524efe8cdc",
            "compositeImage": {
                "id": "fd086815-27b8-4f1c-bc6b-3606c65b53b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d180c1-2cab-4757-b1e1-1d53f9961205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23751c5b-d724-4d88-b7c5-03ef53251a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d180c1-2cab-4757-b1e1-1d53f9961205",
                    "LayerId": "c878f117-a84e-462d-83c0-711f63aa3cf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "c878f117-a84e-462d-83c0-711f63aa3cf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33e73682-7fd1-447a-9cc0-25524efe8cdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 1
}
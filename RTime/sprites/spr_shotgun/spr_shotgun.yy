{
    "id": "faef97b8-2430-4d62-9e9d-9c42b046cad6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 60,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52ab3a4b-ebc4-40ce-9628-92fb1ebf98ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faef97b8-2430-4d62-9e9d-9c42b046cad6",
            "compositeImage": {
                "id": "1ca1c0a0-cdac-4046-8337-27b7e48783cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52ab3a4b-ebc4-40ce-9628-92fb1ebf98ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c0c74b-c9a8-426a-8802-bf65ce7f55c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52ab3a4b-ebc4-40ce-9628-92fb1ebf98ab",
                    "LayerId": "d848ad72-ff49-4b2b-9a41-cd8c8331cbef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d848ad72-ff49-4b2b-9a41-cd8c8331cbef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faef97b8-2430-4d62-9e9d-9c42b046cad6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}
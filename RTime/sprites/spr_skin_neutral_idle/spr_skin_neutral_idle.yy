{
    "id": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_neutral_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a836c49-88d5-41b8-a7dc-75d90c854c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "361ef6a0-e863-4fdb-b6a6-172ba561b453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a836c49-88d5-41b8-a7dc-75d90c854c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830d6051-27fa-4ff7-848a-a32aa13662f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a836c49-88d5-41b8-a7dc-75d90c854c14",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "709802fd-2271-44cc-a6f2-9e4f4379910d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a836c49-88d5-41b8-a7dc-75d90c854c14",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "eeefd9ea-4a37-47ca-9add-2a4bedec7d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "7a63b2c6-7c90-491b-86a5-97feb6f1e36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeefd9ea-4a37-47ca-9add-2a4bedec7d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ab8e42-6126-478a-a370-986794596314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeefd9ea-4a37-47ca-9add-2a4bedec7d0a",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "2134347a-944b-4287-920e-52e2cbfff999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeefd9ea-4a37-47ca-9add-2a4bedec7d0a",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "3f6d2f41-6afb-45d8-a80f-4f2cc7322a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "3c513e0f-d8ff-422d-a24e-748233615a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f6d2f41-6afb-45d8-a80f-4f2cc7322a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f40090-16d8-4904-9be6-741581b47ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6d2f41-6afb-45d8-a80f-4f2cc7322a06",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                },
                {
                    "id": "08631037-86c6-4ea0-abe7-4da20573121e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6d2f41-6afb-45d8-a80f-4f2cc7322a06",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                }
            ]
        },
        {
            "id": "ea7e557f-595b-4002-98f1-1b7dc3b06391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "6ebbd781-fe7f-4bb6-b247-eb29b6517b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7e557f-595b-4002-98f1-1b7dc3b06391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16504db3-367a-401f-a569-b012fb66d96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7e557f-595b-4002-98f1-1b7dc3b06391",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "3a28f51e-b6c4-4251-8e4e-144d56e6a244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7e557f-595b-4002-98f1-1b7dc3b06391",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "cfd50f4f-79b5-4f25-ac5d-d7195d5e16d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "2113d490-4f38-4877-990b-d325c7ffd1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfd50f4f-79b5-4f25-ac5d-d7195d5e16d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d0553a-944c-45f3-87b7-8c090c291267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfd50f4f-79b5-4f25-ac5d-d7195d5e16d1",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "7fd3aeac-ca15-4db7-86c2-b3b80fbc88ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfd50f4f-79b5-4f25-ac5d-d7195d5e16d1",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "7430cd4e-cc0c-46dc-bc77-630e3cf6b186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "f2f5d1de-f0c9-4208-83a2-3833c5073df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7430cd4e-cc0c-46dc-bc77-630e3cf6b186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "867b8541-c791-4450-bd13-3f1bfbaf568b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7430cd4e-cc0c-46dc-bc77-630e3cf6b186",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "88e23f2f-afa8-4baf-99c7-95ae405494d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7430cd4e-cc0c-46dc-bc77-630e3cf6b186",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "4ae2a125-0f89-4a3f-8a3d-4079b76072b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "b6fcaa3d-c331-4252-9847-e2229f928603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae2a125-0f89-4a3f-8a3d-4079b76072b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "336835eb-868b-42a9-b4d8-ea03b54f38ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae2a125-0f89-4a3f-8a3d-4079b76072b2",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "0e56ae87-36fe-47cf-8af0-285427f0c097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae2a125-0f89-4a3f-8a3d-4079b76072b2",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        },
        {
            "id": "298698cf-1825-4f57-ada6-7f9c6338b458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "compositeImage": {
                "id": "ab93885f-b877-42ac-ac27-6a1e2b55f22c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298698cf-1825-4f57-ada6-7f9c6338b458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ab8f4c-6f37-46b3-8b2e-83f6cb26b338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298698cf-1825-4f57-ada6-7f9c6338b458",
                    "LayerId": "858ceedb-7260-4a00-ba83-0b8c0cd129e5"
                },
                {
                    "id": "cb8b7f06-223d-45e5-be37-4ca3b575bc38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298698cf-1825-4f57-ada6-7f9c6338b458",
                    "LayerId": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "858ceedb-7260-4a00-ba83-0b8c0cd129e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Hands",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "36d7de53-c75d-43e8-82cd-14ba7d9fa0c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Body",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "10f4c51e-ac05-4852-8757-9d4bd80d97db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_pellet_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5d110f7-56b1-4ded-b84e-c938140aa9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10f4c51e-ac05-4852-8757-9d4bd80d97db",
            "compositeImage": {
                "id": "f7a97934-91ea-40e8-8064-a31f410ad835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d110f7-56b1-4ded-b84e-c938140aa9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d6db5a-98bc-40ee-a32b-f3c939842c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d110f7-56b1-4ded-b84e-c938140aa9c1",
                    "LayerId": "feb8b3c0-8bfe-4c73-94a1-06ca77d0621d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "feb8b3c0-8bfe-4c73-94a1-06ca77d0621d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10f4c51e-ac05-4852-8757-9d4bd80d97db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}
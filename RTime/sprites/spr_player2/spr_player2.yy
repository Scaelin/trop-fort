{
    "id": "bb6e169b-4ed6-4ffc-ad92-af9fe21a9335",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd64998d-c2da-49fc-b809-ff96f246c511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb6e169b-4ed6-4ffc-ad92-af9fe21a9335",
            "compositeImage": {
                "id": "3e99990e-0185-4135-a4b5-69380991f6f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd64998d-c2da-49fc-b809-ff96f246c511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c936b0-c95f-48c5-bdee-87fe965a1227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd64998d-c2da-49fc-b809-ff96f246c511",
                    "LayerId": "b04fae3c-6834-43d1-b7b6-dfd38d121284"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "b04fae3c-6834-43d1-b7b6-dfd38d121284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb6e169b-4ed6-4ffc-ad92-af9fe21a9335",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
{
    "id": "ef42507e-9886-4996-825b-2d943000bbeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flamethrower_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 86,
    "bbox_right": 185,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "091b26f9-241d-4fe1-90b4-12dd3db6f0bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef42507e-9886-4996-825b-2d943000bbeb",
            "compositeImage": {
                "id": "50a76169-7848-4a21-b04e-286207dad296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091b26f9-241d-4fe1-90b4-12dd3db6f0bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0d7f1f-c7d1-4748-8d88-e1a9f8b30404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091b26f9-241d-4fe1-90b4-12dd3db6f0bb",
                    "LayerId": "8f1e8fc9-9b78-41ee-8146-1dd5920f5b96"
                }
            ]
        },
        {
            "id": "80e5d88c-f175-414d-a795-757c08335c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef42507e-9886-4996-825b-2d943000bbeb",
            "compositeImage": {
                "id": "b54fa637-8e6d-4ef1-b70f-66fc7467ac01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e5d88c-f175-414d-a795-757c08335c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b16945f-ecc8-4438-9a18-33d8ca244302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e5d88c-f175-414d-a795-757c08335c77",
                    "LayerId": "8f1e8fc9-9b78-41ee-8146-1dd5920f5b96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8f1e8fc9-9b78-41ee-8146-1dd5920f5b96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef42507e-9886-4996-825b-2d943000bbeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 40,
    "yorig": 48
}
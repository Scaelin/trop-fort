{
    "id": "414535c6-7c4e-4b3c-a4bc-697b46d2405c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weaponSpawner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bdb1981-fb2b-41ba-abf8-07b2d76d02c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "414535c6-7c4e-4b3c-a4bc-697b46d2405c",
            "compositeImage": {
                "id": "9f6e57a3-0774-4ba2-bda3-cb506f691d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bdb1981-fb2b-41ba-abf8-07b2d76d02c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb15eab3-e03b-4041-8fa8-63312012edb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bdb1981-fb2b-41ba-abf8-07b2d76d02c2",
                    "LayerId": "1e5a81c6-258c-4a32-b230-6b0437a6c96c"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 60,
    "layers": [
        {
            "id": "1e5a81c6-258c-4a32-b230-6b0437a6c96c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "414535c6-7c4e-4b3c-a4bc-697b46d2405c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}
{
    "id": "b97cb246-dca4-4997-8a24-aafff3354a64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_windgun_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57eeaacf-da65-43e2-992a-1ee8015fb538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b97cb246-dca4-4997-8a24-aafff3354a64",
            "compositeImage": {
                "id": "391c30ef-231d-424c-820f-92af506015ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57eeaacf-da65-43e2-992a-1ee8015fb538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e3a105a-6669-4277-83fd-1670941a80fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57eeaacf-da65-43e2-992a-1ee8015fb538",
                    "LayerId": "ec2a0fbf-d683-4673-a58c-4ead0f9ec901"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ec2a0fbf-d683-4673-a58c-4ead0f9ec901",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b97cb246-dca4-4997-8a24-aafff3354a64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
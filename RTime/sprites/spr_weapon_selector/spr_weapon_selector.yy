{
    "id": "8353861d-a06c-4844-b632-62485a285065",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 123,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb238aab-d931-41d2-8ddb-8d8128a8abc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8353861d-a06c-4844-b632-62485a285065",
            "compositeImage": {
                "id": "c14ed19c-3b5c-4f0f-92f0-24d96947f073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb238aab-d931-41d2-8ddb-8d8128a8abc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e77ca70-5de5-4fde-9e76-c8113236599b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb238aab-d931-41d2-8ddb-8d8128a8abc9",
                    "LayerId": "0b9f215a-14a5-4d3f-be7b-24b8002ea4ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "0b9f215a-14a5-4d3f-be7b-24b8002ea4ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8353861d-a06c-4844-b632-62485a285065",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 42
}
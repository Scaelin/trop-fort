{
    "id": "6fa70214-b2da-4945-8d8f-905a4a97fd6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework_pellet_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39ca9bc7-591e-47d4-a492-0194ada68f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa70214-b2da-4945-8d8f-905a4a97fd6a",
            "compositeImage": {
                "id": "91cd7a79-0fe0-42b4-8165-e38dd767de86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ca9bc7-591e-47d4-a492-0194ada68f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b938e8-3088-47bb-9705-7a46fd30b03c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ca9bc7-591e-47d4-a492-0194ada68f87",
                    "LayerId": "ecc686b5-6594-4b04-bc8f-48dcdeb61de2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "ecc686b5-6594-4b04-bc8f-48dcdeb61de2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fa70214-b2da-4945-8d8f-905a4a97fd6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}
{
    "id": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swapgun_portal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9b5d7ba-f400-4e62-af59-179cdf4b00e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "f9665b9e-55a7-4336-a289-65ce7d1be39e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b5d7ba-f400-4e62-af59-179cdf4b00e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47347c7b-9b6d-4bbb-8f20-7c8d1cabe961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b5d7ba-f400-4e62-af59-179cdf4b00e9",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "22a9ca70-9bec-4578-9b38-f0ef87eb7ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "47ba4ee7-fb33-49c9-adca-d64cd2e02da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a9ca70-9bec-4578-9b38-f0ef87eb7ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e249ad02-e0f0-4c95-acb1-9d9df8611d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a9ca70-9bec-4578-9b38-f0ef87eb7ae3",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "7978778a-999d-40d8-882a-edbac54efb92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "e47597b7-9d14-45fc-888c-2f2843be2390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7978778a-999d-40d8-882a-edbac54efb92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a33525f9-b5d8-40e6-8a19-aeee68c58a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7978778a-999d-40d8-882a-edbac54efb92",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "9260d92d-301a-4d0a-be95-ddd0280239dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "9dc41f22-44fd-44ed-b2a1-08253a551213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9260d92d-301a-4d0a-be95-ddd0280239dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1b94cd-ed8f-417f-bac6-63033b11d939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9260d92d-301a-4d0a-be95-ddd0280239dc",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "baa89b0c-039d-41ac-a6bd-8e5c6285baac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "43f74121-69ae-4ae6-94d6-350aa546b6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa89b0c-039d-41ac-a6bd-8e5c6285baac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25484c29-834d-46db-8ba1-9d955d3739b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa89b0c-039d-41ac-a6bd-8e5c6285baac",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "9277fbc0-0201-4d62-9910-3ef3cdcae860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "dd51eb8a-809e-437f-8f25-f4a7f2a06d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9277fbc0-0201-4d62-9910-3ef3cdcae860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "114a7409-efc8-4b27-a28d-b9e2dca443d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9277fbc0-0201-4d62-9910-3ef3cdcae860",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "aa0d5033-54b7-4d4d-940c-39731d3692d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "5bafd7b8-5771-4d13-98f7-7d868313d362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0d5033-54b7-4d4d-940c-39731d3692d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3178fb-19ed-48e4-a64e-ffa7777f3ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0d5033-54b7-4d4d-940c-39731d3692d3",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "695b5b93-6950-4f5e-9ccf-270a66eb7752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "fa03a04c-0cc5-4867-b3b7-50fcdefc9430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "695b5b93-6950-4f5e-9ccf-270a66eb7752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb92796-c1cc-41a0-b894-d9384ac1592b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "695b5b93-6950-4f5e-9ccf-270a66eb7752",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "d12c103d-221f-4bbc-acd7-dda17db09f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "91c9250c-c253-4234-9858-df811f1290be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12c103d-221f-4bbc-acd7-dda17db09f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e7f097-2e89-42f5-b5bc-69f547fa7a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12c103d-221f-4bbc-acd7-dda17db09f29",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "8e77b6d8-c92d-4c5b-8bac-f40d3d3de030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "887aa6d7-a5fc-4d47-96dd-a289d7e999c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e77b6d8-c92d-4c5b-8bac-f40d3d3de030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450bffb1-d0ca-4d78-a8a8-8d5b33e70019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e77b6d8-c92d-4c5b-8bac-f40d3d3de030",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "6d42fe7e-f7c8-4563-a17d-6844a16a41bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "db342959-0c6b-4691-af0f-5c4b605bced7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d42fe7e-f7c8-4563-a17d-6844a16a41bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97205a2c-087f-47c8-a4b6-c372dc03a126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d42fe7e-f7c8-4563-a17d-6844a16a41bb",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        },
        {
            "id": "4edb9263-183a-446b-8b13-541efcd5ddf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "compositeImage": {
                "id": "aadece86-3eeb-4bb2-98e2-d5060b6321fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4edb9263-183a-446b-8b13-541efcd5ddf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf9681b-8382-4984-8e14-ed610eea691f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4edb9263-183a-446b-8b13-541efcd5ddf7",
                    "LayerId": "cb440620-732c-429f-8e20-62a77d9d1a46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cb440620-732c-429f-8e20-62a77d9d1a46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a020d441-4703-4bd0-b3c3-87c0c4e87805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
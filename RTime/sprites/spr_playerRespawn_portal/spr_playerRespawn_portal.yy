{
    "id": "030309ad-c112-4061-a0f0-edf88f099c48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerRespawn_portal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d245012d-9b8c-4598-b40b-7121434ace6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "d1481bad-4788-406a-991a-916c60df5f09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d245012d-9b8c-4598-b40b-7121434ace6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "810fbcd5-9373-4f15-8072-8250bb33bda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d245012d-9b8c-4598-b40b-7121434ace6a",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "9dbb10db-79fa-4c24-b777-e6e7b87ceb4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "9908392b-b86c-46fb-8e22-143bae595f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbb10db-79fa-4c24-b777-e6e7b87ceb4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e1a88f-05f6-4ccc-986b-9a5160c9ad7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbb10db-79fa-4c24-b777-e6e7b87ceb4d",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "989d15b5-7c22-4d20-836d-b7fdefc77049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "b4e7856e-017c-4d05-b843-344db5b23629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989d15b5-7c22-4d20-836d-b7fdefc77049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e6c3e0-1dec-4cf4-825f-6649cb43c8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989d15b5-7c22-4d20-836d-b7fdefc77049",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "4cc45f85-dfaf-4487-a171-1e38d9540a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "cc57568c-dc80-42ee-9bab-cc77fbbb9522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc45f85-dfaf-4487-a171-1e38d9540a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8bb346-0f83-41e7-9f28-3223aae9436b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc45f85-dfaf-4487-a171-1e38d9540a3c",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "fce788db-78ba-4e78-b425-0362f12d63b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "9876e3a8-f18b-4e4a-b3d8-2401b19dbc0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce788db-78ba-4e78-b425-0362f12d63b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0640d2b7-69f5-4a61-adea-a7d4ffbc4259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce788db-78ba-4e78-b425-0362f12d63b6",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "a5bff9f9-7751-49a9-8372-e2bb5f008ae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "637c3365-516c-4a60-bb99-9bccea94a4d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5bff9f9-7751-49a9-8372-e2bb5f008ae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af7cbab7-ee65-4219-8bce-83169f1c0ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5bff9f9-7751-49a9-8372-e2bb5f008ae2",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "ebc657cb-39b7-4fc4-8543-f7adaea50701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "bdcff27f-1276-4a5f-9b29-e92fc310b6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc657cb-39b7-4fc4-8543-f7adaea50701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc81cab-304f-42d9-9cd1-81d7a1127fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc657cb-39b7-4fc4-8543-f7adaea50701",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "b0af9eed-8210-44a9-a3ee-38ae974412cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "71727c66-dd48-45fd-b550-a5e664e3d91d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0af9eed-8210-44a9-a3ee-38ae974412cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7872b92a-ad06-4e39-ada2-37dea0323d8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0af9eed-8210-44a9-a3ee-38ae974412cf",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "d5a28db4-6887-48da-b246-bca9768b7523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "a4bf3243-e3e8-4d52-9474-667c4ff4eede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a28db4-6887-48da-b246-bca9768b7523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312f85c0-ea87-40e8-8951-44134fd8af03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a28db4-6887-48da-b246-bca9768b7523",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "68d63307-8183-4acf-84e4-36495ea05c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "00fb49cd-6510-4f3b-a9a8-062fb1f7d96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d63307-8183-4acf-84e4-36495ea05c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81e39323-6b2b-40f1-aac9-4f7f22f4f932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d63307-8183-4acf-84e4-36495ea05c5e",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "496f3732-f21f-45af-8240-d82af52447b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "3a0800c8-5568-4ddc-a51c-122bdfcef4f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "496f3732-f21f-45af-8240-d82af52447b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1929226c-e45b-4026-a8a9-4aec726a57ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "496f3732-f21f-45af-8240-d82af52447b5",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "9f2b84af-38c0-47d5-88ff-19e1f33a0bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "39af136d-4d9a-449f-bd53-689c72f5b7bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f2b84af-38c0-47d5-88ff-19e1f33a0bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e908ba0d-64d1-4d15-9189-63269093df82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f2b84af-38c0-47d5-88ff-19e1f33a0bc7",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "5b4b3b76-6068-4167-bb22-e212e6ecd247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "a9c865cc-248e-48de-b3a9-b7846ed0c866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4b3b76-6068-4167-bb22-e212e6ecd247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033ce64e-f204-4514-92b7-64639e82f3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4b3b76-6068-4167-bb22-e212e6ecd247",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "1ca63b74-9ad6-43a3-8acf-f88f5304f767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "e7b8b484-bf0f-4bd8-a422-04b4717f960e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca63b74-9ad6-43a3-8acf-f88f5304f767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600fd8a7-9406-49fa-af2d-25f7777c9262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca63b74-9ad6-43a3-8acf-f88f5304f767",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "be571ce4-6354-437a-a022-0766d868cbfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "02da0b98-ad9d-4d8b-973f-2f662d6ae986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be571ce4-6354-437a-a022-0766d868cbfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf3bc92-20ac-4290-bcb6-e8cb5f030959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be571ce4-6354-437a-a022-0766d868cbfa",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "debc6d3c-16ac-4999-a005-08c22588ac75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "a54bd836-5b61-439f-bb1b-a2060c079578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "debc6d3c-16ac-4999-a005-08c22588ac75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72025ba1-de5b-44fe-ab40-34e301a9dd2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "debc6d3c-16ac-4999-a005-08c22588ac75",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "56e667ae-52af-41cc-a607-2c453210dc38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "57653a5c-a529-4765-8cf4-ca180ea86fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e667ae-52af-41cc-a607-2c453210dc38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2408e293-97e8-4f46-a97e-5edf635268b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e667ae-52af-41cc-a607-2c453210dc38",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "77a71a83-dce8-49f2-8892-a20957e76e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "de846a01-b9c8-4d74-808c-02030ff4a50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77a71a83-dce8-49f2-8892-a20957e76e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "affb3fa5-8777-406b-b569-937c090394b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77a71a83-dce8-49f2-8892-a20957e76e0a",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        },
        {
            "id": "9c5ee071-56ea-4adb-b0f8-2ab2777cc0a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "compositeImage": {
                "id": "c2728280-5331-4604-8127-3d1644a6e855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5ee071-56ea-4adb-b0f8-2ab2777cc0a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33945e4-d4d9-4fc7-8dc9-e70b1abd9ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5ee071-56ea-4adb-b0f8-2ab2777cc0a0",
                    "LayerId": "4e688feb-4e5a-4c82-8fff-c73346d77a95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4e688feb-4e5a-4c82-8fff-c73346d77a95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "030309ad-c112-4061-a0f0-edf88f099c48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
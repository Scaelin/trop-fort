{
    "id": "8194c248-beb1-4f6a-8714-30f130de379b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29756c73-d08e-43c9-a9c2-23603b047ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8194c248-beb1-4f6a-8714-30f130de379b",
            "compositeImage": {
                "id": "1584e53f-af42-47cf-a7fe-dacd24970e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29756c73-d08e-43c9-a9c2-23603b047ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4b738a-5538-4076-979b-e0a53104410e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29756c73-d08e-43c9-a9c2-23603b047ffb",
                    "LayerId": "9f6ab6f2-e82e-47e5-9e37-219e8c676e6b"
                }
            ]
        }
    ],
    "gridX": 80,
    "gridY": 40,
    "height": 1080,
    "layers": [
        {
            "id": "9f6ab6f2-e82e-47e5-9e37-219e8c676e6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8194c248-beb1-4f6a-8714-30f130de379b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}
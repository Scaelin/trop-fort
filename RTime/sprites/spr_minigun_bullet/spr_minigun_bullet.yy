{
    "id": "40718c3b-b5df-459a-92bf-31e810501ffd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_minigun_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c491c6d1-484e-4eee-9751-2c61d6268835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40718c3b-b5df-459a-92bf-31e810501ffd",
            "compositeImage": {
                "id": "a6557421-ccca-4fce-8cc9-ab83618a074a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c491c6d1-484e-4eee-9751-2c61d6268835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "930748a6-cea1-4da1-b7ef-cf9cf2932968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c491c6d1-484e-4eee-9751-2c61d6268835",
                    "LayerId": "e0d22a05-32d7-456a-a400-5b524e2f79bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e0d22a05-32d7-456a-a400-5b524e2f79bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40718c3b-b5df-459a-92bf-31e810501ffd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
{
    "id": "d6134a37-c33d-4255-b81e-c601b4827747",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 23,
    "bbox_right": 89,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "337aeb54-4bfb-404b-ad62-8883df42c93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6134a37-c33d-4255-b81e-c601b4827747",
            "compositeImage": {
                "id": "ff9485a2-97b3-43c2-b3c9-576c9156f396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337aeb54-4bfb-404b-ad62-8883df42c93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657dfe94-93f8-4ddf-9ba1-d33a54bbf8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337aeb54-4bfb-404b-ad62-8883df42c93f",
                    "LayerId": "a0e22a17-74af-4264-8f83-281cae9945b0"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "a0e22a17-74af-4264-8f83-281cae9945b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6134a37-c33d-4255-b81e-c601b4827747",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 95
}
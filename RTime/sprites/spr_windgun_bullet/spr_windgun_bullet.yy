{
    "id": "d0c34d21-f2cb-4ad6-beef-752b3ad0a236",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_windgun_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7acfc164-9f02-4547-a863-4a28616a03ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0c34d21-f2cb-4ad6-beef-752b3ad0a236",
            "compositeImage": {
                "id": "05cfc8e6-1269-40d7-8286-d2a2a15d489e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acfc164-9f02-4547-a863-4a28616a03ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8bb6ce6-41c9-47c4-bf7d-41270adbef03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acfc164-9f02-4547-a863-4a28616a03ba",
                    "LayerId": "a81fb994-0f9e-4bd8-aecc-87a6b94b90d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a81fb994-0f9e-4bd8-aecc-87a6b94b90d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0c34d21-f2cb-4ad6-beef-752b3ad0a236",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "a28967a4-4ff1-4481-b6c6-2cce52ae5045",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_crusader_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c3f5422-b6ce-4f29-ab9e-51d6c6b6ca69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28967a4-4ff1-4481-b6c6-2cce52ae5045",
            "compositeImage": {
                "id": "75f2d4ae-dc7b-4d64-a96d-d686260a64b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c3f5422-b6ce-4f29-ab9e-51d6c6b6ca69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0af8145-0e37-42fa-91fa-5fbb5a6765d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c3f5422-b6ce-4f29-ab9e-51d6c6b6ca69",
                    "LayerId": "074f5188-728a-472d-9d43-4a09dfdc0b84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "074f5188-728a-472d-9d43-4a09dfdc0b84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a28967a4-4ff1-4481-b6c6-2cce52ae5045",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
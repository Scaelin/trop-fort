{
    "id": "8874854d-cefc-487c-8326-392212d4bdf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player3_corpse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34b7342d-2f10-43a9-8723-9bd170cb402f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "35fefe40-af19-43f8-93a3-1d9fd33b9371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b7342d-2f10-43a9-8723-9bd170cb402f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d89d67-9eaa-4686-9e2b-c9b4024a14b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b7342d-2f10-43a9-8723-9bd170cb402f",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "d4913ba1-5742-4691-9089-1b4b0f71b2eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "b05eaa08-a5fd-44f0-9de2-bb32be501f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4913ba1-5742-4691-9089-1b4b0f71b2eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed6fb0e-e4dc-4c8d-b1cf-23c3f03dc01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4913ba1-5742-4691-9089-1b4b0f71b2eb",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "e68506be-6920-4758-9085-9886a3f3a332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "1b01f772-e965-48e6-865c-032c6034e98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e68506be-6920-4758-9085-9886a3f3a332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632a2aa3-809a-43cf-8dca-7c0f7e153ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e68506be-6920-4758-9085-9886a3f3a332",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "2a533031-4d07-43ae-aa57-8509cb827811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "824c22ec-5d03-483e-8113-0d5beeb960a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a533031-4d07-43ae-aa57-8509cb827811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef5efca-c5a9-4125-b197-aec18a32613d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a533031-4d07-43ae-aa57-8509cb827811",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "199629ac-a8b4-41b0-ba29-292263242347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "be1004d4-b60d-4097-b04c-d790acc88866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199629ac-a8b4-41b0-ba29-292263242347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94491547-da7b-471b-b917-966d0804846b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199629ac-a8b4-41b0-ba29-292263242347",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "1b43499b-671e-4429-acb1-021a4050d512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "1d3b8b0f-3541-49e4-82d5-df4d5d00046a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b43499b-671e-4429-acb1-021a4050d512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb2d226-138d-4558-95a9-87994aa8c002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b43499b-671e-4429-acb1-021a4050d512",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        },
        {
            "id": "2ad3044e-eb37-4bec-ac52-8f083e4e02dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "compositeImage": {
                "id": "cf72731f-1ccc-4111-bea6-850bed102113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad3044e-eb37-4bec-ac52-8f083e4e02dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96030f67-9408-4673-b48e-0a326d075e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad3044e-eb37-4bec-ac52-8f083e4e02dc",
                    "LayerId": "892ac463-f90b-48c8-bae2-b0767c0fcc24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "892ac463-f90b-48c8-bae2-b0767c0fcc24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8874854d-cefc-487c-8326-392212d4bdf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
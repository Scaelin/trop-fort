{
    "id": "592ba594-d91c-4c43-8925-070da9bae691",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a436fb6b-cf50-4068-be84-b0aca696a1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "592ba594-d91c-4c43-8925-070da9bae691",
            "compositeImage": {
                "id": "cb9beeed-de2a-4497-957b-409c8131a9a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a436fb6b-cf50-4068-be84-b0aca696a1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdf22cb-ab4b-4c4c-9f16-de84e70ea644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a436fb6b-cf50-4068-be84-b0aca696a1fa",
                    "LayerId": "be83907e-bc20-406e-b5ef-fb592b48e999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "be83907e-bc20-406e-b5ef-fb592b48e999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "592ba594-d91c-4c43-8925-070da9bae691",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
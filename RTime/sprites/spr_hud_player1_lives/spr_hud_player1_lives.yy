{
    "id": "cc71b4d7-a00c-41a4-b2d5-9344aac6b6d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_player1_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d28553e-2a24-4108-8a6e-3bdbc177ff68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc71b4d7-a00c-41a4-b2d5-9344aac6b6d9",
            "compositeImage": {
                "id": "5b6e692c-c175-40a5-aa74-a691da3f2b52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d28553e-2a24-4108-8a6e-3bdbc177ff68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d69038-cde3-4e84-8b0c-78640b949a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d28553e-2a24-4108-8a6e-3bdbc177ff68",
                    "LayerId": "4c37e6c5-6916-4588-bdb4-acd2b06662ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "4c37e6c5-6916-4588-bdb4-acd2b06662ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc71b4d7-a00c-41a4-b2d5-9344aac6b6d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}
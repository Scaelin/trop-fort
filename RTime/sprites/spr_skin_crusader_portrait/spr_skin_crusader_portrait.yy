{
    "id": "83f66c78-2f38-473e-8b45-0efc5f894b07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skin_crusader_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a72e38f-ec62-4a0b-a08c-d724eda833b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83f66c78-2f38-473e-8b45-0efc5f894b07",
            "compositeImage": {
                "id": "d235db3b-989b-43a7-896b-d3432c485cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a72e38f-ec62-4a0b-a08c-d724eda833b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9f4272-e3af-42f4-97ca-863a27719aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a72e38f-ec62-4a0b-a08c-d724eda833b0",
                    "LayerId": "0186634e-4ee6-4436-bbca-07fc1ee3d039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "0186634e-4ee6-4436-bbca-07fc1ee3d039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83f66c78-2f38-473e-8b45-0efc5f894b07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}
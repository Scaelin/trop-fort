{
    "id": "7a571bd6-df50-4a45-a401-3e02558ce40b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_windgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb778dd2-f8ba-43a9-b959-a14aa1ff566a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "0d2a46c3-0638-4e3b-86b9-509aaafd7b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb778dd2-f8ba-43a9-b959-a14aa1ff566a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19014763-69d0-4b2e-b04e-62cabe48cd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb778dd2-f8ba-43a9-b959-a14aa1ff566a",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "bd06e3c0-d5d4-4236-856c-06257baba34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "77439fda-5882-4cb3-8974-59cd75292ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd06e3c0-d5d4-4236-856c-06257baba34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3662c1-c6f6-4600-823d-3466b4896567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd06e3c0-d5d4-4236-856c-06257baba34a",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "f4989403-a656-40ba-a194-8f92b99f07dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "f657783f-d170-4df3-97eb-e5f8a051a938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4989403-a656-40ba-a194-8f92b99f07dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d850076-633f-4d14-ba7f-18dbacc2e6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4989403-a656-40ba-a194-8f92b99f07dc",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "a585bae8-6bfb-4197-94cc-f9de00b6478b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "e0e88034-8995-4699-9b99-a667cd4abe34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a585bae8-6bfb-4197-94cc-f9de00b6478b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f04082d-14be-4599-9110-d89d377b667f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a585bae8-6bfb-4197-94cc-f9de00b6478b",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "a42552bd-e5c7-42d4-a1dd-993c744947f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "f92515ac-6394-4f4e-b4b5-612fbe9a85d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42552bd-e5c7-42d4-a1dd-993c744947f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c481503-8607-4c6d-adc3-3d745ad029a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42552bd-e5c7-42d4-a1dd-993c744947f1",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "b24b01ed-0b45-40b5-a6d7-8565f027cafe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "788f3ee2-32b3-492f-b6d6-27ba1034901d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b24b01ed-0b45-40b5-a6d7-8565f027cafe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed65290-4e6e-4524-a51f-fd229a0bfadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b24b01ed-0b45-40b5-a6d7-8565f027cafe",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "7379bd76-4f19-4644-8b53-934e0850c117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "b2ea37ec-d967-4c0c-9844-5db268458125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7379bd76-4f19-4644-8b53-934e0850c117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80567bb2-99ec-4a22-bae2-0751aa03b357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7379bd76-4f19-4644-8b53-934e0850c117",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        },
        {
            "id": "2276e42e-2623-4b0e-96fa-aa4acb3d5bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "compositeImage": {
                "id": "10993914-ec8f-4dee-a61a-87fbba0ecd90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2276e42e-2623-4b0e-96fa-aa4acb3d5bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a68135-cecc-4410-91db-630c40e99730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2276e42e-2623-4b0e-96fa-aa4acb3d5bd7",
                    "LayerId": "3b77afec-ac01-48b1-b971-f34b5aaf7342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b77afec-ac01-48b1-b971-f34b5aaf7342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a571bd6-df50-4a45-a401-3e02558ce40b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 12,
    "yorig": 15
}
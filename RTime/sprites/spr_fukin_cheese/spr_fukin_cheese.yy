{
    "id": "0b635602-c4b1-47b1-830e-0968e9b9d9bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fukin_cheese",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 13,
    "bbox_right": 51,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac69502c-15ae-4ab6-ac5e-f71c7a7548fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b635602-c4b1-47b1-830e-0968e9b9d9bd",
            "compositeImage": {
                "id": "8e0f4097-a809-4672-b9b4-3bf6ba7c73bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac69502c-15ae-4ab6-ac5e-f71c7a7548fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30abaa1-9d04-4657-8716-10d8108b84b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac69502c-15ae-4ab6-ac5e-f71c7a7548fc",
                    "LayerId": "1c8f7bae-850b-43e6-9341-586d60a1579c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1c8f7bae-850b-43e6-9341-586d60a1579c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b635602-c4b1-47b1-830e-0968e9b9d9bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
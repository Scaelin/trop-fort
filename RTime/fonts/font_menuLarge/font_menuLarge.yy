{
    "id": "66f26fde-568a-476a-84f5-ce0452095134",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_menuLarge",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Microsoft YaHei UI",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6a0bad4a-cd79-4f07-8258-d85db469e569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 55,
                "y": 187
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ba2240f2-5082-48eb-95c8-d3492bc1c022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 169,
                "y": 187
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1b9c6fc8-0439-4135-a5de-e8b5642781e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 103,
                "y": 187
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f454ebba-36ad-4849-a7e2-79f7441a461c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 160,
                "y": 39
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2b615196-33a1-473d-b4d5-f1f8bfd787cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 209,
                "y": 113
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e2a47705-5fb2-4bab-9ceb-3fd451b93be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fa3601b6-4112-4495-9196-ab253d974675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ac61be28-15e6-4f8a-9bfd-41921d86baf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 175,
                "y": 187
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e215cbe8-3d97-4a8d-a309-307f8839ca42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 112,
                "y": 187
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d5da6d44-a277-4617-a50a-e212fccb5d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 187
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "80ddb1d6-de2c-4c27-a8be-600372ba968e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 202,
                "y": 150
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9c3c60d3-9805-4c6f-91e4-0aa90a4dc2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 218,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cc5e901c-1a96-4bfe-95d8-778e7a106e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 137,
                "y": 187
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8fd5035b-e859-424d-827f-3fa31449f664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 65,
                "y": 187
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "43cf57de-44b6-4e5a-9f1f-de89a229512a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 151,
                "y": 187
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e2ee4a12-9bf4-4af6-8529-d32dad67a007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "39fea452-0fd5-414e-8184-0e37c578cc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e4892093-5f25-4b50-a743-cfae4e8a2308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 2,
                "y": 187
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d87d271e-79ae-4960-bab1-6d5b4b7604c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f489ccad-9b92-437d-ad0f-7950697ec9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 47,
                "y": 150
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "008e4c59-0d79-4a59-a7e3-d0593062aeec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ea833163-bc56-4359-b315-af84e8f24e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 162,
                "y": 150
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fe998336-aae3-49d7-8d6f-8cc2ba54078f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 202,
                "y": 76
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "68afee70-16ea-4748-8553-17aa376bacb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 113
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8760cac0-4c8b-4674-ba1a-2a715344d03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 113
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "78c7ffa2-ac8f-4b33-9212-f23a93b31f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 113
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7dd66e54-2d49-4dbe-b548-f94b8c672165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 157,
                "y": 187
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b11a38ee-2e2a-4e34-8d37-2a67747649f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 129,
                "y": 187
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d5b308ca-5615-4374-9678-d227a1efba79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 130,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6a88807b-f96d-4e3b-aff7-ef6c25eccfe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 146,
                "y": 113
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "aaf4458f-390f-47a7-8775-1fbfe6804925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 162,
                "y": 113
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d26599c4-9b53-4073-975e-c2bc7fe20516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 176,
                "y": 150
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cf70adba-bf2e-4125-8876-42e039230a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b28a51c7-7f92-4669-b4e8-205d2a90c413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2583b3d6-ced4-478a-bad6-cc4fae0b454e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 34,
                "y": 113
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ffc59169-c5b5-4b35-b4b7-cd8e6a095e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 76
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3ac882a1-812d-4b1d-8090-36a96cd2f120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 83,
                "y": 39
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b0842472-c622-42b9-89e7-9635263499c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 148,
                "y": 150
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c16c58c2-39e2-4ec5-9710-d61f20759ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 120,
                "y": 150
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0a5501da-dea0-486b-b0d0-f9cd937b3458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 23,
                "y": 39
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "684013ff-3864-4a12-b9c9-2a3b9361a73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 141,
                "y": 39
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "95c0ef48-b28c-4433-8097-90cf5d0b8759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 163,
                "y": 187
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9b189c53-d507-40b9-a920-5e48b4993e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 187
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5e2b4a3c-325a-4afa-90ca-295808ac52ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 179,
                "y": 39
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "58bb6e51-9d5b-415d-9830-59dea33b334b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 134,
                "y": 150
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "594e2a47-91ed-4dbf-9db7-36151de1d01f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2f22423a-b3f6-40b4-86d5-26f8434b1f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 63,
                "y": 39
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f55ad9b8-3c74-444c-a68e-ec1b61addc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5fa91c27-f73a-4864-8d8f-f830b95a6561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 106,
                "y": 76
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4ae391db-7d5b-4e6b-b5e7-dc8cf67ba204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9f014908-2969-45ac-b311-f1aa83af1912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 215,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "16d6af22-a62c-4396-ae6d-73dd63382631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 122,
                "y": 76
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5d311333-c7c7-4241-a310-20c5d50f26e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 38,
                "y": 76
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0eefc4f3-9f23-41c8-a654-1ec51e650309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 233,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f64a6b63-83d5-490f-8a66-27d618becda9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d91a4a42-cc80-411e-a8cf-c9cde31c20d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8084d0a0-2cf5-4c0d-8a60-41047eaa875a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 43,
                "y": 39
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fb655498-0caa-44a3-a569-2e43b27d5e4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 122,
                "y": 39
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9720b502-38d0-45bb-9c14-ec8b1be19e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 103,
                "y": 39
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "33307239-792b-48bc-8883-71b5f6af754e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 187
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bb909ecb-c606-4865-a52c-56869d6aebd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 77,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "610da493-e7d3-465e-b49a-c7d4ff31fa4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 187
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d111501a-ba87-4143-9ec7-db69159e3d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 138,
                "y": 76
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a87f2d37-0eb2-4413-b498-6c5812586b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 62,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a0e03ad6-085a-4ca8-8883-fb766bcbbd18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 121,
                "y": 187
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "347e9489-e7c2-455d-89e9-fac525674509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 92,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "70593ef0-10bc-4242-a090-5a164c659faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 170,
                "y": 76
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "56995e58-8f17-479c-b189-0e0a8ad18d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 106,
                "y": 150
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ea2b52b5-26f5-4b92-a6ec-539a35c9620d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 114,
                "y": 113
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fc411f17-c5cc-4f87-aeaa-d83895945711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 113
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a3b6c17f-3165-4af9-86b8-93cd2b3129ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 227,
                "y": 150
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9dcc63a5-3430-45a0-9c66-debc19896936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 18,
                "y": 113
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ecb70e94-88a6-4d54-b2a4-0fcb144739d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 17,
                "y": 150
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "afb5e223-3f3b-4761-8c80-a952cb021caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 144,
                "y": 187
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7ade25af-e8dd-4890-a4a0-029ebba27ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": -3,
                "shift": 7,
                "w": 9,
                "x": 13,
                "y": 187
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e743d1a7-80be-4244-8cae-0843fd516a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 150
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5f002bbd-6a5c-4e9f-90f3-43ac98f15b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 185,
                "y": 187
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "660f202e-7900-423f-a567-969c44bcfb2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0d3609d9-a62d-4f50-9300-71d7b52360d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 194,
                "y": 113
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ee5f2c12-ca03-4b6a-9ed9-f9adf0414383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 55,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a93212db-c6f6-4e18-b5a8-7ca4a7e28631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 98,
                "y": 113
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9172ea00-48ee-4db5-9cb0-c41117d993fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 154,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8649ad2b-d917-4c4d-801c-7483f585aa34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 150
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3ba83d74-093a-4878-9424-9b286642a3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 189,
                "y": 150
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "795de020-d988-45b7-99d4-ccea118ffe3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 215,
                "y": 150
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "dc625809-ba5b-40b0-86f3-5a00c7f5a375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 239,
                "y": 113
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9a2b871a-0f70-4a32-a03b-a140a1511d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 89,
                "y": 76
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3ca4beea-62a8-436a-a17f-6393bdfd07b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8c86cfda-0b17-4192-90fd-f52acebb018d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 186,
                "y": 76
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d86c48ef-ce0e-431a-9b18-486eba1f1453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 72,
                "y": 76
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1d7b4d1d-b9dd-4dc0-8278-c706cf5dceb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 224,
                "y": 113
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b5a304bb-cde3-411a-8107-fbb7d596b00d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 35,
                "y": 187
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6c11bb17-8501-4ae1-982c-4e0d82e8533e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 180,
                "y": 187
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9e01eb27-ee54-4f88-9e81-98985572428c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 45,
                "y": 187
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ee444ebc-45a6-4faf-b1f8-0f4503ebf9ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 197,
                "y": 39
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "9ad3fea6-5bff-4b1f-83d0-e897cdbc5c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 190,
                "y": 187
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "19139be3-d6f7-48a7-8537-0158caf22f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "24358e04-8b31-4ad1-9ec2-29ed1b2a8a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "112ec1f9-c3bb-42d1-a626-fe2beab278b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "353189c1-26c8-4478-8200-f1dbc3c6f47a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "af8f5dd3-f917-461d-9c5e-7c1fcbcac0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 106
        },
        {
            "id": "2609831c-842e-496b-96be-73e51b0a9c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "ed8b75b5-c645-4ec9-b3a9-d56af8edc30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 74
        },
        {
            "id": "f19c0fc7-13e3-4f59-aa02-edc5e01b153e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "7e60b5cd-b99b-4609-a760-f7536a75c690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "1eaa3199-e060-4e99-939c-feeee3cf04c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "fe7a9ad4-8362-414b-b426-9eaf72671991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "54888f9e-b5cc-4168-94e6-2edb23dc26c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "aca9f8f4-d3ad-4a5b-bb1a-7ea7b146d698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "872c01e5-eba7-4268-8f65-50feabeb9323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "af071e00-9765-4a81-a574-dfb97fa002d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "750e0ac6-5938-49e9-971e-445292d22510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "df8b4afa-709a-4dd0-aaa6-f31462196e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "c7c5efd9-ef0c-4d1c-8fa0-3baf028eb176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "fd2da106-3523-49a2-bf27-085a52bc1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "60093a4a-a177-421c-ac21-3c084ac3d0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "de93b941-0cbd-4cc7-953f-283069851768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "e9e53895-6648-4516-a433-d4b09c901af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "ec8d21a9-7230-4e96-9f84-7e36851cf19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "3f035e2c-2a13-413a-97ef-596da493f11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "24cdaa85-72d9-4c40-a35a-10db5000a837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "43b5472f-c0b9-44c3-bb05-e6556d462e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "6fe17321-c5ac-44ea-bc00-2489d8e88caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "489f539b-a598-42ec-9261-de0be58b91d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "682ca897-dc3d-44bc-b7f2-8031d20fc9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "487ea59e-c61d-49c1-be5d-29a8aa0ed3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "e26564e4-05c0-4480-b0b8-c77290d09f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "c66ffbcc-1254-4b91-b8c4-c2112ab52001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "8b045654-76f8-40d2-bb8d-6c2288e96e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "8835799a-5904-4c40-a9b8-9f12175731b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "1250c2a8-60de-4b40-a6a3-7cac72ab19ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "2e6c682f-d393-42b7-9bba-28fe90a55194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "107cbf1f-5ce8-4fd5-98d8-3cff05f54373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "8c8d114b-5c3f-46c0-83c1-c4bfd9a3f0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "6b8227ed-e43c-40d9-b827-bae347a4bcf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "f72d367f-cecf-4190-9287-f44ea6b789c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "630b1500-3a41-465a-aeab-f6f3b2013a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "a2883524-0fbf-4f71-91a5-2e6453f77e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "1fd8e0d0-06e0-4797-b071-60b6297253dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "94e0e215-53fc-41ce-8f1a-03bba4bb89e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "b924f8d5-9b68-431d-b862-348dcffae1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "db9a33ca-0dbe-4764-8e85-0d0c5ae2b9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "672fac2d-ed65-46af-9e58-efadc495eff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "41f74af2-e50f-4526-843f-047a6f1272ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "21f33819-28f1-46ca-b087-f5ba5a89e238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "5a7f5b4b-eada-467b-aa12-0b660fe5288f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "110cda1c-a956-42e1-b080-512ff6c267a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "accea561-1a15-42d8-b47c-21d94800cd5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "4919224d-50c6-463a-9e49-fc6d50e0a613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "00996c59-6faf-4381-a5c4-29ab60331af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "0bed0b6f-9001-4867-8360-a0c56a82600e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "6164fc5f-ced5-48d9-8504-4bd2f35ae594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "ed652c85-4c44-4cbd-8848-4ea2ad77ef05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "028583c5-9c24-4ec3-a521-20b2c6309b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "4d1620ff-420f-4c77-a1a5-8073d844c458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "802011e5-c744-4258-bc3a-488832b880dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "aea64c51-032b-430b-b57a-0c681997cec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "c5fa0ebc-199a-4f04-9dc4-1c4767779fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 59
        },
        {
            "id": "ff438813-d82b-454a-b530-2009364de7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "a0e7b2b8-05a6-455b-8db6-f8e4a267935e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "016d7709-c94b-4024-8a5e-b74be700843f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "78569326-9699-4936-9bf7-1cda5a507698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "9f823589-6e73-4ec1-ad9e-0a9ebd231948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "f127603b-2059-4ac2-b385-0ec1e7ed0ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 88
        },
        {
            "id": "f768e894-e74e-416d-a2e1-10633cf6abb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "4dbed975-6dd5-4c20-90aa-84791c6a9bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "5bbdf44b-6d0a-454e-aa62-d336d88369a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "411a14a9-3271-4618-8e20-ef3a6e8469f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "5d70cbd8-a4fa-4426-ab96-0aa744961f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "8cc6ad9a-8ffa-4363-862c-f8970d647412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 237
        },
        {
            "id": "8a81edf6-899c-4b9f-9bf8-957228479fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 42
        },
        {
            "id": "69ce4984-0ab0-4a8f-a557-bf59f680626e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "d00a6657-bfb9-4020-ba2f-bd4220475b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "031bf737-a03c-489b-b8f8-394ff5e0fd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "f0c35529-4340-4466-ae18-f8aa476c7f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "c91cd87c-84fc-4f81-a796-9ff940cd9162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "0f85bfad-f580-44f1-b265-f5cab8f713d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "e5c824aa-2dbb-4e7f-9338-05f8e7bbf429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "6900ec6d-4a27-4e40-9535-61d15dac6620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "f4f0c701-5845-4712-a5b2-28a23aaa11f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "7b7f842e-85bf-4c25-83bc-d6575c37d89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e579869e-dd50-420d-8a2e-a48565b5020b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "34d995b9-e855-44ab-a836-b8b1f66ed63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "6c6fde09-8ae9-497b-aace-53f8ee2ac680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "38c3f322-8d35-4f80-87d2-922968aee956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "b7ac00c4-fc2b-4d96-93d4-59894684a1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "9677bbf1-4bb0-4fd1-802b-6f2b1b1c620e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "edad7c98-5e0e-4912-a17a-f1108cb86661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "1e9833f7-34ec-4580-a99d-a7f03676e0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "60753283-201f-4b12-aca6-844571525146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "3918a3ff-6177-4b08-bac4-b3164f5bf81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "604dc055-6088-482b-b139-d379925fb604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "3d82e27e-5b9f-49d2-af50-0cd639fcb3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "fd05577a-c702-4e99-991c-df20915815c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "f53c7205-8780-44bd-bd7a-ef9ba9d85190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "602affcc-4f34-4b93-b2a6-97520de47847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "3c2183ad-0b45-439e-b7b2-bc32dd8247e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "8d74c8b8-f3de-461f-9c98-bcdd3b63ca83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "b9a958d1-a838-4c81-ad2d-bbb1b6183c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "a6fd6e00-86b1-4be9-b4d0-ebbf035f88b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "a7b6b867-172c-4de3-a2ba-220cd79cd998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 87
        },
        {
            "id": "b537ee68-55f9-48de-8fdf-ede31a4ed274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "905a0893-b2b5-4eb8-9cf8-392bd2ef1926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "b72d0e07-6cfd-47ec-8e15-18f0ba412917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "9ac2a6bb-27c3-499e-ac26-1ed7eb9aff6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "d6e43bcb-55eb-46a7-86a0-587ee3bc6e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "a19df9c8-3fcc-4cdb-9a6e-8aadf10c59fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "39b1f4d9-2b52-4440-901a-7e41e1e72515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "96789156-ee57-41a7-ae8b-0ae946951859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "21252455-352e-48a1-8c32-84c86778bb12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "e225c6a1-2e49-43d7-9253-80ce3ff484b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "edb500b5-b490-4c3c-b689-1d273341cfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "4811ed3b-5331-45a5-93ea-5ac5575f82e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "a41772b0-12ba-4ed7-976f-d4118f7a923c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "736badf7-0fa6-4ef6-8c64-b01cb8a3da60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "ab4129eb-1056-4065-b527-18d21199c08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "bbd9b5d6-57e7-47c7-b419-fea36e644af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8230
        },
        {
            "id": "5ba1f600-63bb-4e72-a8a1-06ece6c8051f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "088dac97-d858-46ae-9da4-96b5a75c0ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "0b5ec687-05c1-469c-82d3-15eb10e1e763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "ce2b8540-3b50-4d44-b7f4-981b22e2d351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "b71b0a9d-be90-4555-ac7f-bc78372b8911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "f688e932-a5fd-4872-886f-b86f926abe4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8230
        },
        {
            "id": "907a3812-df77-4e9f-a3e0-24073f181477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "5e68d6b3-c2b2-4fb2-88fd-d17d35b801af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "5463e3ff-2859-416d-9715-d38a6ad4dbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "c3c4fe68-091c-4b3d-9f38-37c760712e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "0c731736-c759-44ce-8b8b-7b53fa303978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "fbb8cb13-ddce-48ce-948a-23db54a6d3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "adaae875-1b4e-41f6-8b25-75d95eec505f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "b317a972-f6bd-4f21-aa47-918ef794bc51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "014236b8-95db-47d6-8ffb-ea2f17586047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "d0fa11bd-495d-49d3-89e1-79e42d385f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "868ef403-9480-46ac-93cf-ff8e564838ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "f8de7c8c-760b-445e-8fb2-fb72c7074555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "72f00b8b-c063-421e-a6bc-96ac03280a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "d62e077d-f4a2-45dd-b721-0fc97ff54315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "2d589687-4561-4979-abae-270fb9520e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "7cb3977b-8d47-4017-af72-30d57bc28b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "45aca551-a576-43d6-a609-1a6c9308039c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "2bcd6935-955b-4197-a0e9-b9b762ccdb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "7cc0b5b4-3d40-4e2c-8d54-a54a912e9182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "d706d4d7-4321-4479-a93a-8c9b200be6f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "7ed065fe-7b6d-4b58-9832-cc8e4872340d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "9ddf8883-bb9f-4e01-b5f7-97492572b81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "a40141d7-6c65-438e-b263-8e797c3ecc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "1888417b-a77d-4416-a3a0-4c4a91b41edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "c0fca480-90fd-40b8-ac55-8da60d8ab4f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "8d3126ec-4d34-4f47-aad7-478e133ffff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "5c18cac0-6dd5-48ed-8813-c03ad7e8a051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "662e0e28-553b-47d2-a644-f815fd9d97d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "e54e16ec-fba1-4004-b2db-84628ad093aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "1147dc69-6008-40c4-a71d-c3b3c4aae90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "0e440e72-cc17-4de2-a00f-f640ac7bbad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "808a7f20-38ed-4f90-8bd5-2b473337d315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "d3ea2476-1606-41fd-b89a-aee2359f84d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "1db99ebb-4cb7-4614-95ec-5883457c2fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "fe7f86e6-5d0a-4a84-8add-c977e27db582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "cc7ecbf1-408c-4f7a-9733-3199b9dba591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "b6b885ae-0825-44cb-bee2-b8acfe5bfd4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "3d9e74c3-8027-4564-b880-09ebccaa879e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "1e778323-01f1-40ec-addd-e3174fdd7ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "1fb16e44-ce8e-4150-8ab7-0a477d8a3ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "1d6b2059-fcd7-42e9-bb61-1dc2dee47cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "36c5d3e2-5939-4be7-81b0-9bfe138462b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "aa524e33-a74d-4b7d-b82e-9c1a99dae815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "0c859f3b-a03d-48b0-a5c4-d24c617a38ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "48f7fe9c-23c9-4513-bd0a-0ee835930283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "ca5e2a47-1169-486a-8773-c932b9923840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 120
        },
        {
            "id": "ac0130a5-8630-4723-9cac-f159e95caf23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "d51ccf0f-fb85-4ec1-99eb-9bf7b6b85758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "5e02a537-afa5-4a05-815a-cf82cdb80280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "4a47a5eb-0d62-49c0-bade-3196662e8b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "054e5ac7-addd-4df6-bb04-c9e4a757fcc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "5f3f45a4-40fa-409d-8820-151012b30b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "94035c63-4f07-45eb-b661-ce2a7ea213b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "f3cb590a-d5ce-4125-8c6f-5303c409b977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "c9ad3290-6bf5-4f14-ad0f-3fd469a955cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "1b9825f3-fe1e-4968-a613-a99c65530d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "2f1debf4-c40f-4f23-8503-b7f68b89e4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "df290df1-1796-4366-8364-97b86f979e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "b8058236-d394-48b2-998d-3f990fc571af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "b23d4265-87c9-47ae-92d5-3ca7ec99fc6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "4f61198a-3792-4746-9d7a-3122f0e7e9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8230
        },
        {
            "id": "e6060eed-71ac-4e40-997e-2d650afa4871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "8ba552b7-2ab6-4aef-90bb-60a46935171b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "d87c3a8e-8231-4d04-9525-334e2b33c98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "dba0c230-9e05-4e8c-9899-97b555d2f435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "f597f62c-173d-458c-a903-7584802b1197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "f1c9c7d9-61ba-4636-9158-8c513feff4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "d6f2686c-0a6d-422f-865c-f9ce677a8269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "f0f28521-8b77-494c-8684-faa987c6583c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "840265bf-b91a-41fa-ae1e-5462fd292f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "08386c81-1cda-436d-bbfc-210b944a3e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "26a134a4-ab56-47ef-87d1-94010536d846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "c8f05ef7-e33d-439a-946d-55127c6cfd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "2c001c94-9e02-4450-b3bd-e0bcfb8ff548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "c634650a-7d6d-4f13-b9ab-20301607065a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "c3894b65-020e-4dbe-8dd0-4f90ecb172df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "f803cc05-c1c6-4d4f-8677-3fba1896e93f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "ca2c2dce-fa9a-4e42-aeb5-0736d63628a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "fb1fc64b-a9da-4ce1-b7c9-58c4462745c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "89581ad3-9726-4b60-95f8-62c02e70b357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "20075a58-0e36-4ff7-98e2-cca259010cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "b5ce6fae-6e59-48a6-951c-35be801a3d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "ed7b829c-6dd7-42ed-9854-4a387e69e55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7a54d5f9-caa7-4af4-9e45-f40aff0b2a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "4b1872a2-db1c-4cab-b5c3-6159b04fe4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "05d3646a-c6e7-454e-b682-04afff7e6eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "f552ee81-97b2-40b4-a2bd-96be80f49c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "4cb28751-f77c-4526-abf4-f64b73e25e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "37f2fc67-9d12-4745-af65-bc598c74ca0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "15f4f603-2f0e-4efe-9b10-05a6902d8849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "995a46e8-bda8-4b52-b937-6770ee60e088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "fed4b4b6-a062-44e7-a578-60a16aca3158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "aed0ea6a-dbe5-4dcc-8435-48a79dcad1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "ddc5283b-96c5-4537-8478-6d43984b2a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "a142ff46-0702-4011-9463-4e2ab53e097b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "d9f04e0b-66d8-4196-9443-62626b6272e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "d9414b24-0d25-4e80-b797-c94640afd1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "a30d8826-7f19-43d3-84cc-87a2c15da228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 84
        },
        {
            "id": "6198b16c-0075-47ea-a833-153be59bebb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "65c640ac-1e83-4c90-9d68-a13a636b2a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "56ee32e6-623e-4e97-b386-b37bb45dd93b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "1b3b482f-7a5a-46a8-9c88-148e055727dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "7bf0942a-4a40-4dd7-bdde-4cfb365a051d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "4699de3b-b971-45f2-8f0b-8f9deaa8db10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "6bb4766d-959f-4691-b399-2dcc841df308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "fae656ba-0453-4584-8f9e-d4d6eace113c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "d37952e6-3bd2-49f7-9b5b-2cbaaff5dd85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "22994086-626e-4726-a9fd-d7a8bf33ed45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "d37b7ed7-be6e-4ebb-b3c5-302959f50561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "3b6905d4-8218-4eed-8eeb-19640314a887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "eb75f8d9-334a-44cb-ad30-5d099b297bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "df6451a8-10ce-40b7-98fa-a7c297f95a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "02e886f8-2dbf-40c8-a15b-9ab660d19ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "3a04a960-d6e7-44c0-81e1-7cbb0553b56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "52dfd6c7-540c-4d90-be70-45e2e6e15f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "90c989b5-aa1a-464c-b8fb-cfd155985163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "576d914e-9991-4c96-aba3-2005e3a4385a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "10c1cfcd-1c5a-4c68-a006-910d9bc9b44f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "031dfaa4-a958-49b2-91a8-25906b5ab3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "ab7aae8a-c00b-49cd-a6b8-6a5c8f4d0061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "0dfb4611-a7f7-4e22-abe5-98736904aa3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "141d236a-9b38-467a-8e6d-a5e57fc497ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "985a7936-52d8-4f78-96f3-f43a172ad3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "27d9dd6c-78ef-482f-97b5-51446b148850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "65836aa4-d1eb-4875-96a9-d0a1395b4b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "c63bbc9b-a356-4f1e-857a-2c61560ee00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "0ae97cc2-4245-4a7b-bc91-13775f111c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "4f9e6d82-4451-4ea4-a88d-e8fc74ea2880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "55e02c92-6e51-4b77-883d-e32473bff6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "7513286a-6afa-40a7-b244-90010d912e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "e1500e76-41fd-43f4-b14a-4b15705b3f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "e77b5ebd-1ed4-44aa-b0f2-5563d4d9e1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "b24266c1-232b-45d5-aee8-ad7230aa8af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "3f1779f3-60ab-4b5d-b193-c37df0784a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "3e1852b4-ac75-4af7-99f7-4dc518f1484c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "a4c1caee-797c-4c3e-b6b4-5199fe73efc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "a7ec568f-1182-47e4-b04d-d9e4330ed08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "62a92dfc-de65-4625-8aff-acb67037e4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "a5f2bf99-9c8c-4c0a-9a0f-a0cf1e8ab25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "4e5ffbe9-0c87-4603-9dd4-46447543314c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "a511ec20-be96-454a-9784-57bea11a2078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "b3a8f620-e936-475d-9337-6422af0577d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "76366923-8c98-44c7-bfe5-3fb19b2c7309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "d8b959f9-b297-461c-b8b3-9ca8fc2fa318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "13feb1af-5413-4f11-94ee-96ee61199df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "dd35a121-074a-4c78-a922-679d85db51f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "aa5974c3-560e-4002-a812-1b2df4407d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "704aecf6-9b28-4656-9910-3ca090494b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "7cd05919-4f5a-4ea5-8124-17190586800f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "0f8c892b-d031-47ba-acc0-589f37da8cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "b5e86cd5-4b3c-4a1f-af91-a583a1a2bb30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8230
        },
        {
            "id": "fd3c2355-ea3f-472d-82ab-c11611c121c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "8878bb94-548f-4d08-babb-60f336b914e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 84
        },
        {
            "id": "7ded9144-9df4-4b3b-b5c3-1ea1bd05a7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "509d5fde-15f6-4264-b290-b089a8f99d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 106
        },
        {
            "id": "bf0f3050-8311-4b23-a85e-a4cab8757c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "21ea0bfe-c77f-436f-8abd-439332f37082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "b97811b7-a27f-4dcd-8ab4-7a029c04ef6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "9f83cde7-5f84-41ce-abe2-f7574abe939e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "2bd070e3-c011-4f6a-b6dd-daa51c5cb01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "92034c45-b0ea-4509-8389-c9ff989dc571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 41
        },
        {
            "id": "aef53fab-0279-49a5-8ca5-12c1d9623a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "ad7e9562-5a2c-4017-b8c5-a4cf1065eddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "b43889e3-2938-4c37-ae6d-328c1d237a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "2ed0b40e-fc9a-477f-ac7c-970b5e5166fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "4d3091fe-752b-49ab-9e5b-479e26394647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "1abe1252-4e3f-4298-8f5f-0a7c928937cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "f0978a5c-0e1a-4ae4-8706-1266fd21a32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 93
        },
        {
            "id": "bdaaedbb-d854-4c62-a90f-96c79e3c09f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "56846ead-c1d5-4e7e-95bd-a0926ebd4da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "9d109ddb-5b9b-4153-b10a-eac15e494929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "d1223f0d-3ce7-481f-be26-6fc181a24d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "e2e527cb-5df1-4b68-b16f-badd63ea4aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 236
        },
        {
            "id": "79ffc990-b212-4e52-b0c5-345416b41a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "bdfbac4f-95d2-4b53-80ab-8a8fe151f9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "9a81c098-ea7f-4035-bd26-411dadc5d044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "7b905200-90a9-489f-bc8d-598f2e202c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "6c76290e-0f6d-49c9-b754-8484fa87c862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "1f970115-49cb-465b-b7a8-15f4de88e343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 106
        },
        {
            "id": "8c4d03b1-4f36-4854-84be-3d0e2b0ef3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 106
        },
        {
            "id": "f0bff58f-0152-448c-9653-0b3c8c633f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "c0cd9e77-514f-40d5-9721-507f3cbd83e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "e4ae8988-bbe4-4ece-bb8b-051b551dcd19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "4dea6745-2a55-4170-ac5e-f4b42b5d42c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "3a61d518-35da-4899-a26b-0cbc8dce1b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "5b8405fe-abe5-46c8-9808-c18df2232a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "474c944b-fee0-438c-bb62-3107bcd7f426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "5e70705f-ac4f-45af-9f35-1eff506dd9fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "90ffe2f1-3ada-44e4-b509-5c35c6d15e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "9fbb6319-e760-46a8-9cc3-dfd136d1c4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "01f57f0f-00d0-4c7d-8b67-8e4c0e713779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "22c2491f-66c3-447a-bc54-279435e5cade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "411fab17-0b5d-4209-aeb2-eac644612b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "55195238-6cd4-4976-af9e-01fdfa58b161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "ea893bfc-d3e3-4804-9f23-6efc88337798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "08b26b31-9655-4c50-b04f-4ec08cd3364e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "3adfe848-0fd1-48e8-a7c0-b9cd48af700a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "5e34fe25-ae0e-402e-a30f-c07f610a3fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 34
        },
        {
            "id": "cf920b4c-be23-4a11-b8d4-e00bcef2c4de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 39
        },
        {
            "id": "393b83db-1911-45de-bd25-921deb72bc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 102
        },
        {
            "id": "3e6fd7be-6612-4de3-833a-a505ef1540e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "1ba73ec6-7409-49df-a1ce-f34df290e6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8217
        },
        {
            "id": "ea7ee84a-12ea-4d82-9e13-6bbdfaff7904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "c6c9668c-cce4-4056-b625-ffb1a6c484bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8221
        },
        {
            "id": "8a99ab75-e581-4871-b5cc-9e6b31c07229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "9ed133cd-19ae-469e-a182-bf0a4813daf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8216
        },
        {
            "id": "9e19a361-2b3b-4b8c-b986-0e153235f52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8217
        },
        {
            "id": "86bbb73d-61ec-44af-8512-341d98c4f27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8220
        },
        {
            "id": "9a2181f2-9c61-4e21-b3d5-455c939f4732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8221
        },
        {
            "id": "7701fe13-5888-4ed3-b46e-bbcab6da87dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "7aee8cdd-e81f-49bf-925c-54c89bc2ed18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "4a5ee444-b406-4c64-a4bc-7dd274671225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "d994a672-d6e8-480b-97c9-d8b43dd20db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "372ab289-3e44-46bd-8547-baba7f6616f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "bb3157eb-1b9c-47f4-989f-37f0256b4a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "8e3e417b-9441-4629-bb19-4c2933526c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "70b82e2d-4baa-402e-8e16-d28e3154f109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "c4328362-afc3-4ca5-a2d4-0c40e60ffe2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "897416c1-2bd9-4554-a646-1059e0638227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "0025d6e7-ea19-4707-9001-842882ef3be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "8e88d263-dfa3-4999-910f-ad5db4302c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "824cd23a-14f7-4130-86c1-278f68f595d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "06708fff-9be7-44a4-9a93-6c8a6948e786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8216
        },
        {
            "id": "02fe9000-0163-4d33-bcd5-bee9edadff31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "ed659cab-addf-4172-8d67-5ee235e3fc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8220
        },
        {
            "id": "a09d4848-ee95-4dfa-87a3-1e7b75ca3eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "65da67b3-b8eb-4960-9320-87ca4dbe8379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "219fd27d-78b1-4dc5-a4cb-e52c6325e6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 45
        },
        {
            "id": "ec77eaf0-80bd-4d5f-8998-6b896f864c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "73869bfd-48e4-4713-8833-49c844f30ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "b5f6d53f-6ac1-4568-8392-43451d11250d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "f379121c-7b4a-4245-a0b2-8391010eb09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "a2e71986-e3cd-455a-8e26-30a88972a1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "e770babc-e89a-42e4-838a-c70130389cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "0b4aa789-426a-4172-beb3-09a9f2edca3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 224
        },
        {
            "id": "1f87830f-45f9-40c5-9b97-9396b47a4511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 225
        },
        {
            "id": "0bc5b906-9904-4217-9477-b8560993f296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "3c921ab5-c5c2-4165-98d4-984198ae0974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "b88ed4de-0699-46c7-b701-df49f1f4df07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "5cabc7dd-585d-41c1-a383-b98919221011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "0bebf4ab-0daf-457d-9016-7d6231835756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "47964607-994f-458f-8324-3cf5c3a2c11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "1b072c8d-128a-407e-ba8a-726db8e19b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "6bf94ef2-1dc3-46c1-8b80-e2aad697a4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8230
        },
        {
            "id": "47e12c3e-e034-4311-b617-865f27208259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "43a56de3-f55a-44bd-8635-5fb8c4147986",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_menuNormal",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Microsoft YaHei UI",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c4649a9e-5c03-4d84-9e45-794372f980e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 122
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7fa42392-cf33-4f4b-9314-3bd3dc3ee227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 132,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a5711dbd-6011-4aa9-a89b-969921e39372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a25b5638-b386-4681-81e6-e6ec77cf2429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 93,
                "y": 32
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b4af28a5-2185-4c50-87b2-ca20b3802235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "65abcc93-150d-40ca-a6b4-0fe96d2ecb39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6b22ed74-175a-439d-bc9d-228d1cf4437e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7725e9f1-fedb-4b11-91a4-7d476e0d5f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 175,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4a484ab8-9749-4440-a699-13239ff18766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b8ac1726-7001-4fb1-ae13-48969e2fb9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 89,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ce449edb-e5f3-4081-94f2-c5b503463a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "eda3c8fb-e529-4aa2-9292-944ec67d8541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "64f77235-d398-473b-9da3-ebb42bb152a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "06f35324-2187-4de9-9375-c3de1080f411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c2a2dbea-35dd-44ea-90c9-1419d4cb3b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 155,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5f3a78bb-014e-453e-a94c-167b0ef94c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 71,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3d1b7ab7-6b65-40ee-a99c-93570693d3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4327c4a7-bcce-4b12-92d3-3b3f473b763a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 81,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "430cf01a-a74c-4a6e-86a8-9d2648353d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "23ccf71d-3d6a-4a78-8305-43680d140def",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 79,
                "y": 92
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ce3260da-10b3-4634-a5dd-60799e9f0d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "711f7833-70af-4424-a6ae-382fbb94a240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "66bab036-0eb6-4c25-ac77-2af3d5388655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 149,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f53d4d9d-4a5e-4bc3-80ba-dfb6cb895f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2ab32ebb-badc-4a5a-b1dd-22e9e7b70b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "73453dad-c47d-44de-909e-41a48bb2d7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 175,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "09865355-b771-4d59-b3f5-8986a2b0156c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 165,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0a0af028-298b-457a-a73d-93bba7bd9333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 138,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "35d00f66-c14d-497c-b216-24c764de02d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 115,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "23c929f8-f597-4d5f-af0d-d844e6172536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "07ad3cf3-1c3f-4d1e-9378-5dfff3193d7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 151,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8a3f3ceb-a01c-4ce1-b815-5fdd8162167a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1dace78b-7a5e-4f4e-a69a-46c2c958f25f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ddd2be07-6a85-4618-bc74-d1591a01d6d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5b90acd0-2202-4de1-8062-561150f4ec86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 240,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fa90e977-27ea-4ee9-80d2-b366fb6aec79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "585590c3-9564-4f9e-86f2-2d07fa992b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 63,
                "y": 32
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "892d8b69-09e3-4739-aeba-61f111af523a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 196,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "10cd88c6-9cc6-4bd1-b254-b654eafe0d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 174,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5a7daf1a-56eb-404e-838c-cb395c590551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6b1227ce-d25f-4283-8e1c-59dbbbdd1f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 78,
                "y": 32
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0b843e45-5589-42ce-b38a-0d3318e2bf31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 170,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0fd25488-bd9a-4c36-ab80-32541262215e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 40,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a0220618-1d62-47ad-8baa-ad8fbe23081d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 206,
                "y": 32
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "97a603db-77f0-48dc-93dc-00c1306eb036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 185,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ce87f3b8-52ee-4c62-b27b-0324df1bcaaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0f878238-7173-4384-9dfa-a1669fa9d883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9e095908-a4f2-4aee-9a47-4c998059f962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4a9f0a6d-6d39-4254-a08d-496d16ef2b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 91,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eaba492a-0db4-45d6-87c2-880950a8b409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1810f679-9982-4e8d-9814-7d42536be9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 178,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e7d1f192-251a-4cc7-b197-444efac5bdeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "389f88b4-8129-4702-8bbc-f49d326daf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 192,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6b92ea46-3979-4c9c-9bd8-f85efa0c4f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 48,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8a16ccd5-f643-4423-bf2d-a2906736150a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "10a20286-23e1-4d42-9b09-3d939356b007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eb257f6f-4e46-48d7-a793-acbf712acd69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d150eed9-f994-4b98-beb7-0c85d6970b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 33,
                "y": 32
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "aba51ff2-03c0-44f8-b8de-d7df48746e04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3c165116-03f4-4588-8a98-bfb501ae90ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2713a493-5006-4b61-88c3-19d6e988bd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 15,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "807531c9-b780-4166-a8d2-6fa89272fce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 105,
                "y": 122
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "845f9334-99c1-4307-af60-aa794e1d3b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 234,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "732c6425-7050-4ad6-bf6e-3db9a3c6320e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 139,
                "y": 92
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0b7b3988-d0e8-4e73-bfd3-47c95a59e924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f242bfa0-518c-42e8-a464-928a79e41c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 123,
                "y": 62
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6021bc95-0442-4850-86a2-65fa1159207b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 108,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0a93c1b5-95fc-4bd3-9b77-2723c5d04bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 163,
                "y": 92
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "880793df-e193-4801-92f6-49ecb99ad970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 92
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e3ff35b4-3a20-48fc-a340-6737a6a0a455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 227,
                "y": 62
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1b5b035e-112d-4ebd-b387-e80553b6bb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 239,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "14788cff-6088-460b-9e24-aff28c5710c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 214,
                "y": 62
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "23a180ed-8381-469c-b5a2-6e4113f4a85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9602e993-115f-46ad-9ffd-a71b3f2a603d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 126,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3f11bd57-a279-4249-b411-1e9ae8248deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -3,
                "shift": 6,
                "w": 8,
                "x": 12,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2cc52b66-0162-494e-a17e-a682d9297247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 188,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "eaf39e99-b884-4d98-8499-abce1174d038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 160,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b9c8066c-b75b-401e-b2ec-ca51cbb5e16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7625081a-ad3e-42e6-a7f8-46f7c2476208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b4477ab4-5697-43fd-9716-7fd4de0328cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 164,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9fa59a9e-fdbe-4f29-a454-bb466d0228f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 150,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b6b5a9cd-6894-4264-bc67-11fa24aa93ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eeee8c80-160d-4352-9ad5-36e245998150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 22,
                "y": 122
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2d4adf56-35a2-4c71-8c4a-f019ec61fa1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 229,
                "y": 92
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a06d504c-dfb7-4caa-8572-910f08506297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d5459a96-0ac4-4bb4-9b9a-5f9218bbccb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4cb172f1-63b9-4dcc-9662-8d2ae948669e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 136,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "cfa0ae1a-de02-4c6b-b683-53cb945ea8eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "05221928-a7bc-457d-b615-ff3362ad7ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9c215c5f-2965-4c4f-9b03-cf58162cbaa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 122,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3fd608cf-f833-4b15-889a-8ce98b7c0b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "53cd478a-e13a-4a97-8e54-a9f2631d2f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 122
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f0535a38-c280-47c6-b4a3-31f63dabb6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 150,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4d44b0df-3bf0-4177-af19-103232761415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 122
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6a491955-68ed-42cc-97fe-9ace0c37d277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 136,
                "y": 32
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "1b42768d-b6f6-400f-a1d3-ed2bbbecbeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 180,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6e0fb5ac-1979-460e-8c57-0ae6152661f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "264997ec-b30a-4a4a-9060-81706a1b820d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "2a1e05ab-f681-40cc-b2ab-aab12c3531e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 40,
            "second": 106
        },
        {
            "id": "619aad90-e87c-43d7-8fd8-a30df86b116e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "4279cc7f-5466-441d-a347-1e9eac905b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 74
        },
        {
            "id": "062e29cc-13b6-413e-b8b2-af65ea8484b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "1869cb44-825a-42c6-a56a-61e4cd887bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "89558e49-7186-4cd2-adcd-378149c6e409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "e83d106e-3392-4747-b9c7-b99224130c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "41e4b6db-1cf6-4983-bfea-3a6e2c0f7a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "13e4a7c4-9faf-43fa-958e-c1a104870afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "ec1c8aae-47ae-4b72-bd50-16a55a8df537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8216
        },
        {
            "id": "3bc0fa2b-9f07-4234-9d89-c08b096aa80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8217
        },
        {
            "id": "2e81f7c0-e913-4bba-95af-fe3be1e4f71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8220
        },
        {
            "id": "95b72ac7-30f2-4f62-9c5b-1dc1898dfc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8221
        },
        {
            "id": "898bbef0-d31d-4913-8042-f832d15dbd37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8216
        },
        {
            "id": "f80cddd7-65cb-4078-8eee-3fec4b344500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8217
        },
        {
            "id": "cf7b2c93-f5f7-4e39-9c14-64c2d29c353a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8220
        },
        {
            "id": "3c965235-2dc4-491d-a22c-185d74d511b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8221
        },
        {
            "id": "26fddc1a-56f7-439b-8e9e-e9e8e750fb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 42
        },
        {
            "id": "6f156256-5a72-4d26-9eb8-3cea0c2fe12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "e5b4b74b-2bef-469c-92e9-3ae5e1d5aa0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "95888f42-3741-4d0f-857a-2f3dee088b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "4cb7a5f7-5c7a-43a8-8825-ca60e81abf08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "bf0d59a9-7905-43c9-91b0-3887cfeb830a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a9e1a992-05e5-4848-a122-6616b8f915a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "7a569e17-2c9b-4f46-b915-07f5778a8310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "73975b4c-02f6-4e51-bc62-0bb11ee3ac32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "d63b7f6d-3808-449f-a65e-d145b646ac33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "e89d0355-3e61-4573-a440-6b55666a7f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "18693684-8f67-419d-96eb-fc6320c8e011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "c532bfe9-b615-4a2b-aa52-f0bc3e0469fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "140630f2-8394-4119-a21a-be228a65d985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "146a8ab8-3428-48dc-b1bf-8d2bd6c7c435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "8ab2e972-2197-45c2-b3e8-075a900d7485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "6daef9ff-7357-4396-b290-caf6d4d66d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "046b7cb7-2acf-4e1d-8ac0-757c62e9dfdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "2e7e175f-a3b8-4c2a-9321-71a4591dabd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "2c0646ee-3f5d-4dad-b79f-deca441409d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "bc7b83b4-90eb-4928-96b8-6a57b30928c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "2b38c112-7196-4bef-a4a6-9f0accde5a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "159a103d-0883-429c-917d-d2464ccd0d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "30eb70b3-5a83-47f4-8acd-1ba52d4dc146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "55cd2bbf-c05b-4498-b4e0-22a4e1dc7c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c0381be7-1e88-439f-aac3-0ca2f6d57577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "cbd81a32-53d4-4d19-8297-a8057b197200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "e7684187-395a-40be-aa41-091fcf0cbaef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "9a094ed2-2f78-4b27-9f48-f0b2ca4a45a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "348b9132-dc42-4e18-9cf5-c285f05e32ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "3a031de0-1302-4f5a-bc05-02eb8d8c63eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "92e1225c-be49-431d-9c37-ba1ad1fb4122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "b9f79e24-64ae-45a6-ae30-e1d6decc6368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "f3043f65-9b8a-4d13-84bb-efec9bb1dc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "34a4ec6c-d158-4717-826d-cc3deed6077c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "e8d77d0a-2d13-496a-854f-05b40039e28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "3e79f2ca-7c25-47d6-8281-b6c8c4360a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "e2642f82-ea96-402c-aa87-f5a938e42d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "dd995e7f-2c85-4cd6-9d80-7ebdcadbef1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "b32c303d-e7d8-475b-9df6-b373f46dbe31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "b6fae058-6675-49c7-9e94-c3436022c7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 42
        },
        {
            "id": "4123a07a-a75f-4e32-862b-e8be71470e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "9f63a10c-9638-49d4-b799-f58fb1a17ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "70816bf3-49c9-4553-af80-89d105532f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "9d6ecbec-6781-4175-8af9-85e91acb949e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "5df4f806-c1db-431f-9db4-cbb6b8b5f802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "b143c512-eeb6-4bfc-89a1-977e5db10f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "38a766bd-9f58-4148-9e88-81205389a2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "e5df01c7-1890-4bb4-87d6-dc38bc063df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "1b1b3fb0-c43c-42ab-82ca-0fb30189fc32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8a6a003f-31ae-4b8e-9bd3-85c6636bc3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "fa9ca385-1684-4cb0-8c26-7dbdad7886a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "788ceb21-4045-40ec-ba7b-4c4c01d079ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "73f5deb3-b3c4-4047-ae40-8660aea2da07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "2bacff43-fc5f-42df-8161-52266c56fc28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "26b79590-1a43-4b48-b5f9-bf99dc8eb62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "ffc32d72-6542-438c-896b-a6540988517a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "49ad7be8-85f9-4d7e-a679-4ea8bc00876f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "78a2210f-d4f0-432b-927b-36981525f9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "342357fe-20bd-452c-8f38-8ac4f7dac86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "79952f17-0241-4249-975c-e657baaf1669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "b0cab15e-1a93-43b5-972c-578c93953e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "146b6316-7bcb-4888-b859-183055351e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "fd7b2a53-969e-44a5-9e69-36d253264cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "6b2d3865-5daf-461b-acf9-de98cf02dbd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "e3c949a6-b7ad-496a-9781-ec2c6cd692c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "4b568713-4b80-4fa4-8806-d28895df82d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "9591578d-70a7-4e77-8c8b-710d90898b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "3ae5d357-61f1-41d2-8d5e-748008836bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "bb7eeef6-6636-4fd8-80f2-ee8b31a51430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "2b8c1dbc-862c-4a5e-bfad-a8023dfefb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "a8921b74-926d-4bcb-9413-b3c645989bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "f0da656c-7435-47dd-afc2-01ca72886ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "10fd7730-897f-4065-9b69-b8b8b9ac1825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "110b77f4-1770-4c54-addb-9eba34c2a1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "cd684707-ee36-4c9f-991c-5db63fa9b6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "b1db57e3-d6db-4043-a5d7-e89effa013dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "cf1d8c97-5f7e-4922-bcd0-7095123e10f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "4cef8b38-cdbe-4c1c-89d3-632e5bed8925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "dd969cc0-0df6-4209-b2c3-30d70c3e8ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "28d8cdd3-c284-4219-881c-24f3ac628612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "b4da3ad4-d226-4eb7-969c-98fb2adde66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "f143eb97-afeb-419b-810b-5c9d0301c044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8230
        },
        {
            "id": "892902d3-d760-48d3-81bb-fb12eb8619f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "2f0e3a65-5753-4f9d-9b20-3239d5e6daf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "bf881244-a490-4c72-8de4-48fb34cd4f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "acb9fa46-3fa9-4b80-a627-d7d51cebb7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8230
        },
        {
            "id": "c46ad127-4d74-410c-a45d-bd6e99b8db0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "cd5a03e9-5168-4855-9006-6f49169f9eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "c3b1c551-4012-428e-85ff-8f87fe57c1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "a7f6f0c7-ce26-4116-b336-10ba6e61d706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "32cfafa7-cc69-46bd-83bd-ea96e74dadc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "bb60fb52-27c3-4578-92d3-87120be0b338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "6948bc79-3c70-4b89-9a5a-b5789d1cf9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "a1c3cd45-289f-4146-bf1c-aeaf98e15e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "60623999-2f53-4d0c-9f0f-23e09f408e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "e80084fd-8d78-4e76-be80-3937d22458ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "c12dbd78-a2d0-4f93-bbf0-10b0825f4d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "8b32826a-fba3-4a7a-baa1-470c36bd83af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ebdec053-6901-452a-b174-af5b2c1793ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "e48351fe-6567-4e99-9d24-b42b2befcd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "55675208-201f-45c1-b96e-c274aa133777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "9f84d70c-ddbe-4244-b02f-7920d684c593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "fe6cdb96-af53-47cf-a178-8306d195ceb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "a92c887b-a851-48e2-8253-94ed6bd29afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "6430766f-e118-4395-8e13-50eb221b37f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "3d7e0730-3c8f-4937-a678-8ee5d9638664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "5447715c-0d56-43dd-b226-f1421203fe85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "74b2ce66-cab3-4706-b91f-766dd421429b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "3e875631-53f5-47db-ae36-16caa0e9ea9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "e0553096-8ceb-4f8d-9733-965280185eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "95318007-67eb-4408-a3ae-e88851278c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "998859fd-dde4-4426-ae5a-8134b80ce420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "bb615eae-e756-43a3-b733-9d49cdf867ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "9b25210d-8f80-4a78-8fd3-c4556bae26b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "a74e0303-043d-4964-aebf-002c59dd6783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "ebf17d86-a04c-44c8-bb48-9ccbf156a112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "3364d15d-968d-43f6-b378-b4ff11c2b007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "077a0db6-3c55-4379-a1f3-1f3d95b40041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "4952af10-1a39-4735-b8f8-71e1d2cac5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "37384620-eed8-444e-a86b-7b981e0e69e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "d5c1a4c9-f6e3-412c-8246-7b48a13bac93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "42eb3b97-b08d-4270-a02f-58cc383ed3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "c4da397b-4f15-4ed9-8c64-440588c44e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "cb09242e-6949-4e0b-a4c6-0a2778437981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "349b4de3-22c6-47d6-b28a-8bc442044605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 224
        },
        {
            "id": "61866b60-80e3-4cab-9376-e241c3bc1ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "4e1b6a91-4d30-4d58-b6a7-0ebf13ce35dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "b3fd0b4c-3872-4fef-a84d-d6180ccfcd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "85d80d78-4185-4b96-a8ab-164cc267f8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "2e32e78d-b7ea-4bba-93b7-c8629b311c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "13b0ab5d-a4aa-4938-a770-0da02ec27315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "b33ae7ce-5177-4ad9-8846-e446a5ccf8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "8de509bc-331c-4598-9f9a-7b64b5881120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "1483ac91-7736-4359-b796-a0580e53d516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "342c8511-2281-4d29-906f-863ceaf102f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "45d3ee47-5a93-4751-8199-a1d3c47a77d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "ef9814b2-5c49-46be-aab2-54c60ab2afea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "0d3f376d-d391-45d8-95c3-d30f12196246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "cccea700-e4f0-4d9e-b908-130378101b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "4b675c25-c778-4b45-8faf-3132677ffcd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "41c3c803-492b-46de-a054-a9b72b7f8305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "feed08f3-235c-4a8a-a3a1-ab0fc6e68ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "eab4a9be-ed7e-44a0-acf7-5035ae8f87c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "4d92cfcd-0ecc-43ed-ba74-84fa35754578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "557ba735-25d2-4595-b743-dba2bb224033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "2bf0779c-2ef5-465a-9183-7c9793ba0164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "ca069bba-e696-4d6c-99a3-6f23b0500074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "2d012668-7c6d-45df-a240-b144db314d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "3b45db48-3bea-4cd4-9bfb-2dca029ac249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "9a93a9e7-b693-4598-b906-63f3cda506dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "107f7818-b931-4fc7-b860-2af16fc6d5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "b5fd9250-08e1-4a2d-9963-35910da7b8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "be47a86a-b990-44be-9205-60794b6720b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "2b7fa4f4-052a-4f51-a025-e0c621d4c72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "775bd6ee-d09d-44db-a1c8-7b2a271203ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "47bd2641-8f0f-41ab-a270-75809a932447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "5d3aef55-e38d-4f07-b88c-ca05d234c42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "4a42883b-dfd8-4e4a-b411-c6df65ced0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "65d6c0f1-248e-42ef-8ed0-a9d0654299c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "075bcb15-be28-4c71-a036-74fde5540e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "2a451730-87e8-4452-8d0b-933443d89cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "bc17a195-f896-47bc-bdb5-1a3874baf9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "2c4113b7-f1fb-4a1c-b6c5-550f2992bae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "e3f230a5-fd3d-4b7e-8fcb-85c5f618b07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "cae6cb60-806b-4d04-b5d1-86f322408714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "3824f600-0ff2-48fb-bf82-840681e43d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "ac7b7d3a-688b-46ae-a641-0b5f42c61104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a4776daf-e832-48a6-bc42-910689ec6941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "82e93bd7-ff6f-4654-917b-c9572f812730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "1fcdf4c0-6174-4de9-8f35-ad4fec27eca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "015aaf0f-287c-4070-91f9-f19a9c7e90a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "91b22dad-f04c-44f0-9ec3-e7e130b57f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "5a426d44-3698-4d99-8133-7bd79527b69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "40d5ef68-9205-4cc1-85c4-e79c1dcfb7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "6ecef35b-a0d1-4141-b537-8840e8fb1a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "db57f602-0777-4e16-8a69-c1fa9b755e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "14ed941b-5bde-4879-9cd0-00be98a6156e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "80fc1846-ad1d-4d40-8c46-992befe8cbf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "51c62f3c-fbf7-449f-abc1-86de0edbdccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "0c08e746-a4f8-4eca-96bf-2ddf103b4546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "c72d59fc-5127-4d43-919d-6840aa4ddfea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "43dc4a64-803c-4f38-9916-3c9bba5dfda9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "b593f6d7-41b4-44ec-b73d-046e5c6cbfd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "05f52d40-3998-4460-afc4-5a74ec6af4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "36daa393-5573-4ffa-9d8a-36d05becd30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "30de4023-d22d-4abc-974c-b2cac8aed011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "29df7247-befd-4512-9911-a92bcc304b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "974d7bcb-7fd5-44bb-b5e6-f018c30098d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "5082a94f-e0ae-4260-b836-d429d51c4751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "3944e384-35db-46a2-9088-9cd92c5e5014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "38dfdc1f-b232-441e-b897-cf9d22da33de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "4fe7f9d5-4bc2-444c-86c6-74971034b265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "55fbe615-4ea0-4f34-9b1a-1a602c8c548a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "fe792612-a9f2-4326-8f45-30091069959e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "a42c6915-952d-4314-a8e0-6eaebd1cda14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "66799951-0643-46a3-8cf4-a9fb938d9693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "b809cdaa-fc8b-4da9-9b69-62a8b5092617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "3fe92242-127f-4f41-af9c-43ae89d4a194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "54d325b4-9266-4004-ac7f-f275cf23c41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "f9bcbd15-e571-4b23-a70e-7534aa945349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "1ac9e2fe-bf38-4d65-8017-14f418689fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "77c75e25-f479-4b67-8380-05c44cab5ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "a1aaf217-3b4a-4199-acea-f1df2c2e808d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "4c96e5b5-62f8-478d-82bc-a57fb89c4381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "8baac01c-3282-4ed0-abad-5b8c5fdd474d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 91,
            "second": 106
        },
        {
            "id": "e361b550-d497-4584-8e2a-7bf8e036d90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "0319452b-6eeb-4ff0-b771-53e0a8fc2081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "f5c2dd79-7fa5-44cc-9127-1bbe49ff4a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "515eb5b8-1adc-4f34-8898-46325c057164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "06d90e14-dd77-49ac-9884-333363e8a9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "c12befe9-c4cc-4251-b67e-28d2016c2b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "8a51e67f-3942-4f27-a9c8-c9d2599ed58a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "65a4c42c-2e51-4866-9336-8d03c25a4b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "3c413f27-05b8-4b9b-aa28-05233a93eec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "fcd49cbe-50ac-499a-aba5-cd7bf193bd99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "765bccba-af59-4744-a7de-4214f837fd0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "6623e12e-704a-4beb-8dd4-c0816425bead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "138b4c63-e8aa-446c-b03e-2ec2015a205b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "e8b691fd-2a7c-4a64-b05e-d3fdf948f5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "8d0ef55d-d448-42c3-abc0-f849f2aad587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "aa3ee6f3-58ee-4eef-a22e-69921c36e8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "bdee01e9-9ab6-4183-a458-32b3be60e5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "e4be5c1d-6bc7-4081-8ffb-82ab61669c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "06513b3f-084e-49e6-9fd8-16851f14b634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "a724b1d2-92cb-4200-8f62-2c1222a1e976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "8c15b2e5-2af4-48b4-89fa-31e08547a818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "0f7c69e9-b8c2-4535-8a31-19685247350f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "86451671-cb18-4227-841e-880b6ff012eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "963962c6-3e95-4c1c-937b-5a7bcceddde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "88c5ab60-8abd-48c2-bf08-4cc9c41d6895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "a0967b5a-a920-4842-bb03-c1aad7378ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "f7aaa1b6-5e1b-4585-9357-7242d5d1ecd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "5827df41-beb1-4cf6-b2bc-4100dc35def7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "ecc31d36-5126-4f46-adf6-f82433e92f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "b419ac0a-c267-46bb-aa78-550970c2c838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "21c326a7-bd49-4e8b-afbf-f904911167b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8217
        },
        {
            "id": "c1554bf9-7957-4819-83c5-d3e87eb9f344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "be5e71b6-3184-42c9-8228-551b96f7efb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8221
        },
        {
            "id": "d78cb9be-71e7-4179-9692-9f11b19a60d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8216
        },
        {
            "id": "6c09a7cc-3a4f-4408-a808-eb23faaf4177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8217
        },
        {
            "id": "810a2622-2ffe-4bed-bd94-9e2a790ee2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8221
        },
        {
            "id": "f3ac1efa-39ca-4c9a-8329-f259e34bff01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "3029f657-b7ae-409d-b690-00cc577abd7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "41698886-967f-4f3f-97e5-7fbea426ca8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "98cfc854-e6c3-48e6-be80-df6de87396dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "4054d3dc-7bd3-48fc-bd83-c37cd5e276c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "65fbecc0-b565-41a1-bbd0-4d55eb2aa268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "7595d405-bd44-4cda-949d-4b44187ead03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "daae72b4-29da-4c8d-8e69-c3140ddc7b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "ba017299-4567-4ccd-814f-4e20326d4039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "dd115018-cdfc-4b13-abd7-222cb4bcc307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "57c5723f-36fa-4a57-b0a0-8b6a24f6d905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "1eab2e47-a5f9-4e97-96ec-3c5d0671d33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8216
        },
        {
            "id": "b9a3555f-386b-45be-9bd7-5225650de28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "89b6ef69-5519-46e4-8b5a-357b6148b0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8220
        },
        {
            "id": "02ff98f0-1c7d-4b23-bb5d-36c60635bf1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8221
        },
        {
            "id": "09b011d4-7abb-4776-bf4b-750091fafc25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "5df2e512-7f64-4577-9d83-b7cf9799abb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "5cae3309-9b81-4b6b-befb-5359943241c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "41bcb940-6770-4034-b72e-2a7de0e7e8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "9245ff5b-4514-49c1-8b04-c141f4c6a0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3bf10eb3-6470-43b3-a02f-4f7c107fb7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "cf6b8909-2370-415f-ae1e-3eccc6013f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "c3b9e53b-f99b-459d-a948-761e34704b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "41c80bf3-63bb-4ff3-a96d-a668f581cc22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "48acad02-a191-4fbe-8987-90fe4cc04853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "7bcde7e4-5e0a-401c-a536-c4c1a05ec26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0456b513-9371-43cd-b4d2-5665522afe47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "66bf29db-dee6-4e8c-ab0d-79594a419f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "9d601c39-5fc6-4866-9130-2e707da2c67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        },
        {
            "id": "0cef63d2-4c15-42ac-9a02-c7a2ec9b6d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
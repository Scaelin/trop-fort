{
    "id": "4cfc75a8-ea4f-4273-9ffd-5f628d389292",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leftHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db8460d9-9ef0-4216-b6f6-38c8d0daec5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cfc75a8-ea4f-4273-9ffd-5f628d389292",
            "compositeImage": {
                "id": "b5d3dc2d-a0d7-42a8-95f5-32588924e1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8460d9-9ef0-4216-b6f6-38c8d0daec5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20302c23-9c50-4e03-bef4-800085bed39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8460d9-9ef0-4216-b6f6-38c8d0daec5d",
                    "LayerId": "efe3dad2-2778-48d2-b1ec-b4749e5c458a"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 100,
    "layers": [
        {
            "id": "efe3dad2-2778-48d2-b1ec-b4749e5c458a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cfc75a8-ea4f-4273-9ffd-5f628d389292",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}
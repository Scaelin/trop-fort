{
    "id": "2305bd5e-bbb7-479c-bae7-640462bcb97a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35a6c759-b021-4579-9c72-148cb18ba1e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2305bd5e-bbb7-479c-bae7-640462bcb97a",
            "compositeImage": {
                "id": "ba5a2e00-83f2-4961-83e7-69fa01ebda73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a6c759-b021-4579-9c72-148cb18ba1e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f9b06d2-b717-409b-bb45-8343f179f74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a6c759-b021-4579-9c72-148cb18ba1e3",
                    "LayerId": "cd975609-eaf9-4bf5-bdfa-b1853adde6f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cd975609-eaf9-4bf5-bdfa-b1853adde6f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2305bd5e-bbb7-479c-bae7-640462bcb97a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
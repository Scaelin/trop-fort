{
    "id": "4ea5cb3f-4c67-42fc-8240-f938ed6f0c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventorySelector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11599830-991a-4d0a-bd62-1bd3779f0e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ea5cb3f-4c67-42fc-8240-f938ed6f0c66",
            "compositeImage": {
                "id": "9bd2325d-42e6-4c7b-b6fa-d549fdb841e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11599830-991a-4d0a-bd62-1bd3779f0e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e86cb08f-fe37-4c6e-a37d-43ec6d024ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11599830-991a-4d0a-bd62-1bd3779f0e8e",
                    "LayerId": "5cff7aa0-aa0e-472b-802e-a273fe98cedc"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 110,
    "layers": [
        {
            "id": "5cff7aa0-aa0e-472b-802e-a273fe98cedc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ea5cb3f-4c67-42fc-8240-f938ed6f0c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 55,
    "yorig": 55
}
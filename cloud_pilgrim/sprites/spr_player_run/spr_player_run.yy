{
    "id": "5053b01a-dec0-4646-92ca-aacd5cfde1a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c51130fc-c320-4371-a63c-b8ac0325d5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5053b01a-dec0-4646-92ca-aacd5cfde1a4",
            "compositeImage": {
                "id": "2275ab08-1d5c-4b81-a74b-6df3b84c1b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51130fc-c320-4371-a63c-b8ac0325d5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f96bcb-0ed8-4b80-8993-f71eb4b200e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51130fc-c320-4371-a63c-b8ac0325d5f2",
                    "LayerId": "4e6c36bc-cf6b-44a7-aff0-73b5938d814a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4e6c36bc-cf6b-44a7-aff0-73b5938d814a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5053b01a-dec0-4646-92ca-aacd5cfde1a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
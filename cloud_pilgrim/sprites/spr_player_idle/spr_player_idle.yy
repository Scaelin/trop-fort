{
    "id": "81a5f360-cc0d-4302-a4cd-18d1d136eeb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5200986c-c006-46bc-86d9-09ebf2e2384c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a5f360-cc0d-4302-a4cd-18d1d136eeb8",
            "compositeImage": {
                "id": "75d71ceb-2654-468a-bd36-4391a5b1f042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5200986c-c006-46bc-86d9-09ebf2e2384c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8711c32e-f82c-4add-87f4-4fff82ef54b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5200986c-c006-46bc-86d9-09ebf2e2384c",
                    "LayerId": "d140ed56-fde8-4ae8-907f-115d7b303d79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d140ed56-fde8-4ae8-907f-115d7b303d79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81a5f360-cc0d-4302-a4cd-18d1d136eeb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
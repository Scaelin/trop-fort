{
    "id": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bag_open",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77d2c1c5-ce9c-4921-b83f-bf69bae3c063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "11245e0d-bc73-413f-a4b2-a8c630ed4d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d2c1c5-ce9c-4921-b83f-bf69bae3c063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1a9fa1-7b41-4bad-8994-cb82e221b349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d2c1c5-ce9c-4921-b83f-bf69bae3c063",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "edc63378-b71d-4e2f-a7fa-8cf14a77ca31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "c0fb8af1-adac-4d6c-82d4-1678334cca0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc63378-b71d-4e2f-a7fa-8cf14a77ca31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8c68cd-9112-4e27-a7b1-ce4cfd3598a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc63378-b71d-4e2f-a7fa-8cf14a77ca31",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "8226480d-f7aa-4971-80ac-70a0cfcebd37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "512f97e3-b443-401b-81e7-c428cb409990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8226480d-f7aa-4971-80ac-70a0cfcebd37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bb83402-47e8-4550-88e2-ba514270946a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8226480d-f7aa-4971-80ac-70a0cfcebd37",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "707e2b87-a8e4-4bd9-be78-ba85222306e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "6294b894-6ddc-458e-bb11-5611a62a0e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707e2b87-a8e4-4bd9-be78-ba85222306e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debd4c40-c872-42fe-9d0c-221da542fc9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707e2b87-a8e4-4bd9-be78-ba85222306e0",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "90a45613-8497-4c51-8ae1-11eb7fd84fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "6b7a7935-8ab0-4283-9a8e-1fe805eebee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90a45613-8497-4c51-8ae1-11eb7fd84fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34757a80-f5e7-449f-badc-769cb7d6a761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90a45613-8497-4c51-8ae1-11eb7fd84fd7",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "0256c909-f068-418c-99d3-a9e6851e0662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "4dd4c534-491d-48e6-8030-0c5bc5d651b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0256c909-f068-418c-99d3-a9e6851e0662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6528a019-62ad-495a-968b-1f1c026c7297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0256c909-f068-418c-99d3-a9e6851e0662",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "e3e45595-914b-48db-b007-3a5ac9ceb5bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "019fbcd6-6bd1-4535-a689-8da91e6342ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e45595-914b-48db-b007-3a5ac9ceb5bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc9c03fc-c802-4cd7-a9e9-645aac1de0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e45595-914b-48db-b007-3a5ac9ceb5bf",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        },
        {
            "id": "b735486b-b824-4166-9d63-dca0f1a7c833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "compositeImage": {
                "id": "f534a0f8-93e9-436f-b7de-4dac11dc5d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b735486b-b824-4166-9d63-dca0f1a7c833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83cc2e8d-81b0-4282-9266-6b5763e7b5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b735486b-b824-4166-9d63-dca0f1a7c833",
                    "LayerId": "207c29b6-cb5c-41cc-939e-69d666715938"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "207c29b6-cb5c-41cc-939e-69d666715938",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e63e6a9f-ef8a-4940-bcce-e0e83da7b4f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
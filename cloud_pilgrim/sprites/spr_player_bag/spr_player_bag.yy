{
    "id": "dd8b036e-1924-416f-a0da-cde64d27a309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "963ecba6-734f-461d-95ac-f614f4da05c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd8b036e-1924-416f-a0da-cde64d27a309",
            "compositeImage": {
                "id": "9b3e929a-0c33-4cd9-beb4-e29840adab64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "963ecba6-734f-461d-95ac-f614f4da05c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7d5986-2f42-44a5-85fa-cf71ff180a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "963ecba6-734f-461d-95ac-f614f4da05c7",
                    "LayerId": "9c02ad70-93bb-48b4-a773-4e6402c20713"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c02ad70-93bb-48b4-a773-4e6402c20713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd8b036e-1924-416f-a0da-cde64d27a309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
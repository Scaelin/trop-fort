{
    "id": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bag_close",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5eee6bbc-a291-4daa-a3e5-55ab49cc157f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "317fc006-bb1f-47a6-b022-e520b208246d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eee6bbc-a291-4daa-a3e5-55ab49cc157f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a26d580-14c5-4f5e-ae47-6d95e8db0f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eee6bbc-a291-4daa-a3e5-55ab49cc157f",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "150a0183-fce7-4f6f-83ea-5265549344f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "53c0f0cd-27ad-4854-aa48-31bc238dbb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150a0183-fce7-4f6f-83ea-5265549344f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1c456a-65e9-426e-803a-1393686700b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150a0183-fce7-4f6f-83ea-5265549344f1",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "32b8347b-c6ae-4b0e-ad22-0fee35c25f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "477f91a6-4bdd-404d-82d4-0f2c656b9ddd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b8347b-c6ae-4b0e-ad22-0fee35c25f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cbaca5-7248-4d80-a6dd-0522282315bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b8347b-c6ae-4b0e-ad22-0fee35c25f35",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "12289d82-ee3e-47f1-a719-f5a774a2ebab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "260e6184-e4f7-4795-9e0c-9a367a6e3dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12289d82-ee3e-47f1-a719-f5a774a2ebab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17942dc4-226f-48c8-bde5-138ffab9f7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12289d82-ee3e-47f1-a719-f5a774a2ebab",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "c2a2cd13-4cd2-478c-b8a8-a4753cb32526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "e51cb142-0ad2-4307-ad12-c455b669a434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a2cd13-4cd2-478c-b8a8-a4753cb32526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb3530c-dfa4-4b28-898a-5bb00f41e6c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a2cd13-4cd2-478c-b8a8-a4753cb32526",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "3fa8a3dd-f8d2-45e6-ba88-9b84755e3fec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "416bc3a6-8dfa-47f8-a9e2-6e34a22c55dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa8a3dd-f8d2-45e6-ba88-9b84755e3fec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9f77a0-4245-4d32-8bc9-fa6a2c6fe002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa8a3dd-f8d2-45e6-ba88-9b84755e3fec",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "0eaf07e8-8325-462d-a9a3-046cdbb8d2e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "18bd30ac-d1b4-4d6b-9ff1-ded983d30940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eaf07e8-8325-462d-a9a3-046cdbb8d2e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5402ff09-9834-43bf-88bf-dba50b001439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eaf07e8-8325-462d-a9a3-046cdbb8d2e7",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        },
        {
            "id": "98a7dfa5-de46-4f44-8cf8-133642fade9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "compositeImage": {
                "id": "b1b7c379-87d4-4e6f-af7f-001d66db3b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98a7dfa5-de46-4f44-8cf8-133642fade9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d621901-6e3e-45b7-9056-d1753816a1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98a7dfa5-de46-4f44-8cf8-133642fade9b",
                    "LayerId": "63564f0d-4d3c-4063-83b0-5bc32a7cd570"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "63564f0d-4d3c-4063-83b0-5bc32a7cd570",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b360fb9-37fc-4cb0-9cc7-b82aa4e90313",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
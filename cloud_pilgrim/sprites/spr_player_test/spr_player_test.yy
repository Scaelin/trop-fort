{
    "id": "d89fdbef-d881-4bab-8aa5-20204ce4f9d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46b5bf2d-d97a-4471-9d20-54ea6c220652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d89fdbef-d881-4bab-8aa5-20204ce4f9d5",
            "compositeImage": {
                "id": "0d0d6ce0-3077-4fd4-bd69-a67a36360f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b5bf2d-d97a-4471-9d20-54ea6c220652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383d8529-607c-4131-b4b6-abceebb309cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b5bf2d-d97a-4471-9d20-54ea6c220652",
                    "LayerId": "8e8dc25d-ffc7-476f-98eb-f18289f9c16c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8e8dc25d-ffc7-476f-98eb-f18289f9c16c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d89fdbef-d881-4bab-8aa5-20204ce4f9d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}
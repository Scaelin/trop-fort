{
    "id": "96507f58-09ee-41c5-9ff2-7b6150c967a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rightHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a9ffa8e-c753-4e1c-87fc-c5412ba5ac82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96507f58-09ee-41c5-9ff2-7b6150c967a1",
            "compositeImage": {
                "id": "8d5fb4c8-ff44-4714-bea8-89bd983ebd92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9ffa8e-c753-4e1c-87fc-c5412ba5ac82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff352635-4199-48d2-b56c-f7c59abfc8a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9ffa8e-c753-4e1c-87fc-c5412ba5ac82",
                    "LayerId": "7d07a5e4-c17c-491b-81da-66b06418b071"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 10,
    "height": 100,
    "layers": [
        {
            "id": "7d07a5e4-c17c-491b-81da-66b06418b071",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96507f58-09ee-41c5-9ff2-7b6150c967a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}
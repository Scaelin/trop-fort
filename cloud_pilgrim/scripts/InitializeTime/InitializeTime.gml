///InitializeTime()
//No argument
//This script initializes values for deltatime (dt), speedFactor and neutralTime (nt)

global.dt = delta_time/1000000;
	//Time scale for game wide slow motions. If speedFactor == 0, this means that the game is paused, and in game objects should still stop
global.speedFactor = 1.0;
	//Usable delta time for normal object that are subject to slow motion
global.nt = global.dt*global.speedFactor;

//External factors
gamepad_set_axis_deadzone(0, 0.3);
///PlayerGetInputController()
//No argument
//This script retrieves the pressed buttons from the controller to check what action the player can perform

//Left stick
if(gamepad_axis_value(0, gp_axislv) < 0)
{
	up = -(gamepad_axis_value(0, gp_axislv));
	down = 0
}
else if(gamepad_axis_value(0, gp_axislv) > 0)
{
	up = 0;
	down = gamepad_axis_value(0, gp_axislv);
}
else
{
	up = 0;
	down = 0;
}
if(gamepad_axis_value(0, gp_axislh) < 0)
{
	left = -(gamepad_axis_value(0, gp_axislh))
	right = 0;
}
else if(gamepad_axis_value(0, gp_axislh) > 0)
{
	left = 0;
	right = gamepad_axis_value(0, gp_axislh);
}
else
{
	left = 0;
	right = 0;
}

//Buttons
interact = gamepad_button_check_pressed(0, gp_face1);
inventory = gamepad_button_check_pressed(0, gp_face4);



//Triggers
use_object1 = gamepad_button_check_released(0, gp_shoulderl);
charge_object1 = gamepad_button_check_pressed(0, gp_shoulderl);

use_object2 = gamepad_button_check_released(0, gp_shoulderr);
charge_object2 = gamepad_button_check_pressed(0, gp_shoulderr);




///PlayerGetInputKBM()
//No argument
//This script retrieves the pressed buttons from the mouse and keyboard to check what action the player can perform


//Keyboard
up = keyboard_check(ord("Z"));
down = keyboard_check(ord("S"));
left = keyboard_check(ord("Q"));
right = keyboard_check(ord("D"));

interact = keyboard_check_pressed(ord("E"));
inventory = keyboard_check_pressed(vk_tab);


//Mouse

use_object1 = mouse_check_button_released(mb_left);
charge_object1 = mouse_check_button_pressed(mb_left);

use_object2 = mouse_check_button_released(mb_right);
charge_object2 = mouse_check_button_pressed(mb_right);


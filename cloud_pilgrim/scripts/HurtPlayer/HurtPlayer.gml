///HurtPlayer(type, duration, target);
//Deals damage to the player on one of the possible locations
//type : 0 is random, 1 is targeted
//duration : duration is the time in seconds in the case of a temporary wound. 0 if not temporary
//target : useful only when the type is targeted, 0> Head, 1> Arm, 2> Leg, 3> Storage and 4> Battery

type = argument0;
duration = argument1;
target = argument2;

//Random target
if(type == 0)
{
	var randomTarget = irandom_range(0, 4);
	
	while(status[target] == 0)
	{
		randomTarget = irandom_range(0, 4);
	}
	
	status[target] = 0;
	if(duration != 0)
	{
		statusTimer = duration;	
	}
}
else
//Targeted
{
	if(status[target] == 1)
	{
		status[target] = 0;
		if(duration != 0)
		{
			statusTimer = duration;
		}
	}
}
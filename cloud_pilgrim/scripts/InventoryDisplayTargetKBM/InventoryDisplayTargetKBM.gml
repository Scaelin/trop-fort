///InventoryDisplayTargetKBM()
//No argument

//Check first line
if((device_mouse_y_to_gui(0) > 280) and (device_mouse_y_to_gui(0) < 420))
{
	if((device_mouse_x_to_gui(0) > 680) and (device_mouse_x_to_gui(0) < 820))
	{
		inventoryTarget = 0;
	}
	else if((device_mouse_x_to_gui(0) > 820) and (device_mouse_x_to_gui(0) < 960))
	{
		inventoryTarget = 1;
	}
	else if((device_mouse_x_to_gui(0) > 960) and (device_mouse_x_to_gui(0) < 1100))
	{
		inventoryTarget = 2;
	}
	else if((device_mouse_x_to_gui(0) > 1100) and (device_mouse_x_to_gui(0) < 1240))
	{
		inventoryTarget = 3;
	}
}
//Check second line
else if((device_mouse_y_to_gui(0) > 420) and (device_mouse_y_to_gui(0) < 560))
{
	if((device_mouse_x_to_gui(0) > 680) and (device_mouse_x_to_gui(0) < 820))
	{
		inventoryTarget = 4;
	}
	else if((device_mouse_x_to_gui(0) > 820) and (device_mouse_x_to_gui(0) < 960))
	{
		inventoryTarget = 5;
	}
	else if((device_mouse_x_to_gui(0) > 960) and (device_mouse_x_to_gui(0) < 1100))
	{
		inventoryTarget = 6;
	}
	else if((device_mouse_x_to_gui(0) > 1100) and (device_mouse_x_to_gui(0) < 1240))
	{
		inventoryTarget = 7;
	}
}
//Check third line
else if((device_mouse_y_to_gui(0) > 560) and (device_mouse_y_to_gui(0) < 700))
{
	if((device_mouse_x_to_gui(0) > 680) and (device_mouse_x_to_gui(0) < 820))
	{
		inventoryTarget = 8;
	}
	else if((device_mouse_x_to_gui(0) > 820) and (device_mouse_x_to_gui(0) < 960))
	{
		inventoryTarget = 9;
	}
	else if((device_mouse_x_to_gui(0) > 960) and (device_mouse_x_to_gui(0) < 1100))
	{
		inventoryTarget = 10;
	}
	else if((device_mouse_x_to_gui(0) > 1100) and (device_mouse_x_to_gui(0) < 1240))
	{
		inventoryTarget = 11;
	}
}
//Check fourth line (if storage is not injured)
else if((device_mouse_y_to_gui(0) > 700) and (device_mouse_y_to_gui(0) < 840) and (status[3] == 1))
{
	if((device_mouse_x_to_gui(0) > 680) and (device_mouse_x_to_gui(0) < 820))
	{
		inventoryTarget = 12;
	}
	else if((device_mouse_x_to_gui(0) > 820) and (device_mouse_x_to_gui(0) < 960))
	{
		inventoryTarget = 13;
	}
	else if((device_mouse_x_to_gui(0) > 960) and (device_mouse_x_to_gui(0) < 1100))
	{
		inventoryTarget = 14;
	}
	else if((device_mouse_x_to_gui(0) > 1100) and (device_mouse_x_to_gui(0) < 1240))
	{
		inventoryTarget = 15;
	}
}

//Display the target

draw_sprite(spr_inventorySelector, 0, 750 + (140 * (inventoryTarget mod 4)), 350 + (140 * (inventoryTarget div 4)));
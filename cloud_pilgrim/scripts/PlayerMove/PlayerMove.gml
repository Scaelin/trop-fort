///PlayerMove()
//No Argument
//Moves the player depending on the control inputs

hMove = (right - left) * walkSpeed * global.nt;
vMove = (down - up) * walkSpeed * global.nt;


//Moving and checking for collisions

	//Horizontal

if(place_meeting(x + hMove, y, obj_solid))
{
	if(hMove > 0)
	{
		while(!place_meeting(x + 1, y, obj_solid))
		{
			x = x + 1;
		}
	}
	else
	{
		while(!place_meeting(x - 1, y, obj_solid))
		{
			x = x - 1;
		}
	}
	hMove = 0;
}

x = x + hMove;

	//Vertical

if(place_meeting(x, y + vMove, obj_solid))
{
	if(vMove > 0)
	{
		while(!place_meeting(x, y + 1, obj_solid))
		{
			y = y + 1;
		}
	}
	else
	{
		while(!place_meeting(x, y - 1, obj_solid))
		{
			y = y - 1;	
		}
	}
	vMove = 0;
}

y = y + vMove;
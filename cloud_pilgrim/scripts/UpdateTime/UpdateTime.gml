///UpdateTime()
//No argument
//This script updates values for deltatime (dt) and neutralTime (nt)

global.dt = delta_time/1000000;

	//Usable delta time for normal object that are subject to slow motion
global.nt = global.dt*global.speedFactor;
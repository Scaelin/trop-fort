///InventoryDisplayTargetControler()
//No argument

//Get target movement with the controler

//Consider the input
if((left) and (controlerStickNavCooldown <= 0))
{
	controlerStickNavCooldown = controlerStickNavCooldownMax;
	inventoryTarget = inventoryTarget - 1;
}
if((right) and (controlerStickNavCooldown <= 0))
{
	controlerStickNavCooldown = controlerStickNavCooldownMax;
	inventoryTarget = inventoryTarget + 1;
}
if((up) and (controlerStickNavCooldown <= 0))
{
	controlerStickNavCooldown = controlerStickNavCooldownMax;
	inventoryTarget = inventoryTarget - 4;
}
if((down) and (controlerStickNavCooldown <= 0))
{
	controlerStickNavCooldown = controlerStickNavCooldownMax;
	inventoryTarget = inventoryTarget + 4;
}

//Regulate the obtained result

if(status[3] == 1)
{
	while(inventoryTarget < 0)
	{
		inventoryTarget = inventoryTarget + 16;	
	}

	while(inventoryTarget > 15)
	{
		inventoryTarget = inventoryTarget - 16;	
	}
}
else
{
		while(inventoryTarget < 0)
	{
		inventoryTarget = inventoryTarget + 12;	
	}

	while(inventoryTarget > 11)
	{
		inventoryTarget = inventoryTarget - 12;	
	}
}

//Display the target

draw_sprite(spr_inventorySelector, 0, 750 + (140 * (inventoryTarget mod 4)), 350 + (140 * (inventoryTarget div 4)));
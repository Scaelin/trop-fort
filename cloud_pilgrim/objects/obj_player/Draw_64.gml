/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur


if(sprite_index == spr_player_bag)
{
	draw_set_alpha(fadingInventory);
	draw_rectangle_color(0, 0, 1920, 1080, c_black, c_black, c_black, c_black, false);
	
	//Draw the inventory part
	
	//Selected hands
	draw_sprite(spr_leftHand, 0, 890, 150);
	draw_sprite(spr_rightHand, 0, 1030, 150);
	
	#region Inventory
	//First line
	//0
	draw_rectangle_color(700, 300, 800, 400, c_white, c_white, c_white, c_white, false);
	//1
	draw_rectangle_color(840, 300, 940, 400, c_white, c_white, c_white, c_white, false);
	//2
	draw_rectangle_color(980, 300, 1080, 400, c_white, c_white, c_white, c_white, false);
	//3
	draw_rectangle_color(1120, 300, 1220, 400, c_white, c_white, c_white, c_white, false);
	
	//Second line
	//4
	draw_rectangle_color(700, 440, 800, 540, c_white, c_white, c_white, c_white, false);
	//5
	draw_rectangle_color(840, 440, 940, 540, c_white, c_white, c_white, c_white, false);
	//6
	draw_rectangle_color(980, 440, 1080, 540, c_white, c_white, c_white, c_white, false);
	//7
	draw_rectangle_color(1120, 440, 1220, 540, c_white, c_white, c_white, c_white, false);
	
	//Third line
	//8
	draw_rectangle_color(700, 580, 800, 680, c_white, c_white, c_white, c_white, false);
	//9
	draw_rectangle_color(840, 580, 940, 680, c_white, c_white, c_white, c_white, false);
	//10
	draw_rectangle_color(980, 580, 1080, 680, c_white, c_white, c_white, c_white, false);
	//11
	draw_rectangle_color(1120, 580, 1220, 680, c_white, c_white, c_white, c_white, false);
	
	//Fourth line (can be desactivated if storage is damaged)
	//12
	draw_rectangle_color(700, 720, 800, 820, c_white, c_white, c_white, c_white, false);
	//13
	draw_rectangle_color(840, 720, 940, 820, c_white, c_white, c_white, c_white, false);
	//14
	draw_rectangle_color(980, 720, 1080, 820, c_white, c_white, c_white, c_white, false);
	//15
	draw_rectangle_color(1120, 720, 1220, 820, c_white, c_white, c_white, c_white, false);
	
	#endregion
	
	draw_set_alpha(1.0);
	
	if(gamepad_is_connected(0))
	{
		InventoryDisplayTargetControler();
	}
	else
	{
		InventoryDisplayTargetKBM();	
	}
	
	
	
}


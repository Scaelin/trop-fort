/// @description Finite state machine

state = "idle";
//State can be :
//- idle
//- damaged (damage animation)
//- charging
//- dashing 
//- interacting
//- menuing


walkSpeed = 200;

//Using variables
useStartupDelay = 0;
useRecoveryDelay = 0;

//Status variables

	//Health
// 0 > Head
// 1 > Arms
// 2 > Legs
// 3 > Storage
// 4 > Battery
status[0] = 1;
status[1] = 1;
status[2] = 1;
status[3] = 1;
status[4] = 1; 

statusTimer[0] = -1;
statusTimer[0] = -1;
statusTimer[0] = -1;
statusTimer[0] = -1;
statusTimer[0] = -1;

//This is increased when the player takes damage
staggerTimer = 0;


#region Inventory
//The inventory has 16 slots, plus equipments.
//It is reduced to 12 slots when the storage is damaged
//Index[x, y] :
//x is the array number between 0 and 15. If the slot is empty, it is valued -1
//y is the number of this kind of item in the inventory, if possible. If it is empty, it is valued -1


inventory[0, 0] = -1;
inventory[0, 1] = -1;

inventory[1, 0] = -1;
inventory[1, 1] = -1;

inventory[2, 0] = -1;
inventory[2, 1] = -1;

inventory[3, 0] = -1;
inventory[3, 1] = -1;

inventory[4, 0] = -1;
inventory[4, 1] = -1;

inventory[5, 0] = -1;
inventory[5, 1] = -1;

inventory[6, 0] = -1;
inventory[6, 1] = -1;

inventory[7, 0] = -1;
inventory[7, 1] = -1;

inventory[8, 0] = -1;
inventory[8, 1] = -1;

inventory[9, 0] = -1;
inventory[9, 1] = -1;

inventory[10, 0] = -1;
inventory[10, 1] = -1;

inventory[11, 0] = -1;
inventory[11, 1] = -1;

inventory[12, 0] = -1;
inventory[12, 1] = -1;

inventory[13, 0] = -1;
inventory[13, 1] = -1;

inventory[14, 0] = -1;
inventory[14, 1] = -1;

inventory[15, 0] = -1;
inventory[15, 1] = -1;

#endregion

//Hand slots refer to the slot of the actual inventory
leftHandSlot = -1;
rightHandSlot = -1;

//Equipment value refers to the item index in the item log
equipment[0] = -1;
equipment[1] = -1;
equipment[2] = -1;

fadingInventory = 0;
inventoryTarget = 0;


//This allows the player to move through the menus witht the left stick
controlerStickNavCooldown = 0;
controlerStickNavCooldownMax = 0.15;


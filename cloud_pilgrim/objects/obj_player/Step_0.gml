/// @description Finite State Machine

if(global.speedFactor != 0)
{
	#region Idle state
	if(state == "idle")
	{
		if(gamepad_is_connected(0))
		{
			PlayerGetInputController();
		}
		else
		{
			PlayerGetInputKBM();
		}
	
		PlayerMove();
	
		if(inventory)
		{
			state = "menuing";
			sprite_index = spr_player_bag_open;
			
		}
		
	
	}
	#endregion
	#region Damaged state
	else if(state == "damaged")
	{
		
		//Check if the player is dead
		if((status[0] + status[1] + status[2] + status[3] + status[4]) <= 2)
		{
			state = "dead";
			//Play death animation
		}
		else if(staggerTimer > 0)
		{
			staggerTimer = staggerTimer - global.nt;
		}
		else if(staggerTimer >= 0)
		{
			state = "idle";
			sprite_index = spr_player_idle;
		}
	}
	#endregion
	#region Using state
	else if(state == "using")
	{
		//Reduce the delay before using
		if(useStartupDelay > 0)
		{
			useStartupDelay = useStartupDelay - global.nt;
		}
		else if(useRecoveryDelay > 0)
		{
			useRecoveryDelay = useRecoveryDelay - global.nt;
		}
		else
		{
			state = "idle";
		}
	
	
	}
	#endregion
	#region Menuing state
	else if(state == "menuing")
	{
		if(gamepad_is_connected(0))
		{
			PlayerGetInputController();
		}
		else
		{
			PlayerGetInputKBM();
		}
		
		if(sprite_index == spr_player_bag_open)
		{
			if(image_index > image_number - 1)
			{
				sprite_index = spr_player_bag;
				image_index = 0;
				global.speedFactor = 0.25;
				obj_camera.inventoryCamera = 200;
			}
		}
		else if(sprite_index == spr_player_bag)
		{
			//Fading in
			fadingInventory = clamp(fadingInventory + 0.05, 0, 0.5);
			//Exit inventory
			if(inventory)
			{
				sprite_index = spr_player_bag_close;
				image_index = 0;
				global.speedFactor = 1;
				
				obj_camera.inventoryCamera = 0;
			}
			
			//Assigning to any hand
			
		}
		else if(sprite_index == spr_player_bag_close)
		{
			if(image_index > image_number - 1)
			{
				fadingInventory = 0;
				
				sprite_index = spr_player_idle;
				state = "idle";
			}
		}
		
	}
	#endregion
	
	//Things to do in any 
	
	
}

//Menuing with stick
if(controlerStickNavCooldown > 0 )
{
	controlerStickNavCooldown = controlerStickNavCooldown - global.dt;	
}
/// @description Camera adaptation


//Set the new camera position
normalX = lerp(normalX, obj_player.x, 0.3);
normalY = lerp(normalY, obj_player.y - inventoryCamera, 0.3);

x = normalX + modifierX;
y = normalY + modifierY;


//Ajust the modifier
if(modifierX != 0)
{
	modifierX = modifierX * 0.75;
	if(abs(modifierX) < 1)
	{
		modifierX = 0;	
	}
}
if(modifierY != 0)
{
	modifierY = modifierY * 0.75;
	if(abs(modifierY) < 1)
	{
		modifierY = 0;
	}
}
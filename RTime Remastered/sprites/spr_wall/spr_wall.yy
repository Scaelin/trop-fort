{
    "id": "0edd3598-8dd3-4025-a809-3ccea206fca2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef2ecf6a-dbec-484c-b933-1bfde15b4249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0edd3598-8dd3-4025-a809-3ccea206fca2",
            "compositeImage": {
                "id": "160db2e7-a1ff-4ecf-a086-4512e4bc69cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2ecf6a-dbec-484c-b933-1bfde15b4249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f095a48-42b2-40c3-96da-55cb9b93e506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2ecf6a-dbec-484c-b933-1bfde15b4249",
                    "LayerId": "78d9a241-aa6c-4cc1-b22f-a0598c072a8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "78d9a241-aa6c-4cc1-b22f-a0598c072a8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0edd3598-8dd3-4025-a809-3ccea206fca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}
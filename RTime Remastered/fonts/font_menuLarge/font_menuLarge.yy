{
    "id": "db5ff411-769d-44e8-8f22-2c88fb4b074d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_menuLarge",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Microsoft YaHei",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8c318da2-beb8-49f4-a54c-b89c903980f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 55,
                "y": 187
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c0071e32-6dda-4629-b716-ef90af3a3efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 169,
                "y": 187
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "393465ae-2975-4582-8788-d709714dc780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 103,
                "y": 187
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "96708164-beba-423b-b2fa-c699c50b25d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 160,
                "y": 39
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "825626be-8b2d-4228-88e8-73258eab4d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 209,
                "y": 113
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c11dd489-48c0-456a-aa09-2fe0cff8659d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9d43e4d9-0f42-4dd5-840a-8ea870bc8904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "697969dd-8c58-426b-aa0f-0e7bdcc3a007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 175,
                "y": 187
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4601566d-da36-4534-a0ff-c80aba22ee94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 112,
                "y": 187
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "df5e7801-5f11-4008-9b23-599caf48094b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 187
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6d76b7d6-3d19-4843-a2a7-0f97f26a2492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 202,
                "y": 150
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8333fda2-e99d-46d7-afe2-b9d12df5db20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 218,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1e4d2c47-80af-4b26-abf2-23ee044f6445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 137,
                "y": 187
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "08ead613-18ce-4e4f-a7fa-4cbc2d0ca11a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 65,
                "y": 187
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7e4bf936-916f-402f-86da-0965b7f9e310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 151,
                "y": 187
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3f7d69ec-3103-45cc-925d-e7d5f154f0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e54a0d0e-076c-4fd0-a039-fd7a9e51ddab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4a4a2ba5-684f-4aba-a00b-cc85702dc64f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 2,
                "y": 187
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e9541678-e512-4f89-8a2b-3a0c8ed1cee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3867cd94-41d7-4269-bc95-cb4bb1364206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 47,
                "y": 150
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3351b19d-d78b-4f44-80e5-a16a08e82e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cde99b2d-d3a5-4678-abbc-b3d491c8023f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 162,
                "y": 150
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "af73cd86-d3a1-4755-964d-4470442ed9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 202,
                "y": 76
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "179106e9-cd1e-4663-9a5f-a547323d978a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 113
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7678d061-ecff-41c4-936d-ff6034bc93d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 113
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8b754e46-4511-49be-b3f6-bb9391ba1a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 113
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e04c8253-2088-4c9a-8558-afc953380731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 157,
                "y": 187
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "25789198-c3b2-4ce7-8c37-0521255b51dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 129,
                "y": 187
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "51fc8f8f-6012-4fbf-ab8f-95434008707a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 130,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2c4be325-3fb6-4bd0-ba61-aa5eb712c8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 146,
                "y": 113
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eca78f5d-e876-4a0e-8508-d0ac49128ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 162,
                "y": 113
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "300b1c71-3415-473d-91d1-f1c1d17c6f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 176,
                "y": 150
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "88a4b5cb-ae23-4f1c-bb51-8ee15b2166df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bd9dcb9c-aaf6-4659-bc3c-a796c9ebbc9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a5543c9f-c7f0-4f4b-979f-c5d8fb78050c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 34,
                "y": 113
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c806067-9b8d-4540-b332-b91e52d2017b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 76
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "89952e94-3de7-4e18-bed3-564224b42714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 83,
                "y": 39
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "85bc91a3-2d0a-460e-872c-9ddbe7be5278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 148,
                "y": 150
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "51dddabe-fe51-494c-9631-dec9c0e7940c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 120,
                "y": 150
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f8faed9d-9dc6-45a7-b6cb-9453f75fef6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 23,
                "y": 39
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "df3d1fc4-646f-4e25-96ac-fe7b404b2f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 141,
                "y": 39
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b7c1a699-01c2-4c7d-910c-19b25b8cb4fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 163,
                "y": 187
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1e128bf9-470b-4977-b211-b617e6e5f2fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 187
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1b0677ac-fd99-4a0d-a4f4-6d12966a104f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 179,
                "y": 39
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c19be6c0-40cc-42d8-8dce-7269a8e4f3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 134,
                "y": 150
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9297358d-e9df-4f41-b5b3-265ac1f592f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a13d4a13-42e1-4bcb-aa27-f48dcdbe0ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 63,
                "y": 39
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0eaed9f6-053a-4542-90ca-86f1da8e56ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e9b5ee00-8307-4a35-a8de-c779524c4a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 106,
                "y": 76
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "347587ff-b39b-443a-a5fa-f55aabfd80ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b02a5024-36d4-4db0-8b9f-a272b3df7f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 215,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "88ea266e-952c-4746-995a-8338622bda21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 122,
                "y": 76
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "95313671-0a2e-46f9-85f7-9d9578bbd8aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 38,
                "y": 76
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1ebcf3a2-f399-4cb0-b3f8-e52057a0fbeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 233,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "34862541-525c-40bf-bfdd-9d58f6a9c8d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "395a4bea-5693-4852-baa2-9c2d6136eadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f2ba7f55-6349-41f5-afed-edb67819814b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 43,
                "y": 39
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "583bba4a-a0a9-4605-9293-218a43844dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 122,
                "y": 39
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4d6ce959-75bb-436b-bf68-04e1e7862203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 103,
                "y": 39
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "152f1dc0-6bff-4c61-ab77-9459b68c0e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 187
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c555b888-1f7a-4b9f-be8b-41838cdaf1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 77,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7509a40c-f543-4b34-813f-673872e6dbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 187
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d678dd01-221e-4fb0-a3a5-7e2df246865c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 138,
                "y": 76
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e5c6ff06-b497-4798-94e8-4306160497c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 62,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "949e017a-48c3-4774-84e8-53371706c3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 121,
                "y": 187
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "725ef8b1-fec3-4006-bd80-6e71d7af1ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 92,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d2630733-3832-44bc-82f7-10e30aa57fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 170,
                "y": 76
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b5372144-5935-4d12-8df2-b5a152fac7de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 106,
                "y": 150
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f65da4bd-7aab-471c-8971-ea79d7d44606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 114,
                "y": 113
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9b26219d-92a8-4a52-a937-9c37c9251f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 113
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "357231a7-c4f4-45be-a22b-87bb0a0270fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 227,
                "y": 150
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b7de7210-c00f-42bc-8337-45c9e39f64e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 18,
                "y": 113
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "64634bf1-2c71-4fb0-9350-eefde8762945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 17,
                "y": 150
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "708ad599-b514-4394-ad7d-ea60472abb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 144,
                "y": 187
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3432dd5a-dca8-4206-8f23-e343662ee303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": -3,
                "shift": 7,
                "w": 9,
                "x": 13,
                "y": 187
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "953cd2da-d10f-4a69-941b-b14279c07cce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 150
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c776493a-0b3e-49a7-84c2-f7791d29c52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 185,
                "y": 187
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8a7e1f1f-f835-4b51-a758-6f5345c3bc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "22c53ac8-433b-43ae-83d5-3d138c9d9a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 194,
                "y": 113
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "371fc42d-7a81-4f1b-8acc-e0f9779d7e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 55,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1ece41d6-777c-410d-b2b5-fa58880122c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 98,
                "y": 113
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5f1da945-ff0e-42a2-b8fb-63a50ed69339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 154,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1d6ca1b8-a46a-4438-9bc6-2079a697df86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 150
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e13a9d96-2dc0-4255-be76-40311656eb68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 189,
                "y": 150
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5c2932a-3fd5-4cb2-a929-69b4d37591a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 215,
                "y": 150
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8e6df818-66b6-4564-852f-c9628014473e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 239,
                "y": 113
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a2b24e69-738b-499e-b8da-66652a17195c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 89,
                "y": 76
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "55d29ee8-5b23-4077-842f-27a42724cf19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "feec494a-7a5e-4562-a3c4-7fc237fb069b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 186,
                "y": 76
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2c9e8010-cb9d-472a-88ae-f5a9f7a72571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 72,
                "y": 76
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a8cf7ca0-8262-4acf-b733-0bef74acf978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 224,
                "y": 113
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0496a1cb-72ea-4b2e-a3b3-796246bce592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 35,
                "y": 187
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "577d0967-dfab-47cd-916b-6fb35b2dd56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 180,
                "y": 187
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ab229197-05eb-4418-b076-bdf4f00aae28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 45,
                "y": 187
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d2003ace-9856-4a84-a992-b1e6214d0712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 197,
                "y": 39
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "e8044913-183f-4f58-abb2-fb1426b8a605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 190,
                "y": 187
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4d7c7e81-3b01-4f04-a5dd-0e2f544c852e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "c82ed5af-897f-46e1-b035-a186f3bf6ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "fc297fab-d32c-43e7-8ff1-f6d25aaa602f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "9e6341ba-c217-4785-8dfb-67e679bb1579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "6d51c419-b08b-4c69-a660-74bd8d904514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 106
        },
        {
            "id": "38b629d0-bc6f-4763-8c1a-2df32253d35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "be1c3d67-981e-4c61-a816-f2fe6f1b531e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 74
        },
        {
            "id": "49d8f1c8-d901-48ba-8c6f-5f2f60b43719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "641ca399-d7fb-436e-9ea3-580c90294328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "b782d72b-043b-4614-81e7-c3b8edcf5d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "86b121af-0fbd-455a-a94f-80906e4dfe36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "e8b90cfc-e2f7-41f5-9489-42a3d8b69eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "cb723b8b-9a85-4977-889c-43acc1768160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "ede50dfd-6f0d-4867-acc6-acf84bcf5325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "6d3a2b83-98e6-4133-afd3-c6112d5652c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "53b34461-9076-441d-9b2e-a19ba72298cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "65e8ff83-27d0-4a0c-934c-9564a362f82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "58857f77-f99d-4556-b924-1f24a9c8c9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "17f2a056-4703-4bbf-b64f-346ac6dcc186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "313a0c26-da3d-4589-b376-4c44fa2dd86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "190a70e9-3ab7-4bb5-854f-08d46a311fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "88bc30c1-ea4b-4bba-b54c-e492e5f0398c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "cdc04e6a-b53a-450c-9deb-2f4f03840d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "d74b7300-e613-4c0d-bd40-5ef3c88ca3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "d90ab7fc-e3ff-4610-adb2-a2d978cf0440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "7a287205-d04e-43f8-93e7-701e5454cffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "e3b1e5d9-40fc-4032-9bc8-8ee3582edcd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "71ba7c4e-9a93-4252-9730-b19a339cf904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "dd411b57-f92a-4eeb-810a-9d0c543d9a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "fd525603-d876-420c-afa5-511ec7f29d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "aadd55dc-19ce-47b6-9ebb-4a651ec347bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "75d29e81-c31b-40ec-b158-d1873839f3de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "488cbde7-5d64-44ea-bcad-47addb802e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "69d75773-3178-4e5d-a70d-ef4ceb25286e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "3feac855-0123-4a45-a313-e1a5d9912022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "2ea4dc3f-a81a-47ac-90e3-1c35225b4cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "0cfb9a49-b62d-4ded-a101-89721e9262c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "93bad72b-336c-43cc-abf9-2549a8692119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "2b0f7e6b-b6c9-4028-918d-a6da9eaa80cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "19eaf2ee-3766-48f9-9e89-a4901a599d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "2cabfa54-3f30-42cc-9f79-2efe99fe184b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "200e2f03-d75f-412c-b4ba-550df37c598b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "515e5d26-60d8-4cd9-9dd9-28d369f25166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "c98804a1-4e20-48ea-ac9a-fe71a8763009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "dc7740ea-46df-44ae-b2b5-aaa1598c28c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "d032fd79-957c-48f6-b2f0-8015f40ccc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "0d114f02-bf3e-4c60-a2fa-8f582b34b62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "772e59af-5b89-4e0f-9896-603857cd2f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "6fddebb7-17d3-4b14-ad57-d1fecf178d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "2a69807a-6b8c-48b3-a2c3-48d90ad45b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "531ab206-c99e-47b0-905a-bf063d91d338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "441357c6-e198-4571-9e16-9c3371d42b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "b39ad1fb-44c2-455b-b3d5-a607cbd9e2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "67f86c5b-e51b-4c65-96fc-45373e5e2b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "deb639f2-fc4a-485c-a76e-002e66f53719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "0e77c149-52c9-4e5a-a218-a427ae18bd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "e695a3a3-f0c9-4626-a965-ad9776c77c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "7db79686-d394-4bc4-b3b9-864d1b8eda5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "7add1f77-369b-4b77-b322-75a84c46dcd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "28b406f2-14c2-4d1b-bdc4-13770cd01631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "113441b4-3ce5-4133-ac0a-06d6d7f4f144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "6cab5f3d-890c-499d-9790-e9c22e915948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 59
        },
        {
            "id": "57cafeb7-e188-4c4b-b540-dc671c92aea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "e5066adf-1792-4d7a-9ce7-8f54d3858936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "529759b0-1546-40e0-8127-7a7e68418b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "7a3e0e20-64b6-4709-b2aa-1e3ea5f7b15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "9d2413db-0235-4c16-b44e-e45c0b2ab0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "8c1e0d6b-abcf-4b20-8083-8daf52c5d555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 88
        },
        {
            "id": "535290a0-f700-47f0-9da5-de4040ad96e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "48171c19-7308-4689-8521-358943fd613b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "b37a3d2e-53f3-4f31-b09d-1be801394ba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "8d577c43-eb38-47dc-a0f0-493e92e6e6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "52821f3b-7c6c-490e-a764-41831c0e4144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "2c4fd618-78d1-4ac2-9a4a-5fe99ea4018e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 237
        },
        {
            "id": "cd12d516-f192-45f5-b93f-88a413f30717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 42
        },
        {
            "id": "9b7b3ecc-03fe-4bac-a86d-97fbc057b4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "08f21684-1e69-401b-aa5b-f5da2921e080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "43e3005c-f3ad-40dd-9646-e90bf2ac3774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "d1956047-fa32-46fb-8b4d-5d42dc595a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "cae6c506-7e11-4aa2-af80-26a479d396eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "7db64b83-7eb6-494b-a64d-ae2a7b1e42f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "0b7a07bc-5759-494c-a68f-cc8923a7d8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "1b234239-96aa-489d-864c-8b6a19a3e66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "e17d6dc2-8174-422c-9bf8-bd66661c3e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "6193cedf-b916-4152-b4d3-d0455dc6071f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "014436a4-4b33-4b7d-9709-c03f5bccc391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "da492163-8783-441f-98f3-91e27999159a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "b358d3cb-7850-4383-9f26-c5078ddef78b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "b0b84eb0-6df2-4b0c-88cc-8802c8e3536d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "f7277fc9-80af-4cb6-a640-faa04e901e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "02a78f0b-04ed-4d93-b4e8-d1a7ecc60584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "0e5aa407-7243-4383-914f-443d98b7c6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "20d92507-7c02-46b1-a4a1-5c9ab2c51caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "c355bee8-1c4a-4f5f-b098-78ecc42638a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "e58c68f7-0164-4307-9198-9fecfb68d05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "fae586bd-510b-479e-b554-7db6c83d7023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "62a69651-6c28-4e86-81ef-5489d3fbf01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "68cd8c8e-1813-45f6-9b87-4504d2bad5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "aa05d4b1-de1e-4140-8e9b-1383d0c56a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "f7c3f698-8c2a-4734-b615-1908de961b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "f5589ea0-d2c0-4340-a2ef-aec2b6a7e6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "306fd7e9-4d0d-4339-a3f7-c3e1f0009b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "80214688-431d-4c64-84cf-ad421bc87e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "f4ff33e1-0f97-443a-9c63-f15fe4950da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "709c5c6f-bec5-4eae-937a-43b87d034d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 87
        },
        {
            "id": "62d73ff5-1f83-4c9f-a221-d7c2b31f8f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "4342240c-cc32-4d17-938e-b97c76c29d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "d7c18289-10b0-47bf-a53e-814e11496053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "7db08fdf-c5e9-47b6-b9bf-0cd2f1e64dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "3c13aea5-9d10-4d42-aefe-8be744e92f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "0a38ee6d-b23d-47b9-854d-b15ab76d81e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "d78eaec9-9713-4c4d-aed7-62c31649bebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "c8372ce7-a41f-4221-a68a-298f1fad8288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "25f490f2-22d8-4935-bcd5-ce5c34f4e434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "42a4bbc9-7080-486e-9ba2-0819b4e1b15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "fad9dcb3-74bf-4a92-b098-4aae5fc8a5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "75b32681-74a0-412e-bcb6-173374bffd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "872813b4-2c0b-4917-b5ae-6110e9d1f4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "5616de9d-b91f-445e-b0ab-792861ae277f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "448ac85d-631d-4cd7-89ee-4bd8280506b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "c054327b-5245-4db9-8af6-4297838695c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8230
        },
        {
            "id": "5c57add5-0553-46df-985b-666b42fa914a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "204a1e9c-9a3b-453d-9cc3-c5ff6a977baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "dd675e2d-f79b-4a6c-a142-301410a8fcbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "9a6f8ddf-2863-4939-9027-81724867b5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "92c27166-6531-4411-b6e8-d57f751319b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "21d44794-7e38-4c0e-bfdf-a5d40232a469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8230
        },
        {
            "id": "c01d07da-2142-46c5-8d08-a47e65f59111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "339eb80a-4523-432c-82ea-ca88dc75ee5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "1645d274-5302-43a9-ac6d-79837111389e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "e78fca18-6335-4384-ae47-f9f6296e6498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "023e699d-b21c-4bb9-b9ce-fe2b51ad59b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "a22de1b8-1d28-4700-99dc-f29aa023b93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "3b1e7c4f-c682-4e30-aa1e-6aaaf6a8f520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "b15ea29a-20f6-41d5-85ea-9f1fa49c4e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "560a1381-bdeb-487e-858b-8eca5f550c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "d7754529-bd24-4524-9739-d184ffbc4325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "88532d4a-8b95-474b-8c62-0bb382d909ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "c80ec999-6329-4a27-aa06-f2ffb663074a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "5587c568-5854-4480-a1ac-8bc4076235e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "bd15c7be-2882-4e9d-a700-daf76c3c556f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "fb75ff98-2c37-4be3-b7f9-28be5725ae8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "56a96c1d-12c4-4775-93d2-cb65c2e37532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "a902a7a7-323d-4c87-bb33-9b56ce7664c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "335f9e64-0c48-44a1-97aa-6c8286b1e389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "492104d2-025a-4664-9c29-a7e3c4ec0b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "51e34228-c611-46d1-9cda-d14eeb4ea815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "3e9bd068-c28b-4420-aadf-28c25a6325a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "2498fb26-412b-46f8-a0d5-763e441c3c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "d08e9d09-7598-43e9-aebd-93a88911f0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "96735b49-7542-4fdb-883f-188f87ebbfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "254e7297-fa2d-4c8c-b6f0-97687b33eec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "03ba9305-4c51-4904-b28e-99a39fea7523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "9eac30cf-54c8-48bc-bd40-4a2dc65f0fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "5f9ecb9e-e840-49af-a024-29adef4f99b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "8555cf9b-8ba2-44a8-b1ac-455680926fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "46f02a48-9f63-4a22-96d2-594d985e55ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "7a8512f7-b36b-41df-9d50-401d6ec1ab5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "1e017cec-bafc-47b7-9eba-fcda1165c5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "840406fa-9817-4a90-b37f-d8e0acbbca80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "febe93ef-ca8e-4caf-bc14-66a94dec5d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "7d94484b-11fc-4bed-846f-04b61d9e68bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "bec48dc8-6f13-4938-ad01-df1649de20d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "68e2f3e8-ed53-4e5b-83b4-df99c2d9fcc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "01d8be45-96b6-495c-8649-f297a016feb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "c398ebdd-7f5c-468e-8fef-16a6a91a6d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "fda015e2-4860-454c-94fe-1326fc967451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "472f7785-c6ac-4c40-a8ab-4b24f25e196d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "efb59d77-39f5-4fed-94ae-2a91962aef90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "4470a6f3-8205-466a-a930-620f6c0b2784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "6e20991b-d586-4125-8e14-b9d977e14cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "7bdc6186-542c-407b-9e91-953e7a8d61d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "b8081f55-3d6a-43fa-9b2d-74909a8e7cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 120
        },
        {
            "id": "18a3173b-d9f8-4ecb-a6e6-336ba05c462c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "918cb789-9612-4f17-a9c0-d18b793cf221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "4aaff0f1-15b7-47ca-83d6-e1775791f720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "b2a58c72-fe08-4dac-9b5b-a5f4665c8ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "b5517071-1411-45c5-92c7-105c65044c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "ce7bae87-7311-414e-8784-0867f1bcb649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "ce6ec7ff-5820-4db1-8551-b0a753fdd6eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "a64ff13d-8e83-424e-91af-058e99976a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "6a345fe6-1a2b-4d0b-bc98-e4f048d73b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "3e57444b-42d5-451e-81df-d9aed70955ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "908f27ef-beba-4825-b6e7-32cd42cafe86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "35dfeb7b-1ea3-4c94-ac31-6fbeca420aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "df5b68ad-4f38-4226-a191-d0dfbca449dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "23b1f789-28d0-4ad6-9cbd-1813b4ecd12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "88d29aa8-604c-4ed1-a08e-a130169ff336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8230
        },
        {
            "id": "c469431f-8768-441e-9608-4923e08ae99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "3f533f67-9b02-4744-b479-5141034c6069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "f4ed6349-a196-4346-97e9-88d6187a0d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "fe2a340f-515b-429e-8224-050007fcdbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "ad2a3217-7af0-4f00-98ad-d8490125fc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "d08052da-0900-4de4-9f35-bc1d1a057aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "899bc56c-cdef-4852-8809-e7de8e0d69ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "7ed4e225-84c8-4b58-90cc-490134afd3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "191a1de2-58e0-4f97-a44f-f01bd146ee52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "d00a7ed2-efed-47a0-b224-177c4da39d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "2f33a9b6-605a-4c61-939b-a19107d1a12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "5c3d3930-4520-42c0-91fc-88ae9966c759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "f09e2707-3ebb-48a4-9ed0-976dbe2c72c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "2fe420d7-4d0c-4d50-87b4-38a661eb0fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "8a395dea-a2f4-4961-8049-dff02440c211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "c2ff4bea-68de-4fbd-b051-b3395adbd8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "265706c6-edf5-4564-adfb-64363692a321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "c74b0172-8b23-4c0b-99eb-2e34341f65f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "09db4d32-470f-43e5-b3d4-ba34cb1bf4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "e10fa4ec-bdaf-437a-9f56-77ce5cb9720e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "cab4cf6d-581f-465c-aeb7-19ecc7e560ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "feea76f9-e0aa-4597-9790-c25866f73e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "65408142-1dd9-480a-bf6e-f34bab5ac505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "16662cea-7c9e-4a74-be97-bb30df0b575a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "981c2d51-4aa7-4745-a3ed-69f29c92d1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "81b9e135-4226-4e14-82bb-deccf701bf30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "94b4c668-953d-4882-8b53-cdfb79fbec04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "5014bf49-c3d5-49c1-9361-b803a9d66745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "ef8029fa-a3b2-413c-95ae-923638988725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "3f0914f3-4cd2-4fe3-ab51-67fe65594a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "41abef82-6949-48d5-894f-97ee4c74997e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "27d07667-7bf7-4cc5-bbc2-6110f5c2bc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "20fe080a-90d8-4915-89be-e51adcd25e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "01d75378-7586-4099-ae1d-f93789111ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "82843e4c-bc2b-4e43-8437-208a3842b5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "33d30f76-19e2-417e-be51-88d433d4c41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "f3143594-c0c2-4921-ad3d-d209cb0b2a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 84
        },
        {
            "id": "d1d43fc3-16d7-4448-939e-a8f9eed5a51c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "c7cdd411-31c3-4bd8-b382-7f0ac164f4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "437bd0d9-62e5-479a-8c9a-64779fea27fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "9e56243f-0b42-4fa0-824e-565db7fc0b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2ab97068-23a4-417a-874a-b57b5280fe27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "1bfd552e-acd9-4686-9143-14e575edcf70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "30531555-da8e-4e79-9dc5-e18377b7dbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "89fa5a67-f801-41a8-acc9-666bc3edad0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "22fe1d8e-5a70-4524-833f-22ab4f089369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "ceb47876-c1d4-4a81-812f-0d1024bda505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "1b713582-0975-4f05-9867-88bfcc94dfdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "4bb38ded-2b40-4e06-9fff-ca8007a81442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "310d12ef-a368-4bac-b033-ded5a4ae2726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "f900913a-fe82-4ed2-aefc-2d522caf2909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "309dfd52-4364-46e3-8d0b-54419480436e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "8362917c-8ea3-4f60-be40-3db45b4d1fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "a7392561-d919-4023-9036-0edf974c8da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "c17a0b1c-8dcb-4aff-96dd-495da30e2846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "bce0dfde-1137-4241-94cb-7ca6f1f955b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "a0dacdec-a473-4bc2-bf9c-8003f3f3000d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "47367fef-4efa-4a2d-b309-4bc076419a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "fdb78f96-ebe4-4dba-a415-ebf5b2827afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "a0f537c0-7e12-47c3-b951-b7241b759352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "7944d9bf-2552-4e8d-b725-7b12c5d2e84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "c26a93eb-2222-4225-b84a-4528b94f5bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "a33e5f4f-1de7-42f0-8e97-d13169ad9599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "4459e76d-42f3-42b4-87cc-5e0ef567643b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "598abdc2-607b-43ad-a4f7-d8dea7aeae50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "91b87230-4329-4ef9-94b9-99fd175002b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "d68334c2-af5e-4bbe-a06b-6cfd6cc895e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "d6ce1c8c-21f1-4573-a480-191b188765c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "e112f653-c2d4-458f-9f57-abd66eb8c702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "eba18013-9e29-4cb5-a964-dbf99a898189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "7fdac9e1-ae0f-44d4-8371-771ae5680e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "89e77f5d-dcfb-4f18-9063-30f71d10114d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "11e1b749-e021-49de-95c9-975594638311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "7f036464-1cd0-4d3a-846a-67d284009df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "9c394ca0-a7b6-465c-a35d-575ff0ed2cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "0f6a5b0a-0e1f-41fb-877a-4ea83818f814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "cd1ae917-401c-4ca3-b3b5-422be46d36b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "a252870f-0d65-4832-b02c-8fe49b1484d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "fe7a2c87-ac6c-4de6-a231-97a867c86a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "676ac97f-5238-49d2-b255-88007fb40c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "2610a716-f9d4-4871-9acb-bf287b9c2bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "b70aaa04-392a-4b1f-9e55-b9b25f3ad8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "7db78348-ef80-4426-b986-007794a75b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "ab3f8c40-3c5c-4642-bce6-457c0b23cd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "6581a69b-4472-4ccb-a5a9-3266a05c6365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "8d5fa2bd-658a-4a62-8fc9-afedaa6b4bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "bc0a89e7-d391-4876-805b-1fb5e3f357a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "565ed199-7ed5-4929-a097-abf3a425f203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "8f14cd42-166c-4c66-ba9a-e36c24657991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "fd0ba53a-9434-445a-8186-06cb10964487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8230
        },
        {
            "id": "a132aee6-7a4a-44f3-8bcd-01af7387d439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "53aa6fdb-56f8-4e3f-9c3e-93dc2de0185a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 84
        },
        {
            "id": "be3d0c22-fb17-4638-a20d-1949bcdaa81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "5c986337-18fb-4a25-8a55-810d90af4b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 106
        },
        {
            "id": "978eedb7-5398-4b22-b511-9c1262647dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "2b3366dd-5140-44b4-a292-c14e247f06bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "2627fef9-a13f-4c82-a88d-994a45c3d613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "7ed1e780-41b8-427e-a306-1e3115d2ce3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "b9e694d2-babe-4e48-a63a-49b54c49f458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "33df31d9-1dbf-45e6-bda6-6bdde7d47616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 41
        },
        {
            "id": "2b5cab29-cdc5-436a-bd76-c8702024cd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "5e27925b-21e3-479f-bc23-52064a4ca265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "bf80e85e-ae6b-4def-af27-e46a3871f02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "9f9f0704-0eb7-41d3-a295-09bff4460529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "bca18b68-3cb2-4c91-bd38-edd0b10fe05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "54954a44-6c5f-4e70-ac66-0db6a4233239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "6aded88e-5c1c-437a-bb6f-5285d5ea4dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 93
        },
        {
            "id": "c92dfca8-f457-4c2a-8026-b19804f527bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "6794852d-767c-454a-98d5-ab95219301de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "d4739d3a-7b08-4b0d-a420-9d5cc952f3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "f49b3575-e9d3-42b6-b9ef-5f19fc7ae898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "ec06fbdc-954c-40d7-ac5a-2d83c93df33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 236
        },
        {
            "id": "dfc018b2-5471-4834-9685-d192c1889db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "8c889dd7-a3af-4395-a6e7-bb75d49f55a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "5fa8facc-5999-448f-99ce-e21ad9d16298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "1c6ac12a-7d32-473d-b853-74ac2a3dedab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "1d0721c4-916b-4f15-b030-0c1875db25a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "8d32adaf-7036-4b19-96f7-9e2d8ca36bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 106
        },
        {
            "id": "71bb7b3c-ca67-4f77-9805-1b42272fbcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 106
        },
        {
            "id": "e6ff5fb1-d632-4ad7-8ad7-ff268c51297f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "c23c3509-8951-4934-ab5a-d6ad76a3f446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "6b524005-e615-486d-8cdf-c88a8557297f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "64e83dbf-148c-4770-b745-1eea3202a063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "25ad8167-6366-4c3d-b697-d2df2ec50c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "e98e05ad-bd1f-4b5f-9959-1663ddce4fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "8ac05b75-062c-4169-8d1e-cbe659f395d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "aae9890f-dd4a-4518-a833-5a5d6b2799d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "417977d5-3b17-4c26-be3b-30588fb43fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "7cb7b7f3-4f1c-42af-a0e9-da6f349e4126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "0f1dd576-3af1-47b4-97dc-dc30d0acdca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "eb36fe9c-ad03-4265-abb0-fc49051c5b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "0806bf6f-06c4-4d3d-8b32-5227dca14410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "043bc875-a0bd-410a-b2d8-4b5fbcafe80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "e4852f30-bf4b-42d8-8a0e-dedf3b24643e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "ed0174da-c6c6-4530-87a7-67002b2b64e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "58958874-7dec-4159-a4a2-61a9dc661343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "1b0878b4-83f5-461e-816e-920bac71eb65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 34
        },
        {
            "id": "e21d2a68-9afd-4e5b-a009-c23ac1cb5f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 39
        },
        {
            "id": "a66f7219-3a87-4878-a70a-118ceaa2a4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 102
        },
        {
            "id": "e49ffec6-02fc-4e33-b18b-afee6f4d0580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "46988566-90b7-4b16-b80d-5ce4642bc912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8217
        },
        {
            "id": "842e3ec1-ced1-4a08-9caa-491206071611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "3edc207b-1f6f-43f2-ad4a-1ae0008aa920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8221
        },
        {
            "id": "e614d551-8a15-4140-b92b-5e3e62932a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "e31bef9e-3835-40cd-8983-414f383662d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8216
        },
        {
            "id": "eb9b6982-485d-4c1b-80bf-a4200ee0425a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8217
        },
        {
            "id": "f09f4773-d537-46cd-b529-5d21ee11839c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8220
        },
        {
            "id": "4550de08-0a0c-4f2e-b98b-ec4bbc7a160a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8221
        },
        {
            "id": "653e7283-e71b-4f1c-ad99-5a5869e73a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "f0a22930-bf8e-41c2-a312-348803455bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "eb488232-5489-458c-8d75-37e3d174dd0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "2e6d5ad4-8779-4927-9e2e-d5998e3eb55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "670ca063-a030-4802-805f-b119e19eb7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "86560829-9c2b-4361-a190-76a305f9c8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "670322f0-5316-4d86-96c9-ce2b0465e9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "aaab1761-f67d-4c4f-acfa-242db66d783d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "8b20638a-e53e-4156-86d4-fba6b9885181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "820ff695-a0e5-4193-aa98-a9b686cdfb99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "c8e564cc-3aaa-4f15-8664-dc9f261424c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "b2ac8e72-866c-4e57-abd3-4f00119ea6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "be06ba1a-a658-4d05-99f9-b819510c7539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "5e0743cc-18f8-4918-a8f1-c177ceec5582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8216
        },
        {
            "id": "47f6f4d3-b1f5-4b88-a10c-b814cc88129f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "2049a216-ec78-40e2-8aa3-5491b922b66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8220
        },
        {
            "id": "866bdd70-2867-481c-bbe0-6ffda8d9d5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "acfd62b7-36c7-4f99-a79a-740858baa965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "29d1cc7e-051f-4c12-908e-f5851fbde019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 45
        },
        {
            "id": "0d433ff0-3c51-447e-b784-07e30b4a8b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "64754084-adb7-4cfc-8156-2384d7ed3da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "d9253a18-479a-4a6d-a727-2c74d97bbf43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "a36c984c-5929-451d-951e-2ae79ff180d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "725451c1-ac3c-4468-ace6-bb40257060c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "ce9fa1a5-28e1-44cc-b865-c4bc9d281b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "f1baf66a-64b6-4b2b-a6d3-c8c72eb74274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 224
        },
        {
            "id": "37b4f410-26e8-4d61-b927-8f5e93e03d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 225
        },
        {
            "id": "1ddcce98-7a69-4e5b-a21c-1f85ffc6dbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "31c23941-0f18-4554-92b3-7dc52312ae36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "5ec02132-7390-4c85-9aa2-5f22ab903337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "64e37b51-ad49-4a47-ad8a-20d7c5fb77a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "908b4b73-7503-47a2-ac88-b65a6d4a686b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "aece14e6-014f-4b3b-ad32-5915377326cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "55ebf187-e54e-41d7-ad28-c3df7cee6f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "43f3d6ab-fa77-49e6-bae8-a99750ac8f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8230
        },
        {
            "id": "edb5126b-2193-4b24-a28c-59c34d1faecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "c451b48a-9f88-4eec-afc8-c06fd65a3723",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_menuNormal",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Microsoft YaHei",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7753e0a2-fda6-4123-8d66-3e93c203c7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 122
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d4500fe5-6bfb-44dd-9646-48de5c504cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 132,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7a3056cc-78bb-48bb-a4e0-3491e57e9593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3bd258d9-22fe-495b-92a7-d57c429e1a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 93,
                "y": 32
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f7a86a47-8728-4959-ad6d-7c1867b1fa4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "836757e1-d387-4ab7-966e-480e117fdbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "089f3ed5-09fa-4c55-8917-a79ecbc4725d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8fd9f919-b1db-4549-91da-f7b6dca2d5ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 175,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "36775f28-9bba-4fd4-a253-e64328867700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "51d8c411-b7d3-4a18-a828-14c766bcf4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 89,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "96bb62fd-36c5-4f26-8c1e-39dbe23ba15c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9abdfa91-752a-40f6-91b0-7281e6c2de12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d1df1036-ef30-4792-9f31-e858c0f710f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3a8cc2d5-4422-451c-9080-bbb3701ea919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "62808c2d-cce5-43bf-896d-9ca90de0824f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 155,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e658795d-5080-4d2d-a948-b2c9f554253e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 71,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "30e64578-cdb1-44aa-9788-f6f57040527c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "66e8c748-34ba-4a6e-bdd6-3519d140b394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 81,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d3b9b24f-7740-4174-aa85-e06557af0cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b6424309-3cd7-49af-84ce-015dcc33cca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 79,
                "y": 92
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "674e7163-9498-409d-8e31-8f2dc504e437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6bf1c648-4131-44e2-8b0a-87f67caaf93d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0d581d32-6e04-49f9-a80c-fe396fbc000f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 149,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b2f7b262-c7f2-4b8f-8800-7045bcf93b57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c10fe5ac-d03c-47cf-b68a-9e527a179ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8cabcd71-77e0-44f8-b15e-adcd7a204552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 175,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "14c46401-d83a-42c4-951c-131763afaea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 165,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6be24f1a-48bd-408c-bce2-789ce4002b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 138,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5db7ff87-e9df-4866-8a58-302e71ff9342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 115,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a2f3266b-a1f7-4bd5-b33c-2f56a41a13c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "26d28b4a-1fa4-4081-87d0-b106e56a6464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 151,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d2a8ead0-d9d8-44a6-84f5-998a3d9a95db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f687fd80-b5e9-4427-810c-9df9d297b8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b5b48bca-a2a7-4bde-965f-c36451b2228a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3b5f1f89-6a5f-4a46-b2b7-ab91c704be1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 240,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "21cb28c6-a49c-4140-9a6a-0f36d45e6a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ec0fd5f7-7c20-4090-8403-7a647a8a90b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 63,
                "y": 32
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1863fd3e-2a64-42f6-8935-eea6480d6fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 196,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b6a0e67d-0a4a-480a-9125-63121b69f1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 174,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5c2ce080-5f48-428e-8057-6fab4f6baa5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e2d60406-c8b6-4ab9-915e-f5be09245fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 78,
                "y": 32
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e402b2bc-0249-42f0-a2e4-ad4cefffe7bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 170,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b34d9bd4-bcbf-43f3-8961-ee7bfcb67e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 40,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2893c662-a719-48b9-afcf-8dc961e56446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 206,
                "y": 32
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1aeb935f-5718-4745-8026-15d3d3b3660e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 185,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "515108a6-a4f9-41f2-a493-e4a871f7c169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a7c248ab-5619-4060-aa6a-5d7bb4541947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0baea279-40bc-46e6-86e7-e65ee1640498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "59214012-0695-4733-abc8-91342601da1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 91,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "28ea2b87-6ed4-4214-b925-fad362b0a571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "986cfebd-b359-4368-9c9a-d3f7162faf15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 178,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cc3e47ca-fc2c-431e-bc9a-6609ed9a440b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "196562f5-e980-4c83-bdbb-7966cf08f1ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 192,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a2ffc9d6-de05-4ede-a0a3-df7417ef6a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 48,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9742c628-42d0-4b26-ae85-4e99ab4c4ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b663d4d3-4afe-45fe-98f7-c4de96e8eb32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fd057c00-85a6-4a94-b8ed-8feda3b29d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f50592fb-77ba-4e04-8fc2-f4227cf4464b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 33,
                "y": 32
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ccec92a8-e377-48ca-8a09-1d3c70aeba00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "04c1df62-6413-443c-a684-f6f4862487ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d63e5a52-096b-4d42-8b65-c988a22dcab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 15,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "25f2fac2-0265-42cf-9aec-e043154269a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 105,
                "y": 122
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9241421b-ddd7-4fa1-ac19-36ef7aa64c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 234,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f4d74455-7a87-41cc-bdca-427a30c02278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 139,
                "y": 92
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7d43ad66-8933-4785-bcc2-cfdcf5ac45bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8e7066c9-bceb-46a6-85e2-9684d51c881c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 123,
                "y": 62
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6d15ba22-9c12-4612-b643-ded93a452456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 108,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9e8a97ac-9fd6-4979-971a-f851852e73d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 163,
                "y": 92
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9b3bc474-2ff3-4cab-a7dd-0fe3f5259017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 92
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d13b9a6f-cb5f-42d0-b4c4-583ad99468e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 227,
                "y": 62
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3d573097-a9e6-422d-9a33-a797f3dc4fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 239,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2e35e1e3-cadc-4504-ac24-ea8b661e24cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 214,
                "y": 62
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "785d6ce6-b3fd-4d2d-a689-1b7d83b73576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "841ff340-89f1-4a27-ab2c-bb6cebef7c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 126,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4878e0e6-b7ac-479d-bd3b-1ca5c3b64c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -3,
                "shift": 6,
                "w": 8,
                "x": 12,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "56ff1fed-9829-4af7-87cf-ae5c395bf387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 188,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c74e6f5f-2cf0-4734-866b-fd062b2715b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 160,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "19899a0e-db1b-4402-8367-7c0d701793df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7f7aa3ae-d24b-453c-9295-c7abc8b28dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ff972182-2966-43b2-b8a3-e7062646ea30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 164,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d9627e0d-d7d2-482c-8f95-bbbd157cd982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 150,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "46c6d62a-0209-40e3-9e96-0acc0aa1d67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3bd65107-f6c1-4ae3-a54e-91ff8b866c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 22,
                "y": 122
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "862479f6-6585-4aea-a311-9e12a052f880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 229,
                "y": 92
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "eb0024c6-2afe-4362-8014-1fd9f03a4df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5fa25505-983a-4cdc-a5bd-9d9bedbf6753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d8436189-9164-4724-8871-30a541202057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 136,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b1fbc030-7399-49d4-b0af-2a68dca6186d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "156e17f9-cf08-4e6a-9cde-577bd60dd346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e851cebc-2e4f-4dd9-ad1d-c662f4ab4dba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 122,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1e157c26-b601-42f5-bab9-a6723ca0b90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "85a2629d-ba18-4350-aeb6-bf146bc901ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 122
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5360c9c2-3ef1-40a1-8fe7-fbd07756f16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 150,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "42c63143-4e5c-406e-bba5-2a56b3c933de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 122
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "88f19132-faa5-4663-9354-40a98d7bf2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 136,
                "y": 32
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "26ce03bf-b028-4892-a11f-47f2be6e0ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 180,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "81913c15-6e10-4669-a1f3-3c29c9942c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "5684fa98-35c8-4fc9-b25b-22eb5d1d257c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "6577a422-d3f1-40fa-942c-687b79e0bd2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 40,
            "second": 106
        },
        {
            "id": "6c6bde57-7f84-4280-829e-70b4e0b4e975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "6866a883-f981-405e-ad81-3b4d7a0e3f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 74
        },
        {
            "id": "7464d1f3-6efb-42c3-b841-cd0d90d1e4d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "b2c84ba1-51f8-4e7b-973d-753fcdf25803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "e197f8fc-2aa7-4af4-babc-2a33a5fd0d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "fb77000d-f6bc-42be-a7d5-ae02390c815f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "93af87c1-3877-4166-a8d8-3e36b32724dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "dc98d548-2ffa-4b26-a7b1-ed4a60fe6999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "b4478365-23ae-49ff-9909-86c4facb1188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8216
        },
        {
            "id": "8578027a-1833-4a4b-9b03-4733558c0a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8217
        },
        {
            "id": "84015464-d531-4e7a-9ec8-2c937834144b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8220
        },
        {
            "id": "3d39df1d-bc17-4821-8393-bf43eba84f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8221
        },
        {
            "id": "4eff8efc-a289-48e7-98b3-6e21f9127117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8216
        },
        {
            "id": "a5d4270c-e9a6-44b7-883b-13523dee03e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8217
        },
        {
            "id": "7562efe1-917b-41e1-8dab-fa1ca5e71bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8220
        },
        {
            "id": "0be160b9-b837-400d-93cc-1f3fb8604f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8221
        },
        {
            "id": "4ed0203f-898e-4d5e-b737-d10555f3990d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 42
        },
        {
            "id": "6b6e1c49-8d58-4ebe-a2c1-fd7b61f19c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "cc58f6ce-1727-48d3-98f5-47bf7cc18cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "904dc69c-ead7-412c-9bca-eb47d01c09d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "551fec49-9813-4dc8-bcc5-7fca511fabd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "653aec89-e77a-4585-b161-0fae67bc0393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2368abb2-43e2-4f2f-8933-f24f0b4b0e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "c33d8f0f-5ba9-4988-a5b9-7c43df6fa9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "39ca1f31-4750-4801-a665-f6f9f791897a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "9af83f64-539e-49ec-9d71-324274506938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "15e9d145-7e7f-425a-959d-577c610f2113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "32a3508e-192a-464b-a995-fe93fe825e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "51515341-edec-4943-aa87-4f6dbd11ba47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "1ce60c02-0274-452f-ac34-5baa24bf7a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "1e9a2b9b-d020-4fc3-be98-933347578476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "9ddf8da8-c72b-451d-9609-a33d0608430e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "966a35c6-2f6f-41fd-8d35-cb0381ee1c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "42b770ff-401c-4387-bb64-13c64a70ecbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "63c05163-4c60-4327-8da9-3c46f4aa023a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "4eff7bbc-2d1d-48a0-bd76-ba71b1edba82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "a0a55189-6824-42cc-9403-0b0ccbd0c4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "0c9c0a06-f7b8-454f-b1e5-ab79ecf21333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "62097a7f-54d5-4d3f-ab6d-e8dd385d8e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "0133c073-bcfb-448f-871e-a50347c34824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "844e0ee7-084e-4b29-b199-cdd88d4f1b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "e70607b4-142b-406d-807a-6ab8e51688ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "6fdcbf08-94f4-4699-8edc-6af818da12fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "18214808-5460-43c3-a6b7-0d3a725b0756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "38bcdba6-5b7a-4c20-aed1-0c7675770a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "749a2af2-f97a-4250-b4c9-b082524fc6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "8ceb6850-acda-402f-ad42-5deed4c7d9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "19eb6416-a898-4646-b6f7-d92c2231e37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "7c318fee-d729-4b22-9e99-5bb386f6bb7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "5975dfb5-cfd7-43e7-b92b-2980106ed437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "11672817-51c7-4bc8-9e1b-5fcd89c84507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "59a7c809-a334-4a24-9f03-37ac27e5dcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "2d262209-d0f2-4277-b12f-041db34d5ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "0348014d-5d11-46dc-8d38-c1fa0bda136a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "ce29a779-301d-4ca1-9089-2afc18bbdd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "87b28c1a-594a-4b9e-97ef-369296a3a715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "4f22b20d-5289-44fe-ab28-8534cf37c958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 42
        },
        {
            "id": "cfc3d669-5bca-4fb0-ac08-ca1d213376b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "ea73a1d7-eeff-41fd-ba29-fe9b64fd6cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "23045647-1444-4413-910f-77cf82fcc24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "9a5fbeb3-6fe9-43df-82df-46a5c455e1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "452f9ac2-e5ce-4c24-bb01-bf39ebce88e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "e473b563-9e76-43a9-b312-96b1fa609a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "1f7d0084-32b2-45bf-9db1-184f8932ff51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "b4c3f929-d070-40d0-b09f-2480e1bd62db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "cd37c1fc-73a4-43bf-8500-b2a361f11032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "7006db3e-983a-489f-b3ed-b4c25496abbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c05150ca-33fc-4693-aaee-1a19fd320eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "62391af3-aba6-4aaa-85c4-85170dd989af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "231180a1-d859-4d01-b79c-f6e2cdf678b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "937551ac-85e4-459d-8104-9aeaa484efe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "825b2d02-7ef6-4fa7-be41-c9d24b8a619c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "3339f601-6156-4f33-80b0-f56dceba66d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4fd519a7-8a60-4d8b-84ae-68b467c6d2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "5e8a79a0-39dc-4f75-9e25-967f87a9a5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "a4e2e9ee-7567-4404-b9c1-717aeb18673a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "d1324fcb-bdd3-4202-912b-34ddd6956c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "ff361201-6b94-4e54-8a48-3d87794a09e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "f18e5c93-a112-40cf-8c01-7cc1dbf08532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "57f58c56-1f9d-491f-9bf7-5a00fa3538ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "7e4b484d-56b0-405c-81ed-f7f65795ddd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "cce95f47-8031-442b-96a1-25f3fc3d2758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "c736415d-2f4b-4d6b-8ed5-dfc709fb039f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "630b16e4-142e-480c-bb20-4e2d790b0cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "572420c9-62f6-4c65-a1e3-450d16d006df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "3e7dfac9-ae40-4ca1-ae8e-7cf4c83787ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "05e6eed1-02ce-4c96-8bde-75459fbca2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "dde076a6-de19-41d1-94a4-1405f8e4e360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "9019eefc-e362-436c-a6a9-3cc753be7155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "f64198f8-3c10-4c38-b3a7-8984aaea5fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "bc6cab52-ce17-44bf-ab24-10f8d9bd925b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "c29158d2-374d-4d4e-8337-d5c3f0dbcc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "c8543478-b363-4177-b1de-0f18f649708b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "c35a40ca-4c82-4bb5-ac08-d86b34fb3aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "b12778bd-fe13-4d76-8d77-e28c1338b7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "d0775368-8040-433e-bd71-d1d051353e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "dbbd0042-2ac2-46c6-b3a4-ce38a035f0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "1d53e249-019b-4499-a34a-86d7a53fb21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "c20dcb75-2ed1-400b-925e-98ec1d2d2bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8230
        },
        {
            "id": "75d6d2a1-c130-4547-9dd5-9f1ffd9950af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "26a8bd65-fd6c-45c7-bf45-4ac5ac1e8d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "737c55ca-b14f-4589-9ebc-be5081933757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "42f6b501-14b1-489f-97ae-6f6c6b3e3e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8230
        },
        {
            "id": "324205a1-d59c-44bf-8447-c64d77806140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "7c6400d5-2b51-4225-8965-6c1fbc405d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "e47ceecd-a1f0-4c1a-a958-01c28a2b34c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "fafe7076-410b-40a3-83ad-23e5b410b8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "e6800fa3-4c89-4cfc-88cf-54e09bd93633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "c1b34a31-09e1-4842-9e92-925bd22c2a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "4dc96c09-f7ff-4264-be79-884631287e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "31328146-69bc-4142-bbe3-565dd1ec49e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "f6aecb01-93df-4793-85dd-19adeb2e29af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "08295c8d-e33e-4658-b250-98f70a8b9f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "83e91103-db4f-4de7-9b73-ea4ef1415f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "41744e81-ed9c-44d4-aafe-603fa7dcbd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "73c1b150-f3ed-43c6-a629-490464cc2b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "f877ce67-6db1-43ea-91f2-5cbdb9e26d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "44b65370-1085-4c3e-b432-250c370c66c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "037373f6-2958-47b1-a1a7-f6cd184b2bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "e2d64dca-1e42-49d6-ab79-f9ceaf1d505e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "0bee8d1a-0c2e-4bce-9543-9d3f3bc6cfdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "969897c7-efd3-4b14-a8aa-c495a2bbc933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "a71cd26d-6dd7-4ff8-ae26-97037451bcde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "0d9b55e2-a627-4dd3-b483-0a8c8abf1c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "a929353c-01d2-4afb-9ef2-58412a8eda7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "0522e567-1be4-4c22-b634-6c598427bb6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "63c85271-a63f-4fcc-a3cf-f1466c82348c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "6a3df1e7-64d2-419d-a76c-286c83eca949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "96582b31-451c-4c01-8a79-7dfcecced7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "ed6c8f2a-136c-4760-82ac-2572717d5b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "ddcc784c-f6b0-4bec-afd4-f768c1a05152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "0d7bee2a-33c8-4d49-87d0-7db713ad5596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "052eb1d3-e098-45d0-8f80-525495e96c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "badabf8d-b751-49d2-9f59-c45f43e6f93f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "42a59fb0-13fb-463d-ac15-d5a221bfbaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "a191420c-709c-4670-a41c-de593b15e220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "4e465dda-0bf4-4130-993b-d80be5276648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "b0793349-2f2a-4428-8617-466abd4ba834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "6cc9221a-9562-4d5f-aba9-b6e82c7b84b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "a33d0560-4f0b-4f92-a98c-1b9be9fe430f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "6402cf9e-5bb7-4af1-9acb-94911cbf4c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "da35a401-607b-452a-b54b-a0dd1b2b182b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 224
        },
        {
            "id": "26f20236-e44b-4f7d-bfbc-a9cdf5a1a668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "bed80702-516e-4dd7-978f-192b05dc8fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "ec2bb72c-a2b0-47e1-8207-d2bdd2570a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "9a3db74c-f11a-469e-ba04-11d2f4dc9a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "1000697d-0709-4349-9ec8-ddaaac4708e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "0a9257b4-a025-4114-b36f-d3e83c20565e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "4814dc76-e122-4c5c-8734-5b4d1873e2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "1d2ab9da-82b0-422f-93a0-0806a6b929a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "d6e92af0-e481-435b-89f8-5b7a01ebd081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "1f6eb8e0-458e-4eaf-83b7-45fcce828215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "43ff4d1f-dd18-466b-a0e3-3393cec7c3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "e34795a8-b7eb-4cf7-bd76-c79daa4d5689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "b5fb5d65-24ec-40cf-af5a-c44fd9a519cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f3fbe250-1521-4864-a0f2-fde902adff8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "67918a0d-671f-453d-b227-e87c8c9bc200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "fc65cbec-c387-4f3f-8721-3cc0f51da3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "f03141e3-522f-4e4b-b3ee-2181f56e9e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "49ad2636-896e-47f1-a997-e91246eee948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "029e7b7e-e751-4429-a67f-99082acf47ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "9bcdf59b-2361-4693-84d9-6f0cd8fa42ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "c10e24ac-30de-41b0-9f19-92b37b1ba4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "08defe33-77c0-4eb3-9fed-42af8889d0dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "50de527c-1ffc-4c24-8db9-c8825afeb6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "fb97a88f-adb7-4e59-8e17-664592c54b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "dc8854fb-0a69-4a4b-a3f2-15d3d3bb120e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "a7d33947-8b9b-4b48-90a0-99909e56f623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "e59ff399-c1dd-434f-a2f1-3645f41fbe7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "fbeb8f8a-c0a5-485c-8b80-ca0e59d93fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "07b08661-2917-42e8-a0b8-75d67fa5f564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "de83cf57-7016-4638-ac57-7d1d044725dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "d4fb1a6e-b245-400b-b236-8ec9b9290f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "79bd320f-9021-479b-bed2-7192ea3663e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "a9054dbf-79bf-42e5-9e9b-f209bb916777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "3bfb49e3-ca14-4df3-8361-8b329390b9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "0d9d6287-8186-4aa4-9415-349f05b3d0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "b3e28f01-57aa-4058-a0c2-c10c3978a768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "da7fcd8b-798f-4e41-bc4b-bc723712eafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "dd34e632-6897-4fc7-b18c-c33df8fa3a9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "a23e0c2a-5eed-40c7-b7e2-e6da0030573f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "eb6ea32c-1d5f-4777-84f6-49cbfaed45bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "fa7825c3-bb31-4841-9648-1841dc0b5739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "c338fa1e-cdce-4c1f-94e4-272b5ef28ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "e7f8b9dd-234e-4711-9d30-621ac5ecd2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "db9ad702-bea4-4650-bf7f-94a2de6966ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "3ef11c38-bfdf-4b03-9655-289d53c56246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "4eb2df21-8b77-4a05-a199-106fb84e2c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "814b0a6e-26bc-42aa-9330-0da763735334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "c3e4a6f4-22d5-4c79-9c90-e418170002f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "3b30f5f0-8bc6-4da5-9272-887ace8414fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "99d6e98b-fd2a-4a3a-a8fe-f2c9981468a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "3f985664-39e3-4b15-a77a-89034832aa80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "7aeb36de-488f-4e56-8d29-7a8cde1c3c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "6701b8cb-956e-4704-895d-63c10e5b0b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "8f8cacbb-79d7-4eb3-bd7d-7033762adacf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "02d447ff-2d61-4e3d-977c-136b411ad236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "eda3cf7d-b446-4919-8295-c33c6818a136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "99323731-c9f5-4dad-914e-456c1225e7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "c5faac21-ba56-40df-a74b-e7551e809ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "da004952-13d5-49af-b577-628720cc5080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "be550f1e-5689-4a37-b07b-5be0f42b935e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "7b87bc63-2e21-4b56-8640-c997284bd760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "6c559977-c07a-4bd7-ae6c-9e336bcd70bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "754f661c-dd60-4a59-8599-c1190b070755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "d8a00ad1-0f81-4606-b2da-571d4e8c9e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "1d26d7d9-171f-4987-a173-d6780df88128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "934a1c02-24ef-4823-ad0e-7ce0614d46fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "37cda50a-0757-4051-b513-5a2d88332ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "e8bea5ec-e656-4c61-88f9-876bf2325d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "47e85867-91d1-4f64-a89c-1ecb3f04c02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "aa94e5de-9b5f-44d1-bf8b-e5513e36e945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "9e546c82-762b-44ec-8c02-54082446b622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "81a7828e-9edd-4f39-9a21-af0bf8f87a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "332b84c7-f9d9-405f-82cc-5afe9232df57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "8b282f33-e928-4974-8e95-da66e0871a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "265976ba-1316-43f0-9097-46658e85aef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "2528e952-db40-472a-bbaf-07281f892072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "39a860f2-a743-4d27-8e27-824f7dc3a7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "4d59a2da-582a-4a48-b451-e76cdbd4bdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "a8573bc3-6608-4af1-88e4-88a2f57d7b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "112a386b-6031-4466-a003-3ec7c563d862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 91,
            "second": 106
        },
        {
            "id": "3023c930-4eaf-46ca-a4f4-1e3f247dcb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "1adaabe4-40c8-453e-a543-15e017577386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "60976547-216e-40a4-b8c3-c0375d116335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "24361e41-bb9e-4fec-b526-ddffdad6746d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "ab6cef8c-7aa3-482f-ad38-64d76ba5c64e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "4ef7060a-d5c8-45be-ae27-99dd8360cc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "a0fe861a-46ab-49b4-9a6e-979d06264864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "f8fd3e0e-f8a6-4692-8fdc-bfef8e03c9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "a336452c-9d6a-431a-85db-18581b2c4757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "e7435824-27b3-41b9-b8c3-40f55f5fd0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "b14a64bb-31a0-40f8-b1a0-bf03458ca1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "4062c07e-e07e-45fd-a9df-e989c0c69181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "f8deff3a-0e13-4eac-997e-d8c2a74f41db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "1baa10a0-383a-476f-b839-288dd8b26634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "6c39c4b7-0dc3-4778-8b26-ec8ad4e0f1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "c545bcbd-12e9-466e-8f5c-b330ee133490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "89273923-ec84-4623-b9d2-1873b8a8c44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "bb5e2048-8e5e-4a02-8d5b-64cde5414b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "b160c47e-a93c-4048-bc8a-d0fb7974203e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "31348320-fc40-4872-9e9a-165e31c3c61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "c2590b34-83b4-40c4-b908-0264d54b2469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "88aa3130-5e95-42c6-8561-82fe9d67c56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "4535bc9f-1ff2-4402-b2ef-c96d24867dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "fce2d2c5-c8b2-4bca-b39f-87e22030fbf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "5197240a-89a6-4573-ba5f-0e222c0dc838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "743360da-50c2-47fb-9d83-f05c889604df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "51d07260-3ebd-4f99-b037-e595bf04f766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "fa0f75fe-8b76-4c3d-b998-5b071647e321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "aa74e1ef-18d9-47b7-be46-8f455f70166b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "efeda141-3871-4949-a3d3-c67fcfd6180f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "7601a4d8-7a24-494a-86e1-034787462511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8217
        },
        {
            "id": "aec369b4-9037-41e5-91d5-0cf52b3eae10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "60a97abc-180a-45a2-864c-2380f17987fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8221
        },
        {
            "id": "973f0b51-e8d6-40dd-baf1-a51ff1c021c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8216
        },
        {
            "id": "33ce34f9-273a-49fe-b130-ff0fbb6050f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8217
        },
        {
            "id": "4dafcded-bf8b-434a-918e-320ff7e4c87f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8221
        },
        {
            "id": "158d92e1-62dc-41bb-bf32-9fec4d3131c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "d34d02c7-9ff6-4a14-9d67-c2941d5e9bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "35ba4b81-e1da-48c2-a098-c111b69cdb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "8250ce83-6899-48a5-bc17-a19a611e4708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "f642dc00-1c47-4470-8565-6e2f763f51ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "1a7aca7f-e8dc-4dde-ab31-03d811c577c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "4325db02-eb19-4413-9af6-88d182324a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "ef5b82ce-d80c-4cf5-8f5c-84c85f852a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "660342ed-1046-4547-a81b-a8ac8cc08026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "f99e832a-6d3d-46d8-bd34-cd3443de7ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "dcb077f2-db92-4cd6-9441-9979a7b4b3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "50325f0d-008c-403f-bb79-9f213d0ae8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8216
        },
        {
            "id": "533da31a-947b-4060-a3ba-017ea188eb5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "320e9200-dbc7-475c-a9c6-f99ce0560742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8220
        },
        {
            "id": "f7b114da-f747-4a67-9977-3034beeea2a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8221
        },
        {
            "id": "ce99d17b-d48c-449e-8b1f-4edae46e171f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "ffc08329-d18e-4a59-aa87-d86b44fc7f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "627deb29-0d4e-4bff-a024-da425e330d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "0c3077c6-0e54-4b83-b4e9-54d3701865d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "f4d1989c-242d-48d9-95e9-244cc6db28c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "48402b7d-2cc7-4a8c-8a0e-e8090991e2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "631f44fb-847f-4d46-8f73-2930a65f592e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "d8f3b738-fdd0-4b22-999a-46921de18d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "2a584910-a98f-4f3e-8b91-e3062dd83bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "bd961b65-b66e-4d66-8e14-8b71b12d10a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "203aca47-4df7-49a4-9b6b-ffb568604310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "2fe7939d-8a9e-4863-bedb-c000ffdde50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "aa60bca0-69b7-44b6-9d27-a01e1e92eb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "bf61b586-5a4b-4bfa-b7b5-47f919ac0866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        },
        {
            "id": "f60ca9c0-7149-4c95-abfb-36c54f3d3505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
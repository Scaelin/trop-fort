///AddScreenshake(strength, direction)
//ADD SCREENSHAKE :D

strength = argument0;
direct = argument1;

if(obj_gm.screenshake)
{
	obj_camera.screenshakeStrength = obj_camera.screenshakeStrength + strength*(obj_gm.screenshake/10);
	obj_camera.screenshakeDirection = direct;
}

///LeftMenu(index, mx)
//Check for direction "left" to move through the menu
// argument mx indicates the maximum index it can reach, getting over this value will cycle back to 0;

left = argument0;
mx = argument1;

if(gamepad_button_check_pressed(0, gp_padl) or
   gamepad_button_check_pressed(1, gp_padl) or
   gamepad_button_check_pressed(2, gp_padl) or
   gamepad_button_check_pressed(3, gp_padl) or
   keyboard_check_pressed(vk_left) or
   keyboard_check_pressed(ord("Q")))
{
	left = left - 1;
}



if(left < 0)
{
	left = mx-1;	
}


return left;

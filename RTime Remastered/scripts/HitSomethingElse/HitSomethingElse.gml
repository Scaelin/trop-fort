///HitSomethingElse(avoided)
//Checks if the calling instance is colliding with something other than the avoided player (often the creator)
//Returns :
// 0 if it is not colliding with anything
// -1 if it is colliding with an "obj_solid" child
// the ID of the player it is colliding with 

avoid = argument0;

if(place_meeting(x, y, obj_player))
{
	if(avoid == obj_player1)
	{
		if(place_meeting(x, y, obj_player2))
		{
			return instance_place(x, y, obj_player2);
		}
		else if(place_meeting(x, y, obj_player3))
		{
			return instance_place(x, y, obj_player3);
		}
		else if(place_meeting(x, y, obj_player4))
		{
			return instance_place(x, y, obj_player4);
		}
	}
	if(avoid == obj_player2)
	{
		if(place_meeting(x, y, obj_player1))
		{
			return instance_place(x, y, obj_player1);
		}
		else if(place_meeting(x, y, obj_player3))
		{
			return instance_place(x, y, obj_player3);
		}
		else if(place_meeting(x, y, obj_player4))
		{
			return instance_place(x, y, obj_player4);
		}
	}
	if(avoid == obj_player3)
	{
	
		if(place_meeting(x, y, obj_player1))
		{
			return instance_place(x, y, obj_player1);
		}
		else if(place_meeting(x, y, obj_player2))
		{
			return instance_place(x, y, obj_player2);
		}
		else if(place_meeting(x, y, obj_player4))
		{
			return instance_place(x, y, obj_player4);
		}
	}
	if(avoid == obj_player4)
	{
	
		if(place_meeting(x, y, obj_player1))
		{
			return instance_place(x, y, obj_player1);
		}
		else if(place_meeting(x, y, obj_player2))
		{
			return instance_place(x, y, obj_player2);
		}
		else if(place_meeting(x, y, obj_player3))
		{
			return instance_place(x, y, obj_player3);
		}
	}

}
else if(place_meeting(x, y, obj_solid))
{
	return -1;	
}
else
{
	return 0;	
}
///DownMenu(index, mx)
//Check for direction "down" to move through the menu
// argument mx indicates the maximum index it can reach, getting over this value will cycle back to 0;

down = argument0;
mx = argument1;
 
if(gamepad_button_check_pressed(0, gp_padd) or
   gamepad_button_check_pressed(1, gp_padd) or
   gamepad_button_check_pressed(2, gp_padd) or
   gamepad_button_check_pressed(3, gp_padd) or
   keyboard_check_pressed(vk_down) or
   keyboard_check_pressed(ord("S")))
{
	down = down + 1;
}


if(down >= mx)
{
	down = 0;	
}

return down;

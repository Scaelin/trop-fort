{
    "id": "f66caa92-f828-462a-8de8-68226162d456",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "403022be-6235-4ffe-a4f0-124f5520aae1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f66caa92-f828-462a-8de8-68226162d456"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd4e5bf2-39dc-4a17-b373-12e38fd75a1d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
    "visible": true
}
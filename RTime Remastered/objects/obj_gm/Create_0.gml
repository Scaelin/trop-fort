/// @description Once again
// 

depth = -300;

//	Finite state machine
//Variable "state" will be responsible of the state of the game

state = "title";

draw_set_font(font_menuLarge);

//Pause setting to stop everything
global.stop = 0;

//Options in the options menu

screenshake = 10;
generalVolume = 10;
musicVolume = 10;
soundVolume = 10;

#region //Menu selector variables

selectorWidth = string_width("Training") + 10;
selectorHeight = string_height("Training") + 10;

selectorX = 395;
selectorY = 6800;

#endregion

#region //Data used to navigate through the main menu

menuIndex = 0;

mainMenu[0] = "Training";
mainMenu[1] = "Versus";
mainMenu[2] = "Options"
mainMenu[3] = "Quit";

maxMainMenu = 4;

#endregion

#region //Data used to navigate through the options menu

optionsIndex = 0;

optionsMenu[0] = "General volume";
optionsMenu[1] = "Music volume";
optionsMenu[2] = "Sound volume";
optionsMenu[3] = "Screenshake intensity";
optionsMenu[4] = "Back";

maxOptionsMenu = 5;

#endregion

#region //Versus menu data

playersJoin = 0;
playersReady = 0;

#endregion

#region //Training data

trainingState = "no player";

trainingPlayer = "none";

trainingCharacter = 0;


#endregion

#region //Game parameters menu

parameterIndex = 0;

parameterMenu[0] = "Start";
parameterMenu[1] = "Lives";
parameterMenu[2] = "Map";
parameterMenu[3] = "Back";

maxParameterMenu = 4;


lifeCount = 5;

#endregion

#region //In game Versus data

	//Respawn timers (counting down, respawns at 0)
	//-1 means that they can't respawn, being out of lives or not in the game
player1respawn = -1;
player2respawn = -1;
player3respawn = -1;
player4respawn = -1;

	//player lives left
player1lives = 0;
player2lives = 0;
player3lives = 0;
player4lives = 0;

playersAlive = 0;


//Characters selected
player1character1 = -1;
player1character2 = -1;

player2character1 = -1;
player2character2 = -1;

player3character1 = -1;
player3character2 = -1;

player4character1 = -1;
player4character2 = -1;


#endregion

#region //list of maps (levels/rooms)

	//[X, Y]
	// X here is the index of the map
	// if Y is 0, it's the in game displayed name
	// if Y is 1, it's the room name
/*
maps[0, 0] = "Ultimate arrival";
maps[0, 1] = rm_map_ultimate_arrival;

maps[1, 0] = "Pong"
maps[1, 1] = rm_map_pong;*/

#endregion 

#region //List of characters

	// [X, Y]
	// X is the index of the character
	// if Y is 0, it's the displayed name of the character
	// if Y is 1, it's the displayed description of the character
	// More to come
	
characters[0, 0] = "Crusader";
characters[0, 1] = "Deals melee damage. more to come";

characters[1, 0] = "Viking";
characters[1, 1] = "Deals melee damage. he is angry";

characters[2, 0] = "Toy soldier";
characters[2, 1] = "Mechanical sniper";

characters[3, 0] = "Penguin";
characters[3, 1] = "Shotgun enthusiast, noot noot motherfucker"

maxCharacters = 4;
	
#endregion






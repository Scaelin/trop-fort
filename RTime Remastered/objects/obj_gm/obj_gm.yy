{
    "id": "70d4fdc3-8dce-4645-ab18-7325c3675944",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gm",
    "eventList": [
        {
            "id": "112949a2-23a3-4e76-a5d9-cfe68b87aad2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70d4fdc3-8dce-4645-ab18-7325c3675944"
        },
        {
            "id": "b7856748-a8b3-4b45-ad9b-8e8bfb08b5c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70d4fdc3-8dce-4645-ab18-7325c3675944"
        },
        {
            "id": "0f73c6bb-e261-4809-8d9d-03a02abee6ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "70d4fdc3-8dce-4645-ab18-7325c3675944"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
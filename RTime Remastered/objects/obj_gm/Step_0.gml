/// @description Finite state machine
// 


//Lets go

#region //Title screen

if(state == "title")
{
	//Moving throught the menu
	menuIndex = UpMenu(menuIndex, maxMainMenu);
	menuIndex = DownMenu(menuIndex, maxMainMenu);
	
	
	if(mainMenu[menuIndex] == "Versus")
	{ 
		if(ConfirmMenu())
		{
			state = "versus lobby";
			menuIndex = 0;
		}
	}
	
	else if(mainMenu[menuIndex] == "Training")
	{
		if(ConfirmMenu())
		{
			state = "training lobby";
			menuIndex = 0;
		}
	}
	
	else if(mainMenu[menuIndex] == "Options")
	{
		if(ConfirmMenu())
		{
			state = "options"
			menuIndex = 0;
		}
	}
	
	else if(mainMenu[menuIndex] == "Quit")
	{
		if(ConfirmMenu())
		{
			//Time to save if possible
			game_end();
		}
	}
}

#endregion

#region //Options

else if(state == "options")
{
	
	//Moving through the menu
	optionsIndex = UpMenu(optionsIndex, maxOptionsMenu);
	optionsIndex = DownMenu(optionsIndex, maxOptionsMenu);
	
	
	if(optionsMenu[optionsIndex] == "General volume")
	{
		generalVolume = LeftMenu(generalVolume, 11);
		generalVolume = RightMenu(generalVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Music volume")
	{
		musicVolume = LeftMenu(musicVolume, 11);
		musicVolume = RightMenu(musicVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Sound volume")
	{
		soundVolume = LeftMenu(soundVolume, 11);
		soundVolume = RightMenu(soundVolume, 11);
	}
	else if(optionsMenu[optionsIndex] == "Screenshake intensity")
	{
		screenshake = LeftMenu(screenshake, 11);
		screenshake = RightMenu(screenshake, 11);
	}
	else if(optionsMenu[optionsIndex] == "Back")
	{
		if(ConfirmMenu())
		{
			state = "title";
			optionsIndex = 0;
			//Save new changes if possible
		}
	}
}

#endregion
	
	
#region //Training lobby
else if(state == "training lobby")
{
	if(trainingState == "no player")
	{
		//Checking if a player joins
		
			//Player 1
		if(keyboard_check_pressed(vk_enter) or keyboard_check_pressed(vk_space) or gamepad_button_check_pressed(3, gp_face1))
		{
			trainingState = "selecting";
			trainingPlayer = obj_player1;
		}
			//Player 2
		else if(gamepad_button_check_pressed(0, gp_face1))
		{
			trainingState = "selecting";
			trainingPlayer = obj_player2;
		}
			//Player 3
		else if(gamepad_button_check_pressed(1, gp_face1))
		{
			trainingState = "selecting";
			trainingPlayer = obj_player3;
		}
			//Player 4
		else if(gamepad_button_check_pressed(2, gp_face1))
		{
			trainingState = "selecting";
			trainingPlayer = obj_player4;
		}
		
		//Check if players want to go back
		if(BackMenu())
		{
			state = "title";
		}
	}
	
	#region //Selecting
	else if(trainingState == "selecting")
	{
		//Player1 navigation 
		if(trainingPlayer = obj_player1)
		{
			
			//Navigating the choices
			
				//Skipping the one already chosen
			if(trainingCharacter == player1character1)
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Left 
			if(keyboard_check_pressed(ord("Q")) or 
			   keyboard_check_pressed(vk_left) or
			   gamepad_button_check_pressed(3, gp_padl))
			{
				trainingCharacter = trainingCharacter - 1;
				if(trainingCharacter < 0)
				{
					trainingCharacter = maxCharacters;	
				}
			}
				//Right
			if(keyboard_check_pressed(ord("D")) or 
			   keyboard_check_pressed(vk_right) or 
			   gamepad_button_check_pressed(3, gp_padr))
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Confirm
			if(keyboard_check_pressed(vk_enter) or
			   keyboard_check_pressed(vk_space) or
			   gamepad_button_check_pressed(3, gp_face1))
			{
				//First character was already selected
				if(player1character1 != -1)
				{
					player1character2 = trainingCharacter;
					trainingState = "ready";
					
				}
				//First character yet to be selected
				else
				{
					player1character1 = trainingCharacter;
					trainingCharacter = trainingCharacter + 1;
					if(trainingCharacter == maxCharacters)
					{
						trainingCharacter = 0;	
					}
				}
			}
			
				//Back
			if(keyboard_check_pressed(vk_escape) or
			   gamepad_button_check_pressed(3, gp_face2))
			{
				//First character was already selected
				if(player1character1 != -1)
				{
					player1character1 = -1;
				}
				else
				{
					trainingState = "no player";
					trainingPlayer = "none";
					trainingCharacter = 0;
				}
			}
			
				//Info
			if(keyboard_check_pressed(ord("I")) or
			   gamepad_button_check_pressed(3, gp_face4))
			{
				trainingState = "info";
			}
		}
		//Player2 navigation 
		else if(trainingPlayer = obj_player2)
		{
			//Navigating the choices
			
				//Skipping the one already chosen
			if(trainingCharacter == player2character1)
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Left 
			if(gamepad_button_check_pressed(0, gp_padl))
			{
				trainingCharacter = trainingCharacter - 1;
				if(trainingCharacter < 0)
				{
					trainingCharacter = maxCharacters;	
				}
			}
				//Right
			if(gamepad_button_check_pressed(0, gp_padr))
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Confirm
			if(gamepad_button_check_pressed(0, gp_face1))
			{
				//First character was already selected
				if(player2character1 != -1)
				{
					player2character2 = trainingCharacter;
					trainingState = "ready";
					
				}
				//First character yet to be selected
				else
				{
					player2character1 = trainingCharacter;
					trainingCharacter = trainingCharacter + 1;
					if(trainingCharacter == maxCharacters)
					{
						trainingCharacter = 0;	
					}
				}
			}
			
				//Back
			if(gamepad_button_check_pressed(0, gp_face2))
			{
				//First character was already selected
				if(player2character1 != -1)
				{
					player2character1 = -1;
				}
				else
				{
					trainingState = "no player";
					trainingPlayer = "none";
					trainingCharacter = 0;
				}
			}
			
				//Info
			if(gamepad_button_check_pressed(0, gp_face4))
			{
				trainingState = "info";
			}
		}
		//PLayer3 navigation
		else if(trainingPlayer = obj_player3)
		{
			//Navigating the choices
			
				//Skipping the one already chosen
			if(trainingCharacter == player3character1)
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Left 
			if(gamepad_button_check_pressed(1, gp_padl))
			{
				trainingCharacter = trainingCharacter - 1;
				if(trainingCharacter < 0)
				{
					trainingCharacter = maxCharacters;	
				}
			}
				//Right
			if(gamepad_button_check_pressed(1, gp_padr))
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Confirm
			if(gamepad_button_check_pressed(1, gp_face1))
			{
				//First character was already selected
				if(player3character1 != -1)
				{
					player3character2 = trainingCharacter;
					trainingState = "ready";
					
				}
				//First character yet to be selected
				else
				{
					player3character1 = trainingCharacter;
					trainingCharacter = trainingCharacter + 1;
					if(trainingCharacter == maxCharacters)
					{
						trainingCharacter = 0;	
					}
				}
			}
			
				//Back
			if(gamepad_button_check_pressed(1, gp_face2))
			{
				//First character was already selected
				if(player3character1 != -1)
				{
					player3character1 = -1;
				}
				else
				{
					trainingState = "no player";
					trainingPlayer = "none";
					trainingCharacter = 0;
				}
			}
			
				//Info
			if(gamepad_button_check_pressed(1, gp_face4))
			{
				trainingState = "info";
			}
		}
		//Player4 navigation
		else if(trainingPlayer = obj_player4)
		{
			//Navigating the choices
			
				//Skipping the one already chosen
			if(trainingCharacter == player4character1)
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Left 
			if(gamepad_button_check_pressed(2, gp_padl))
			{
				trainingCharacter = trainingCharacter - 1;
				if(trainingCharacter < 0)
				{
					trainingCharacter = maxCharacters;	
				}
			}
				//Right
			if(gamepad_button_check_pressed(2, gp_padr))
			{
				trainingCharacter = trainingCharacter + 1;
				if(trainingCharacter == maxCharacters)
				{
					trainingCharacter = 0;	
				}
			}
				//Confirm
			if(gamepad_button_check_pressed(2, gp_face1))
			{
				//First character was already selected
				if(player4character1 != -1)
				{
					player4character2 = trainingCharacter;
					trainingState = "ready";
					
				}
				//First character yet to be selected
				else
				{
					player4character1 = trainingCharacter;
					trainingCharacter = trainingCharacter + 1;
					if(trainingCharacter == maxCharacters)
					{
						trainingCharacter = 0;	
					}
				}
			}
			
				//Back
			if(gamepad_button_check_pressed(2, gp_face2))
			{
				//First character was already selected
				if(player4character1 != -1)
				{
					player4character1 = -1;
				}
				else
				{
					trainingState = "no player";
					trainingPlayer = "none";
					trainingCharacter = 0;
				}
			}
			
				//Info
			if(gamepad_button_check_pressed(2, gp_face4))
			{
				trainingState = "info";
			}
		}
	}
	#endregion
	
	else if(trainingState == "info")
	{
		//Getting back once the player has read the information
		if(trainingPlayer == obj_player1) and 
		  ((keyboard_check_pressed(vk_escape) or
		  (gamepad_button_check_pressed(3, gp_face2))))
		{
			trainingState = "selecting";		
		}
	}
	else if(trainingState == "ready")
	{
		
	}
}

#endregion
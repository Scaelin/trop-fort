/// @description Mostly menu render

#region //Title screen
if(state == "title")
{
	//Draw the selection rectangle
	if(mainMenu[menuIndex] == "Training")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 6800 - (string_height("Training")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Training") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Training") + 10)/4;
	}
	else if(mainMenu[menuIndex] == "Versus")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 6900 - (string_height("Versus")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Versus") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Versus") + 10)/4;
	}
	else if(mainMenu[menuIndex] == "Options") 
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 7000 - (string_height("Options")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Options") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Options") + 10)/4;
	}
	else if(mainMenu[menuIndex] == "Quit")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 7100 - (string_height("Quit")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Quit") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Quit") + 10)/4;
	}
	draw_set_alpha(0.5);
	draw_rectangle_color(selectorX, selectorY, selectorX+selectorWidth, selectorY+selectorHeight,
						 c_ltgray, c_ltgray, c_ltgray, c_ltgray, false);
	draw_rectangle_color(selectorX, selectorY, selectorX+selectorWidth, selectorY+selectorHeight,
						 c_black, c_black, c_black, c_black, true);
	draw_set_alpha(1);
	
	
	//Draw the menu options
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuLarge);
	
	draw_text(400, 6800, "Training");
	
	draw_text(400, 6900, "Versus");
	
	draw_text(400, 7000, "Options");
	
	draw_text(400, 7100, "Quit");

}	
#endregion

#region //Options
else if(state == "options")
{
	//Draw the selection rectangle
	if(optionsMenu[optionsIndex] == "General volume")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 4800 - (string_height("General volume")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("General volume") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("General volume") + 10)/4;
	}
	else if(optionsMenu[optionsIndex] == "Music volume")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 4900 - (string_height("Music volume")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Music volume") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Music volume") + 10)/4;
	}
	else if(optionsMenu[optionsIndex] == "Sound volume") 
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 5000 - (string_height("Sound volume")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Sound volume") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Sound volume") + 10)/4;
	}
	else if(optionsMenu[optionsIndex] == "Screenshake intensity")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 5100 - (string_height("Screenshake intensity")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Screenshake intensity") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Screenshake intensity") + 10)/4;
	}
	else if(optionsMenu[optionsIndex] == "Back")
	{
		selectorX = (3*selectorX + 395)/4;
		selectorY = (3*selectorY + 5500 - (string_height("Back")/2))/4;
		selectorWidth = (3*selectorWidth + string_width("Back") + 10)/4;
		selectorHeight = (3*selectorHeight + string_height("Back") + 10)/4;
	}
	draw_set_alpha(0.5);
	draw_rectangle_color(selectorX, selectorY, selectorX+selectorWidth, selectorY+selectorHeight,
						 c_ltgray, c_ltgray, c_ltgray, c_ltgray, false);
	draw_rectangle_color(selectorX, selectorY, selectorX+selectorWidth, selectorY+selectorHeight,
						 c_black, c_black, c_black, c_black, true);
	draw_set_alpha(1);
	
	
	//Draw the menu options
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(font_menuLarge);
	
	draw_text(400, 4800, "General volume ");
	
	draw_text(400, 4900, "Music volume");
	
	draw_text(400, 5000, "Sound volume");
	
	draw_text(400, 5100, "Screenshake intensity");
	
	draw_text(400, 5500, "Back");
}
#endregion
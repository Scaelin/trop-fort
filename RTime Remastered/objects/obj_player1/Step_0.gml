/// @description Inputs, then inherit

//Check for 4 controlers, to replace KB+M

//Controler [4]
if(gamepad_is_connected(3))
{
	gamepad_set_axis_deadzone(3, 0.3);
		
	left = 0;
	right = gamepad_axis_value(3, gp_axislh);
		
	fire = gamepad_button_check(3, gp_shoulderrb);
	swap = gamepad_button_check_pressed(3, gp_shoulderlb);
		
	//The condition is here to prevent the need for always aiming with the stick
	if((gamepad_axis_value(3, gp_axisrh) != 0) or (gamepad_axis_value(3, gp_axisrv) != 0))
	{
		aim = point_direction(0, 0, gamepad_axis_value(3, gp_axisrh), gamepad_axis_value(3, gp_axisrv));
	}

}
//KB+M
else
{
	left = keyboard_check(ord("Q"));
	right = keyboard_check(ord("D"));

	fire = mouse_check_button(mb_left);
	swap = mouse_check_button_pressed(mb_right);

	aim = point_direction(x, y, mouse_x, mouse_y);

}

//Proceed with normal obj_player behaviour
event_inherited();

/// @description Adaptive camera


//Main menu movement

if(obj_gm.state == "title")
{
	x = (x*3 + titleX)/4;
	y = (y*3 + titleY)/4;
}
else if(obj_gm.state == "options")
{
	x = (x*3 + optionsX)/4;
	y = (y*3 + optionsY)/4;
}
else if(obj_gm.state == "versus lobby") or (obj_gm.state == "training lobby")
{
	x = (x*3 + lobbyX)/4;
	y = (y*3 + lobbyY)/4;
}
else if(obj_gm.state == "versus parameters") or (obj_gm.state == "training parameters")
{
	x = (x*3 + parameterX)/4;
	y = (y*3 + parameterY)/4;
}


//In game applications
else if(global.stop == 0)
{
	//Center the camera
	x = (x*3 + (room_width/2))/4;
	
	if(mapCurrentScreen == 1)
	{
		y = (y*3 + multiScreenY1)/4;
	}
	else if(mapCurrentScreen == 2)
	{
		y = (y*3 + multiScreenY2)/4;
	}
	else if(mapCurrentScreen == 3)
	{
		y = (y*3 + multiScreenY3)/4;
	}
	
	if(screenshake > 0)
	{
		//Screenshake is going to neutralize naturally through normal camera movement
		x = x + lengthdir_x(screenshakeStrength, screenshakeDirection);
		y = y + lengthdir_y(screenshakeStrength, screenshakeDirection);
	}
}

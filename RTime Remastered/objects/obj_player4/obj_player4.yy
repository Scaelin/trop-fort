{
    "id": "41d60af2-8351-498c-838a-2484ee9ddb98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player4",
    "eventList": [
        {
            "id": "4070e7a6-199f-4c23-b524-d867892dca34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "41d60af2-8351-498c-838a-2484ee9ddb98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd4e5bf2-39dc-4a17-b373-12e38fd75a1d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
    "visible": true
}
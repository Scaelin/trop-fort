/// @description Shit's happenin
// 

//Stops everything during pauses and other such events
if(global.stop == 0)
{
	//Inputs will be depending of the player, so it will be done after inheritance
	
	#region //Movement 
	
		//From the player's input
	input_hspd = (right - left)*movespeed;
	input_vspd = 0;
	
		//Gravity
	if((!instance_place(x, y+1, obj_solid)))
	{
		if(grav <= 21)			//Legacy of the retard
		{
			grav = grav + 0.6;	//Wtf am i even doing with my life	
		}
		input_vspd = input_vspd + grav;
	}
	
		//Something is under the player, stop the gravity for some reason
	if(instance_place(x, y+1, obj_solid))
	{
		grav = 0;	
	}
	
		//Horizontal friction
	if(other_hspd > 0)
	{
		if(instance_place(x+1, y, obj_solid))
		{
			other_hspd = other_hspd - 1.5;
		}
		else
		{
			other_hspd = other_hspd = 0.5;	
		}
	}
	else if(other_hspd < 0)
	{
		if(instance_place(x-1, y, obj_solid))
		{
			other_hspd = other_hspd + 1.5;
		}
		else
		{
			other_hspd = other_hspd + 0.5;
		}
	}
	
		//Vertical friction
	if(other_vspd < 0)
	{
		if(instance_place(x, y-1, obj_solid))
		{
			other_vspd = other_vspd + 1.5;	
		}
		else 
		{
			other_vspd = other_vspd + 0.5;	
		}
	}
	else if(other_vspd > 0)
	{
		if(instance_place(x, y+1, obj_solid))
		{
			other_vspd = other_vspd - 1.5;	
		}
		else
		{
			other_vspd = other_vspd - 0.5;
		}
	}
	
		//Cancelling
	if(other_hspd > -0.5) and (other_hspd < 0.5)
	{
		other_hspd = 0;
	}
	if(other_vspd > -0.5) and (other_vspd < 0.5)
	{
		other_vspd = 0;	
	}
	
		//Final speed
	hspd = input_hspd + other_hspd;
	vspd = input_vspd + other_vspd;
	
		//Collisions and movement
		
	#region //Horizontal
	
		//Movement segmentation
	if(hspd > 0)
	{
		while(hspd > 15)
		{
			if(instance_place(x+15, y, obj_solid))
			{
				while(!instance_place(x+1, y, obj_solid))
				{
					x = x + 1;	
				}
				hspd = 0;
			}
			else if(hspd > 0)
			{
				x = x + 15;
				hspd = hspd - 15;
			}
		}
		
		//Leftover movement (hspd < 15)
		if(instance_place(x+hspd, y, obj_solid))
		{
			while(!instance_place(x + hspd, y, obj_solid))
			{
				x = x + 1;	
			}
			hspd = 0;
		}
		
		x = x + hspd;
	}
	else if(hspd < 0)
	{
		//Movement segmentation for ultra high speed
		while(hspd < -15)
		{
			if(instance_place(x-15, y, obj_solid))
			{
				while(!instance_place(x-1, y, obj_solid))
				{
					x = x - 1;	
				}
				hspd = 0;
			}
			if(hspd < 0)
			{
				x = x - 15;	
				hspd = hspd + 15;
			}
		}
		//Leftover mvoement (hspd > -15)
		if(instance_place(x+hspd, y, obj_solid))
		{
			while(!instance_place(x-1, y, obj_solid))
			{
				x = x - 1;	
			}
			hspd = 0;
		}

		x = x + hspd;

	}
	
		
	#endregion
	
	#region //Vertical
	
	//Movement segmentation
	
	if(vspd > 0)
	{
		while(vspd > 15)
		{
			if(instance_place(x, y +15, obj_solid))
			{
				while(!instance_place(x, y+1, obj_solid))
				{
					y = y + 1;	
				}
				vspd = 0;
			}
			if(vspd > 0)
			{
				y = y + 15;	
				vspd = vspd - 15;
			}
		}
		//Leftover (vspd < 15)
		if(instance_place(x, y+vspd, obj_solid))
		{
			while(!instance_place(x, y+1, obj_solid))
			{
				y = y + 1;	
			}
			vspd = 0;
		}

		y = y + vspd;	

	}
	else if(vspd < 0)
	{
		while(vspd < -15)
		{
			if(instance_place(x, y-15, obj_solid))
			{
				while(!instance_place(x, y-1, obj_solid))
				{
					y = y - 1;	
				}
				vspd = 0;
			}
			if(vspd < 0)
			{
				y = y - 15;	
				vspd = vspd + 15;
			}
		}
		//Leftover (hspd > -15)
		if(instance_place(x, y+vspd, obj_solid))
		{
			while(!instance_place(x, y-1, obj_solid))
			{
				y = y - 1;	
			}
			vspd = 0;
		}

		y = y + vspd;

	}
	
	#endregion
	
	#region //Endless vertical loop
	
	if(y >  camera_get_view_height(view_camera[0]) + camera_get_view_y(view_camera[0]))
	{
		y = y - camera_get_view_height(view_camera[0]);	
	}
	
	if(y < camera_get_view_y(view_camera[0]))
	{
		y = y + camera_get_view_height(view_camera[0]);
	}
	
	#endregion
	
	
	
	#endregion
	
	#region //Swap
	
	if(swap) and (state == "idle") and (swapCooldown == 0)
	{
		//Do things that happen when the character swaps out
		if(character1 == "")
		{
			
		}
		
		//Swap the characters
		var tempChar = character2;
		character2 = character1;
		character1 = tempChar;
		
		//Need to change the sprite 
		
		//state change
		state = "swap";
		swapDuration = swapDurationMax;
		swapCooldown = swapCooldownMax;
		
		
		//Do things that happen when the character swaps in
		if(character1 == "")
		{
			
		}
	}
	
	#endregion
	
	#region //Shoot
	
	#endregion
}
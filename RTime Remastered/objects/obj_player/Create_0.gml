/// @description Initialisation
// 


//All 4 players will inherit properties from obj_player, to lighten the code

//Controls
left = 0;	//Q or left stick
right = 0;	//D or left stick

fire = 0;	//LMB or Right trigger
swap = 0;	//RMB or Left trigger

aim = 0;	//Mouse or right stick

//Movement

	//Player input
input_hspd = 0;
input_vspd = 0;

	//Caused by external factors
other_hspd = 0;
other_vspd = 0;

	//Gravity
grav = 0;

	//Final speed
hspd = input_hspd + other_hspd;
vspd = input_vspd + other_vspd;

	//Movespeed
movespeed = 7;

//Shooting
reload = 0;
ammo = 0
shotdelay = 0;

//Swapping
swapDuration = 0;
swapDurationMax = room_speed*0.5;

swapCooldown = 0;
swapCooldownMax = room_speed*4;

//Chronorekt effect
hchronorekt = 0;
vchronorekt = 0;

//Player that last hit this player (for kill feeds)
killer = -1;

//Characters chosen
character1 = "none";
character2 = "none";

//Finite state machine
state = "idle";

//Different states are
// "idle", meaning the player is behaving like normal
// "stunned", meaning the player cannot input movement or actions until the stun duration is over
// "swap", meaning the player is currently swapping characters, and is immune to damage
	



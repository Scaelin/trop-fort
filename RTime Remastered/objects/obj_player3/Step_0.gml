/// @description Inputs, then inherit

if(gamepad_is_connected(1))
{
	gamepad_set_axis_deadzone(1, 0.3);
		
	left = 0;
	right = gamepad_axis_value(1, gp_axislh);
		
	fire = gamepad_button_check(1, gp_shoulderrb);
	swap = gamepad_button_check_pressed(1, gp_shoulderlb);
		
	//The condition is here to prevent the need for always aiming with the stick
	if((gamepad_axis_value(1, gp_axisrh) != 0) or (gamepad_axis_value(1, gp_axisrv) != 0))
	{
		aim = point_direction(0, 0, gamepad_axis_value(1, gp_axisrh), gamepad_axis_value(1, gp_axisrv));
	}

}

//Proceed with normal obj_player behaviour
event_inherited();

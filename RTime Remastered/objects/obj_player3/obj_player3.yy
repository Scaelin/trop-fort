{
    "id": "741e795c-1d6e-4710-9d9e-4636745512ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player3",
    "eventList": [
        {
            "id": "02a04917-824d-4a77-9d12-a5c036419304",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "741e795c-1d6e-4710-9d9e-4636745512ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd4e5bf2-39dc-4a17-b373-12e38fd75a1d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb28fe1a-7e6f-4cf0-a7df-ad172e9ed7ea",
    "visible": true
}
{
    "id": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_noiseLow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 29,
    "bbox_right": 34,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34bbec76-6436-477f-9ce3-81602f5e66f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "8477860a-eb32-4504-8614-d592d2e8b5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bbec76-6436-477f-9ce3-81602f5e66f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c536560-2e82-46db-9e2f-586c50f9daa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bbec76-6436-477f-9ce3-81602f5e66f2",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "14b6ef3b-636f-44a8-8cac-bf68b49ba96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "4adcd54b-172e-434f-b98d-3465657aeea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b6ef3b-636f-44a8-8cac-bf68b49ba96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619d3bb1-3d47-4697-8278-68ecaed25908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b6ef3b-636f-44a8-8cac-bf68b49ba96e",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "fa64eb32-da3d-4484-9bdc-5456ea972050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "22a4df45-bc65-44fa-a4aa-3c5e855939cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa64eb32-da3d-4484-9bdc-5456ea972050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4028909-7944-4ec9-a4d4-d54b91ada58e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa64eb32-da3d-4484-9bdc-5456ea972050",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "d3832ca1-0af8-46e0-8c8f-0a5731b9f7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "4bb3bf9c-958c-4e62-88c5-341291f35b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3832ca1-0af8-46e0-8c8f-0a5731b9f7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c4774ed-99ae-4d94-bc9b-84ba7c0a1062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3832ca1-0af8-46e0-8c8f-0a5731b9f7c4",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "cdeba639-8e14-4446-847e-b3f9986602ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "708482a5-d878-445a-826c-f294df56ece5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdeba639-8e14-4446-847e-b3f9986602ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e3b6a0-7095-4ff3-8004-98797f607015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdeba639-8e14-4446-847e-b3f9986602ae",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "6b03a7c5-d555-4724-a48e-8fda06462ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "2f0b4368-dc86-4edd-b319-cc537d4820b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b03a7c5-d555-4724-a48e-8fda06462ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f42384-a590-471c-9465-d615bc08eaf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b03a7c5-d555-4724-a48e-8fda06462ea3",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "7f155bc8-45bf-4787-99e7-2449a96a495a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "1f447d41-2f06-4f22-ac6f-e77e56b59077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f155bc8-45bf-4787-99e7-2449a96a495a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a171dc3-bceb-44e2-ac49-0b020cc5ba56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f155bc8-45bf-4787-99e7-2449a96a495a",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        },
        {
            "id": "e59a1fc3-bd6d-40d1-9477-797c682f4b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "compositeImage": {
                "id": "b4133eff-49dc-45cb-833a-d8a60d8eb559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e59a1fc3-bd6d-40d1-9477-797c682f4b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a7b8a8-ef79-46dd-b2f1-1b144162b953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e59a1fc3-bd6d-40d1-9477-797c682f4b52",
                    "LayerId": "96d66165-e808-4de8-acc0-9dd83e30c9a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "96d66165-e808-4de8-acc0-9dd83e30c9a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
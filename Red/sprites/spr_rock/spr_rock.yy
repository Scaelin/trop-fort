{
    "id": "2c5a5be6-ec3d-44fc-b62c-2cf22cefa672",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29e3317a-952f-4bd2-b2a1-737f68a8f744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c5a5be6-ec3d-44fc-b62c-2cf22cefa672",
            "compositeImage": {
                "id": "a385cf7b-4692-4e08-8f02-abe949f2de62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e3317a-952f-4bd2-b2a1-737f68a8f744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcef3a2-0caa-460c-a8b1-96cbcc4d7d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e3317a-952f-4bd2-b2a1-737f68a8f744",
                    "LayerId": "09f43961-bce7-444e-ae6b-089555dab80a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "09f43961-bce7-444e-ae6b-089555dab80a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c5a5be6-ec3d-44fc-b62c-2cf22cefa672",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
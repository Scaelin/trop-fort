{
    "id": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_noiseMedium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 29,
    "bbox_right": 34,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f94ab40-da12-4c34-9f38-e64c94a30787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "a5603f1a-65ee-445a-a427-bf6e0299b709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f94ab40-da12-4c34-9f38-e64c94a30787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "480ceecd-0195-4797-b243-2e5c916c6f8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f94ab40-da12-4c34-9f38-e64c94a30787",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "daf01849-b554-4d58-9e55-894dd9167a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "5e43f498-425f-4988-9a6c-173467990a21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf01849-b554-4d58-9e55-894dd9167a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa77ebe-c3c2-4ef8-bec6-7aaa9f4f6747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf01849-b554-4d58-9e55-894dd9167a8b",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "72702974-acaf-482c-bdc4-f885299e3019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "15ae6cfc-f938-4aa0-8f3f-78aa26f8d4c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72702974-acaf-482c-bdc4-f885299e3019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fec6626-7812-4627-9e8c-a41d120be339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72702974-acaf-482c-bdc4-f885299e3019",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "fc22ed67-f8fd-4db5-ae48-16fd4b2c8f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "a5d5880a-9441-450a-988e-6e2a99681214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc22ed67-f8fd-4db5-ae48-16fd4b2c8f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70257c44-b4bf-49c6-b577-904e875102e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc22ed67-f8fd-4db5-ae48-16fd4b2c8f84",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "2099182a-158b-4e83-ab29-31ea6e594f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "aacab9fd-0b28-4d8c-a4b6-697adbc9f882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2099182a-158b-4e83-ab29-31ea6e594f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033d0077-b2e7-4231-8266-1aeb840073f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2099182a-158b-4e83-ab29-31ea6e594f11",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "02d2db07-7ffa-45c5-8c53-f721cf9676c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "1ee8fbb6-e01d-4ea5-a917-01321d9a80a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d2db07-7ffa-45c5-8c53-f721cf9676c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564e56e2-4210-4c39-9d73-a6438ce6eb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d2db07-7ffa-45c5-8c53-f721cf9676c5",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "1a30ab30-436f-406e-9f23-6c9581821755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "4780bb58-a272-4051-a89e-5ce1889a2b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a30ab30-436f-406e-9f23-6c9581821755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d720b1-805d-494e-8d77-d157c2b1fb5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a30ab30-436f-406e-9f23-6c9581821755",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        },
        {
            "id": "e910643d-03e1-43d0-b645-40199e6b8522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "compositeImage": {
                "id": "07dd71e1-83e4-4db7-be2d-9ea8fb39b085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e910643d-03e1-43d0-b645-40199e6b8522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5000c546-3876-4fed-a4a6-f532b6c95a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e910643d-03e1-43d0-b645-40199e6b8522",
                    "LayerId": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a5809e01-a527-4f12-8a56-3c8eb5ba8f54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cb8cd59-1f19-439d-a3e2-1c45bb5fc7cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
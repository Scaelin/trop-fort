{
    "id": "f8e9081e-fcc8-46cc-beb9-9586b83b9b2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smellTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d83161f0-bbbb-4536-90d8-d71896893f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e9081e-fcc8-46cc-beb9-9586b83b9b2b",
            "compositeImage": {
                "id": "fa009720-5225-41a3-9c0e-a90ecfdb175e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d83161f0-bbbb-4536-90d8-d71896893f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7b9baaa-6a90-4f9b-8c84-5d946c2a795f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d83161f0-bbbb-4536-90d8-d71896893f0c",
                    "LayerId": "8f4f094c-23a9-4a7f-b45d-25a26afc8c40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f4f094c-23a9-4a7f-b45d-25a26afc8c40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8e9081e-fcc8-46cc-beb9-9586b83b9b2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
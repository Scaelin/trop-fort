{
    "id": "25391e32-fe2a-4af7-b9b5-bae984b3b9d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86932413-29c1-4d31-96a7-8575bb13889e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25391e32-fe2a-4af7-b9b5-bae984b3b9d9",
            "compositeImage": {
                "id": "f937c94b-7e35-4f35-9a9e-dbd26f68e0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86932413-29c1-4d31-96a7-8575bb13889e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd12d63-2301-4f83-8da1-008a8fc6b78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86932413-29c1-4d31-96a7-8575bb13889e",
                    "LayerId": "3811e38f-82ed-4e28-9062-4e99a7a5482b"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "3811e38f-82ed-4e28-9062-4e99a7a5482b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25391e32-fe2a-4af7-b9b5-bae984b3b9d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "6ca9b7fc-fd2d-467e-9a1a-7a3a0a4f1342",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baee2f72-eebb-4c3f-ad02-494c5b1489a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ca9b7fc-fd2d-467e-9a1a-7a3a0a4f1342",
            "compositeImage": {
                "id": "5cbe6aff-d781-4988-8061-5373fefecb3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baee2f72-eebb-4c3f-ad02-494c5b1489a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca7537b-1708-4267-8215-79e4742c7791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baee2f72-eebb-4c3f-ad02-494c5b1489a6",
                    "LayerId": "a805bcbb-c946-4cc9-ad1f-93660785d10a"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 40,
    "layers": [
        {
            "id": "a805bcbb-c946-4cc9-ad1f-93660785d10a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ca9b7fc-fd2d-467e-9a1a-7a3a0a4f1342",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}
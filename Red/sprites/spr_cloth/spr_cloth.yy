{
    "id": "a96eea73-0f52-4b7f-8734-651fe02e7df1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce17f8ff-59e8-4086-a289-8e4709f3a3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96eea73-0f52-4b7f-8734-651fe02e7df1",
            "compositeImage": {
                "id": "90ad4422-ce0a-4221-983d-e9bbd0bc8fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce17f8ff-59e8-4086-a289-8e4709f3a3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f868a855-cc34-4ec0-bc79-98f323c8436e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce17f8ff-59e8-4086-a289-8e4709f3a3ff",
                    "LayerId": "207c9ec7-bab8-409f-a817-d78091a11300"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "207c9ec7-bab8-409f-a817-d78091a11300",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a96eea73-0f52-4b7f-8734-651fe02e7df1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
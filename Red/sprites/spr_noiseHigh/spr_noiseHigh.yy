{
    "id": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_noiseHigh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 29,
    "bbox_right": 34,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8930ea95-2bdf-42a6-88c4-b7a423f6dcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "a494c4d4-f8b3-48ca-9106-c8ffbf93992e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8930ea95-2bdf-42a6-88c4-b7a423f6dcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff94eb66-7516-4a45-b470-3f187d2e2cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8930ea95-2bdf-42a6-88c4-b7a423f6dcf4",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "8bcec163-491b-41d5-ada7-22059f7c9ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "a8b41d7f-a9a9-4614-ad01-0ec6b9fe3bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bcec163-491b-41d5-ada7-22059f7c9ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81781dc6-7c68-48da-9175-ae5670f8e6c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bcec163-491b-41d5-ada7-22059f7c9ed6",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "26360ba2-7711-4dba-b111-551e3b83478c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "7c398269-b9ca-4ac7-9831-e0e1a0bbf5da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26360ba2-7711-4dba-b111-551e3b83478c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64eb6363-a14e-45fe-b966-08f6bd155229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26360ba2-7711-4dba-b111-551e3b83478c",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "a412560a-cbfa-43c8-84d7-5ed6f9e23098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "257d2853-4c06-46d1-96cb-f68cb0e15cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a412560a-cbfa-43c8-84d7-5ed6f9e23098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a720b74f-18db-44cd-bafd-de369a0ddd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a412560a-cbfa-43c8-84d7-5ed6f9e23098",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "cbf4d61a-1d9a-4cbf-ba56-4d8e92b7d604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "f2f34303-9cc9-4185-ba4a-b00c9fded847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf4d61a-1d9a-4cbf-ba56-4d8e92b7d604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8250a50d-110c-452e-94b7-50979b5c0e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf4d61a-1d9a-4cbf-ba56-4d8e92b7d604",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "7ac0db91-0cd3-42e3-968c-03ad33a2c175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "5b4fb312-c806-4dc3-a81f-1e490861009e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac0db91-0cd3-42e3-968c-03ad33a2c175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5840c7ac-39df-4fc8-90fa-3948e7259f6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac0db91-0cd3-42e3-968c-03ad33a2c175",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "30971596-df5a-4c85-9c24-3c6d939315c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "a8273f24-f529-4442-b62b-f90225fdc0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30971596-df5a-4c85-9c24-3c6d939315c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "796f23e6-25a1-469f-97b8-ba96d578cbbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30971596-df5a-4c85-9c24-3c6d939315c2",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        },
        {
            "id": "d3fc4fe4-5c08-4558-861b-6d22988f5caf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "compositeImage": {
                "id": "bedbcbb9-9017-4e7e-a73e-0e4256b06c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3fc4fe4-5c08-4558-861b-6d22988f5caf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e65d933a-684d-49b3-8973-b3df9b0abcd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3fc4fe4-5c08-4558-861b-6d22988f5caf",
                    "LayerId": "1b12714c-5039-47fb-a08a-d9218a05a867"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b12714c-5039-47fb-a08a-d9218a05a867",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad3a870a-82bf-4d77-81ba-b186c0a9e4c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
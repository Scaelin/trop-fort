{
    "id": "7fefbbe3-558b-456d-a2f7-d5803796728d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26bf2c10-6a7c-47e1-9756-0d6f5ebbefd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fefbbe3-558b-456d-a2f7-d5803796728d",
            "compositeImage": {
                "id": "4e7aacf2-72df-41f2-bcb8-9c0b7c69684c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26bf2c10-6a7c-47e1-9756-0d6f5ebbefd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2750504-6f7c-41e4-8df0-f524096af77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26bf2c10-6a7c-47e1-9756-0d6f5ebbefd8",
                    "LayerId": "771abe2e-f654-4581-9628-55a6252c2d8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "771abe2e-f654-4581-9628-55a6252c2d8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fefbbe3-558b-456d-a2f7-d5803796728d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
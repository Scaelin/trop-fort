{
    "id": "eb9868c0-6a0d-49fd-85a6-1e6662b23151",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_crouching",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88ae9b0b-7839-44da-be14-aec0e54ea13e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb9868c0-6a0d-49fd-85a6-1e6662b23151",
            "compositeImage": {
                "id": "6d6e80dd-980c-4acd-92b6-96497a8ce7d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ae9b0b-7839-44da-be14-aec0e54ea13e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5df585-f401-4d23-9863-6022dfc55156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ae9b0b-7839-44da-be14-aec0e54ea13e",
                    "LayerId": "b6e2be19-d013-4748-a961-aa872987f4db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b6e2be19-d013-4748-a961-aa872987f4db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb9868c0-6a0d-49fd-85a6-1e6662b23151",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
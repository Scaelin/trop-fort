{
    "id": "a5cdf418-b217-4c0b-b1e1-b9aee833c7fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_objective",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "237a7fe0-f7d0-4c64-a0a2-400a8d4cffaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5cdf418-b217-4c0b-b1e1-b9aee833c7fe",
            "compositeImage": {
                "id": "fb36e710-034b-41d8-8acd-ba737ecb5d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237a7fe0-f7d0-4c64-a0a2-400a8d4cffaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a36ad7-298a-44bf-8d50-4795ea9be6fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237a7fe0-f7d0-4c64-a0a2-400a8d4cffaa",
                    "LayerId": "62e0f813-1f55-47b6-a8e9-29483335d43f"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 128,
    "layers": [
        {
            "id": "62e0f813-1f55-47b6-a8e9-29483335d43f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5cdf418-b217-4c0b-b1e1-b9aee833c7fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b32e81dd-4249-43ba-8eea-85365fbc2933",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolfSight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6300312-4acc-4a7d-b463-6716822d0ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b32e81dd-4249-43ba-8eea-85365fbc2933",
            "compositeImage": {
                "id": "afeec24a-2669-4d8b-993f-21858cf72a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6300312-4acc-4a7d-b463-6716822d0ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7c141c-230b-4464-b829-e173de1496f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6300312-4acc-4a7d-b463-6716822d0ad4",
                    "LayerId": "6c0ee623-8516-4c62-a51a-c75a5d6e6105"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "6c0ee623-8516-4c62-a51a-c75a5d6e6105",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b32e81dd-4249-43ba-8eea-85365fbc2933",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}
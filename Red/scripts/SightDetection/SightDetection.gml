///SightDetection()
//Returns 1 if Wolf can see Red, 0 otherwise
if(distance_to_object(obj_red) < sightRange)  
{
	if(abs(angle_difference(sightDirection, point_direction(x, y, obj_red.x, obj_red.y))) < sightRadius/2)
	{
		if(!collision_line(x, y, obj_red.x, obj_red.y, obj_wall, false, true))
		{
			sightX = (obj_red.x div 32) * 32 + 16;
			sightY = (obj_red.y div 32) * 32 + 16;
			return 1;
		}
	}	
}

return 0;
/// @description Throw
// Vous pouvez écrire votre code dans cet éditeur


if(state == "ground")
{
	if(place_meeting(x, y, obj_red))
	{
		if(obj_red.rock < obj_red.rockMax)
		{
			obj_red.rock = obj_red.rock + 1;
			instance_destroy(); 
		}
	}
}
else if(state == "thrown")
{
	#region //Movement and collision
	
		//Get hMove and vMove
	hMove = lengthdir_x(spd, dir) * global.dt;
	vMove = lengthdir_y(spd, dir) * global.dt;
	
	
		//Horizontal
		
	if(place_meeting(x + hMove, y, obj_wall))
	{
		if(hMove > 0)
		{
			while(!place_meeting(x + 1, y, obj_wall))
			{
				x = x + 1;
			}
		}
		else
		{
			while(!place_meeting(x - 1, y, obj_wall))
			{
				x = x - 1;
			}
		}
		hMove = 0;
		spd = 0;
	}

	x = x + hMove;

		//Vertical

	if(place_meeting(x, y + vMove, obj_wall))
	{
		if(vMove > 0)
		{
			while(!place_meeting(x, y + 1, obj_wall))
			{
				y = y + 1;
			}
		}
		else
		{
			while(!place_meeting(x, y - 1, obj_wall))
			{
				y = y - 1;	
			}
		}
		vMove = 0;
		spd = 0;
	}

	y = y + vMove;
	#endregion
	
	spd = spd - 1400*global.dt;
	if(spd <= 0)
	{
		//Generate sound
			//The div part is to create them in the middle of the grid square, for pathfinding
		var noise = instance_create_depth(x, y, depth - 1, obj_noise);
		noise.noiseTier = 3;
		
		state = "ground";
	}
}
{
    "id": "f770c74e-162d-433a-9a63-65dc5bc06465",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rock",
    "eventList": [
        {
            "id": "f970494c-6040-4c17-8cc1-c07fbd7c1aca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f770c74e-162d-433a-9a63-65dc5bc06465"
        },
        {
            "id": "4f6cc14f-af70-4b99-812c-683884cbbe6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f770c74e-162d-433a-9a63-65dc5bc06465"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c5a5be6-ec3d-44fc-b62c-2cf22cefa672",
    "visible": true
}
{
    "id": "f18a6374-fa8f-45c8-b518-26755223d6f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tree",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "484083c2-e233-4e00-a664-cbb720293f89",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25391e32-fe2a-4af7-b9b5-bae984b3b9d9",
    "visible": true
}
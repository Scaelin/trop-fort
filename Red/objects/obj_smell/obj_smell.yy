{
    "id": "939155d6-c4cb-4c3a-a7bc-860017e551de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smell",
    "eventList": [
        {
            "id": "8f4f4a73-131c-4a10-a1f5-5111fda8bb87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "939155d6-c4cb-4c3a-a7bc-860017e551de"
        },
        {
            "id": "456e4387-272a-4369-8387-d097013795b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "939155d6-c4cb-4c3a-a7bc-860017e551de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8e9081e-fcc8-46cc-beb9-9586b83b9b2b",
    "visible": true
}
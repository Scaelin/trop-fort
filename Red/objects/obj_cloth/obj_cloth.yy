{
    "id": "2f147d8e-a999-456d-99cc-d00cbb4786a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloth",
    "eventList": [
        {
            "id": "1480dad9-09a6-4186-a82c-183addc96c6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f147d8e-a999-456d-99cc-d00cbb4786a4"
        },
        {
            "id": "e2e3d15e-36c9-4dcf-9728-6dfe3c6ff683",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f147d8e-a999-456d-99cc-d00cbb4786a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "939155d6-c4cb-4c3a-a7bc-860017e551de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a96eea73-0f52-4b7f-8734-651fe02e7df1",
    "visible": true
}
{
    "id": "6a58255c-3f83-47dd-8c3d-ec9fcef9731a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_noise",
    "eventList": [
        {
            "id": "44f9234c-de72-4159-a665-a03e62b76f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a58255c-3f83-47dd-8c3d-ec9fcef9731a"
        },
        {
            "id": "d3afd898-6054-4177-bd1a-b45686105bd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6a58255c-3f83-47dd-8c3d-ec9fcef9731a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c96088f-b2fa-4df3-9f01-3e6c1abdf21e",
    "visible": true
}
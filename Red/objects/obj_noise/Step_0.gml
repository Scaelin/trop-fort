/// @description Check the Wolf

//Do things one time
if(setSprite)
{
	if(noiseTier == 1)
	{
		sprite_index = spr_noiseLow;
	}
	else if(noiseTier == 2)
	{
		sprite_index = spr_noiseMedium;
	}
	else if(noiseTier == 3)
	{
		sprite_index = spr_noiseHigh;
	}

}


if(image_index >= image_number -1)
{
	instance_destroy();
}
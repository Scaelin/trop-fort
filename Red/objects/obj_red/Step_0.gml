/// @description Red behaviour
// 

#region //Movement behaviour and controls

#region	//Check controls

left = keyboard_check(ord("Q")) or keyboard_check(vk_left);
right = keyboard_check(ord("D")) or keyboard_check(vk_right);
up = keyboard_check(ord("Z")) or keyboard_check(vk_up);
down = keyboard_check(ord("S")) or keyboard_check(vk_down);

//Crouch is toggle
crouch = keyboard_check_pressed(vk_shift) or keyboard_check_pressed(vk_control);

//Throw rocks
throw = mouse_check_button_pressed(mb_left);
//Tear a piece of fabric
tear = mouse_check_button_pressed(mb_right);
#endregion

	//Actual movement behaviour

#region //Toggle crouch
if(crouch)
{
	if(isCrouching)
	{
		isCrouching = 0;	
		sprite_index = spr_red_walking;
	}
	else
	{
		isCrouching = 1;
		sprite_index = spr_red_crouching;
	}
}
#endregion

#region //Move

if(isCrouching)
{
	hMove = (right - left) * crouchSpeed * global.dt;
	vMove = (down - up) * crouchSpeed * global.dt;
	
	
}
else
{
	hMove = (right - left) * walkSpeed * global.dt;
	vMove = (down - up) * walkSpeed * global.dt;
	
	
}
#endregion

#region//Collisions
	
	//Horizontal

if(place_meeting(x + hMove, y, obj_wall))
{
	if(hMove > 0)
	{
		while(!place_meeting(x + 1, y, obj_wall))
		{
			x = x + 1;
		}
	}
	else
	{
		while(!place_meeting(x - 1, y, obj_wall))
		{
			x = x - 1;
		}
	}
	hMove = 0;
}

x = x + hMove;

	//Vertical

if(place_meeting(x, y + vMove, obj_wall))
{
	if(vMove > 0)
	{
		while(!place_meeting(x, y + 1, obj_wall))
		{
			y = y + 1;
		}
	}
	else
	{
		while(!place_meeting(x, y - 1, obj_wall))
		{
			y = y - 1;	
		}
	}
	vMove = 0;
}

y = y + vMove;
#endregion

#region //Throw rocks

if(throw)
{
	if(rock > 0)
	{
		var thrown = instance_create_depth(x, y, depth, obj_rock);
		thrown.state = "thrown";
		thrown.dir = point_direction(x, y, mouse_x, mouse_y);
		thrown.spd = 1000;
	
		rock = rock - 1;
	}
}

#endregion

#region //Drop cloth

if(tear)
{
	if(cloth > 0)
	{
		var clth = instance_create_depth(x, y, depth, obj_cloth);
		
		cloth = cloth - 1;
	}
}

#endregion


#endregion

#region //Scent drop

if(scentTimer <= 0)
{
	//Create a tier 3 scent object	
	instance_create_depth((x div 32)*32+16, (y div 32)*32+16, depth, obj_smell);
	scentTimer = scentTimerMax;
}
else
{
	scentTimer = scentTimer - global.dt;	
}

#endregion


//Getting caught
if(place_meeting(x, y, obj_wolf))
{
	room_restart();	
}
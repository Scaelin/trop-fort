{
    "id": "3201bcd7-51c2-473b-bed7-e08ce44207f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red",
    "eventList": [
        {
            "id": "93a68fcc-97e9-4a93-9c72-55d1468399d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3201bcd7-51c2-473b-bed7-e08ce44207f1"
        },
        {
            "id": "43002250-ea27-4cac-bef4-2793eddea799",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3201bcd7-51c2-473b-bed7-e08ce44207f1"
        },
        {
            "id": "4438cdcc-0d94-4f9a-aa0a-9c13110a2c40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3201bcd7-51c2-473b-bed7-e08ce44207f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7fefbbe3-558b-456d-a2f7-d5803796728d",
    "visible": true
}
/// @description Variable definition
// 

//Stats
walkSpeed = 100;
crouchSpeed = 40;

hMove = 0;
vMove = 0;

//Frequency of scent drop
scentTimerMax = 3;
scentTimer = scentTimerMax;

//No finite state machine
isCrouching = 0;

//Inventory
cloth = 3;
clothMax = 3;

rock = 1;
rockMax = 3;

/// @description Win the game if hit
// Vous pouvez écrire votre code dans cet éditeur
global.dt = delta_time/1000000;

if(place_meeting(x, y, obj_red))
{
	playerHasWon = 1;
	obj_red.x = 512;
	obj_red.y = 3000;
	winTimer = winTimerMax;
}


//Reset the game after a win
if(playerHasWon)
{
	if(winTimer <= 0)
	{
		room_restart();
	}
	else
	{
		winTimer = winTimer - global.dt;
	}
}
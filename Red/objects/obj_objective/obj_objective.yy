{
    "id": "da79c7fc-49ca-4bd5-8f7c-2b47ddb6c1c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_objective",
    "eventList": [
        {
            "id": "1a887a30-973d-427c-b077-6b4daf7ad912",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da79c7fc-49ca-4bd5-8f7c-2b47ddb6c1c7"
        },
        {
            "id": "13eb847f-2753-4427-83b2-33256e516296",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da79c7fc-49ca-4bd5-8f7c-2b47ddb6c1c7"
        },
        {
            "id": "4ec22128-8e1a-446f-80fa-0d537e85c705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "da79c7fc-49ca-4bd5-8f7c-2b47ddb6c1c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5cdf418-b217-4c0b-b1e1-b9aee833c7fe",
    "visible": true
}
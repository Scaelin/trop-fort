/// @description Sight display
// 

//Draw sight
var dir = sightDirection - (sightRadius/2);
var len = 0;
while((len < sightRange) and (!position_meeting(x+lengthdir_x(len, dir), y+lengthdir_y(len, dir), obj_wall)))
{
	draw_sprite_ext(spr_wolfSight, 0, x+lengthdir_x(len, dir), y+lengthdir_y(len, dir), 1, 1, dir, c_white, 1);
	len = len+6;
}

var dir2 = sightDirection + (sightRadius/2);
var len2 = 0;
while((len2 < sightRange) and (!position_meeting(x+lengthdir_x(len2, dir2), y+lengthdir_y(len2, dir2), obj_wall)))
{
	draw_sprite_ext(spr_wolfSight, 0, x+lengthdir_x(len2, dir2), y+lengthdir_y(len2, dir2), 1, 1, dir2, c_white, 1);
	len2 = len2+6;
}

//DEBUG : Draw the state
draw_text(x, y - 30, state);

//Draw the wolf
draw_self();
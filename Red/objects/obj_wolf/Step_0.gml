/// @description Finite State Machine


//Main Routine : looking > sniffing > trackingSmell > looking > ...

//trackingSight overrides everything 
//trackingNoise overrides everything but trackingSight
//trackingSmell only overrides looking and sniffing

#region //state looking
if(state == "looking")
{
	
	//Look around
	if(sightRotationNumber < sightRotationNumberMax)
	{
		if(sightRotation < sightRotationMax)
		{
			sightDirection = sightDirection + sightRotation;
			sightRotation = sightRotation + 0.25*global.dt;
		}
		else
		{
			sightRotation = 0.5
			sightRotationNumber = sightRotationNumber + 1;
			
		}
		
		
	}
	else
	{
		state = "sniffing";
		sightRotationNumber = 0;
		sightRotation = 0;
	}
	
	//Check sight
	if(SightDetection())
	{
		state = "trackingSight";
		sightRotationNumber = 0;
		sightRotation = 0;
		if(mp_grid_path(global.grid, path, x, y, sightX, sightY, 1))
		{
			path_start(path, runSpeed, path_action_stop, false);
		}
	}
}
#endregion

#region //state sniffing
else if(state == "sniffing")
{
	if(sniffDelay <= 0)
	{
		if(instance_exists(obj_smell))
		{
			//Check for the best smell to track
			var first = true;
			var smells;
			for (var i = 0; i < instance_number(obj_smell); i = i + 1)
			{
				smells[i] = instance_find(obj_smell, i);
				if(first)
				{
					smellX = smells[i].x;
					smellY = smells[i].y;
					first = false;
				}
				else
				{
					if(point_distance(x, y, smellX, smellY) > point_distance(x, y, smells[i].x, smells[i].y))
					{
						smellX = smells[i].x;
						smellY = smells[i].y;
					}
					
				}
			}
			state = "trackingSmell";
			if(mp_grid_path(global.grid, path, x, y, smellX, smellY, 1))
			{
				path_start(path, runSpeed, path_action_stop, false);
			}
			
		}
		else
		{
			state = "looking";
		}		
		sniffDelay = sniffDelayMax;
	}
	else
	{
		sniffDelay = sniffDelay - global.dt;
	}
	
	//Sight detection
	if(SightDetection())
	{
		state = "trackingSight";
		sightRotationNumber = 0;
		sightRotation = 0;
		if(mp_grid_path(global.grid, path, x, y, sightX, sightY, 1))
		{
			path_start(path, runSpeed, path_action_stop, false);
		}
	}
}
#endregion

#region //state trackingSmell
else if(state == "trackingSmell")
{
	//Change direction
	sightDirection = direction;
	
	//Check if he has reached destination, if so go on with routine
	if(x <= smellX+16) and (x >= smellX-16) and (y <= smellY+16) and (y >= smellY-16)
	{
		smellX = 0;
		smellY = 0;
		state = "looking";
	}
	else
	//Sight detection
	{
		if(SightDetection())
		{
			state = "trackingSight";
			if(mp_grid_path(global.grid, path, x, y, sightX, sightY, 1))
			{
				path_start(path, runSpeed, path_action_stop, false);
			}
		}
		
	}
}
#endregion

#region //state trackingNoise
else if(state == "trackingNoise")
{
	//Change direction
	sightDirection = direction;
	
	if(x <= noiseX+16) and (x >= noiseX-16) and (y <= noiseY+16) and (y >= noiseY-16)
	{
		noiseX = 0;
		noiseY = 0;
		state = "looking";
	}
	else
	//Sight detection
	{
		if(SightDetection())
		{
			state = "trackingSight";
			if(mp_grid_path(global.grid, path, x, y, sightX, sightY, 1))
			{
				path_start(path, runSpeed, path_action_stop, false);
			}
		}
		
	}
}
#endregion

#region //state trackingSight
else if(state == "trackingSight")
{
	sightDirection = direction;
	
	if(x <= sightX+16) and (x >= sightX-16) and (y <= sightY+16) and (y >=sightY-16)
	{
		sightX = 0;
		sightY = 0;
		state = "looking";
	}
	else
	{
		if(SightDetection())
		{
			state = "trackingSight";
			sightRotationNumber = 0;
			sightRotation = 0;
			if(mp_grid_path(global.grid, path, x, y, sightX, sightY, 1))
			{
				path_start(path, runSpeed, path_action_stop, false);
			}
		}
	}
}
#endregion


#region //Noise detection is in every state except trackingSight

if(instance_exists(obj_noise))
{
	var first = true;
	var noises;
	var bestIntensity;
	for (var i = 0; i < instance_number(obj_noise); i = i + 1)
	{
		noises[i] = instance_find(obj_noise, i);
		if(first)
		{
			noiseX = noises[i].x;
			noiseY = noises[i].y;
			bestIntensity = point_distance(x, y, noiseX, noiseY) - (noises[i].noiseTier*noises[i].noiseDistance);
			first = false;
		}
		else
		{
			if(bestIntensity > (point_distance(x, y, noises[i].x, noises[i].y) - (noises[i].noiseTier*noises[i].noiseDistance)))
			{
				noiseX = noises[i].x;
				noiseX = noises[i].y;
				bestIntensity = point_distance(x, y, noiseX, noiseY) - (noises[i].noiseTier*noises[i].noiseDistance);
			
			}
					
		}
	}	

	//Set parameters once a noise is heard
	
	if(obj_wolf.state != "trackingSight") and (bestIntensity < 0)
	{
		state = "trackingNoise";
		if(mp_grid_path(global.grid, path, x, y, noiseX, noiseY, 1))
		{
			path_start(path, runSpeed, path_action_stop, false);
		}
		
	
		//Reset previous variables
		smellX = 0;
		smellY = 0;
	
		sightRotationNumber = 0;
		sightRotation = 0;
		sniffDelay = sniffDelayMax;
	}
}
#endregion

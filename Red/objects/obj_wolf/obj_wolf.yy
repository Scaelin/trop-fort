{
    "id": "4af11a7e-3b59-4d79-b812-abd54257a454",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wolf",
    "eventList": [
        {
            "id": "0023db30-020e-43ed-abe0-0117ea0856b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4af11a7e-3b59-4d79-b812-abd54257a454"
        },
        {
            "id": "6bb396d6-1ad4-4f39-9ee0-588387ebabd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4af11a7e-3b59-4d79-b812-abd54257a454"
        },
        {
            "id": "2c1182a9-0322-439c-bdca-3abf755f8a7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4af11a7e-3b59-4d79-b812-abd54257a454"
        },
        {
            "id": "95e60efe-892b-4239-8671-b79867045387",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "4af11a7e-3b59-4d79-b812-abd54257a454"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ca9b7fc-fd2d-467e-9a1a-7a3a0a4f1342",
    "visible": true
}
/// @description Define variables
//

/*
The wolf priorizes sight > noise > smell
When he arrives onto a position, he starts to look around for a few seconds, then 
sniffs globally for other smells to look for, then start again. 
This routine is interrupted if he gets new cues.
Tracking is interrupted if he receives cues with equal of higher priority
For example, if a new smell appears within the correct range while the Wolf is
investigating a noise, he won't pick it up. He will only react if a new noise is heard, 
of if he sees Red directly

Exception : Tracking smell is only overriden by higher priorities, not equal ones (other smells)
*/

//State
state = "looking";

/*
State can be :
- looking : the wolf is standing still seeking Red (sight)
- sniffing : the wolf is standing still seeking Red (smells the closest smell)
- trackingSmell : the wolf is running towards a smell
- trackingNoise : the wolf is running towards a noise
- trackingSight : the wolf is running towards a position where he has seen Red


*/

//Localisation of the smell
smellX = 0;
smellY = 0;

//Localisation of noise
noiseX = 0;
noiseY = 0;

//Localisation of where Red was last seen
sightX = 0;
sightY = 0;

//Movement

runSpeed = 2.5;

//Sight
sightDirection = direction;
sightRadius = 110;
sightRange = 500;

	//Looking variables
//The max angle he will rotate each time
sightRotationMax = 1.5;
sightRotation = 0.5

//Amount of times the Wolf will rotate
sightRotationNumberMax = 1;
sightRotationNumber = 0;

//It will accelerate afterwards
sightRotationSpeed = 0;

	//Sniffing variables
//Sniffing delay
sniffDelayMax = 1;
sniffDelay = sniffDelayMax;

//Pathfinding methods

cellWidth = 32;
cellHeight = 32;

hCells = room_width div cellWidth;
vCells = room_height div cellHeight;

global.grid = mp_grid_create(0, 0, hCells, vCells, cellWidth, cellHeight);

//Add the walls in the grid
mp_grid_add_instances(global.grid, obj_wall, false);


//Create the path
path = path_add();

